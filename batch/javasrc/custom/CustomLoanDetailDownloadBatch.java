package com.infosys.custom.ebanking.batch;

import java.util.List;

import com.infosys.ci.common.DateUtil;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailResultVO;
import com.infosys.ebanking.batch.util.FileProcessUtils;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractBusinessProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.types.valueobjects.ReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;

/**
 * Batch specific implementation for processing data related to LNDTLDWLD
 * 
 * 
 * 
 */
public class CustomLoanDetailDownloadBatch extends AbstractBusinessProcessBatch {

	/*
	 * Batch Program Identifier
	 */
	public static final String ID = "LNDTLDWLD";
	private static final FEBAUnboundString BATCH_SERVICE = new FEBAUnboundString("CustomLoanDetailsService");
	private static final String OUTPUT_FILE_NAME = "OUTPUT_FILE_NAME";
	private static final String LN_DTL_VO = "com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailEnquiryVO";
	private static final String LN_DTL_RESULT_VO = "com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailResultVO";
	private static final String OUTPUT_FOLDER_PATH = "OUTPUT_FOLDER_PATH";
	private static final String BATCH_METHOD = "fetch";

	/*
	 * Utility and validator for common tasks and validations
	 */

	private EBBatchUtility eBBatchUtil;

	/**
	 * 
	 * Instantiates and initializes the required parameters.
	 * 
	 */
	public CustomLoanDetailDownloadBatch() {

		super();
		this.setId(ID);
		this.setRecordLevelCommit(true);
		this.setSingleThread(false);
		eBBatchUtil = new EBBatchUtility();
	}

	protected IFEBAValueObject getRecord(FEBABatchContext context, String recordLine, ReadOnlyUserParams userInputs)
			throws BusinessException, CriticalException, FatalException {
		return (CustomLoanDetailEnquiryVO) FEBAAVOFactory.createInstance(LN_DTL_VO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFileProcessBatch#
	 * processBatch(com.infosys.feba.framework.batch.FEBABatchContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 * java.lang.Object,
	 * com.infosys.feba.framework.batch.impl.ReadOnlyUserParams)
	 */
	protected void processBatch(FEBABatchContext context, IFEBAValueObject uow, Object bjTxnWM,
			ReadOnlyUserParams userInputs)
					throws BusinessException, CriticalException, BusinessConfirmation, FatalException {
		CustomLoanDetailResultVO detailsVO = (CustomLoanDetailResultVO) uow;

		FEBADate currentDate =  new FEBADate(DateUtil.currentDate());
		currentDate.setDateFormat("ddMMYYYY");

		String fileName="LOAN_STAT_AGENTPINANG_"+ currentDate.toString() +".TXT";

		FileProcessUtils.writeToOutPutFileAppendMode(context, userInputs.getParam(OUTPUT_FOLDER_PATH),fileName, true,
				detailsVO.getLoanDetail().toString());
	}

	@Override
	protected IGenericInputData getListOfRecordsToProcess(FEBABatchContext pContext, ReadOnlyUserParams pUserInputs)
			throws BusinessConfirmation, CriticalException, FatalException, BusinessException {

		CustomLoanDetailEnquiryVO detailsVO = (CustomLoanDetailEnquiryVO) FEBAAVOFactory.createInstance(LN_DTL_VO);
		FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(pContext);
		objContext.setServiceName(BATCH_SERVICE);
		objContext.setMethodName(new FEBAUnboundString(BATCH_METHOD));
		try {
			LocalServiceUtil.invokeService(objContext, BATCH_SERVICE.getValue(), new FEBAUnboundString(BATCH_METHOD),
					detailsVO);
		} catch (CriticalException e) {
			throw new CriticalException(true, objContext,"BTCHLNDTL0001",
					"Error in processing iMobile upload batch",
					211301, e);

		} catch (FatalException fe) {
			throw fe;
		}

		GenericInputData batchInputData = new GenericInputData();
		FEBAArrayList results = detailsVO.getResults();

		CustomLoanDetailResultVO header = getDefaultVO();
		header.setLoanDetail("REFFERAL_CODE|CUST_ID|PAYROLL_ACCOUNT_NUM|CUSTOMER_NAME|LOAN_ACCOUNT_ID|APPLICATION_STATUS");
		batchInputData.addItem(header);



		batchInputData.addItems(results);


		return batchInputData;
	}

	private CustomLoanDetailResultVO getDefaultVO() {
		return (CustomLoanDetailResultVO) FEBAAVOFactory.createInstance(LN_DTL_RESULT_VO);
	}

	private CustomLoanDetailResultVO getDefaultVO(String text) {
		CustomLoanDetailResultVO resVO = (CustomLoanDetailResultVO) FEBAAVOFactory.createInstance(LN_DTL_RESULT_VO);
		resVO.setLoanDetail(text);
		return resVO;
	}

	/*
	 * Fetch the list of error messages for all the records for which the
	 * processing has failed.
	 * 
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * getRecordErrorMessagesList
	 * (com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	protected List getRecordErrorMessagesList(FEBATransactionContext context, IFEBAValueObject record)
			throws BusinessException, CriticalException, FatalException {
		return null;
	}

	/*
	 * For preparing the Error message with the list of users, whose creation
	 * has failed
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * getRecordErrorMessage
	 * (com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	protected IReportMessageVO getRecordErrorMessage(FEBATransactionContext context, IFEBAValueObject record)
			throws BusinessException, CriticalException, FatalException {
		ReportMessageVO reportMsgVO = (ReportMessageVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.ReportMessageVO);
		CustomLoanDetailResultVO detailsVO = (CustomLoanDetailResultVO) record; 
		String sRecordID = detailsVO.getUserId().toString();
		String sMsg = sRecordID + " Creation has failed";
		reportMsgVO.setRecordID(sRecordID);
		reportMsgVO.setMessage(sMsg);
		return reportMsgVO;
	}

	/*
	 * For preparing the Success message with the list of successfully created
	 * users
	 * 
	 * @see com.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * getRecordSuccessMessage
	 * (com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	protected IReportMessageVO getRecordSuccessMessage(FEBATransactionContext context, IFEBAValueObject record)
			throws BusinessException, CriticalException, FatalException {
		ReportMessageVO reportMsgVO = (ReportMessageVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.ReportMessageVO);
		CustomLoanDetailResultVO detailsVO = (CustomLoanDetailResultVO) record;
		String sRecordID = detailsVO.getUserId().toString();
		String sMsg = sRecordID + " Created succesfully";
		reportMsgVO.setRecordID(sRecordID);
		reportMsgVO.setMessage(sMsg);
		return reportMsgVO;
	}

	/*
	 * Fetch the list of successful messages for all the records for which
	 * processing has been successful.
	 * 
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * getRecordSuccessMessagesList
	 * (com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext context, IFEBAValueObject arg0)
			throws BusinessException, CriticalException, FatalException {
		return null;
	}

	/*
	 * Notifies that batch processing has been completed.
	 * 
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * getSuccessLogMessage(com.infosys.feba.framework.batch.FEBABatchContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 */
	@Override
	protected String getSuccessLogMessage(FEBABatchContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException, FatalException {
		return "Processing completed for the Batch Proragm";
	}

	/*
	 * Checks if the required parameters are present as required by the batch
	 * specific implementation.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see com.infosys.feba.framework.batch.pattern.IFEBABatch#checkUsage(com.
	 * infosys .feba.framework.batch.impl.ReadOnlyUserParams)
	 */
	public void checkUsage(ReadOnlyUserParams userInputs) throws FatalException, CriticalException {
		checkBatchRequiredParams(userInputs);

	}

	/*
	 * Identify the list of fields that needs to be validated. Validation for
	 * mandatory fields are automatically handled by the framework itself.
	 * 
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFEBABatch#
	 * prepareValidationsList(com.infosys.feba.framework.batch.FEBABatchContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 * java.lang.Object)
	 */
	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext objContext, IFEBAValueObject objInputOutput,
			Object objTxnWM) throws BusinessException, CriticalException, FatalException {
		return null;
	}

	/**
	 *
	 * Checks for the mandatory command line parameters that are required for
	 * the batches to run.
	 *
	 * @param userInputs
	 *            command line parameters batch configuration
	 * @throws FatalException
	 * @throws CriticalException
	 *
	 */
	private void checkBatchRequiredParams(ReadOnlyUserParams userInputs) throws FatalException, CriticalException {

		/*boolean validationPassed = (userInputs.getParam(OUTPUT_FILE_NAME) != null);
		if (!validationPassed) {
			throw new FatalException(null, EBIncidenceCodes.MANDATORY_FIELD_VALIDATION_FAIL,
					"Mandatory Command Line Parameters Missing");
		}*/
	}

	/**
	 * Gets the batch completion success message.
	 *
	 * @return the success message
	 */
	public String getSuccessMessage() {
		return "Processing completed for the Batch Proragm";
	}

	/**
	 * 
	 * Gets the database transaction expected based on the action type
	 * 
	 * @param action
	 *            the type of database transaction
	 * @return string representation of database transaction to happen
	 * @throws BusinessException
	 * 
	 */
	private String getMessage(char action) throws BusinessException {
		String strMessage = "Processing";
		if (action == 'I' || action == 'i') {
			strMessage = "Insert";
		} else if (action == 'U' || action == 'u') {
			strMessage = "Update";
		} else if (action == 'D' || action == 'd') {
			strMessage = "Delete";
		}
		return strMessage;
	}


}
