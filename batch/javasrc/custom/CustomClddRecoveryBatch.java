package com.infosys.custom.ebanking.batch;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.tao.CLDDTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCLDDRecAppDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomFDTTRecDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractBusinessProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.types.valueobjects.ReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

/**
 * Batch specific implementation for processing data related to CLDDRECOVERY
 * 
 * 
 * 
 */
public class CustomClddRecoveryBatch extends AbstractBusinessProcessBatch {

	/*
	 * Batch Program Identifier
	 */
	public static final String ID = "CUSTOMCLDDREC";

	/*
	 * Utility and validator for common tasks and validations
	 */

	private EBBatchUtility eBBatchUtil;

	/**
	 * 
	 * Instantiates and initializes the required parameters.
	 * 
	 */
	public CustomClddRecoveryBatch() {

		super();
		this.setId(ID);
		this.setRecordLevelCommit(true);
		this.setSingleThread(false);
		eBBatchUtil = new EBBatchUtility();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.infosys.feba.framework.batch.pattern.AbstractFileProcessBatch#
	 * processBatch(com.infosys.feba.framework.batch.FEBABatchContext,
	 * com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 * java.lang.Object,
	 * com.infosys.feba.framework.batch.impl.ReadOnlyUserParams)
	 */
	protected void processBatch(FEBABatchContext context, IFEBAValueObject recVO, Object bjTxnWM,
			ReadOnlyUserParams userInputs)
			throws BusinessException, CriticalException, BusinessConfirmation, FatalException {
		FEBAUnboundString batchService = new FEBAUnboundString("CustomCLDDRecoveryService");
		String batchMethod = "insert";
		try {
			FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(context);
			objContext.setServiceName(batchService);
			objContext.setMethodName(new FEBAUnboundString(batchMethod));
			LocalServiceUtil.invokeService(objContext, batchService.toString(), new FEBAUnboundString(batchMethod),
					recVO);
		} catch (Exception e) {
			LogManager.logError(context, e, "ERROR");
		}
	}

	@Override
	protected IGenericInputData getListOfRecordsToProcess(FEBABatchContext pContext, ReadOnlyUserParams pUserInputs)
			throws BusinessConfirmation, CriticalException, FatalException, BusinessException {

		FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(pContext);

		GenericInputData batchInputData = new GenericInputData();
		FEBAArrayList<CustomCLDDRecAppDetailsVO> results;
		 QueryOperator operator =
		 QueryOperator.openHandle(objContext,"CustomCLDDRecAppListInquiryDAL"
		 );
		 try {
		 operator.associate("bankId", objContext.getBankId());
		
		 results = operator.fetchList(objContext);
		 } catch (Exception e) {
		 e.printStackTrace();
		 return batchInputData;
		 }finally{
		 operator.closeHandle((EBTransactionContext) objContext);
		 }
		
		 batchInputData.addItems(results);
//		CustomCLDDRecAppDetailsVO temp = (CustomCLDDRecAppDetailsVO) FEBAAVOFactory
//				.createInstance(CustomCLDDRecAppDetailsVO.class.getName());
//		temp.setApplicationId(new ApplicationNumber("14930"));
//		temp.setApplicationStatus("LOAN_PAID");
//		batchInputData.addItem(temp);
		LogManager.logDebug(pContext, "No of eligible records:" + batchInputData.getSize());

		return batchInputData;
	}

	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext febabatchcontext, IFEBAValueObject ifebavalueobject,
			Object obj) throws BusinessException, CriticalException, FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected IReportMessageVO getRecordErrorMessage(FEBATransactionContext febatransactioncontext,
			IFEBAValueObject ifebavalueobject) throws BusinessException, CriticalException, FatalException {
		ReportMessageVO reportMsgVO = (ReportMessageVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.ReportMessageVO);
		CustomCLDDRecAppDetailsVO detailsVO = (CustomCLDDRecAppDetailsVO) ifebavalueobject;
		String sRecordID = detailsVO.getApplicationId().toString();
		String sMsg = sRecordID + " recovery has failed";
		reportMsgVO.setRecordID(sRecordID);
		reportMsgVO.setMessage(sMsg);
		return reportMsgVO;
	}

	@Override
	protected IReportMessageVO getRecordSuccessMessage(FEBATransactionContext febatransactioncontext,
			IFEBAValueObject ifebavalueobject) throws BusinessException, CriticalException, FatalException {
		ReportMessageVO reportMsgVO = (ReportMessageVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.ReportMessageVO);
		CustomCLDDRecAppDetailsVO detailsVO = (CustomCLDDRecAppDetailsVO) ifebavalueobject;
		String sRecordID = detailsVO.getApplicationId().toString();
		String sMsg = sRecordID + " recovery Success";
		reportMsgVO.setRecordID(sRecordID);
		reportMsgVO.setMessage(sMsg);
		return reportMsgVO;
	}

	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext febatransactioncontext,
			IFEBAValueObject ifebavalueobject) throws BusinessException, CriticalException, FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List getRecordErrorMessagesList(FEBATransactionContext febatransactioncontext,
			IFEBAValueObject ifebavalueobject) throws BusinessException, CriticalException, FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getSuccessLogMessage(FEBABatchContext febabatchcontext, IFEBAValueObject ifebavalueobject)
			throws BusinessException, CriticalException, FatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkUsage(ReadOnlyUserParams readonlyuserparams) throws FatalException, CriticalException {
		// TODO Auto-generated method stub

	}

}
