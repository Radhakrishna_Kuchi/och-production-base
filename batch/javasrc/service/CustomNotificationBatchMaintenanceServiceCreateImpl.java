package com.infosys.custom.ebanking.batch.service;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CNEDTAO;
import com.infosys.custom.ebanking.tao.CNETTAO;
import com.infosys.custom.ebanking.tao.DVDTTAO;
import com.infosys.custom.ebanking.tao.info.CNETInfo;
import com.infosys.custom.ebanking.tao.info.DVDTInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.batch.config.BatchConfig;
import com.infosys.feba.framework.batch.config.ReadOnlyBatchParams;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FreeText;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomNotificationBatchMaintenanceServiceCreateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	/*
	 * This service takes CustomNotificationInOutVO as input and generates a
	 * notification in the table CNED based on the template present in CNET from
	 * the CBET entries or the FEBABatch from the flat file provided by core.
	 * 
	 */



	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomNotificationInOutVO inOutVO = (CustomNotificationInOutVO) arg1;
		CommonCode notificationEvent = inOutVO.getNotificationEvent();
		BankId bankId = context.getBankId();
		CommonCode appId = new CommonCode(PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, context));
		CNETInfo cnetInfo = null;
		try {
			cnetInfo = CNETTAO.select(context, bankId, notificationEvent, appId);
			System.out.println("App Notifications Required - "+cnetInfo.getAppNotfReqd().getValue());
			System.out.println("Push Notifications Required - "+cnetInfo.getPushNotfReqd().getValue());
			if (cnetInfo.getAppNotfReqd().getValue() == 'Y') {
				System.out.println("App notifications");
				/*
				 * Creating App notification i.e. DB Entry in CNED
				 */

				EBTransactionContext ebContext = (EBTransactionContext) context;

				CNEDTAO cnedTAO = new CNEDTAO(ebContext);

				cnedTAO.associateNotificationId(new FEBAUnboundLong(SequenceGeneratorUtil
						.getNextSequenceNumber(CustomEBConstants.NEXT_NOTIFICATION_ID_SEQ, context)));
				cnedTAO.associateBankId(context.getBankId());
				if (!inOutVO.getAmount1().toString().equals(EBankingConstants.NULL_AMOUNT)
						&& !inOutVO.getAmount1().toString().equalsIgnoreCase("0.0")) {
					cnedTAO.associateAmount1(inOutVO.getAmount1());
				}
				if (!inOutVO.getAmount2().toString().equals(EBankingConstants.NULL_AMOUNT)
						&& !inOutVO.getAmount2().toString().equalsIgnoreCase("0.0")) {
					cnedTAO.associateAmount2(inOutVO.getAmount2());
				}
				if (!inOutVO.getAmount3().toString().equals(EBankingConstants.NULL_AMOUNT)
						&& !inOutVO.getAmount3().toString().equalsIgnoreCase("0.0")) {
					cnedTAO.associateAmount3(inOutVO.getAmount3());
				}
				cnedTAO.associateFreeDate1(inOutVO.getDate1());
				cnedTAO.associateFreeDate2(inOutVO.getDate2());
				cnedTAO.associateFreeDate3(inOutVO.getDate3());
				cnedTAO.associateOther1(inOutVO.getOther1());
				cnedTAO.associateOther2(inOutVO.getOther2());
				cnedTAO.associateOther3(inOutVO.getOther3());
				cnedTAO.associateNotificationType(notificationEvent);
				cnedTAO.associateIsNew(new YNFlag("Y"));
				cnedTAO.associateUserId(inOutVO.getUserId());
				cnedTAO.associateMessageHeader(cnetInfo.getMessageHeader());
				cnedTAO.associateDetailMessage(cnetInfo.getDetailMessage());
				cnedTAO.associateShortMessage(populateParams(cnetInfo.getShortMessage().getValue(), inOutVO));
				System.out.println("Inserting in CNED - ");

				cnedTAO.insert(ebContext);
				System.out.println("Inserted in CNED - ");
			}
			if (cnetInfo.getPushNotfReqd().getValue() == 'Y') {
				System.out.println("Firebase Push notifications");

				ReadOnlyBatchParams fireBaseParams = BatchConfig.getInstance().getBatchParams(
						"GoogleFirebaseParams");
				System.out.println("fireBaseParams - "+fireBaseParams.getParamList());
				if (fireBaseParams != null) {
					try{
						String messageScope = fireBaseParams.getParam("MESSSAGE_SCOPES");
						String googleServcieAccountFile = fireBaseParams.getParam("SERVICE_ACCOUNT_FILE");
						System.out.println("googleServcieAccountFile : "+googleServcieAccountFile);
						final String[] SCOPES = { messageScope };
						System.setProperty("https.proxyHost","172.18.104.20");
						System.setProperty("https.proxyPort","1707");


						GoogleCredential googleCredential = GoogleCredential
								.fromStream(
										new FileInputStream(googleServcieAccountFile))
								.createScoped(Arrays.asList(SCOPES));
						System.out.println("Is google access token generated - "+googleCredential.refreshToken());
						System.clearProperty("https.proxyHost");
						System.clearProperty("https.proxyPort");
						inOutVO.setApiToken(googleCredential.getAccessToken());

						System.out.println("Google Access Token - "+inOutVO.getApiToken());
						inOutVO.setDeviceToken(fetchDeviceToken(context, inOutVO.getUserId()));
						inOutVO.setShortMessage(populateParams(cnetInfo.getShortMessage().getValue(), inOutVO));
						inOutVO.setMessageHeader(new FreeText(cnetInfo.getMessageHeader().getValue()));
						System.out.println("Before Host call - "+inOutVO);

						EBHostInvoker.processRequest(context, CustomEBRequestConstants.FIREBASE_PUSH_NOTIFICATION_REQUEST,
								inOutVO);

						System.out.println("After host call Host call");
						if (FEBATypesUtility.isNotNullOrBlank(inOutVO.getCode())) {
							System.out.println("Notification Delivered - "+inOutVO);
						}else{
							System.out.println("Notification delivery Failed - "+inOutVO);

						}
					}
					catch(Exception e){
						e.printStackTrace();
						throw e;
					}

				}
			}
		} catch (FEBATableOperatorException ftoe) {
			throw new BusinessException(context, "Record does not exist.", ftoe.getMessage(), ftoe.getErrorCode());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private FreeText populateParams(String shortMessage, CustomNotificationInOutVO inOutVO) {

		if (null != inOutVO.getAmount1() && inOutVO.getAmount1().getAmountValue() > 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT1
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT1
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatAmount(String.valueOf(inOutVO.getAmount1().getAmount().getNumeralValue().longValue())));
		}
		if (null != inOutVO.getAmount2() && inOutVO.getAmount2().getAmountValue() > 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT2
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT2
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatAmount(String.valueOf(inOutVO.getAmount2().getAmount().getNumeralValue().longValue())));
		}
		if (null != inOutVO.getAmount3() && inOutVO.getAmount3().getAmountValue() > 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT3
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.AMOUNT3
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatAmount(String.valueOf(inOutVO.getAmount3().getAmount().getNumeralValue().longValue())));
		}
		if (null != inOutVO.getDate1() && shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER
				+ CustomEBConstants.DATE1 + CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.DATE1
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatDateBahasa(inOutVO.getDate1().toString().substring(0, 10)));
		}
		if (null != inOutVO.getDate2() && shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER
				+ CustomEBConstants.DATE2 + CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.DATE2
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatDateBahasa(inOutVO.getDate2().toString().substring(0, 10)));
		}
		if (null != inOutVO.getDate3() && shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER
				+ CustomEBConstants.DATE3 + CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.DATE3
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, formatDateBahasa(inOutVO.getDate3().toString().substring(0, 10)));
		}
		if (null != inOutVO.getOther1() && inOutVO.getOther1().getValue().length() != 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER1
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER1
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, inOutVO.getOther1().getValue());
		}
		if (null != inOutVO.getOther2() && inOutVO.getOther2().getValue().length() != 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER2
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER2
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, inOutVO.getOther2().getValue());
		}
		if (null != inOutVO.getOther3() && inOutVO.getOther3().getValue().length() != 0
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER3
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {
			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.OTHER3
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, inOutVO.getOther3().getValue());
		}

		return new FreeText(shortMessage);
	}


	public FEBAUnboundString fetchDeviceToken(FEBATransactionContext context, UserId userId) throws FEBATableOperatorException {

		System.out.println("In FetchDeviceToken ------");
		System.out.println("context.userId"+userId.getValue());
		DVDTInfo dvdtInfo = DVDTTAO.select(context, context.getBankId(), userId);

		return new FEBAUnboundString(dvdtInfo.getDeviceToken().toString());


	}
	public static String formatAmount(String amount){
		String formattedAmount = "";

		try{
//			amount = amount.substring(0, amount.indexOf("."));
			Long longVal = Long.parseLong(amount);
			Locale localIDR = new Locale("in","ID");//100.0
			DecimalFormat formatIDR = (DecimalFormat)NumberFormat.getInstance(localIDR);
			formattedAmount = formatIDR.format(longVal);

		}catch(Exception e){
			e.printStackTrace();
		}
		return formattedAmount;
	}
	public static String formatDateBahasa(String date){
		String formattedDate="";
		Date tempDate = new Date();
		Locale locale = new Locale("id", "ID");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy",locale);

		try{
			tempDate=df.parse(date);
		}catch(Exception e){
			return date;	
		}
		df.applyPattern("dd MMM yyyy");

		formattedDate=df.format(tempDate);

		return formattedDate;
	}

}
