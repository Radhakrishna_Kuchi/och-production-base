package com.infosys.custom.ebanking.batch.service;

import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationStatusInOutVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

import mx4j.log.Log;

public class CustomLoanApplicationMaintenanceServiceUpdateStatusImpl extends AbstractLocalUpdateTran{
	
	private final static String APPEXPIRED="APP_EXPIRED";

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomApplicationStatusInOutVO applnStatusVO = (CustomApplicationStatusInOutVO)arg1;

		CLATTAO clatTao = new CLATTAO(context);

		try {
			
			CLATInfo info = CLATTAO.select(context, context.getBankId(),
					applnStatusVO.getApplicationId());
			clatTao.associateApplicationStatus(new CommonCode(APPEXPIRED));
			clatTao.associateBankId(context.getBankId());
			clatTao.associateApplicationId(applnStatusVO.getApplicationId());
			clatTao.associateCookie(info.getCookie());
			clatTao.update(context);

			new CustomLoanHistoryUpdateUtil().updateHistory(applnStatusVO.getApplicationId(),
					APPEXPIRED, "BATCHSTATUSUPDATE", context);

		} catch (FEBATableOperatorException e) {
			LogManager.logError(context, e);
		}



	} 
}


