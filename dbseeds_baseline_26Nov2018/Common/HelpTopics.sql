delete from COCD where CODE_TYPE='MTP' and bank_id='01';
delete from COCD where CODE_TYPE in('UMUM','PEDL','PEGJ','PENK','PENU','PENA','PESA','PROF','LAIN') and bank_id='01';
delete from TSTM where text_type='MTP' and bank_id='01';
delete from TSTX where text_type='MTP' and bank_id='01';

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','UMUM','001','Umum','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PEDL','001','Pendaftaran dan Login','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PEGJ','001','Pengajuan','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PENK','001','Pencairan Kredit','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PENU','001','Pengambilan Uang','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PENA','001','Pembayaran Angsuran','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PESA','001','Pesan','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','PROF','001','Profil','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MTP','LAIN','001','Lain-lain','N','setup',sysdate,'setup',sysdate);


Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','UMUM','UMU1','001','Apa itu Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','UMUM','UMU2','001','Apa keuntungan menggunakan Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','UMUM','UMU3','001','Apakah saya harus menjadi nasabah BRI AGRO/BRI untuk menikmati layanan aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','UMUM','UMU4','001','Apakah Pinang dapat dipakai untuk berbelanja online?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEDL','PED1','001','Bagaimana cara mendaftar di aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEDL','PED2','001','Bagaimana cara melakukan login?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEDL','PED3','001','Bagaimana jika saya salah memasukkan PIN 3 kali secara berturut-turut saat hendak log in ke aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEDL','PED4','001','Bagaimana jika saya sudah memiliki akun sebelumnya?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG0','001','Apa saja syarat pengajuan pinjaman pada aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG1','001','Dokumen apa saja yang diperlukan untuk mengajukan pinjaman lewat aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG2','001','Bagaimana cara mengecek simulasi kredit pada Pinang? ','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG3','001','Bagaimana cara mengajukan pinjaman pada aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG4','001','Jika progress pengajuan yang saya lakukan sempat terhenti, apakah saya dapat melanjutkannya? ','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG5','001','Bagaimana jika perusahaan dimana saya bekerja belum terdaftar pada aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG6','001','Mengapa pinjaman saya tidak disetujui? Bagaimana caranya agar pinjaman saya dapat disetujui?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG7','001','Apa itu Credit Scoring? Apa pengaruhnya terhadap pinjaman saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG8','001','Berapa lama proses persetujuan pengajuan pinjaman Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEG9','001','Dapatkah saya membatalkan pinjaman yang sudah saya ajukan?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PEGJ','PEH1','001','Dapatkah saya mengajukan pinjaman lagi disaat  pinjaman                                                                                                                                                                     saya yang sebelumnya belum usai?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENK','PEK1','001','Jika pinjaman saya sudah disetujui, apa yang harus saya lakukan untuk mencairkan pinjaman saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENK','PEK2','001','Dapatkah saya membatalkan pinjaman yang sudah saya ajukan?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENK','PEK3','001','Bagaimana jika saya salah memasukkan Kode Pengganti Tanda Tangan saya? ','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENK','PEK4','001','Dapatkah uang pinjaman dicairkan ke rekening saya yang lain (diluar BRI/BRI AGRO)?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENK','PEK5','001','Apakah jumlah limit kredit yang sudah diputus dan diberikan melalui aplikasi Pinang dapat ditambah?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENU','PEN1','001','Dimana saya dapat mengambil uang pinjaman saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENU','PEN2','001','Apakah saya dapat melakukan transaksi langsung melalui aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA0','001','Berapa lamanya periode cicilan angsuran Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA1','001','Bagaimana cara membayar angsuran pinjaman Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA2','001','Apakah saya dapat melunasi pinjaman saya lebih cepat?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA3','001','Dimana saya dapat melihat informasi mengenai pembayaran cicilan dan pinjaman-pinjaman yang sudah pernah saya lakukan?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA4','001','Berapa besar bunga yang harus dibayarkan dalam penggunaan aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA5','001','Apakah meminjam di Pinang ada biaya-biaya lain yang harus saya bayar?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA6','001','Apakah pembayaran cicilan saya akan mempengaruhi limit kredit beserta pengajuan-pengajuan pinjaman saya kedepannya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA7','001','Dimana saya dapat mengecek jadwal pembayaran cicilan saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA8','001','Kapan pembayaran saya jatuh tempo?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PENA','PEA9','001','Bagaimana jika pembayaran cicilan saya melewati tanggal jatuh tempo?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PESA','AKDM','001','Apa kegunaan dari menu Pesan pada aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PROF','PRO1','001','Dimana saya dapat mengakses informasi mengenai akun saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PROF','PRO2','001','Bagaimana jika saya ingin mengakses dokumen-dokumen pinjaman saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PROF','PRO3','001','Bagaimana jika saya ingin mengubah PIN aplikasi saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PROF','PRO4','001','Apa yang dimaksud dengan Digital Signature? Mengapa saya memerlukannya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','PROF','PRO5','001','Dimana saya dapat membaca Syarat dan Ketentuan Penggunaan  Aplikasi Pinang?','N','setup',sysdate,'setup',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI1','001','Bagaimana jika saya melakukan tarik tunai di ATM namun gagal sementara saldo saya pada aplikasi Pinang sudah terpotong?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI2','001','Mengapa aplikasi Pinang saya tidak dapat digunakan?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI3','001','Mengapa aplikasi Pinang meminta untuk mengakses data pada handphone saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI4','001','Apakah informasi yang saya berikan kepada aplikasi Pinang aman?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI5','001','Apa bedanya alamat domisili dengan alamat tempat tinggal?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI6','001','Mengapa sms/angka keamanan tidak masuk ke nomor handphone saya? ','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI7','001','Bagaimana jika saya ingin mengganti nomor handphone saya?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI8','001','Saya mempunyai lebih dari satu handphone/smartphone , dapatkah saya memakai satu akun Pinang pada lebih dari satu perangkat?','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LAIN','LAI9','001','Bagaimana jika saya ingin menghapus/menutup  akun Pinang saya?','N','setup',sysdate,'setup',sysdate);

 
--PEDL
Insert into TSTM  values (1,'01','01','4','MTP','PED1','Untuk mendaftar menjadi pengguna dan nasabah Pinang, anda harus mengunduh aplikasi Pinang melalui google playstore, kemudian menginstall aplikasi Pinang di smartphone anda. Untuk melakukan pendaftaran di aplikasi Pinang dapat mengikutilangkah-langkah sebagai berikut: 
1) Masukkan nomor handphone anda yang aktif. Pastikan bahwa data yang dimasukkan sudah sesuai.
2) Angka keamanan akan dikirimkan ke nomor handphone anda sebagai bentuk verifikasi.
3) Masukkan angka keamanan yang anda terima ke field yang tersedia. Pastikan angka keamanan yang anda masukkan sudah benar. Jika anda belum menerima angka keamanan setelah beberapa menit, pilih opsi “Kirim Ulang Angka Keamanan” untuk mengirimkan angka keamanan kembali ke handphone anda.
4) Buat PIN Aplikasi yang terdiri dari 6 angka untuk mengamankan aplikasi Anda. Hindari menggunakan angka yang berulang. PIN Aplikasi ini akan diperlukan setiap saat anda masuk ke aplikasi (login).
5) Akun anda berhasil dibuat.
6) Setelah PIN dibuat, anda akan diarahkan untuk melakukan login.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PED2','Anda akan diminta untuk melakukan login setiap kali anda masuk ke dalam aplikasi Pinang. Cara untuk login sangat mudah. Anda hanya perlu memasukkan PIN yang sudah anda buat saat registrasi pada field yang tersedia.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PED3','Apabila anda salah memasukkan pin sebanyak 3 kali berturut-turut maka akun anda akan diblokir sementara, dan harus menghubungi Call Center BRI AGRO di 1500494 untuk melakukan verifikasi untuk mengaktifkan akun anda kembali.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PED4','Jika anda sudah memiliki akun sebelumnya, maka langkah-langkah yang harus dilakukan adalah sebagai berikut: 
1.	Masukkan nomor handphone yang anda gunakan untuk mendaftar. Pastikan nomor yang anda masukkan sesuai dan merupakan nomor yang anda gunakan sebelumnya saat melakukan pendaftaran pada aplikasi Pinang.
2.	Masukkan angka keamanan yang baru saja dikirimkan ke nomor handphone anda.
3.	Masukkan PIN aplikasi yang sudah pernah anda buat.
4.	Anda sukses masuk kedalam aplikasi Pinang (logged in).
','N','01.SU',sysdate,'01.SU',sysdate);

--UMUM
Insert into TSTM  values (1,'01','01','4','MTP','UMU1','Pinang adalah produk pinjaman BRI Agro berbasis aplikasi digital yang berjalan dalam sistem operasi android, dimana dengan aplikasi tersebut nasabah dapat mengajukan permohonan pinjaman tanpa agunan secara digital, dimana saja dan kapan saja.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','UMU2','Pinang sebagai solusi pinjaman secara cepat dan fleksibel, dengan keuntungan-keuntungan: 
1.	Pengajuan pinjaman diajukan secara digital tanpa harus ke bank:
2.	Tidak memerlukan agunan.
3.	Memiliki limit pinjaman mulai dari 500 ribu hingga 20 juta.
4.	Tenor mulai dari 1 hingga 12 bulan dengan metode pembayaran yang fleksibel. Pembayaran cicilan Pinang akan dilakukan melalui autodebet.
5.	Waktu pemrosesan pengajuan kredit hanya akan memakan waktu 15-30 menit, sehingga anda dapat mendapatkan pinjaman dengan cepat.
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','UMU3','Untuk sementara ini, yang dapat menikmati layanan aplikasi PINANG hanya terbatas pada nasabah BRI AGRO atau BRI yang memiliki fasilitas payroll di BRI AGRO/BRI. ','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','UMU4','Aplikasi Pinang dapat digunakan untuk berbagai kebutuhan, tidak hanya berbelanja online.','N','01.SU',sysdate,'01.SU',sysdate);

--PEGJ
Insert into TSTM  values (1,'01','01','4','MTP','PEG0','Syarat pengajuan menjadi nasabah dan menggunakan aplikasi Pinang adalah sebagai berikut:
1.	Warga Negara Indonesia:
2.	Memiliki e-KTP, beserta e-KTP pasangan jika anda sudah menikah.
3.	Berusia minimal 21 tahun/sudah menikah, maksimal 54 tahun.
4.	Memiliki penghasilan tetap atau memiliki usaha yang menghasilkan dengan pemasukan di atas 1 juta rupiah per bulannya.
5.	Memiliki rekening gaji di Bank BRI/BRI Agro.
6.	Memiliki kartu ATM rekening gaji.
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG1','Dokumen yang diperlukan untuk membuat akun Pinang adalah.
1.	KTP (kartu tanda penduduk).
2.	Data pemasukan dan pengeluaran perbulan.
3.	KTP Pasangan anda (jika sudah berkeluarga).
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG2','Sebelum mengajukan pinjaman secara sungguhan, anda dapat mencoba layanan simulasi pinjaman yang kami sediakan. Terdapat dua tipe simulasi yang kami sediakan untuk nasabah, yaitu:
a) Simulasi Pinjaman
	Masukkan Jumlah Pinjaman yang diinginkan oleh anda (minimal Rp. 500.000, maksimal Rp. 20.000.000).
	Pilih Lama Pinjaman yang diinginkan oleh anda (minimal 1 bulan, maksimal 12 bulan).
	Pilih Tujuan Pinjaman.
	Hasil Perkiraan Cicilan yang harus dibayarkan oleh anda akan muncul.

b) Dari Cicilan
	Masukkan jumlah cicilan per bulan yang sekiranya sanggup Nasabah bayar.
	Pilih lama jangka waktu pinjaman untuk membantu kami menghitung perkiraan jumlah ajuan pinjaman anda.
	Pilih Tujuan Pinjaman anda.
	Hasil perkiraan pinjaman yang bisa anda dapatkan akan muncul.

Namun perlu diperhatikan bahwa jumlah perkiraan pinjaman/cicilan yang ditampilkan dapat berbeda sesuai dengan hasil penilaian data yang dimasukkan oleh anda.
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG3','Cara mengajukan pinjaman melalui aplikasi Pinang sangatlah mudah, yaitu sebagai berikut: 
1.	Melakukan verifikasi rekening gaji dengan memasukkan nomor E-KTP, nomor kartu ATM rek. gaji, serta tanggal kadaluarsa kartu ATM.
2.	Ambil Foto E-KTP asli milik anda dan pilih “Gunakan Foto”.
3.	Mengisi informasi pribadi nasabah. Pastikan semua informasi telah diisi dan data yang dimasukkan sudah sesuai dan benar adanya sebelum memilih opsi “Simpan”.
4.	Mengisi informasi keluarga nasabah. Pastikan semua informasi telah diisi dan data yang dimasukkan sudah sesuai dan benar adanya sebelum memilih opsi “Simpan”.
5.	Setelah semua data telah diisi, maka anda harus menyatakan bahwa data yang nasabah isi dalam formulir ini adalah benar adanya kemudian silahkan anda klik tanda centang (tick-box) sebelum memilih opsi “Kirim”
6.	Data pengajuan anda akan terkirim, dan anda hanya tinggal menunggu hasil putusan dari pengajuan anda. 
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG4','Anda masih dapat melanjutkan progress pendaftaran anda yang sempat terhenti sebelumnya. Pilih opsi “Lanjutkan Nanti” apabila anda ingin melanjutkan pengisian data dilain waktu, dan aplikasi akan menyimpan progress anda yang dapat anda lanjutkan sewaktu-waktu.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG5','Anda belum bisa mengajukan pinjaman jika perusahaan anda belum memiliki kerjasama dengan aplikasi Pinang. Anda dapat membantu kami untuk hadir di perusahaan anda dengan memasukkan nama perusahaan tempat anda bekerja dan lokasi perusahaan anda, dan pilih opsi “Saya ingin Pinang!”.  Kami akan berusaha agar Pinang dapat segera digunakan di tempat kerja anda.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG6','Adapun kemungkinan pinjaman tidak disetujui karena credit scoring anda tidak mencukupi. Agar pinjaman anda disetujui, anda dapat melakukan hal-hal berikut: 
1.	Pastikan anda memiliki pekerjaan dan penghasilan yang tetap.
2.	Pastikan usia anda diatas 21 tahun atau sudah menikah dan maksimum 54 tahun pada saat melakukan registrasi.
3.	Pastikan anda memiliki rekening gaji di BRI/BRI AGRO.
4.	Mengisi setiap data-data yang diperlukan dengan lengkap dan benar.
5.	Pastikan foto e-KTP tidak blur/jelas. 
6.	Pastikan bahwa anda tidak di-blacklist oleh BI atau sedang memilki kredit macet.
7.	dsb.
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG7','Credit scoring adalah sistem yang digunakan untuk menentukan apakah anda layak untuk mendapatkan pinjaman atau tidak, atau apakah anda layak untuk mendapatkan pinjaman sesuai dengan plafond yang anda inginkan. Credit scoring menggunakan berbagai parameter data untuk menilai diri anda, diantaranya nomor Handphone/Smartphone, e-KTP, analisa media sosial, dll.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG8','Jika anda telah memenuhi semua data yang diperlukan, dan tidak terdapat kendala teknis, maka proses awal sampai dengan disetujui atau tidaknya pengajuan pinjaman anda adalah sekitar 15- 30 menit.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEG9','Pinjaman yang diajukan masih dapat dibatalkan sepanjang anda belum menyetujui dan memilih opsi “Tolak & batalkan pengajuan” pada halaman penawaran kredit. Jika anda telah memilih opsi “Terima penawaran & cairkan” dan menandatangani/menyetujui dokumen-dokumen yang berkaitan serta sudah menerima pencairan, maka pinjaman anda tidak bisa di batalkan.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEH1','Anda tidak dapat mengajukan pinjaman ketika Anda memiliki pinjanam PINANG yang masih aktif. Anda harus menyelesaikan pinjaman anda yang sebelumnya dan melalui proses credit scoring kembali untuk dapat mengajukan pinjaman baru.','N','01.SU',sysdate,'01.SU',sysdate);

--PENK
Insert into TSTM  values (1,'01','01','4','MTP','PEK1','Saat pinjaman anda telah disetujui, maka tahapan berikutnya adalah untuk anda memilih opsi “Terima Penawaran dan Cairkan”, dan anda akan diarahkan untuk melakukan tahapan-tahapan sebagai berikut:

a) Rekening Pencairan Uang Pinjaman
1.	Konfirmasi rekening gaji yang akan anda gunakan dengan memilih opsi “Saya Mengerti”. Cicilan pinjaman akan otomatis ditarik dari rekening ini di tiap tanggal pembayaran payroll melalui mekanisme autodebet..

b) Pembuatan Digital Signature
1.	Ambil foto selfie diri anda untuk diunggah.
2.	Baca sampai akhir Syarat dan Ketentuan Pembuatan Digital Signature, kemudian pilih opsi “Saya Setuju”. Dengan anda menyetujui syarat dan ketentuan tersebut maka anda dengan sadar telah menyetujui dan patuh terhadap syarat dan ketentuan pembuatan Digital Signature yang telah ditentukan oleh bank.

d) Persetujuan Dokumen Pinjaman
1.	Pilih opsi “baca dokumen” pada Dokumen Syarat dan Ketentuan Fasilitas Pinjaman Tunai Pinang.
2.	Baca dokumen hingga akhir kemudian pilih opsi “Saya Telah Membaca Dokumen”.
3.	Pilih opsi “baca dokumen” pada Dokumen Permohonan Fasilitas Pinjaman Tunai Pinang.
4.	Baca dokumen hingga akhir kemudian pilih opsi “Saya Telah Membaca Dokumen”.
5.	Pilih opsi “baca dokumen” pada Perjanjian Elektronik Pemberian Fasilitas Pinjaman Tunai Pinang.
6.	Baca dokumen hingga akhir kemudian pilih opsi “Saya Telah Membaca Dokumen”.
7.	Pilih “Setujui Dokumen”. Pastikan anda sudah membaca dan menyetujui semua dokumen.
8.	Masukkan kode
9.	Masukkan kode pengganti tanda tangan yang telah anda buat, lalu pilih opsi “Cairkan Uang Pinjaman”.
10.	Selamat, uang berhasil dicairkan di rekening anda! Anda dapat memberikan rating atas pengalaman anda menggunakan aplikasi Pinang.
11.	Anda akan langsung diarahkan untuk melihat rekening pencairan anda pada menu “Rekening Pencairan”.
','N','01.SU',sysdate,'01.SU',sysdate);
 Insert into TSTM  values (1,'01','01','4','MTP','PEK2','Pinjaman yang diajukan masih dapat dibatalkan sepanjang anda belum menyetujui dan memilih opsi “Tolak & batalkan pengajuan” pada halaman penawaran kredit. Jika anda telah memilih opsi “Terima penawaran & cairkan” dan menandatangani atau menyetujui dokumen-dokumen yang berkaitan serta sudah menerima pencairan, maka pinjaman anda tidak bisa di batalkan.','N','01.SU',sysdate,'01.SU',sysdate);
 Insert into TSTM  values (1,'01','01','4','MTP','PEK3','Jika anda salah memasukkan Kode Pengganti Tanda Tangan anda selama 3 kali berturut-turut maka anda harus menunggu selama kurang lebih 5 menit sebelum anda dapat mencoba memasukkan kembali kode anda. Jika anda lupa akan Kode Pengganti Tanda Tangan anda, maka anda dapat memilih opsi “Lupa Kode Pengganti Tanda Tangan” dan anda akan diarahkan untuk mengubah Kode Pengganti Tanda Tangan anda.','N','01.SU',sysdate,'01.SU',sysdate);
 Insert into TSTM  values (1,'01','01','4','MTP','PEK4','Untuk saat ini, semua pencairan akan dilakukan pada rekening BRI maupun BRI AGRO (rekening payroll) yang sudah anda daftarkan untuk menerima pencairan, dan tidak bisa dicairkan ke rekening lain.','N','01.SU',sysdate,'01.SU',sysdate);
 Insert into TSTM  values (1,'01','01','4','MTP','PEK5','Anda tidak dapat melakukan penambahan jumlah limit kredit (top up/suplesi) sebelum anda melunasi jumlah pinjaman anda.','N','01.SU',sysdate,'01.SU',sysdate);

 --PENU
 Insert into TSTM  values (1,'01','01','4','MTP','PEN1','Anda dapat melakukan pengambilan uang di ATM BRI, jaringan ATM Bersama, jaringan ATM Link, atau anda dapat melakukan pengambilan uang di teller yang berada pada bank BRI maupun BRI AGRO terdekat dari lokasi anda.

a) Pengambilan Uang di ATM
1.	Datangi lokasi ATM BRI terdekat dengan membawa kartu ATM anda.
2.	Masukkan kartu ATM anda ke dalam mesin dan ketikkan nomor PIN ATM anda. 
3.	Pilih menu tarik tunai, lalu pilih atau masukkan nominal yang anda inginkan, tekan OK.
4.	Anda akan menerima uang sesuai dengan nominal tarik tunai.
5.	Tekan selesai untuk menyudahi transaksi.
6.	Tunggu dan ambil kembali kartu ATM anda dari mesin dan keluar dari bilik ATM.

b) Pengambilan Uang di Teller
1.	Datangi lokasi unit kerja BRI/BRI Agro terdekat dengan membawa kartu ATM dan buku tabungan anda.
2.	Isi Formulir Penarikan uang tunai dengan data yang sebenar-benarnya, dan masukkan nominal yang anda inginkan.
3.	Bawa formulir ke teller dan berikan formulir tersebut kepada teller, beserta dengan kartu ATM dan buku tabungan anda.
4.	Anda akan menerima uang sesuai dengan nominal tarik tunai.
5.	Tunggu dan ambil kembali kartu ATM serta buku tabungan anda dari teller untuk menyudahi transaksi anda.
','N','01.SU',sysdate,'01.SU',sysdate);
 Insert into TSTM  values (1,'01','01','4','MTP','PEN2','Anda tidak dapat melakukan transaksi secara langsung melalui aplikasi Pinang. Pada menu Rekening Pencairan, anda akan diarahkan untuk membuka aplikasi BRI Mobile ataupun BRI AGRO Mobile untuk melakukan pengecekan saldo, transfer, dan transaksi lainnya sesuai kebutuhan anda.','N','01.SU',sysdate,'01.SU',sysdate);

 --PENA
 Insert into TSTM  values (1,'01','01','4','MTP','PEA0','Periode cicilan Pinang dimulai dari 1 (satu) hingga 12 (dua belas) bulan.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA1','Untuk sementara, pembayaran angsuran pinjaman hanya dapat dilakukan melalui auto debit melalui rekening payroll BRI/BRI AGRO. Namun begitu anda juga dimungkinkan melakukan pelunasan maju dan anda akan mendapatkan cashback saat anda melakukan pelunasan maju sebagai hadiahnya. ','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA2','Anda dapat melakukan pelunasan maju dan anda akan mendapatkan cashback. Namun perlu diingat bahwa saat anda melakukan pelunasan maju, maka selain membayarkan pokok pinjaman, maka anda juga harus turut membayar sisa bunga yang belum dibayarkan secara penuh.
Anda dapat melakukan pelunasan langsung dengan cara-cara sebagai berikut:

1.	Masuk ke dalam menu “Pinjaman” dan pilih opsi “Lihat Detail Pinjaman”, kemudian pilih opsi “Bayar Lunas Sekaliguas” (bayar lunas tidak dapat dilakukan kurang dari 48 jam batas akhir pembayaran cicilan, atau jika ada cicilan yang terlambat).
2.	Konfirmasi bayar lunas. Pilih opsi “Saya Ingin Bayar Lunas Pinjaman.” Pastikan saldo di rekening anda mencukupi, karena saldo akan langsung terpotong pada saat anda menyetujui pembayaran lunas ini. Bayar lunas akan otomatis batal apabila saldo anda tidak mencukupi. 
3.	Pilih “Ya” jika anda yakin untuk melakukan bayar lunas sekaligus.
4.	Masukkan PIN untuk mengkonfirmasi pengajuan bayar lunas.
5.	Selamat, pembayar lunas sekaligus anda berhasil!
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA3','Anda dapat melihat riwayat pembayaran cicilan anda dan juga riwayat pinjaman anda pada menu Pinjaman: 
1.	Pada menu Pinjaman, pilih opsi “Riwayat”. 
2.	Pilih opsi “Pembayaran” jika anda ingin melihat detail pembayaran cicilan yang sudah anda lakukan pada pinjaman anda yang masih berlangsung.
3.	Pilih opsi “Pinjaman” jika anda ingin melihat detail pembayaran cicilan pada pinjaman anda yang sudah selesai maupun yang masih berlangsung. 
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA4','Suku bunga Pinang adalah sebesar 1.24% flat setiap bulannya','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA5','Anda tidak akan dikenakan biaya apapun pada penggunaan aplikasi PINANG','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA6','Ya, pembayaran cicilan anda akan berpengaruh kepada pengajuan-pengajuan pinjaman anda kedepannya. Apabila anda membayar selalu tepat waktu, credit scoring anda kedepannya dapat menjadi lebih baik, yang dapat memberikan limit kredit yang lebih besar untuk pengajuan pinjaman yang berikutnya.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA7','Anda dapat mengecek jadwal pembayaran cicilan melalui aplikasi Pinang pada menu “Pinjaman” (Loan Info).','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA8','Setiap pembayaran akan jatuh tempo pada tanggal gajian nasabah payroll. Apabila anda telat membayar, maka biaya keterlambatan dan bunga akan diberlakukan H+1 hari jatuh tempo.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PEA9','Kami akan mengirimkan pemberitahuan kepada anda baik dalam bentuk sms, e-mail, maupun push notification dan kami juga akan menelpon untuk mengingatkan anda sebelum cicilan anda jatuh tempo. Apabila anda tetap membayar setelah tanggal jatuh tempo maka pembayaran cicilan anda akan ditambah dengan bunga per hari keterlambatan, dan hal ini juga dapat memengaruhi credit scoring anda dalam pinjaman berikutnya.','N','01.SU',sysdate,'01.SU',sysdate);

--PESA
Insert into TSTM  values (1,'01','01','4','MTP','AKDM','Pada menu “Pesan” di aplikasi Pinang, anda dapat melihat berbagai pesan yang dikirimkan oleh aplikasi Pinang, di antaranya pesan berisi informasi mengenai: 
1.	Pengingat Batas Pembayaran.
2.	Pemberitahuan mengenai keterlambatan pembayaran.
3.	Pemberitahuan telah melakukan pembayaran cicilan.
4.	Pemberitahuan telah melakukan pembayaran pelunasan maju.
5.	Pemberitahuan telah melakukan pencairan.
6.	Pemberitahuan telah melakukan pembayaran secara lunas.
','N','01.SU',sysdate,'01.SU',sysdate);

 --PROF
 Insert into TSTM  values (1,'01','01','4','MTP','PRO1','Anda dapat mengakses informasi mengenai akun anda pada menu “Profil”, dimana anda akan mendapatkan informasi mengenai nama lengkap, nomor handphone anda yang terdaftar dan fitur-fitur yang dapat membantu anda dalam mengelola akun anda, seperti: 
1.	Dokumen Pinjaman.
2.	Ubah PIN Aplikasi. 
3.	Syarat dan Ketentuan.
4.	Keluar Akun (log out). 
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PRO2','Anda dapat mengakses dokumen-dokumen pinjaman anda dengan langkah-langkah sebagai berikut: 
1.	Pada menu Profil pilih opsi “Dokumen Pinjaman”.
2.	Data-data dokumen pinjaman akan ditampilkan, baik yang sedang berlangsung maupun yang sudah selesai.
3.	Pilih dokumen yang anda inginkan. Dokumen yang dimaksud akan secara otomatis terunduh ke handphone anda dalam format PDF.
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PRO3','Anda dapat mengubah PIN aplikasi anda melalui langkah-langkah berikut:
1.	Masukkan PIN lama anda.
2.	Masukkan PIN baru anda pada field yang tersedia dan ulangi lagi. Pastikan PIN yang dimasukkan terdiri dari 6 angka dan hindari menggunakan angka berulang.. 
3.	 PIN anda berhasil diubah. 
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PRO4','Digital Signature digunakan untuk menggantikan tanda tangan (basah) nasabah pada dokumen-dokumen perjanjian yang harus disetujui nasabah saat melakukan pengajuan pinjaman. Untuk menggunakan Digital Signature, anda akan dimintakan untuk melakukan pengambilan foto (selfie) beserta OTP yang akan dikirimkan ke nomor handphone anda.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','PRO5','Anda dapat membaca syarat dan ketentuan penggunaan aplikasi Pinang dalam menu “Profil” dan pilih opsi “Syarat dan Ketentuan Penggunaan Aplikasi Pinjaman Pinang”.','N','01.SU',sysdate,'01.SU',sysdate);

 --LAIN
Insert into TSTM  values (1,'01','01','4','MTP','LAI1','Jika saldo di rekening anda sudah terpotong namun transaksi anda pada ATM gagal, maka anda dapat menghubungi Call Center atau KC Bank BRI Agro terdekat kemudian membuat surat pengaduan nasabah. Penanganan pengaduan nasabah tersebut akan membutuhkan waktu maksimal selama 14 (empat belas) hari kerja.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI2','Kemungkinan penyebab aplikasi anda tidak dapat digunakan adalah karena aplikasi anda belum ter-update/masih merupakan versi lama. Anda dapat memperbaharui aplikasi Pinang anda melalui google play store dengan cara-cara sebagai berikut: 
1.	Pastikan internet anda aktif atau pastikan handphone/smartphone anda terhubung kepada wifi.
2.	Masuk ke dalam google play store.
3.	Pilih menu “aplikasi saya” dan cari aplikasi Pinang.
4.	Klik tombol ‘update’.
5.	Tunggu sampai aplikasi anda ter-update seutuhnya.
6.	Lakukan login untuk kembali mengakses aplikasi Pinang anda.

Jika anda sudah melakukan update namun aplikasi Pinang tetap tidak dapat digunakan, maka anda dapat menghubungi Call Center BRI AGRO 1500494
','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI3','Dikarenakan handphone/smartphone sudah menjadi suatu kepentingan bagi seseorang dalam kehidupan sehari-hari, dan seringkali seseorang melakukan transaksi dengan melalui handphone/smartphone mereka, kami membutuhkan izin untuk mengakses data pada handphone/smartphone anda untuk membantu kami dalam melakukan credit scoring anda dengan cepat. Adapun data yang kami perlukan adalah data perangkat serta akun online yang terdapat pada handphone/smartphone anda. Kami akan selalu memberitahukan anda apabila ada data/fitur yang akan kami akses, yang akan sangat membantu kami dalam melakukan credit scoring terhadap pengajuan pinjaman anda.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI4','Ya, melindungi Informasi setiap nasabah Pinang adalah prioritas kami. Informasi milik nasabah tersimpan dengan aman sesuai ketentuan yang berlaku di server Bank BRI Agro.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI5','Alamat tempat tinggal adalah alamat anda yang tertera di KTP dan Kartu Keluarga anda, sedangkan alamat domisili adalah alamat dimana anda tinggal saat ini, dan alamat domisili dapat berbeda dengan alamat tempat tinggal anda yang terdapat pada KTP.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI6','Pastikan bahwa anda memasukkan nomor handphone anda dengan benar dan lengkap, serta pastikan nomor handphone anda telah ter-register dan aktif. Anda dapat mencoba untuk mengirimkan kembali SMS/angka keamanan  tersebut melalui pilihan pada aplikasi. Jika anda masih tidak menerima SMS/angka keamanan, anda dapat menghubungi Call Center pada 1500494 untuk membantu anda.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI7','Anda tidak dapat mengubah nomor handphone anda yang sudah didaftarkan pada aplikasi Pinang. Anda hanya dapat mendaftarkan satu nomor pada akun Pinang.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI8','Anda dapat memindahkan akun Pinang anda ke perangkat lain dari perangkat anda yang sebelumnya, namun anda tidak dapat login ke akun Pinang pada dua perangkat secara bersamaan. Satu akun Pinang hanya dapat digunakan pada satu perangkat saja dengan nomor handphone yang sama.','N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTM  values (1,'01','01','4','MTP','LAI9','Jika anda ingin menutup/menghapus akun Pinang anda, silahkan hubungi Call Center kami pada 1500494.  Namun perlu diketahui bahwa akun anda tidak dapat ditutup sampai anda sudah melunasi seluruh pinjaman anda secara penuh.','N','01.SU',sysdate,'01.SU',sysdate);

--TSTX entries for display order
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','UMUM',1,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PEDL',2,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PEGJ',3,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PENK',4,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PENU',5,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PENA',6,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PESA',7,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','PROF',8,'N','01.SU',sysdate,'01.SU',sysdate);
Insert into TSTX ("db_ts","bank_id","corp_id","user_type","text_type","text_name","display_order","del_flg","r_cre_id","r_cre_time","r_mod_id","r_mod_time") values (1,'01','01','4','MTP','LAIN',9,'N','01.SU',sysdate,'01.SU',sysdate);
