--Home Ownership

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES1','001','MilikSendiri','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES2','001','RumahDinas ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES3','001','RumahDinas ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES4','001','SewaKontrak  ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES5','001','SewaKontrak  ','N','SETUP',sysdate,'SETUP',sysdate);

--Marital Status

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','MENIKAH','001','Menikah','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','BELUM KAWIN','001','BelumMenikah','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','NS','001','BelumMenikah ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','JANDA/DUDA','001','JandaDuda ','N','SETUP',sysdate,'SETUP',sysdate);


Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DEPN','0','001','0','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LTP','P','001','Fully Paid','N','SETUP',sysdate,'SETUP',sysdate);


Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SD','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SMP','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SMA','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D1','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D2','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D3','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S1','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S2','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S3','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','NONE','001','TidakSekolah','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));

