--OCR verification failed case handling
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','ASVS','OCR_VER_FAIL','001','1','N','setup',sysdate,'setup',sysdate);
COMMIT;

update COCD set cd_desc='19|TABLE|NULL|DHD4|LOAN_SANCTION_AMOUNT;LOAN_SCHEDULE_START_DATE;LOAN_SCHEDULE_END_DATE;MONTHLY_DUE_DATE;LOAN_INTEREST_RATE;LOAN_EMI_AMOUNT;LOAN_EMI_PENALTY;AUTO_INSTALLMENT_PAYMENT;LOAN_TENURE' where code_type='DBTL' and cm_code='DTB3';
COMMIT;