

--custom code type for access scheme on bank user creation screen
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRC','001','BRI Bank Admin for Divisi Kepatuhan','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRM','001','BRI Bank Admin','N','setup',sysdate,'setup',sysdate);

--custom code type for authentication scheme on bank user creation screen
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAUS','BUR','001','Bank User','N','setup',sysdate,'setup',sysdate);

--Removing all other authentication modes except password
delete from cocd where cm_code!='SPWD' and code_type='BATN' and bank_id='01';

update COCD set CD_DESC='Bank ANZ Indonesia' where CODE_TYPE='CCBK' and CM_CODE='ANZ';
update COCD set CD_DESC='Bank Bukopin' where CODE_TYPE='CCBK' and CM_CODE='BUKO';
update COCD set CD_DESC='Bank ICB Bumiputera' where CODE_TYPE='CCBK' and CM_CODE='ICB';
update COCD set CD_DESC='Bank Central Asia' where CODE_TYPE='CCBK' and CM_CODE='BCA';
update COCD set CD_DESC='Bank CIMB Niaga' where CODE_TYPE='CCBK' and CM_CODE='CIMB';
update COCD set CD_DESC='Bank Danamon Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BDI';
update COCD set CD_DESC='Bank ICBC Indonesia' where CODE_TYPE='CCBK' and CM_CODE='ICBC';
update COCD set CD_DESC='Bank Internasional Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BII';
update COCD set CD_DESC='Bank ICB Bumiputera' where CODE_TYPE='CCBK' and CM_CODE='MNDR';
update COCD set CD_DESC='Bank Mandiri' where CODE_TYPE='CCBK' and CM_CODE='BCA';
update COCD set CD_DESC='Bank Mega' where CODE_TYPE='CCBK' and CM_CODE='MEGA';
update COCD set CD_DESC='Bank Negara Indonesia 1946' where CODE_TYPE='CCBK' and CM_CODE='BNI';
update COCD set CD_DESC='PAN Indonesia Bank' where CODE_TYPE='CCBK' and CM_CODE='PIB';
update COCD set CD_DESC='Bak Rakyat Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BRI';
update COCD set CD_DESC='Bank Permata' where CODE_TYPE='CCBK' and CM_CODE='BP';
update COCD set CD_DESC='CITIBANK' where CODE_TYPE='CCBK' and CM_CODE='CITI';
update COCD set CD_DESC='Bank OCBC NISP' where CODE_TYPE='CCBK' and CM_CODE='OCBC';
update COCD set CD_DESC='Standard Chartered Bank' where CODE_TYPE='CCBK' and CM_CODE='SCB';
update COCD set CD_DESC='Bank UOB Indonesia' where CODE_TYPE='CCBK' and CM_CODE='UOB';
update COCD set CD_DESC='BNI Syariah' where CODE_TYPE='CCBK' and CM_CODE='BNIS';
update COCD set CD_DESC='Bank Sinarmas' where CODE_TYPE='CCBK' and CM_CODE='BSIN';
update COCD set CD_DESC='AEON Credit Services' where CODE_TYPE='CCBK' and CM_CODE='OCBC';
update COCD set CD_DESC= concat('The Hongkong ','&',' Shanghai Bank') where CODE_TYPE='CCBK' and CM_CODE='THSB';

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BUA','LPM','001','Loan Product Maintenance','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BUA','LID','001','Loan Interest Details','N','setup',sysdate,'setup',sysdate);
