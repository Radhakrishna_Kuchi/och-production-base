
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_NO_RETRIES','6',
'Number of retries for data fetch from OCR',
'N','setup',sysdate,
'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_IN_PROGRESS_ERROR','in progress',
'In progress error message from OCR',
'N','setup',sysdate,
'setup',sysdate);
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg",
"r_mod_id","r_mod_time","r_cre_id","r_cre_time") values
(1,'01','BWY','OCR_DATA_FETCH_TIMEOUT_BEFORE_RETRY','5000',
'Time out before retrying OCH data fetch',
'N','setup',sysdate,
'setup',sysdate);
