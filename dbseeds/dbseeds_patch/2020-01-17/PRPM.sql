-- Added for ASLIRI
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','ASLIRI_HEADER_TOKEN','bdae4480-70b8-468a-9841-b241890ddd7a','ASLIRI Header token','N','setup',
sysdate,'setup',sysdate);

-- Added for ASLIRI
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','ASLIRI_SELFIE_VERIFY_URL','/asliri/bri_poc/verify_selfie','ASLIRI Selfie Verify End point URL','N','setup',
sysdate,'setup',sysdate);

-- Added for ASLIRI
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','ASLIRI_APPROVAL_RATE','50.0','ASLIRI face comparasion approval rate.','N','setup',
sysdate,'setup',sysdate);

-- Added for ASLIRI
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','IS_ASLIRI_ENABLED','Y','The flag to enable ASLI RI feature for selfie verification','N','setup',
sysdate,'setup',sysdate);


-- to disable EKYC feature.
update ececuser.PRPM set property_val = 'N' where property_name  ='IS_EKYC_ENABLED';

-- Fix for payroll rejection - application batch update
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','PAYROLL_REJ_STATUS_EXPIRY_PERIOD','1','Application Status form expiry period','N','setup',sysdate,'setup',sysdate);