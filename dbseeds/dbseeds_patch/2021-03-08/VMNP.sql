-- Added entry for payroll date change
SET DEFINE OFF;
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CPDUR','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CPDUR','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CPDUR','VTUSR','!','PARENT',1,'N','setup',to_timestamp('2020-10-23 12:41:12.0','null'),'setup',to_timestamp('2020-10-23 12:41:12.0','null'));

