delete from PRPM where property_name='WHITELIST_AUTHORIZATION_TOKEN' and bank_id='01';
delete from PRPM where property_name='PAYROLL_WS_ENDPOINT_URL' and bank_id='01';

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','WHITELIST_AUTHORIZATION_TOKEN','1',
'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IlZiaFYzdkM1QVdYMzlJVlUiLCJjbGllbnRTZWNyZXQiOiJXU1AyTmNIY2lXdnFaVGEyTjk1UnhSVFpIV1VzYUQ2SCIsImV4cCI6MTYwNzE2ODM4OCwiaXNzIjoiQlJJIEFHUk8ifQ.j9UOQBsCFIBe4YhS1s570BeK39yR9G9ObIZjD6fi3-Q'
,'N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','PAYROLL_WS_ENDPOINT_URL','1',
'/api/whitelist/v1.0/inquiry'
,'N','setup',sysdate,'setup',sysdate);


delete from COCD where CODE_TYPE = 'ASVS' and bank_id = '01' and CM_CODE='PAYROLL_APP';
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASVS','PAYROLL_APP','001','0','N','setup',sysdate,'setup',sysdate);
update COCD set cm_code = 'KTP_SAVED|PER_SAVED|PAY_SAVED|CON_SAVED|PAYROLL_APP', r_mod_time = sysdate where CODE_TYPE = 'ASSF';

update PRPM set property_val='/api/cs/v3.0/pinang/payroll' where property_name='CREDIT_SCORE_ENDPOINT_URL' and bank_id='01';