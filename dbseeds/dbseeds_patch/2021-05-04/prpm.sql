Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDIT_SCORE_PRIVILEGE_ENDPOINT','/api/cs/v3.0/pinang/payroll-privilege','Endpoint url for credit score privilege system','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDIT_SCORE_PRIVILEGE_TOKEN','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IlZiaFYzdkM1QVdYMzlJVlUiLCJjbGllbnRTZWNyZXQiOiJXU1AyTmNIY2lXdnFaVGEyTjk1UnhSVFpIV1VzYUQ2SCIsImV4cCI6MTYwNzE2ODM4OCwiaXNzIjoiQlJJIEFHUk8ifQ.j9UOQBsCFIBe4YhS1s570BeK39yR9G9ObIZjD6fi3-Q','Token for credit score privilege system','N','setup',sysdate,'setup',sysdate);



update prpm set property_val='/api/whitelist/v2.0/inquiry'  where property_name='PAYROLL_WS_ENDPOINT_URL'