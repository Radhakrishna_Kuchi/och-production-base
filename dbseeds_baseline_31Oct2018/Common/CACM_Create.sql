
CREATE TABLE ECECUSER.CUSTOM_APP_CONTENT_MASTER
(
  BANK_ID                     NVARCHAR2(11),
  DB_TS                       NUMBER(5),
  START_DATE                  DATE    NOT NULL,
  END_DATE                    DATE,
  CONTENT_TYPE                NVARCHAR2(4)		NOT NULL,
  HEADER                      NVARCHAR2(512),
  DESCRIPTION                 NVARCHAR2(512),
  TEXT_TYPE					  NVARCHAR2(4),
  MERCHANT_ID                 NVARCHAR2(4),
  AND_APP_URL                 NVARCHAR2(512),
  PLAY_STORE_URL              NVARCHAR2(512),
  IOS_APP_URL                 NVARCHAR2(512),
  IOS_STORE_URL               NVARCHAR2(512),
  PHOTO_KEY                   NVARCHAR2(512),
  FILENAME                    NVARCHAR2(512),
  PRIORITY                    NVARCHAR2(4),
  DEL_FLG                     CHAR(1),
  R_MOD_TIME                  DATE,
  R_MOD_ID                    NVARCHAR2(65),
  R_CRE_TIME                  DATE,
  R_CRE_ID                    NVARCHAR2(65) 
);


CREATE UNIQUE INDEX ECECUSER.IDX_CACM ON ECECUSER.CUSTOM_APP_CONTENT_MASTER(PHOTO_KEY);

CREATE OR REPLACE SYNONYM ECECUSER.CACM FOR ECECUSER.CUSTOM_APP_CONTENT_MASTER;
