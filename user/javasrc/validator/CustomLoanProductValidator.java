package com.infosys.custom.ebanking.user.validator;

import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.fentbase.common.FBATransactionContext;

public class CustomLoanProductValidator implements IFEBAStandardValidator {
	private static CustomLoanProductValidator instance = null;

	public void validate(FEBATransactionContext tc, IFEBAType objectToBeValidated)
			throws BusinessException, CriticalException {
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objectToBeValidated;
		FBATransactionContext ctx = (FBATransactionContext) tc;

		ArrayList<BusinessExceptionVO> pExceptionListList = new ArrayList<>();
		BusinessException completeErrors = new BusinessException(tc,
				(ArrayList<BusinessExceptionVO>) pExceptionListList);

		if (customLoanProductDetailsVO.getMerchantID().toString().equals("")) {
			completeErrors.add(new BusinessException(true, ctx, "CUSER0002", null, "merchantID",
					CustomEBankingErrorCodes.MERCHANT_ID_MANDATORY, null, null));

		}
		if (customLoanProductDetailsVO.getProductCode().toString().equals("")) {
			completeErrors.add(new BusinessException(true, ctx, "CUSER0002", null, "productCode",
					CustomEBankingErrorCodes.PROD_CODE_MANDATORY, null, null));
		}
		if (customLoanProductDetailsVO.getProductCategory().toString().equals("")) {
			completeErrors.add(new BusinessException(true, ctx, "CUSER0003", null, "productCategory",
					CustomEBankingErrorCodes.PROD_CAT_MANDATORY, null, null));

		}
		if (customLoanProductDetailsVO.getProductSubCategory().toString().equals("")) {
			completeErrors.add(new BusinessException(true, ctx, "CUSER0004", null, "productSubCategory",
					CustomEBankingErrorCodes.PROD_SUBCAT_MANDATORY, null, null));
		}
		if (customLoanProductDetailsVO.getConfigType().toString().equals("")) {
			completeErrors.add(new BusinessException(true, ctx, "CUSER0004", null, "configType",
					CustomEBankingErrorCodes.CONFIG_TYPE_MANDATORY, null, null));
		}

		if (completeErrors.count() > 0) {
			throw new BusinessException(ctx, completeErrors.getExceptionList());
		}
	}

	/**
	 * Returns the Singleton instance of this validator.
	 */
	public static CustomLoanProductValidator getInstance() {

		if (instance == null) {
			instance = new CustomLoanProductValidator();
		}
		return instance;
	}
}
