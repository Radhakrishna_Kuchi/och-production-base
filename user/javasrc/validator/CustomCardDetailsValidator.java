/*

 * CustomCardDetailsValidator.java

 * @since feb 17, 2012 - 3:10:40 PM

 *

 * COPYRIGHT NOTICE:

 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,

 * Hosur Road, Bangalore - 560 100, India.

 * All Rights Reserved.

 * This software is the confidential and proprietary information of

 * Infosys Technologies Ltd. ("Confidential Information"). You shall

 * not disclose such Confidential Information and shall use it only

 * in accordance with the terms of the license agreement you entered

 * into with Infosys.

 */

package com.infosys.custom.ebanking.user.validator;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.lists.IFEBAList;
import com.infosys.feba.framework.types.valueobjects.GenericFieldVO;
import com.infosys.feba.framework.valengine.IFEBAStandardListValidator;

import com.infosys.fentbase.common.FBAErrorCodes;

/**
 * 
 * This class validates the card details
 * 
 * @author Renita_Pinto
 * 
 * @version 1.0
 * 
 * @since FEBA 2.0
 * 
 */

public class CustomCardDetailsValidator implements IFEBAStandardListValidator {
	/**
	 * This method validates the card details
	 * 
	 * @author Shilpa_S09
	 * @param tc
	 * @param objectToBeValidated
	 * @throws BusinessException,CriticalException,FatalException
	 *             R
	 */
	public void validate(FEBATransactionContext objContext, IFEBAList list)
			throws BusinessException, CriticalException {

		FEBAHashList<GenericFieldVO> hashList = (FEBAHashList<GenericFieldVO>) list;
		String cardNo = "";
		String cardExpiryDate = "";
		String ktpNo = "";

		if (hashList != null && hashList.size() > 0) {
			cardNo = ((GenericFieldVO) hashList.get("CARD_NUMBER")).getSValue().toString();
			cardExpiryDate = ((GenericFieldVO) hashList.get("CARD_EXP_DATE")).getSValue().toString();
			ktpNo = ((GenericFieldVO) hashList.get("KTP_NUMBER")).getSValue().toString();

			if ((cardNo == null || cardNo == "")) {
				throw new BusinessException(true, objContext, EBIncidenceCodes.IDENTIFICATION_CARD_MANDATORY,
						"Enter the card number.", null, FBAErrorCodes.CARD_NO_MANDATORY, null);
			}
			if (cardExpiryDate == null || cardExpiryDate == "") {
				throw new BusinessException(true, objContext, EBIncidenceCodes.INVALID_EXPIRY_DATE,
						"Enter the Card Expiry date", null, EBankingErrorCodes.ENTER_EXP_DATE, null);
			}
			if (ktpNo == null || ktpNo == "") {
				throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.KTP_CARD_NUMBER_EMPTY,
						"KTP Number empty.", null, CustomEBankingErrorCodes.KTP_CARD_NUMBER_EMPTY, null);
			}

		}
	}

}
