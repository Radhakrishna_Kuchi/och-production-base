package com.infosys.custom.ebanking.user.util;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.Flag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FileName;
import com.infosys.feba.framework.types.primitives.FileSequenceNumber;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.tao.FDTTTAO;
import com.infosys.fentbase.tao.info.FDTTInfo;
import com.infosys.fentbase.types.primitives.FileSeqNumber;


public class CustomPopulateLoanApplicationDetailsUtility {

	private CustomPopulateLoanApplicationDetailsUtility() {

	}

	static String exception = "Exception";

	public static void populateLoanDetails(CustomLoanApplicationDetailsVO detVO, CLATInfo clatInfo) {

		CustomLoanApplnMasterDetailsVO loanMasterDetVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());

		loanMasterDetVO.setApplicationId(clatInfo.getApplicationId());
		loanMasterDetVO.setUserId(clatInfo.getUserId());
		loanMasterDetVO.setRequestedLoanAmt(clatInfo.getRequestedLoanAmt());
		loanMasterDetVO.setRequestedTenor(clatInfo.getRequestedTenor());
		loanMasterDetVO.setLoanIntRate(clatInfo.getLoanIntRate());
		loanMasterDetVO.setMonthlyInstallment(clatInfo.getMonhtlyInstallment());
		loanMasterDetVO.setLegacyCif(clatInfo.getLegacyCif());
		loanMasterDetVO.setPrivyAgreeDate(clatInfo.getPrivyAgreeDate());
		loanMasterDetVO.setProductTnCAgreeDate(clatInfo.getProductTncAgreeDate());
		loanMasterDetVO.setApplicationStatus(clatInfo.getApplicationStatus());
		loanMasterDetVO.setProductType(clatInfo.getProductType());
		loanMasterDetVO.setProductCode(clatInfo.getProductCode());
		loanMasterDetVO.setProductCategory(clatInfo.getProductCategory());
		loanMasterDetVO.setProductSubCategory(clatInfo.getProductSubcategory());
		loanMasterDetVO.setRModTime(clatInfo.getCookie().getRModTime());
		loanMasterDetVO.setApprovedAmount(clatInfo.getApprovedAmount());
		loanMasterDetVO.setLoanAccountId(clatInfo.getLoanAccountId());
		// AGRO payroll changes
		loanMasterDetVO.setBankCode(clatInfo.getBankCode());
		detVO.setLoanApplnMasterDetails(loanMasterDetVO);
	}

	public static void populatePayrollDetails(FEBATransactionContext objContext, CustomLoanApplicationDetailsVO detVO) {

		CLPAInfo clpaInfo = null;
		CustomLoanApplnPayrollAccntDetailsVO payrollDetailsVO = null;

		try {

			clpaInfo = CLPATAO.select(objContext, ((EBTransactionContext) objContext).getBankId(),
					detVO.getLoanApplnMasterDetails().getApplicationId());

			if (clpaInfo != null) {
				payrollDetailsVO = (CustomLoanApplnPayrollAccntDetailsVO) FEBAAVOFactory.createInstance(
						"com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnPayrollAccntDetailsVO");

				payrollDetailsVO.setPayrollAccountId(clpaInfo.getPayrollAccountNum());
				payrollDetailsVO.setPayrollAccountBranch(clpaInfo.getPayrollAccBranch());
				payrollDetailsVO.setAccountPayrollDate(clpaInfo.getAccPayrollDate());
				payrollDetailsVO.setRefferalCode(clpaInfo.getRefferalCode());
				payrollDetailsVO.setSavingAmount(clpaInfo.getSavingAmount());
				payrollDetailsVO.setAgentCode(clpaInfo.getAgentCode());
				payrollDetailsVO.setCreFre3(clpaInfo.getCreditFrequency3());
				payrollDetailsVO.setCreFre6(clpaInfo.getCreditFrequency6());
				payrollDetailsVO.setCreFre12(clpaInfo.getCreditFrequency12());
				payrollDetailsVO.setDebFre3(clpaInfo.getDebitFrequency3());
				payrollDetailsVO.setDebFre3(clpaInfo.getDebitFrequency6());
				payrollDetailsVO.setDebFre3(clpaInfo.getDebitFrequency12());
				payrollDetailsVO.setCreTot3(clpaInfo.getCreditTotal3());
				payrollDetailsVO.setCreTot6(clpaInfo.getCreditTotal6());
				payrollDetailsVO.setCreTot12(clpaInfo.getCreditTotal12());
				payrollDetailsVO.setDebTot3(clpaInfo.getDebitTotal3());
				payrollDetailsVO.setDebTot6(clpaInfo.getDebitTotal6());
				payrollDetailsVO.setDebTot12(clpaInfo.getDebitTotal12());
				payrollDetailsVO.setCreAvg3(clpaInfo.getCreditAvg3());
				payrollDetailsVO.setCreAvg6(clpaInfo.getCreditAvg6());
				payrollDetailsVO.setCreAvg12(clpaInfo.getCreditAvg12());
				payrollDetailsVO.setDebAvg3(clpaInfo.getDebitAvg3());
				payrollDetailsVO.setDebAvg6(clpaInfo.getDebitAvg6());
				payrollDetailsVO.setDebAvg12(clpaInfo.getDebitAvg12());
				payrollDetailsVO.setNetBanking(clpaInfo.getInetBankingStatus());
				payrollDetailsVO.setMobileBanking(clpaInfo.getMobileBankingStatus());
				payrollDetailsVO.setPrimaryPhone(clpaInfo.getPrimaryPhoneStatus());
				payrollDetailsVO.setSecondaryPhone(clpaInfo.getSecondaryPhoneStatus());
				payrollDetailsVO.setSavingAccountOwnershipStatus(clpaInfo.getSavAccountOwnershipStatus());
				payrollDetailsVO.setSavingAccountOwnershipDate(clpaInfo.getSavAccountOwnershipDate());
				payrollDetailsVO.setCompanyName(clpaInfo.getCompanyName());
				payrollDetailsVO.setPersonelName(clpaInfo.getPersonelName());
				payrollDetailsVO.setPersonelNumber(clpaInfo.getPersonelNumber());
				/*
				 * Added fields for Whitelist changes
				 * 
				 */
				
				payrollDetailsVO.setBirthDateWL(clpaInfo.getBirthDateWl());
				payrollDetailsVO.setEmpEndDateWL(clpaInfo.getEmpEndDateWl());
				payrollDetailsVO.setEmpStartDateWL(clpaInfo.getEmpStartDateWl());
				payrollDetailsVO.setNameWL(clpaInfo.getNameWl());
				payrollDetailsVO.setNetIncomeWL(clpaInfo.getNetIncomeWl());
				payrollDetailsVO.setGenderWL(clpaInfo.getGenderWl());
				payrollDetailsVO.setAddressWL(clpaInfo.getAddressWl());
				payrollDetailsVO.setPlaceOfBirthWL(clpaInfo.getPlaceOfBirthWl());
				// Added for BRI AGRO payroll
				payrollDetailsVO.setEmployeeStatus(clpaInfo.getEmployeeStatus());
				payrollDetailsVO.setExpenditureAmount(clpaInfo.getExpenditureAmt());
				
				detVO.setPayrollAccntDetails(payrollDetailsVO);
			}

		} catch (FEBATableOperatorException e) {

			detVO.setIsPayrollDetNull(new Flag("Y"));
			
			Logger.logError(exception + e.getMessage() + e.getStackTrace());
		}
	}

	public static void populateDocumentDetails(FEBATransactionContext objContext,
			CustomLoanApplicationDetailsVO detVO) {
				
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;		
		FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> docList = null;
		QueryOperator queryOperator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_DOCUMENT_LIST);
		String pinangOrCeria=PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", ebContext);
		queryOperator.associate("bankId", ((EBTransactionContext) objContext).getBankId());
		queryOperator.associate("applicationId", detVO.getLoanApplnMasterDetails().getApplicationId());
		if(!ebContext.getUserType().getValue().equals(FBAConstants.STRING_RELATIONSHIP_MANAGER))
		{
		queryOperator.associate("docType", new DocumentType("KTP"));
		}
		FDTTInfo fdttInfo=null;

		try {
			docList = queryOperator.fetchList(objContext);
			if(ebContext.getUserType().getValue().equals(FBAConstants.STRING_RELATIONSHIP_MANAGER))
				{		
					CustomLoanApplnDocumentsDetailsVO docDetVO=(CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory.createInstance(CustomLoanApplnDocumentsDetailsVO.class.getName());
					
					for (int i = 0; i < docList.size(); i++) {
						fdttInfo =FDTTTAO.select(objContext, docList.get(i).getFileSeqNo(),((EBTransactionContext) objContext).getBankId());
						String docType=docList.get(i).getDocType().toString();
						FileName fileName=fdttInfo.getFileName();
						FileSequenceNumber fileSeqNo=docList.get(i).getFileSeqNo();
						if(docType!=null && docType.equals("KTP")){							
							docDetVO.setFileName1(fileName);
							docDetVO.setFileSeqNo1(fileSeqNo);
						}
						
						
						if(pinangOrCeria!=null && pinangOrCeria.equals(CustomEBConstants.PINANG) && docType!=null && docType.equals("DBTL")){
							docDetVO.setFileName3(fileName);
							docDetVO.setFileSeqNo3(fileSeqNo);
							
						}
						if(pinangOrCeria!=null && pinangOrCeria.equals(CustomEBConstants.PINANG) && docType!=null && docType.equals("DAOF")){
							docDetVO.setFileName2(fileName);
							docDetVO.setFileSeqNo2(fileSeqNo);
						}
						if(pinangOrCeria!=null && pinangOrCeria.equals(CustomEBConstants.SAHABAT) && docType!=null && docType.equals("DSCC")){
							docDetVO.setFileName2(fileName);
							docDetVO.setFileSeqNo2(fileSeqNo);
						}
						
					}
					detVO.setLoanDocumentDetails(docDetVO);

					
				}
				else{
					CustomLoanApplnDocumentsDetailsVO docDetVO = docList.get(0);
					detVO.setLoanDocumentDetails(docDetVO);
				}

		} catch (DALException e) {
			Logger.logError("Exception e" + e);
		} 
		catch (FEBATableOperatorException e) {
			Logger.logError(exception + e.getMessage() + e.getStackTrace());

		} 
		finally {
			// Call the closeHandle method which Closes the handle
			queryOperator.closeHandle((EBTransactionContext) objContext);
		}

	}

	public static void populateApplicantDetails(FEBATransactionContext objContext,
			CustomLoanApplicationDetailsVO detVO) {

		CLADInfo cladInfo = null;
		CustomLoanApplnApplicantDetailsVO applicantDetailsVO = null;
		try {

			cladInfo = CLADTAO.select(objContext, ((EBTransactionContext) objContext).getBankId(),
					detVO.getLoanApplnMasterDetails().getApplicationId());

			if (cladInfo != null) {
				applicantDetailsVO = (CustomLoanApplnApplicantDetailsVO) FEBAAVOFactory.createInstance(
						"com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO");

				applicantDetailsVO.setEmailId(cladInfo.getEmailId());
				applicantDetailsVO.setMobileNum(cladInfo.getMobileNo());
				applicantDetailsVO.setName(cladInfo.getName());
				applicantDetailsVO.setNationalId(cladInfo.getNationalId());
				applicantDetailsVO.setGender(cladInfo.getGender());
				applicantDetailsVO.setPlaceOfBirth(cladInfo.getPlaceOfBirth());
				applicantDetailsVO.setReligion(cladInfo.getReligion());
				applicantDetailsVO.setLastEducation(cladInfo.getLastEducation());
				applicantDetailsVO.setHomeOwnershipStatus(cladInfo.getHomeOwnershipStatus());
				applicantDetailsVO.setHomeOwnershipDuration(cladInfo.getHomeOwnershipDuration());
				applicantDetailsVO.setHomePhoneNum(cladInfo.getHomePhoneNum());
				applicantDetailsVO.setAddressLine1(cladInfo.getAddrLine1());
				applicantDetailsVO.setAddressLine2(cladInfo.getAddrLine2());
				applicantDetailsVO.setAddressLine3(cladInfo.getAddrLine3());
				applicantDetailsVO.setAddressLine4(cladInfo.getAddrLine4());
				applicantDetailsVO.setAddressLine5(cladInfo.getAddrLine5());
				applicantDetailsVO.setState(cladInfo.getState());
				applicantDetailsVO.setCity(cladInfo.getCity());
				applicantDetailsVO.setPostalCode(cladInfo.getPostalCode());
				applicantDetailsVO.setCardIssuerBank(cladInfo.getCardIssuerBank());
				applicantDetailsVO.setDateOfBirth(cladInfo.getDateOfBirth());
				applicantDetailsVO.setIsAddressSameAsKTP(cladInfo.getIsAddrSameAsKtp());
				detVO.setLoanApplicantDetails(applicantDetailsVO);
			}

		} catch (FEBATableOperatorException e) {
			detVO.setIsApplicantDetNull(new Flag("Y"));

			Logger.logError(exception + e.getMessage() + e.getStackTrace());
		}
	}

	public static void populateContactDetails(FEBATransactionContext objContext, CustomLoanApplicationDetailsVO detVO) {

		CLCDInfo clcdInfo = null;
		CustomLoanApplnContactDetailsVO contactDetailsVO = null;
		try {

			clcdInfo = CLCDTAO.select(objContext, ((EBTransactionContext) objContext).getBankId(),
					detVO.getLoanApplnMasterDetails().getApplicationId());

			if (clcdInfo != null) {

				contactDetailsVO = (CustomLoanApplnContactDetailsVO) FEBAAVOFactory.createInstance(
						"com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO");
				contactDetailsVO.setMaritalStatus(clcdInfo.getMaritalStatus());
				contactDetailsVO.setSpouseId(clcdInfo.getSpouseId());
				contactDetailsVO.setPartnerNikId(clcdInfo.getPartnernikid());
				contactDetailsVO.setPartnerName(clcdInfo.getPartnername());
				contactDetailsVO.setNoOfDependents(clcdInfo.getNoOfDependents());
				contactDetailsVO.setMotherName(clcdInfo.getMotherName());
				contactDetailsVO.setEmergencyContact(clcdInfo.getEmergencyContact());
				contactDetailsVO.setEmergencyContactRelation(clcdInfo.getEmrConRelation());
				contactDetailsVO.setEmergencyContactAddress(clcdInfo.getEmrConAddress());
				contactDetailsVO.setEmergencyContactPhoneNum(clcdInfo.getEmrConPhoneNum());
				contactDetailsVO.setEmergencyContactPostalCode(clcdInfo.getEmrConPostalCd());
				detVO.setLoanApplicantContactDetails(contactDetailsVO);
			}

		} catch (FEBATableOperatorException e) {

			detVO.setIsContactDetNull(new Flag("Y"));
			Logger.logError(exception + e.getMessage() + e.getStackTrace());
		}
	}

	public static void populateEmploymentDetails(FEBATransactionContext objContext,
			CustomLoanApplicationDetailsVO detVO) {

		CLEDInfo cledInfo = null;
		CustomLoanApplnEmploymentDetailsVO employmentDetailsVO = null;
		try {

			cledInfo = CLEDTAO.select(objContext, ((EBTransactionContext) objContext).getBankId(),
					detVO.getLoanApplnMasterDetails().getApplicationId());

			if (cledInfo != null) {
				employmentDetailsVO = (CustomLoanApplnEmploymentDetailsVO) FEBAAVOFactory.createInstance(
						"com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO");

				employmentDetailsVO.setTaxId(cledInfo.getTaxId());
				employmentDetailsVO.setFinancialSource(cledInfo.getFinancialSource());
				employmentDetailsVO.setMonthlyIncome(cledInfo.getMonthlyIncome());
				employmentDetailsVO.setMonthlyExpense(cledInfo.getMonthlyExpense());
				employmentDetailsVO.setWorkType(cledInfo.getWorkType());
				employmentDetailsVO.setEmployerName(cledInfo.getEmployerName());
				employmentDetailsVO.setEmploymentStatus(cledInfo.getEmpStatus());
				employmentDetailsVO.setEmploymentStartDate(cledInfo.getEmpStartDate());
				employmentDetailsVO.setEmploymentEndDate(cledInfo.getEmpEndDate());
				detVO.setLoanApplicantEmploymentDetails(employmentDetailsVO);
			}

		} catch (FEBATableOperatorException e) {

			detVO.setIsEmploymentDetNull(new Flag("Y"));
			Logger.logError(exception + e.getMessage() + e.getStackTrace());

		}
	}

}
