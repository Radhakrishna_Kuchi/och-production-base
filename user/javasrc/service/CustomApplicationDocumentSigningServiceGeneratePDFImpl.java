package com.infosys.custom.ebanking.user.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocBinaryListVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentSigningReportUtility;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.FrameworkTypesCatalogueConstants;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.types.primitives.StateDescription;
import com.infosys.fentbase.types.valueobjects.CommonCodeCacheVO;

public class CustomApplicationDocumentSigningServiceGeneratePDFImpl extends AbstractHostUpdateTran {
	private static final SimpleDateFormat DT_FMT_DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	public FEBABinary createPDF(CustomApplicationDocEnquiryVO enquiryVO,
			CustomLoanApplicationDetailsVO applicationDetailsVO, FEBATransactionContext txnContext) {

		FEBAUnboundString reportName = enquiryVO.getCriteria().getDocTitle();
		String payrollAccountId = StringUtils.leftPad(applicationDetailsVO.getPayrollAccntDetails().getPayrollAccountId().getValue(), 15, '0');

		applicationDetailsVO.getPayrollAccntDetails().setPayrollAccountId(payrollAccountId);

		FEBAArrayList<CustomLoanApplicationDetailsVO> jrxmlContentList = new FEBAArrayList<>();

		/*
		 * Updating Common Code to Code Description
		 */
		
		jrxmlContentList.addObject(applicationDetailsVO);

		return generateReport(txnContext, jrxmlContentList, reportName);
	}
	private String getCommonCodeDesc(EBTransactionContext context, String codeType, CommonCode code)
			throws CriticalException {
		DescriptionMedium codeDescription = null;
		if (null != code && code.toString().trim().length() != 0) {
			codeDescription = (DescriptionMedium) AppDataManager.getValue(context, FBAAppDataConstants.COMMONCODE_CACHE,
					FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + codeType
					+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
					+ code);
		}

		if (!(FEBATypesUtility.isNotNullOrBlank(codeDescription))) {
			if (FEBATypesUtility.isNotNullOrBlank(code)) {
				return code.toString();
			}
			return "";
		}
		return codeDescription.toString();
	}

	private FEBAUnboundString getCurrentDate(FEBATransactionContext ctx, DateFormat dtFormat) {
		return new FEBAUnboundString(dtFormat.format(DateUtil.getCurrentDate(ctx)));
	}

	@Override
	protected void processHostData(FEBATransactionContext txnContext, IFEBAValueObject objInputOuput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomApplicationDocEnquiryVO enquiryVO = (CustomApplicationDocEnquiryVO) objInputOuput;
		CustomDocumentInputDetailsVO documentDetails = new CustomDocumentInputDetailsVO();
		EBHostInvoker.processRequest(ebContext, "CustomDocumentSigningRequest", enquiryVO);

	}

	@Override
	protected void processLocalData(FEBATransactionContext txnContext, IFEBAValueObject objInputOuput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomApplicationDocEnquiryVO enquiryVO = (CustomApplicationDocEnquiryVO) objInputOuput;

		FEBAArrayList<CustomApplicationDocBinaryListVO> arrayList = new FEBAArrayList<>();
		String appProductId = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", txnContext);
		FEBABinary pdfBinary = null;
		CommonCodeCacheVO cocdCacheVO = (CommonCodeCacheVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.CommonCodeCacheVO);
		CommonCodeVO commonCodeVO = (CommonCodeVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.CommonCodeVO);
		// Set the criteria fields in the CommonCodeVO
		commonCodeVO.setBankID(txnContext.getBankId());
		commonCodeVO.setLanguageID(txnContext.getLangId());
		// Set the passed Code Type as an Input criteria
		commonCodeVO.setCodeType("DTPE");
		// Set the criteria
		cocdCacheVO.setCriteria(commonCodeVO);
		// Declare Hashlist to store the fetched values
		FEBAHashList cocdList = new FEBAHashList();
		try {
			cocdList = (FEBAHashList) AppDataManager.getList(txnContext, FBAAppDataConstants.COMMONCODE_CACHE,
					cocdCacheVO);

		} catch (CriticalException e) {
			// intentionally left blank
		}

		CustomLoanApplicationDetailsVO applicationDetailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationDetailsVO);

		CustomLoanApplnMasterDetailsVO applnMasterDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnMasterDetailsVO);

		applnMasterDetailsVO.setApplicationId(enquiryVO.getCriteria().getApplicationId());
		applicationDetailsVO.setLoanApplnMasterDetails(applnMasterDetailsVO);
		final FEBAUnboundString docTextMaintService = new FEBAUnboundString("CustomLoanApplicationService");
		final FEBAUnboundString docTextMaintMethod = new FEBAUnboundString("fetchLoanDetails");
		final EBTransactionContext ebTranContext = (EBTransactionContext) txnContext;

		txnContext.setServiceName(docTextMaintService);
		txnContext.setMethodName(docTextMaintMethod);

		try {

			// Invoke the service to Mark the LM Structure.
			LocalServiceUtil.invokeService(txnContext, docTextMaintService.getValue(), docTextMaintMethod,
					applicationDetailsVO);
		} catch (FEBAException e) {
			// intentionally left blank
		}
		
		String city="";
		String homeOwnershipStatus="";
		String cardIssuerBank="";
		String lastEducation="";
		String addLine4="";
		String addLine5="";
		String maritalStatus="";
		String emrConRelation="";

		System.out.println("applicationDetailsVO Before Common Code update- "+applicationDetailsVO);

		
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getCity()) && applicationDetailsVO.getLoanApplicantDetails().getState().getValue().length()<5){
			try {
				city=getCommonCodeDesc(ebTranContext, applicationDetailsVO.getLoanApplicantDetails().getState().getValue(),applicationDetailsVO.getLoanApplicantDetails().getCity());
			} catch (CriticalException e) {
				city=applicationDetailsVO.getLoanApplicantDetails().getCity().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getHomeOwnershipStatus())){
			try {
				homeOwnershipStatus=getCommonCodeDesc(ebTranContext, "RES",applicationDetailsVO.getLoanApplicantDetails().getHomeOwnershipStatus());
			} catch (CriticalException e) {
				homeOwnershipStatus=applicationDetailsVO.getLoanApplicantDetails().getHomeOwnershipStatus().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getCardIssuerBank())){
			try {
				cardIssuerBank=getCommonCodeDesc(ebTranContext, "CCBK",applicationDetailsVO.getLoanApplicantDetails().getCardIssuerBank());
			} catch (CriticalException e) {
				cardIssuerBank=applicationDetailsVO.getLoanApplicantDetails().getCardIssuerBank().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getLastEducation())){
			try {
				lastEducation=getCommonCodeDesc(ebTranContext, "EDU",applicationDetailsVO.getLoanApplicantDetails().getLastEducation());
			} catch (CriticalException e) {
				lastEducation=applicationDetailsVO.getLoanApplicantDetails().getLastEducation().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantContactDetails().getMaritalStatus())){
			try {
				maritalStatus=getCommonCodeDesc(ebTranContext, "MST",applicationDetailsVO.getLoanApplicantContactDetails().getMaritalStatus());
			} catch (CriticalException e) {
				maritalStatus=applicationDetailsVO.getLoanApplicantContactDetails().getMaritalStatus().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getAddressLine4())){
			try {
				addLine4=getCommonCodeDesc(ebTranContext, applicationDetailsVO.getLoanApplicantDetails().getCity().getValue(),new CommonCode(applicationDetailsVO.getLoanApplicantDetails().getAddressLine4().getValue()));
			} catch (CriticalException e) {
				addLine4=applicationDetailsVO.getLoanApplicantDetails().getAddressLine4().getValue();
				e.printStackTrace();
			} catch (FEBATypeSystemException e) {
				addLine4=applicationDetailsVO.getLoanApplicantDetails().getAddressLine4().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantDetails().getAddressLine4())){
			try {
				addLine5=getCommonCodeDesc(ebTranContext, applicationDetailsVO.getLoanApplicantDetails().getAddressLine4().getValue(),new CommonCode(applicationDetailsVO.getLoanApplicantDetails().getAddressLine5().getValue()));
			} catch (CriticalException e) {
				addLine5=applicationDetailsVO.getLoanApplicantDetails().getAddressLine5().getValue();
				e.printStackTrace();
			} catch (FEBATypeSystemException e) {
				addLine5=applicationDetailsVO.getLoanApplicantDetails().getAddressLine5().getValue();
				e.printStackTrace();
			}
		}
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplicantContactDetails().getEmergencyContactRelation())){
			try {
				emrConRelation=getCommonCodeDesc(ebTranContext, "RLT",applicationDetailsVO.getLoanApplicantContactDetails().getEmergencyContactRelation());
			} catch (CriticalException e) {
				emrConRelation=applicationDetailsVO.getLoanApplicantContactDetails().getEmergencyContactRelation().getValue();
				e.printStackTrace();
			}
		}
		
		// Added for AGRO payroll start
		if(FEBATypesUtility.isNotNullOrBlank(applicationDetailsVO.getLoanApplnMasterDetails().getBankCode())){

			String bankCode = applicationDetailsVO.getLoanApplnMasterDetails().getBankCode().getValue();
			String appendBankCode= "BANKNAME_" + bankCode;
			String bankName = PropertyUtil.getProperty(appendBankCode, ebTranContext);
			applicationDetailsVO.getLoanApplnMasterDetails().setNamaBank(bankName);
		}
		// Added for AGRO payroll end
		
		DateFormat dtFormat = DT_FMT_DD_MM_YYYY;
		FEBADate dateOfBirth = new FEBADate();
		FEBADate payrollAcctDate = new FEBADate();
		
		if(FEBATypesUtility.isNotNull(applicationDetailsVO.getPayrollAccntDetails().getBirthDateWL())){
			dateOfBirth = applicationDetailsVO.getPayrollAccntDetails().getBirthDateWL();
			dtFormat.format(dateOfBirth.getValue());
			dateOfBirth.setDateFormat("dd/MM/yyyy");
			dateOfBirth.set(dtFormat.format(dateOfBirth.getValue()));
		}
		
		if(FEBATypesUtility.isNotNull(applicationDetailsVO.getPayrollAccntDetails().getAccountPayrollDate())){
			payrollAcctDate = applicationDetailsVO.getPayrollAccntDetails().getAccountPayrollDate();
			payrollAcctDate.setDateFormat("dd/MM/yyyy");
			payrollAcctDate.set(dtFormat.format(payrollAcctDate.getValue()));
		}

		applicationDetailsVO.getLoanApplicantDetails().setCity(new CommonCode(city));
		applicationDetailsVO.getLoanApplicantDetails().setHomeOwnershipStatus(new CommonCode(homeOwnershipStatus));
		applicationDetailsVO.getLoanApplicantDetails().setCardIssuerBank(new CommonCode(cardIssuerBank));
		applicationDetailsVO.getLoanApplicantDetails().setLastEducation(new CommonCode(lastEducation));
		applicationDetailsVO.getLoanApplicantDetails().setAddressLine4(addLine4);
		applicationDetailsVO.getLoanApplicantDetails().setAddressLine5(addLine5);
		applicationDetailsVO.getLoanApplicantContactDetails().setMaritalStatus(new CommonCode(maritalStatus));
		applicationDetailsVO.getLoanApplicantContactDetails().setEmergencyContactRelation(emrConRelation);
		applicationDetailsVO.getLoanApplicantDetails().setDateOfBirth(dateOfBirth);
		applicationDetailsVO.getPayrollAccntDetails().setAccountPayrollDate(payrollAcctDate);
		applicationDetailsVO.getLoanApplicantDetails().setName(applicationDetailsVO.getPayrollAccntDetails().getNameWL());
		
		StateDescription stateDescription=null;
		try {
			 stateDescription =  (StateDescription)AppDataManager.getValue(txnContext, FBAAppDataConstants.STATECODE_CACHE,
							FBAAppDataConstants.COLUMN_CNTRY + FBAConstants.EQUAL_TO + "ID" + FBAAppDataConstants.SEPERATOR
							 + FBAAppDataConstants.COLUMN_STATE_CODE + FBAConstants.EQUAL_TO + applicationDetailsVO.getLoanApplicantDetails().getState().getValue());
			 applicationDetailsVO.getLoanApplicantDetails().setState(new CommonCode(stateDescription.getValue()));
		} catch (CriticalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("applicationDetailsVO - "+applicationDetailsVO);

		CLADInfo cladinfo = null;
		try {
			cladinfo = CLADTAO.select(txnContext, txnContext.getBankId(), enquiryVO.getCriteria().getApplicationId());
		} catch (FEBATableOperatorException e1) {
			// intentionally left blank
		}
		String ktpNo = "";
		if (cladinfo != null) {
			ktpNo = cladinfo.getNationalId().toString();
		}
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = formatter.format(currentDate);
		if (appProductId.equals(CustomEBConstants.PINANG)) {

			for (int i = 0; i < cocdList.size(); i++) {
				CustomApplicationDocBinaryListVO binaryListVO = (CustomApplicationDocBinaryListVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomApplicationDocBinaryListVO);
				CommonCodeVO codeVO = (CommonCodeVO) cocdList.get(i);
				if (!codeVO.getCommonCode().toString().equals("DSCC")) {
					enquiryVO.getCriteria().setDocumentId(new FEBAUnboundString(codeVO.getCommonCode().toString()));
					String[] cdDesc = codeVO.getCodeDescription().toString().split("\\|");
					enquiryVO.getCriteria().setDocTitle(cdDesc[0]);

					pdfBinary = createPDF(enquiryVO, applicationDetailsVO, txnContext);
					String docTitle = formattedDate + "-" + ktpNo + "-" + cdDesc[1];
					binaryListVO.setBinaryCode(pdfBinary);
					String base64Format = getBase64Format(pdfBinary);
					binaryListVO.setDocTitle(new FEBAUnboundString(docTitle));
					binaryListVO.setDocumentId(codeVO.getCommonCode().toString());
					binaryListVO.setDocString(base64Format);
					arrayList.addObject(binaryListVO);
				}
			}
		} else if (appProductId.equals(CustomEBConstants.SAHABAT)) {
			for (int i = 0; i < cocdList.size(); i++) {
				CustomApplicationDocBinaryListVO binaryListVO = (CustomApplicationDocBinaryListVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomApplicationDocBinaryListVO);
				CommonCodeVO codeVO = (CommonCodeVO) cocdList.get(i);
				// remove dscc condition
				if (codeVO.getCommonCode().toString().equals("DSCC")) {
					enquiryVO.getCriteria().setDocumentId(new FEBAUnboundString(codeVO.getCommonCode().toString()));
					String[] cdDesc = codeVO.getCodeDescription().toString().split("\\|");
					enquiryVO.getCriteria().setDocTitle(cdDesc[0]);
					pdfBinary = createPDF(enquiryVO, applicationDetailsVO, txnContext);
					String docTitle = formattedDate + "-" + ktpNo + "-" + cdDesc[1];
					binaryListVO.setBinaryCode(pdfBinary);
					String base64Format = getBase64Format(pdfBinary);
					binaryListVO.setDocTitle(new FEBAUnboundString(docTitle));
					binaryListVO.setDocumentId(codeVO.getCommonCode().toString());
					binaryListVO.setDocString(base64Format);
					arrayList.addObject(binaryListVO);
				}
			}
		}
		enquiryVO.setBinaryList(arrayList);
	}

	private FEBABinary generateReport(FEBATransactionContext objContext,
			FEBAArrayList<CustomLoanApplicationDetailsVO> jrxmlContentList, FEBAUnboundString reportName) {
		CustomDocumentSigningReportUtility reportUtility = new CustomDocumentSigningReportUtility();
		reportUtility.setSaveReport(false);
		reportUtility.addFieldName("loanApplicantDetails.name");
		reportUtility.addFieldName("loanApplicantDetails.nationalId");
		reportUtility.addFieldName("loanApplicantDetails.gender");
		reportUtility.addFieldName("loanApplicantDetails.placeOfBirth");
		reportUtility.addFieldName("loanApplicantDetails.addressLine1");
		reportUtility.addFieldName("loanApplicantDetails.addressLine2");
		reportUtility.addFieldName("loanApplicantDetails.addressLine3");
		reportUtility.addFieldName("loanApplicantDetails.addressLine4");
		reportUtility.addFieldName("loanApplicantDetails.addressLine5");
		reportUtility.addFieldName("loanApplicantDetails.state");
		reportUtility.addFieldName("loanApplicantDetails.city");
		reportUtility.addFieldName("loanApplicantDetails.mobileNum");
		reportUtility.addFieldName("loanApplicantDetails.lastEducation");
		reportUtility.addFieldName("loanApplicantDetails.postalCode");
		reportUtility.addFieldName("loanApplicantDetails.cardIssuerBank");
		reportUtility.addFieldName("payrollAccntDetails.payrollAccountId");
		reportUtility.addFieldName("loanApplnMasterDetails.requestedLoanAmt");
		reportUtility.addFieldName("loanApplnMasterDetails.requestedTenor");
		reportUtility.addFieldName("loanApplnMasterDetails.approvedAmount");
		reportUtility.addFieldName("loanApplnMasterDetails.loanAccountId");
		reportUtility.addFieldName("loanApplnMasterDetails.loanIntRate");
		reportUtility.addFieldName("loanApplnMasterDetails.monthlyInstallment");
		// Added for AGRO payroll start
		reportUtility.addFieldName("loanApplnMasterDetails.namaBank");
		// Added for AGRO payroll end
		reportUtility.addFieldName("loanApplicantDetails.emailId");
		reportUtility.addFieldName("loanApplicantDetails.dateOfBirth");
		reportUtility.addFieldName("loanApplicantDetails.homeOwnershipStatus");
		reportUtility.addFieldName("loanApplicantDetails.homePhoneNum");
		reportUtility.addFieldName("loanApplicantContactDetails.maritalStatus");
		reportUtility.addFieldName("loanApplicantContactDetails.noOfDependents");
		reportUtility.addFieldName("loanApplicantContactDetails.motherName");
		reportUtility.addFieldName("loanApplicantContactDetails.emergencyContact");
		reportUtility.addFieldName("loanApplicantContactDetails.emergencyContactRelation");
		reportUtility.addFieldName("loanApplicantContactDetails.emergencyContactAddress");
		reportUtility.addFieldName("loanApplicantContactDetails.emergencyContactPhoneNum");
		reportUtility.addFieldName("loanApplicantContactDetails.emergencyContactPostalCode");
		reportUtility.addFieldName("loanApplicantEmploymentDetails.financialSource");
		reportUtility.addFieldName("loanApplicantEmploymentDetails.monthlyIncome");
		reportUtility.addFieldName("loanApplicantEmploymentDetails.monthlyExpense");
		reportUtility.addFieldName("loanApplicantEmploymentDetails.workType");
		reportUtility.addFieldName("payrollAccntDetails.companyName");
		reportUtility.addFieldName("loanApplnMasterDetails.applicationId");
		reportUtility.addFieldName("payrollAccntDetails.accountPayrollDate");
		

		FEBABinary outputStream = null;
		try {
			outputStream = reportUtility.generateReport(objContext, reportName.toString(), jrxmlContentList, null, null,
					null, null);
		} catch (CriticalException | BusinessException | BusinessConfirmation e) {
			// intentionally left blank
		}
		return outputStream;
	}

	public String getBase64Format(FEBABinary pdfBinary) {
		String base64Format = Base64.getEncoder().encodeToString(pdfBinary.getValue());
		Base64.getDecoder().decode(base64Format);
		return base64Format;
	}
}
