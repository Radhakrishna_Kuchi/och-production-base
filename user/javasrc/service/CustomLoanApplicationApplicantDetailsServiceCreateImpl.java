package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnApplicantDetailsVO;

import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.common.validators.MobileNumberCheckVal;
import com.infosys.ebanking.common.validators.PhoneNumberPatternVal;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.validators.EmailValidator;

public class CustomLoanApplicationApplicantDetailsServiceCreateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplnApplicantDetailsVO applicantDetVO = (CustomLoanApplnApplicantDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem("emailId", applicantDetVO.getEmailId(), new EmailValidator(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem("mobileNum", applicantDetVO.getMobileNum(), new MobileNumberCheckVal(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem("homePhoneNum", applicantDetVO.getHomePhoneNum(), new PhoneNumberPatternVal(),
						FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.INDEPENDENT), };

	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplnApplicantDetailsVO applicantDetVO = (CustomLoanApplnApplicantDetailsVO) objInputOutput;

		// insertion into CLAAD using TAO call
		insertCLAD(txnContext, applicantDetVO);

	}

	private void insertCLAD(FEBATransactionContext objContext, CustomLoanApplnApplicantDetailsVO loanApplicantDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CLADTAO cladTAO = new CLADTAO(objContext);

		try {
			cladTAO.associateBankId(ebContext.getBankId());
			cladTAO.associateApplicationId(loanApplicantDetVO.getApplicationId());
			cladTAO.associateEmailId(loanApplicantDetVO.getEmailId());
			cladTAO.associateMobileNo(loanApplicantDetVO.getMobileNum());
			cladTAO.associateName(loanApplicantDetVO.getName());
			cladTAO.associateNationalId(loanApplicantDetVO.getNationalId());
			cladTAO.associateGender(loanApplicantDetVO.getGender());
			cladTAO.associatePlaceOfBirth(loanApplicantDetVO.getPlaceOfBirth());
			cladTAO.associateReligion(loanApplicantDetVO.getReligion());
			cladTAO.associateLastEducation(loanApplicantDetVO.getLastEducation());
			cladTAO.associateHomeOwnershipStatus(loanApplicantDetVO.getHomeOwnershipStatus());
			cladTAO.associateHomeOwnershipDuration(loanApplicantDetVO.getHomeOwnershipDuration());
			cladTAO.associateHomePhoneNum(loanApplicantDetVO.getHomePhoneNum());
			cladTAO.associateAddrLine1(loanApplicantDetVO.getAddressLine1());
			cladTAO.associateAddrLine2(loanApplicantDetVO.getAddressLine2());
			cladTAO.associateAddrLine3(loanApplicantDetVO.getAddressLine3());
			cladTAO.associateAddrLine4(loanApplicantDetVO.getAddressLine4());
			cladTAO.associateAddrLine5(loanApplicantDetVO.getAddressLine5());
			cladTAO.associateState(loanApplicantDetVO.getState());
			cladTAO.associateCity(loanApplicantDetVO.getCity());
			cladTAO.associatePostalCode(loanApplicantDetVO.getPostalCode());
			cladTAO.associateCardIssuerBank(loanApplicantDetVO.getCardIssuerBank());
			cladTAO.associateDateOfBirth(loanApplicantDetVO.getDateOfBirth());
			cladTAO.associateIsAddrSameAsKtp(loanApplicantDetVO.getIsAddressSameAsKTP());
			cladTAO.insert(objContext);

		} catch (FEBATableOperatorException e) {
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CLADD", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}

}
