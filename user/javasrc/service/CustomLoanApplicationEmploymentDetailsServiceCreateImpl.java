package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLEDTAO;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanApplicationEmploymentDetailsServiceCreateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanApplnEmploymentDetailsVO empDetVO = (CustomLoanApplnEmploymentDetailsVO) objInputOutput;

		// insertion into CLAAD using TAO call
		insertCLED(txnContext, empDetVO);

	}

	private void insertCLED(FEBATransactionContext objContext, CustomLoanApplnEmploymentDetailsVO loanApplicantEmpDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CLEDTAO cledTAO = new CLEDTAO(objContext);

		try {
			cledTAO.associateBankId(ebContext.getBankId());
			cledTAO.associateApplicationId(loanApplicantEmpDetVO.getApplicationId());
			cledTAO.associateTaxId(loanApplicantEmpDetVO.getTaxId());
			cledTAO.associateFinancialSource(loanApplicantEmpDetVO.getFinancialSource());
			cledTAO.associateMonthlyIncome(loanApplicantEmpDetVO.getMonthlyIncome());
			cledTAO.associateMonthlyExpense(loanApplicantEmpDetVO.getMonthlyExpense());
			cledTAO.associateWorkType(loanApplicantEmpDetVO.getWorkType());
			cledTAO.associateEmployerName(loanApplicantEmpDetVO.getEmployerName());
			cledTAO.associateEmpStatus(loanApplicantEmpDetVO.getEmploymentStatus());
			cledTAO.associateEmpStartDate(loanApplicantEmpDetVO.getEmploymentStartDate());
			cledTAO.associateEmpEndDate(loanApplicantEmpDetVO.getEmploymentEndDate());

			cledTAO.insert(objContext);

		} catch (FEBATableOperatorException e) {
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CLAED", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}

}
