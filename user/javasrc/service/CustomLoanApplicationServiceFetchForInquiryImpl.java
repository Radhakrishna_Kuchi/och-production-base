package com.infosys.custom.ebanking.user.service;

import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;

import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationCriteriaVO;

import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;

public class CustomLoanApplicationServiceFetchForInquiryImpl extends AbstractLocalListInquiryTran {

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit,
			IFEBAValueObject arg2, QueryOperator queryOperator) throws CriticalException {
		final EBTransactionContext eBankingTransactionContext = (EBTransactionContext) context;
		final CustomLoanApplicationCriteriaVO criteriaVO = (CustomLoanApplicationCriteriaVO) objQueryCrit;

		queryOperator.associate("bankId", eBankingTransactionContext.getBankId());
		queryOperator.associate("userId",eBankingTransactionContext.getUserId());
		if(criteriaVO.getApplicationId()!=null && criteriaVO.getApplicationId().getValue()!=0){
			queryOperator.associate("applicationId", criteriaVO.getApplicationId());
		}
		queryOperator.associate("applicationStatus",criteriaVO.getApplicationStatus());
	}

	@Override
	protected final void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		try {

			super.executeQuery(objContext, objInputOutput, objTxnWM);

		} catch (BusinessException be) {
			if (be.getErrorCode() != ErrorCodes.RECORD_NOT_FOUND) {
				throw new CriticalException(objContext, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
						"Record not found", EBankingErrorCodes.RECORD_NOT_FOUND, be);
			}
		}

	}

	/**
	 * 
	 * Associating DAL CustomShoppingMallStatusEnquiryDAL
	 * 
	 * @see CustomShoppingMallStatusEnquiryDAL.xml
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_APPLICATION_INQUIRY;

	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}
}
