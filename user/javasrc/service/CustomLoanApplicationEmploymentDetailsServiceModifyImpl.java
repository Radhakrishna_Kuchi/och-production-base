package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnEmploymentDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;

import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomLoanApplicationEmploymentDetailsServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomLoanApplnEmploymentDetailsVO loanApplEmploymentDetlsVO = (CustomLoanApplnEmploymentDetailsVO) objInputOutput;
		final CLEDTAO cledTAO = new CLEDTAO(ebContext);
		CLEDInfo cledInfo = null;
		try {
			cledTAO.associateBankId(ebContext.getBankId());
			cledTAO.associateApplicationId(loanApplEmploymentDetlsVO.getApplicationId());
			cledTAO.associateTaxId(loanApplEmploymentDetlsVO.getTaxId());
			cledTAO.associateFinancialSource(loanApplEmploymentDetlsVO.getFinancialSource());
			cledTAO.associateMonthlyIncome(loanApplEmploymentDetlsVO.getMonthlyIncome());
			cledTAO.associateMonthlyExpense(loanApplEmploymentDetlsVO.getMonthlyExpense());
			cledTAO.associateWorkType(loanApplEmploymentDetlsVO.getWorkType());
			cledTAO.associateEmployerName(loanApplEmploymentDetlsVO.getEmployerName());
			cledTAO.associateEmpStatus(loanApplEmploymentDetlsVO.getEmploymentStatus());
			cledTAO.associateEmpStartDate(loanApplEmploymentDetlsVO.getEmploymentStartDate());
			cledTAO.associateEmpEndDate(loanApplEmploymentDetlsVO.getEmploymentEndDate());

			cledInfo = CLEDTAO.select(ebContext, ebContext.getBankId(), loanApplEmploymentDetlsVO.getApplicationId());
			cledTAO.associateCookie(cledInfo.getCookie());

			cledTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {

			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}

	}

}
