
package com.infosys.custom.ebanking.user.service;


import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;

/**
 * This service is used for token privy OTP request.
 * 
 */

public class CustomPrivyServiceOTPRequestImpl extends AbstractHostUpdateTran {

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[0];
	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;	
		CustomPrivyRegistrationVO privyRegVO = (CustomPrivyRegistrationVO) objInputOutput;
		validateMandatory(objContext, privyRegVO);	
		FEBAUnboundString token = privyRegVO.getToken();
		String tokenStr = token.toString();

		if (tokenStr != "" && !(tokenStr.isEmpty())) {
			privyRegVO.setToken(privyRegVO.getToken());
			privyRegVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", ebContext));
		}

		try {
			EBHostInvoker.processRequest(objContext, CustomEBRequestConstants.CUSTOM_PRIVY_OTP_REQUEST,
					privyRegVO);		

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}

	
	private void validateMandatory(FEBATransactionContext ebContext, CustomPrivyRegistrationVO privyRegVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		
		if (!FEBATypesUtility.isNotNullOrBlank(privyRegVO.getToken())) {
			valError.add(getBusinessExceptionVO(ebContext,"Token is mandatory"));
		}
		
		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext, valError);
		}
	}


	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	
}
