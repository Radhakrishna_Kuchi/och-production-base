package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CUDPTAO;
import com.infosys.custom.ebanking.tao.CUPRTAO;
import com.infosys.custom.ebanking.tao.info.CUPRInfo;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CSIPTAO;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CSIPInfo;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.CustomerId;
import com.infosys.fentbase.types.primitives.IndividualUserId;
import com.infosys.fentbase.types.valueobjects.UserProfileVO;

public class CustomUserPhoneNumServiceFetchImpl extends AbstractHostInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {

		};
	}

	@Override
	protected void processHostData(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) txnContext;
		UserProfileVO userVO = (UserProfileVO) objInputOutput;
		UserId userId = userVO.getPrincipalIDER().getUserId();
		CorporateId orgId = userVO.getPrincipalIDER().getCorpId();
		CUSRInfo cusrInfoObj = null;
		CustomerId cifId = null;
		try {
			cusrInfoObj = CUSRTAO.select(txnContext, context.getBankId(), orgId, userId);
			if (cusrInfoObj != null) {
				cifId = cusrInfoObj.getCustId();
				userVO.setCustomerId(cifId);

			}
		} catch (FEBATableOperatorException e) {
			LogManager.logError(null, e);

		}

		try {
			if(FEBATypesUtility.isNotNull(cifId)
					&& !cifId.getValue().isEmpty()){
				EBHostInvoker.processRequest(context, CustomEBRequestConstants.HP_NUM_UPDATE, userVO);
			}
		} catch (BusinessException be) {
			throw new BusinessException(context, CustomEBankingIncidenceCodes.CIF_UPDATE_FAILED,
					"An unexpected exception occurred during CIF ID updation",
					CustomEBankingErrorCodes.CIF_UPDATION_FAILED, be);
		}

	}

	@Override
	protected void processLocalData(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		UserProfileVO userProfileVO = (UserProfileVO) objInputOutput;

		UserId userId = userProfileVO.getPrincipalIDER().getUserId();
		CorporateId orgId = userProfileVO.getPrincipalIDER().getCorpId();
		UserPrincipal principalId = userProfileVO.getPrincipalId();
		MobileNumber cusrMobileNum = userProfileVO.getMobileNo();
		CUSRInfo cusrInfoObj = null;
		IndividualUserId individualId = null;
		FEBAArrayList<UserProfileVO> list = new FEBAArrayList<>();
		try {
			cusrInfoObj = CUSRTAO.select(txnContext, ebContext.getBankId(), orgId, userId);
			if (cusrInfoObj != null) {
				individualId = cusrInfoObj.getIndividualId();

			}
		} catch (FEBATableOperatorException e) {
			LogManager.logError(null, e);

		}

		QueryOperator pQueryOperator = QueryOperator.openHandle(txnContext,
				CustomEBQueryIdentifiers.CUSTOM_USER_CHANNEL_FETCH);
		pQueryOperator.associate("bankId", ebContext.getBankId());
		pQueryOperator.associate("individualID", individualId);
		try {
			list = pQueryOperator.fetchList(txnContext);

		} catch (DALException be) {
			if (be.getErrorCode() != ErrorCodes.RECORD_NOT_FOUND) {
				throw new CriticalException(txnContext, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
						"Record not found", EBankingErrorCodes.RECORD_NOT_FOUND, be);
			}
		}
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				UserProfileVO userVO = list.get(i);
				CSIPInfo info = null;
				try {
					info = CSIPTAO.select(txnContext, txnContext.getBankId(), userVO.getLoginChannel(),
							userVO.getIndividualUserId());
					if (info != null) {

						CSIPTAO csipTAO = new CSIPTAO(txnContext);
						csipTAO.associateChannelId(userVO.getLoginChannel());
						csipTAO.associateBankId(txnContext.getBankId());
						csipTAO.associateIndividualId(userVO.getIndividualUserId());
						csipTAO.associatePrincipalId(principalId);
						csipTAO.associateCookie(info.getCookie());

						csipTAO.update(txnContext);

					}
				} catch (FEBATableOperatorException e) {
					LogManager.logError(null, e);
				}
			}
			CUSRTAO cusrTAO = new CUSRTAO(txnContext);
			boolean isMobileUpdate = false;
			
			if (cusrInfoObj != null) {
				cusrTAO.associateBankId(txnContext.getBankId());
				cusrTAO.associateUserId(userId);
				cusrTAO.associateOrgId(orgId);
				cusrTAO.associateCMPhoneNo(cusrMobileNum);
				cusrTAO.associateCookie(cusrInfoObj.getCookie());
				try {
					cusrTAO.update(txnContext);
					isMobileUpdate = true;
					
				} catch (FEBATableOperatorException e) {
					LogManager.logError(null, e);
				} finally {
					try {
						FEBATableOperator.commit(txnContext);
					} catch (FEBATableOperatorException e) {
						LogManager.logError(null, e);
					}
				}
			}
			
			// logic deletion of the CUPR record when mobile number changes by customer start
			if(isMobileUpdate){
				insertCUPDAndDeleteCUPR(txnContext, userId);
				//logicalDeleteCUPR(txnContext, userId);
			}
			// Logic ends here.
		}
	}
	
	/*
	 * Fixed for JIRA ticket # PD-129 
	 * Method to perform delete the record from CUPR and insert the record in to CUDP table.
	 */
	private void insertCUPDAndDeleteCUPR(FEBATransactionContext objContext, UserId userId) {
		CUPRInfo uprtInfo = null;
		try {
			uprtInfo = CUPRTAO.select(objContext, objContext.getBankId(),	new FEBAUnboundString(userId.getValue()));
			
			System.out.println("CUPR record before deletion: uprtInfo::"+uprtInfo);
			
			CUDPTAO cupdTAO = new CUDPTAO(objContext);
			
			cupdTAO.associateBankId(objContext.getBankId());
			cupdTAO.associateUserId(uprtInfo.getUserId());
			cupdTAO.associateKtpNum(uprtInfo.getKtpNum());
			cupdTAO.associatePrivyId(uprtInfo.getPrivyId());
			cupdTAO.associateStage(uprtInfo.getStage());
			cupdTAO.associatePhotoObj1(uprtInfo.getPhotoObj1());
			cupdTAO.associatePhotoObj2(uprtInfo.getPhotoObj2());
			
//			CUPRTAO uprtTAO = new CUPRTAO(objContext);	
//			uprtTAO.associateBankId(objContext.getBankId());
//			uprtTAO.associateUserId(new FEBAUnboundString(userId.getValue()));
//			
//			if(null!=uprtInfo){
//				uprtTAO.associateCookie(uprtInfo.getCookie());
//			}
//			uprtTAO.physicalDelete(objContext);
//			objContext.getConnection().commit();
//			
//			System.out.println("CUPR Record is deleted successfully for the user ID::"+userId);
//			System.out.println("CUPR record after deletion: uprtInfo::"+uprtInfo);

			cupdTAO.insert(objContext);
			objContext.getConnection().commit();
			System.out.println("CUDP Record is inserted successfully for the user ID::"+userId);
			
			}catch (Exception e) {
				System.out.println("CUDP Record is failed for the user ID::"+userId);
				e.printStackTrace();
			}
	}
}
