package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CAKDTAO;
import com.infosys.custom.ebanking.tao.info.CAKDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicantKtpDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomApplicantKtpDetailsServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		
				return null;

	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomApplicantKtpDetailsVO loanApplicantDetVO = (CustomApplicantKtpDetailsVO) objInputOutput;
		final CAKDTAO cakdTAO = new CAKDTAO(txnContext);
		CAKDInfo ckadInfo = null;
		try {
			
			cakdTAO.associateBankId(ebContext.getBankId());
			cakdTAO.associateApplicationId(loanApplicantDetVO.getApplicationId());
			cakdTAO.associateName(loanApplicantDetVO.getName());
			cakdTAO.associateAddrLine1(loanApplicantDetVO.getAddressLine1());
			cakdTAO.associateAddrLine2(loanApplicantDetVO.getAddressLine2());
			cakdTAO.associateAddrLine3(loanApplicantDetVO.getAddressLine3());
			cakdTAO.associateAddrLine4(loanApplicantDetVO.getAddressLine4());
			cakdTAO.associateState(loanApplicantDetVO.getState());
			cakdTAO.associateCity(loanApplicantDetVO.getCity());
			cakdTAO.associatePostalCode(loanApplicantDetVO.getPostalCode());
			cakdTAO.associateNationalId(loanApplicantDetVO.getNationalId());
			cakdTAO.associateIdType(loanApplicantDetVO.getNationalIdType());
			cakdTAO.associateGender(loanApplicantDetVO.getGender());
			cakdTAO.associateDateOfBirth(loanApplicantDetVO.getDateOfBirth());
			cakdTAO.associatePlaceOfBirth(loanApplicantDetVO.getPlaceOfBirth());
			cakdTAO.associateReligion(loanApplicantDetVO.getReligion());
			cakdTAO.associateLegacyCif(loanApplicantDetVO.getLegacyCifNo());
			cakdTAO.associateFinacleCif(new FEBAUnboundString(""));
			cakdTAO.associateLoanAccountId(new FEBAUnboundString(""));
			cakdTAO.associateBankCode(loanApplicantDetVO.getBankCode());
			
			
			ckadInfo = CAKDTAO.select(ebContext, ebContext.getBankId(), loanApplicantDetVO.getApplicationId());			
			cakdTAO.associateCookie(ckadInfo.getCookie());

			cakdTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {

			e.printStackTrace();
			LogManager.logError(null, e);
			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}

	}

}
