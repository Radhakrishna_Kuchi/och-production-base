package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FreeTextSmall;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.PanOrNationalID;

public class CustomLoanApplicationServiceCreateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplnMasterDetailsVO loanAppDetVO = (CustomLoanApplnMasterDetailsVO) objInputOutput;

		// insertion into CLAT using TAO call
		updateCUSR(txnContext,loanAppDetVO.getKtpId());
		insertCLAT(txnContext, loanAppDetVO);

	}

	private void insertCLAT(FEBATransactionContext objContext, CustomLoanApplnMasterDetailsVO loanAppMasterDetVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		// Declare a TAO object
		CLATTAO clatTAO = new CLATTAO(objContext);
		final ApplicationNumber applicationID = new ApplicationNumber(
				SequenceGeneratorUtil.getNextSequenceNumber("NXT_APPLICATION_ID_SEQ", objContext));
		loanAppMasterDetVO.setApplicationId(applicationID);
		try {
			clatTAO.associateBankId(ebContext.getBankId());
			clatTAO.associateUserId(ebContext.getUserId());
			clatTAO.associateApplicationId(applicationID);
			clatTAO.associateRequestedLoanAmt(loanAppMasterDetVO.getRequestedLoanAmt());
			clatTAO.associateRequestedTenor(loanAppMasterDetVO.getRequestedTenor());
			clatTAO.associateLoanIntRate(loanAppMasterDetVO.getLoanIntRate());
			clatTAO.associateMonhtlyInstallment(loanAppMasterDetVO.getMonthlyInstallment());
			clatTAO.associateLegacyCif(loanAppMasterDetVO.getLegacyCif());
			clatTAO.associatePrivyAgreeDate(loanAppMasterDetVO.getPrivyAgreeDate());
			clatTAO.associateProductTncAgreeDate(loanAppMasterDetVO.getProductTnCAgreeDate());
			clatTAO.associateApplicationStatus(loanAppMasterDetVO.getApplicationStatus());
			clatTAO.associateProductType(loanAppMasterDetVO.getProductType());
			clatTAO.associateProductCode(loanAppMasterDetVO.getProductCode());
			clatTAO.associateProductCategory(loanAppMasterDetVO.getProductCategory());
			clatTAO.associateProductSubcategory(loanAppMasterDetVO.getProductSubCategory());
			clatTAO.associateIsKtpVerified(new FEBAUnboundChar("N"));
			// Added for BRI AGRO
			clatTAO.associateBankCode(loanAppMasterDetVO.getBankCode());
			// Added for Missing Simpanan case.
			clatTAO.associateCrRespCode(new FEBAUnboundInt(""));
			// added for simplify loan process
			clatTAO.associateKtpRejCnt(new FEBAUnboundInt(0));
			clatTAO.associateIsKtpReuploaded(new FEBAUnboundChar("N"));


			clatTAO.insert(objContext);
			loanAppMasterDetVO.getRCreTime().setValue(new java.util.Date(System.currentTimeMillis()));

		} catch (FEBATableOperatorException e) {
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in CLAT", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}
	private void updateCUSR (FEBATransactionContext ebContext,FEBAUnboundString ktp) throws BusinessException {

		
		try {
			
			CUSRInfo cusrInfo = null;
			CUSRTAO cusrTao = new CUSRTAO(ebContext);
			CorporateId corpId = new CorporateId(ebContext.getUserId().toString());
			UserId userId = ebContext.getUserId();
			BankId bankId = ebContext.getBankId();
			cusrInfo = CUSRTAO.select(ebContext, bankId, corpId, userId);
			System.out.println("KTP: "+cusrInfo.getPanNationalId());
			System.out.println(FEBATypesUtility.isNotNullOrBlank(cusrInfo.getPanNationalId()));
			if (!FEBATypesUtility.isNotNullOrBlank(cusrInfo.getPanNationalId())) {
				if (!FEBATypesUtility.isNotNullOrBlank(ktp)) {
					throw new BusinessException(true, ebContext, CustomEBankingIncidenceCodes.KTP_CARD_NUMBER_EMPTY,
							"KTP Number is mandatory for First Apply.", null, CustomEBankingErrorCodes.KTP_CARD_NUMBER_EMPTY, null);
				}
			}
			cusrTao.associateBankId(bankId);
			cusrTao.associateUserId(userId);
			cusrTao.associateOrgId(corpId);
			if (FEBATypesUtility.isNotNullOrBlank(ktp)) {
				cusrTao.associatePanNationalId(new PanOrNationalID(ktp.getValue()));
			}
			cusrTao.associateCookie(cusrInfo.getCookie());
			cusrTao.update(ebContext);
			FEBATableOperator.commit(ebContext);
			System.out.println("Update in CUSR successfull for KTP Number");
		}   catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e, "ERROR");
		} 
	}

}
