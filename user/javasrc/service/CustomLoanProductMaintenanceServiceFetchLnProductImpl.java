package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanProductMaintenanceServiceFetchLnProductImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		CustomLoanProductCriteriaVO customLoanProductEnqVO = (CustomLoanProductCriteriaVO) objQueryCrit;
		queryOperator.associate("configType",customLoanProductEnqVO.getConfigType());
		queryOperator.associate("bankId", context.getBankId());
		queryOperator.associate("selectedAmount", customLoanProductEnqVO.getSelectedAmount());
		queryOperator.associate("productCode", customLoanProductEnqVO.getProductCode());
		queryOperator.associate("prodCategory", customLoanProductEnqVO.getProdCategory());
	}

	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanProductEnquiryVO eqVO = (CustomLoanProductEnquiryVO) objInputOutput;
		try {
			super.executeQuery(objContext, eqVO, objTxnWM);
		} catch (BusinessException be) {

			throw new BusinessException(objContext,
                    EBIncidenceCodes.NO_RECORDS_FOUND,
                    "No Records Found",
                    EBankingErrorCodes.RECORDS_NOT_FOUND);
		}
	}
	
	@Override
	public void postProcess(
            FEBATransactionContext objContext,
            IFEBAValueObject objInputOutput,
            IFEBAValueObject objTxnWM)
            throws BusinessException,
                BusinessConfirmation,
                CriticalException {
		
    }
	/**
	 * 
	 * Associating DAL CustomLoanProductDetailsListDAL.xml
	 * @see CustomLoanProductDetailsListDAL.xml
	 */
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_PRODUCT_MAINTENANCE;

	}

	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {

			FEBAValItem[] vls=null;


		return vls;
	}}
