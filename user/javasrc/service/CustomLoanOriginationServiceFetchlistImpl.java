package com.infosys.custom.ebanking.user.service;




import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanOriginationCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanOriginationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanOriginationEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.common.validators.DBFutureDateVal;
import com.infosys.ebanking.common.validators.LimitDateRangeVal;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.types.valueobjects.DateRangeVO;


public class CustomLoanOriginationServiceFetchlistImpl extends AbstractLocalListInquiryTran
{
	

	@Override
	protected void associateQueryParameters(FEBATransactionContext txnContext, IFEBAValueObject objQueryCrit,
			IFEBAValueObject objTxnWM, QueryOperator queryOperator) throws CriticalException {
		final EBTransactionContext eBankingTransactionContext = (EBTransactionContext) txnContext;
		final CustomLoanOriginationCriteriaVO criteriaVO = (CustomLoanOriginationCriteriaVO) objQueryCrit;
			
		FEBAUnboundChar isLoadFlow=criteriaVO.getIsLoadFlow();
		CommonCode applicationStatus=criteriaVO.getApplicationStatus();
		String isLoadFlowStr=isLoadFlow.toString();
		if(isLoadFlowStr.equals("Y")){	
			Calendar cal = Calendar.getInstance();
			Calendar cal1 = Calendar.getInstance();
			Date toDate=cal.getTime();
	        cal1.add(Calendar.DATE, -7);
	        Date fromDate=cal1.getTime();
	        FEBADate fd=new FEBADate(fromDate);
	        FEBADate td=new FEBADate(toDate);	        
	        criteriaVO.setApplicationStatus(CustomEBConstants.DEF_APP_STAUS_LOAN_ORG);
	        criteriaVO.setFromDate(fd);
	        criteriaVO.setToDate(td);
	        
		}

		// associates the query operator with search criteria fields
		queryOperator.associate("BANK_ID", criteriaVO.getBankId());
		queryOperator.associate("FROM_DATE", criteriaVO.getFromDate());		
		queryOperator.associate("TO_DATE", criteriaVO.getToDate());
		if(!(applicationStatus.getValue().equals("ALL"))){			
			queryOperator.associate("APPLICATION_STATUS", criteriaVO.getApplicationStatus());
		}
		queryOperator.associate("USER_HP_NUMBER", criteriaVO.getUserHPNumber());
	
	}

	/**
	 *  This
	 * method prepares the validations list and pass to val engine for the
	 * validations of the same <BR>
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @return
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#prepareValidationsList(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2011 - 6:56:24 PM
	 */
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		
		CustomLoanOriginationCriteriaVO critVO = (CustomLoanOriginationCriteriaVO) objInputOutput;
		FEBAUnboundChar isLoadFlow = critVO.getIsLoadFlow();
		ArrayList<BusinessExceptionVO> pExceptionListList = new ArrayList<>();
		EBTransactionContext context = (EBTransactionContext) objContext;
		BusinessException completeErrors = new BusinessException(context,pExceptionListList);
		String isLoadFlowStr=isLoadFlow.toString();
		if(isLoadFlowStr.equals("N"))
		{
			FEBADate fromDate=critVO.getFromDate();
			FEBADate toDate=critVO.getToDate();
			String fromDateStr=fromDate.toString();
			String toDateStr=toDate.toString();
			if(((fromDateStr==null || fromDateStr.isEmpty()) && (toDateStr!=null && !toDateStr.isEmpty()))||
					((toDateStr==null || toDateStr.isEmpty()) && (fromDateStr!=null && !fromDateStr.isEmpty())))			
			{		
				completeErrors.add(new BusinessException(true, objContext,
						CustomEBankingIncidenceCodes.BOTH_DATES_REQ, null,
    					"FormManagementFG.MOBILE", CustomEBankingErrorCodes.BOTH_DATES_REQ, null,
    					null));
						
				if (completeErrors.count() > 0) 
				{
					throw new BusinessException(context, completeErrors.getExceptionList());
				}
				
			}
			else if(fromDateStr!=null && toDateStr!=null){				
				Date td=toDate.getValue(); 	
				Date fd=fromDate.getValue();
			
	        	long days=td.getTime()-fd.getTime();
	        	long days1=TimeUnit.MILLISECONDS.toDays(days);
				if(days1>30){
					completeErrors.add(new BusinessException(true, objContext,
							CustomEBankingIncidenceCodes.MAX_DATE_RANGE, null,
	    					"FormManagementFG.MOBILE", CustomEBankingErrorCodes.MAX_DATE_RANGE, null,
	    					null));
				}
				if (completeErrors.count() > 0) 
				{
					throw new BusinessException(context, completeErrors.getExceptionList());
				}
				
				DateRangeVO rangeVO = (DateRangeVO) FEBAAVOFactory
						.createInstance("com.infosys.fentbase.types.valueobjects.DateRangeVO");
				DateRangeVO rangeVO1 = (DateRangeVO) FEBAAVOFactory
						.createInstance("com.infosys.fentbase.types.valueobjects.DateRangeVO");
				// setting date to rangeVO
				rangeVO.setFromDate(critVO.getFromDate());
				rangeVO.setToDate(critVO.getToDate());
				rangeVO1.setFromDate(critVO.getToDate());
				rangeVO1.setToDate(critVO.getToDate());
				// here val item is prepared. The validations include
				// * 1. from date validation
				// * 2. todate validation
				// * 3. range validation
				// *
				return  new FEBAValItem[] {
						// mandatory validations						
						// Check for valid date Range
						new FEBAValItem("fromDate", rangeVO, new LimitDateRangeVal(), FEBAValEngineConstants.DEPENDENT,
								EBankingErrorCodes.VALID_DATE_RANGE),

						// Check if fromDate greater than current date
						new FEBAValItem("fromDate", critVO.getFromDate(), new DBFutureDateVal(),
								FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.DEPENDENT,
								EBankingErrorCodes.ERR_FROMDATE_GREATER_THAN_CURRENT_DATE),

						// Check for toDate greater than current date
						new FEBAValItem("toDate", critVO.getToDate(), new DBFutureDateVal(),
								FEBAValEngineConstants.NON_MANDATORY, FEBAValEngineConstants.DEPENDENT,
								EBankingErrorCodes.ERR_TODATE_GREATER_THAN_CURRENT_DATE), };
				
			}
			
		}
		// instantiating DateRangeVO
		return new FEBAValItem[0];
		
	}

	/**
	 *
	 * sets the query identifier <BR>
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @return
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#getQueryIdentifier(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2014 - 6:56:30 PM
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {
		
		return CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_ORIGINATION_LIST;

	}

	/**
	 * 
	 * this calls the execute query method.
	 * 
	 * @param objContext
	 * @param objInputOutput
	 * @param objTxnWM
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @see com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran#executeQuery(com.infosys.feba.framework.commontran.context.FEBATransactionContext,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject,
	 *      com.infosys.feba.framework.types.valueobjects.IFEBAValueObject)
	 * @since Jan 5, 2014 - 6:56:33 PM
	 */
	@Override
	protected void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		
		
	
		try {
			// calls execute query()
			super.executeQuery(objContext, objInputOutput, objTxnWM);
			CustomLoanOriginationEnquiryVO enquiryVO = (CustomLoanOriginationEnquiryVO)objInputOutput;
            //call process method to process the result list
            processResultList(objContext,enquiryVO);
		
		}

		catch (BusinessException be) {
			// throws business exception
			// no record are fetched
				throw new BusinessException(objContext,
	                    EBIncidenceCodes.NO_RECORDS_FOUND,
	                    "No Records Found",
	                    EBankingErrorCodes.RECORDS_NOT_FOUND);
		
		}

	}

	 private void processResultList(
	            FEBATransactionContext objContext,
	            CustomLoanOriginationEnquiryVO enquiryVO) throws BusinessException{
	         FEBAArrayList<CustomLoanOriginationDetailsVO> detailsList=(FEBAArrayList<CustomLoanOriginationDetailsVO>)enquiryVO.getResultList();
	         for (int i = 0; i < detailsList.size(); i++) {
	        	 FEBADate modDate=detailsList.get(i).getLastActionDate();	        	
	        	 Calendar cal=Calendar.getInstance();
	        	 Date todayDate=cal.getTime();
	        	 Date modDate1=modDate.getValue();	        	
	        	 long days=todayDate.getTime()-modDate1.getTime();
	        	 long days1=TimeUnit.MILLISECONDS.toDays(days);
	        	 FEBAUnboundInt daysInCurrentStatus=new FEBAUnboundInt((int) days1);
	        	detailsList.get(i).setDaysInCurrentStatus(daysInCurrentStatus);
				
			} 
	         FEBAArrayList<CommonCode> posAppStatus=new FEBAArrayList<>();
	         posAppStatus.add(new CommonCode("KTP_SAVED"));
	         posAppStatus.add(new CommonCode("PER_SAVED"));
	         // Added for BRI Agro PAYROLL start
	         posAppStatus.add(new CommonCode("PAY_SAVED"));
	         posAppStatus.add(new CommonCode("PAYROLL_REJ"));
	         // Added for BRI Agro PAYROLL end
	         posAppStatus.add(new CommonCode("CON_SAVED"));
	         posAppStatus.add(new CommonCode("EMP_SAVED"));
	         posAppStatus.add(new CommonCode("CR_SCORE_SUB"));
	         posAppStatus.add(new CommonCode("CR_SCORE_APR"));
	         posAppStatus.add(new CommonCode("CR_SCORE_REJ"));
	         posAppStatus.add(new CommonCode("USR_REJECT"));
	         posAppStatus.add(new CommonCode("ACT_LINK_CONF"));
	         posAppStatus.add(new CommonCode("DISB_ACC_CONF"));
	         posAppStatus.add(new CommonCode("EKYC_COM"));
	         posAppStatus.add(new CommonCode("DIG_SIGN_COM"));
	         posAppStatus.add(new CommonCode("APP_EXPIRED"));
	         posAppStatus.add(new CommonCode("DOCUMENT_SIGNED"));
	         posAppStatus.add(new CommonCode("LOAN_CREATED"));
	         FEBAArrayList<CustomLoanOriginationDetailsVO> detailsList1=new FEBAArrayList<>();
			for (int i = 0; i < detailsList.size(); i++) {
	        	 for (int j = 0; j < posAppStatus.size(); j++) {
	        		if(detailsList.get(i).getApplicationStatus().equals(posAppStatus.get(j))){
	        			detailsList1.add(detailsList.get(i));        			
	        		}
				}				
			} 
		 enquiryVO.setResultList(detailsList1);
		 if(detailsList1.size()<=0){
			 throw new BusinessException(objContext,
	                    EBIncidenceCodes.NO_RECORDS_FOUND,
	                    "No Records Found",
	                    EBankingErrorCodes.RECORDS_NOT_FOUND);
		 }
	    }
	
}
