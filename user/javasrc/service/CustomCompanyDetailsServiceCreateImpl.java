package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CMPDTAO;
import com.infosys.custom.ebanking.tao.info.CMPDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomCompanyDetailsVO;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.types.primitives.ARSequenceId;

public class CustomCompanyDetailsServiceCreateImpl extends AbstractLocalUpdateTran{
    
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomCompanyDetailsVO detailsVO = (CustomCompanyDetailsVO) objInputOutput;
		
		if(null==detailsVO.getCompanyName()||detailsVO.getCompanyName().getValue().length()==0)
		{

			throw new BusinessException(
					true,
					objContext,
					CustomEBankingIncidenceCodes.COMPANY_NAME_MANDATORY,
					"Company Name is mandatory",
					null,
					CustomEBankingErrorCodes.ENTER_COMPANY_NAME,
					null);
		} 
		if(null==detailsVO.getCompanyAddress()||detailsVO.getCompanyAddress().getValue().length()==0)
		{

			throw new BusinessException(
					true,
					objContext,
					CustomEBankingIncidenceCodes.COMPANY_ADDRESS_MANDATORY,
					"Company Address is mandatory",
					null,
					CustomEBankingErrorCodes.ENTER_COMPANY_ADDR,
					null);
		} 
				
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomCompanyDetailsVO detailsVO = (CustomCompanyDetailsVO) objInputOutput;
		try {
		CMPDTAO cmpdTAOobj = new CMPDTAO(pObjContext);
		CMPDInfo cmpdInfo= new CMPDInfo();
		String nxtCMPDSeqNumber="NEXT_CMPD_SEQ_NUM";
		
		ARSequenceId  seqNum= new ARSequenceId (SequenceGeneratorUtil.getNextSequenceNumber(nxtCMPDSeqNumber, pObjContext));
		cmpdTAOobj.associateCompanyName(detailsVO.getCompanyName());
		cmpdTAOobj.associateRecordId(seqNum);
		cmpdTAOobj.associateCompanyAddress(detailsVO.getCompanyAddress());
		cmpdTAOobj.associateUserId(pObjContext.getUserId());
		cmpdTAOobj.associateBankId(pObjContext.getBankId());
		cmpdTAOobj.associateCookie(cmpdInfo.getCookie());
		cmpdTAOobj.insert(pObjContext);
		addSuccessMessageToContext(pObjContext);
		
		}
		catch (FEBATableOperatorException e) {
			LogManager.logError(pObjContext, e);
		}
	}
	
	private void addSuccessMessageToContext(FEBATransactionContext pObjContext) {
		BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		infoDetails.setDispMessage("Company Details added successfully");
		infoDetails.setLogMessage("Log:Company Details added successfully");
		pObjContext.addBusinessInfo(infoDetails);
		
	}

}

