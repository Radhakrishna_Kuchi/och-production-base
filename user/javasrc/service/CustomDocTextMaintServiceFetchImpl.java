
package com.infosys.custom.ebanking.user.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextMaintVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextResultVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextTableRowVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDocTextTableVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBAHashMap;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.IFEBATypeComparator;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.lists.FEBAListIterator;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CommonCodeType;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBAAString;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.core.cache.impl.StateCodesFilter;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.valueobjects.CommonCodeCacheVO;
import com.infosys.fentbase.types.valueobjects.StateCodeCacheVO;
import com.infosys.fentbase.types.valueobjects.StateCodeVO;

/**
 * This service is used for fetching document text
 * 
 */

public class CustomDocTextMaintServiceFetchImpl extends AbstractLocalUpdateTran {

	private static final String TYP_JOINTABLE = "JOINTABLE";
	private static final String TYP_PARTTABLE = "PARTTABLE";// PARTTABLE
	private static final String TYP_TABLE = "TABLE";
	private static final String NULL_STR = "NULL";
	private static final String PADDING = "@#@#@";
	private static final FEBAUnboundInt INT_ZERO = new FEBAUnboundInt(0);
	private static final SimpleDateFormat DT_FMT_YYYY = new SimpleDateFormat("YYYY");
	private static final SimpleDateFormat DT_FMT_MM = new SimpleDateFormat("MM");
	private static final SimpleDateFormat DT_FMT_DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");
	private static final String[] DYN_PARMS = new String[] { "APPROVAL_DATE", "AUTO_INSTALLMENT_PAYMENT", "BANK_NAME",
			"COMPANY_ADDRESS", "COMPANY_PHONE_NUMBER", "CUSTOMER_ADDRESS", "CUSTOMER_HAND_PH_NUMBER",
			"CUSTOMER_HOME_PH_NUMBER", "CUSTOMER_HOUSE_OWNERSHIP", "CUSTOMER_KECAMATAN", "CUSTOMER_KELURAHAN",
			"CUSTOMER_KOTA", "CUSTOMER_KTP", "CUSTOMER_LAST_EDUCATION", "CUSTOMER_NAME", "CUSTOMER_POSTAL_CODE",
			"CUSTOMER_RT_RW", "DATE_MM", "DATE_YYYY", "CUSTOMER_NO_OF_DEPENDENTS", "EMERGENCY_CONTACT_ADDRESS",
			"EMERGENCY_CONTACT_CITY", "CUSTOMER_MARITAL_STATUS", "CUSTOMER_MOTHERS_MAIDEN_NAME",
			"EMERGENCY_CONTACT_NAME", "EMERGENCY_CONTACT_PHONE_NUMBER", "EMERGENCY_CONTACT_POSTAL_CODE",
			"EMERGENCY_CONTACT_RELATION", "EMERGENCY_CONTACT_RT_RW", "INDUSTRY_TYPE", "LOAN_ACCOUNT_NUM",
			"LOAN_EMI_AMOUNT", "LOAN_EMI_PENALTY", "LOAN_INTEREST_RATE", "LOAN_SANCTION_AMOUNT",
			"LOAN_SCHEDULE_END_DATE", "LOAN_SCHEDULE_START_DATE", "LOAN_TENURE", "COMPANY_NAME", "MONTHLY_DUE_DATE",
			"MONTHLY_EXPENSE", "MONTHLY_INCOME", "PAYROLL_ACCOUNT_NUMBER", "SEQUNCE_NUMBER", "SOURCE_OF_INCOME",
			"CITY_OF_BIRTH", "DATE_OF_BIRTH", "CUSTOMER_GENDER", "CUSTOMER_EMAIL_ID", "CUSTOMER_ADDRESS_LINE_1" };
	private static final FEBAUnboundString BLANK = new FEBAUnboundString("");
	private static final FEBAUnboundString AUTO_INSTALLMENT_PAYMENT = new FEBAUnboundString(
			"Automatic Installment Payment");

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomDocTextMaintVO docTextVO = (CustomDocTextMaintVO) inOutVO;
		return new FEBAValItem[] { getMandatoryVal("criteria.documentName", docTextVO.getCriteria().getDocumentName()),
				getMandatoryVal("criteria.applicationId", docTextVO.getCriteria().getApplicationId()) };
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject inOutVO, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomDocTextMaintVO docTextVO = (CustomDocTextMaintVO) inOutVO;
		FEBAHashMap docMap = getDocTextMap(context, docTextVO.getCriteria().getDocumentName().getValue());
		FEBAHashMap argMap = getUserArgumentMap(context, docTextVO.getCriteria().getApplicationId());
		FEBAArrayList<CustomDocTextResultVO> docTextList = fetchDocumentText(context, docMap.getKeys());
		FEBAArrayList<CustomDocTextResultVO> results = new FEBAArrayList<CustomDocTextResultVO>();
		FEBAListIterator<FEBAArrayList<CustomDocTextResultVO>, CustomDocTextResultVO> itr = docTextList.iterator();
		while (itr.hasNext()) {
			CustomDocTextResultVO txtDesc = itr.next();
			CustomDocTextResultVO resultVO = (CustomDocTextResultVO) docMap.get(txtDesc.getTextName().getValue());

			if (TYP_TABLE.equals(resultVO.getTextType().getValue())) {
				FEBAUnboundString text = applyDynParams(txtDesc.getText(), argMap,
						resultVO.getDynamicAttributes().getValue());
				resultVO.setTable(getCustomDocTextTableVO(text.getValue()));
			} else if (TYP_JOINTABLE.equals(resultVO.getTextType().getValue())) {
				Iterator<String> strItr = getStrItr(resultVO.getDynamicAttributes().getValue());
				StringBuilder dynAttributes = new StringBuilder("");
				while (strItr.hasNext()) {
					CustomDocTextResultVO partVO = (CustomDocTextResultVO) docMap.get(strItr.next());
					if (partVO != null && TYP_PARTTABLE.equals(partVO.getTextType().getValue())) {
						if (dynAttributes.length() > 1
								&& dynAttributes.lastIndexOf(";") != (dynAttributes.length() - 1)) {
							dynAttributes.append(";");
						}
						dynAttributes.append(partVO.getDynamicAttributes());
					}
				}
				FEBAUnboundString text = applyDynParams(txtDesc.getText(), argMap, dynAttributes.toString());
				resultVO.setTextType(TYP_TABLE);
				resultVO.setTable(getCustomDocTextTableVO(text.getValue()));
			} else if (TYP_PARTTABLE.equals(resultVO.getTextType().getValue())) {
				// skip
			} else {
				FEBAUnboundString text = applyDynParams(txtDesc.getText(), argMap,
						resultVO.getDynamicAttributes().getValue());
				resultVO.setText(text);
			}
			resultVO.setTextIndentation(getIndentationLevel(docMap, resultVO.getParentTextName()));

			results.addObject(resultVO);
		}
		results.sort(new IFEBATypeComparator() {
			@Override
			public int compare(IFEBAType arg0, IFEBAType arg1) {
				return doCompare((CustomDocTextResultVO) arg0, (CustomDocTextResultVO) arg1);
			}

			public int doCompare(CustomDocTextResultVO arg0, CustomDocTextResultVO arg1) {
				return arg0.getRowNumber().compareTo(arg1.getRowNumber());
			}
		});
		docTextVO.getResults().setList(results);
		// docTextVO.setResults(results);
	}

	private FEBAUnboundInt getIndentationLevel(FEBAHashMap docMap, FEBAUnboundString parentTextName) {
		if (FEBATypesUtility.isNotNullOrBlank(parentTextName) && !NULL_STR.equals(parentTextName.getValue())) {
			int indent = 0;
			String parentCode = parentTextName.getValue();
			while (!NULL_STR.equals(parentCode) && docMap.containsKey(parentCode)) {
				parentCode = ((CustomDocTextResultVO) docMap.get(parentCode)).getParentTextName().getValue();
				indent++;
			}
			return new FEBAUnboundInt(indent);
		}
		return INT_ZERO;
	}

	private CustomDocTextTableVO getCustomDocTextTableVO(String text) {
		CustomDocTextTableVO table = (CustomDocTextTableVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomDocTextTableVO);
		Iterator<String> itr = Arrays.asList(text.split("\\|")).iterator();
		while (itr.hasNext()) {
			CustomDocTextTableRowVO row = (CustomDocTextTableRowVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomDocTextTableRowVO);
			String[] vals = itr.next().split(":");
			if (vals.length > 1) {
				row.setValue(vals[1] != null ? vals[1] : "");
			}
			row.setLabel(vals[0] != null ? vals[0] : "");
			table.getRows().addObject(row);
		}
		return table;
	}

	private FEBAUnboundString applyDynParams(FEBAUnboundString text, FEBAHashMap argMap, String params) {
		if ("STATIC".equals(params)) {
			return text;
		}
		String buffer = text.getValue();
		Iterator<String> itr = getStrItr(params);
		while (itr.hasNext()) {
			String param = itr.next();
			buffer = buffer.replace(PADDING + param + PADDING, String.valueOf(argMap.get(param)));
		}
		return new FEBAUnboundString(buffer);
	}

	private Iterator<String> getStrItr(String params) {
		return Arrays.asList(params.split(";")).iterator();
	}

	private FEBAHashMap getUserArgumentMap(FEBATransactionContext ebContext, ApplicationNumber applicationId) {
		FEBAHashMap map = new FEBAHashMap();
		initializeArgMap(map);

		map.put("APPROVAL_DATE", getCurrentDate(ebContext, DT_FMT_DD_MM_YYYY));
		map.put("AUTO_INSTALLMENT_PAYMENT", AUTO_INSTALLMENT_PAYMENT);
		map.put("DATE_MM", getCurrentDate(ebContext, DT_FMT_MM));
		map.put("DATE_YYYY", getCurrentDate(ebContext, DT_FMT_YYYY));
		populateFromCLAD(ebContext, applicationId, map);
		populateFromCLCD(ebContext, applicationId, map);
		poopulateFromCLAT(ebContext, applicationId, map);
		populateFromCLED(ebContext, applicationId, map);
		poopulateFromCLPA(ebContext, applicationId, map);
		return map;
	}

	private void populateFromCLED(FEBATransactionContext ebContext, ApplicationNumber applicationId, FEBAHashMap map) {
		try {
			FEBAUnboundString formattedAmount = new FEBAUnboundString();

			CLEDInfo cledInfo = CLEDTAO.select(ebContext, ebContext.getBankId(), applicationId);
			CLPAInfo clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(), applicationId);
			
			map.put("INDUSTRY_TYPE", cledInfo.getWorkType());
			map.put("COMPANY_ADDRESS", BLANK);
			map.put("COMPANY_PHONE_NUMBER", BLANK);
			map.put("MONTHLY_EXPENSE",
					new FEBAUnboundString(new BigDecimal(clpaInfo.getExpenditureAmt().getAmountValue())
							.setScale(0, RoundingMode.HALF_UP).toString()));
			map.put("MONTHLY_INCOME", new FEBAUnboundString(new BigDecimal(clpaInfo.getNetIncomeWl().getAmountValue())
					.setScale(0, RoundingMode.HALF_UP).toString()));

			map.put("SEQUNCE_NUMBER", cledInfo.getApplicationId());
			map.put("SOURCE_OF_INCOME", cledInfo.getFinancialSource());
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		}
	}

	private void poopulateFromCLAT(FEBATransactionContext ebContext, ApplicationNumber applicationId, FEBAHashMap map) {
		try {
			CLATInfo clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);

			FEBAUnboundString formattedAmount = new FEBAUnboundString();

			map.put("LOAN_ACCOUNT_NUM", BLANK);
			// map.put("MONTHLY_DUE_DATE", BLANK); //Taking this from CLPA
			map.put("LOAN_EMI_AMOUNT",
					new FEBAUnboundString(new BigDecimal(clatInfo.getMonhtlyInstallment().getAmountValue())
							.setScale(0, RoundingMode.HALF_UP).toString()));
			map.put("LOAN_EMI_PENALTY", BLANK);
			map.put("LOAN_INTEREST_RATE", clatInfo.getLoanIntRate());
			map.put("LOAN_SANCTION_AMOUNT",
					new FEBAUnboundString(new BigDecimal(clatInfo.getApprovedAmount().getAmountValue())
							.setScale(0, RoundingMode.HALF_UP).toString()));
			map.put("LOAN_SCHEDULE_END_DATE", BLANK);
			map.put("LOAN_SCHEDULE_START_DATE", BLANK);
			map.put("LOAN_TENURE", clatInfo.getRequestedTenor());
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	private void poopulateFromCLPA(FEBATransactionContext ebContext, ApplicationNumber applicationId, FEBAHashMap map) {
		try {
			final EBTransactionContext ebTranContext = (EBTransactionContext) ebContext;
			CLPAInfo clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(), applicationId);
			FEBADate date = clpaInfo.getAccPayrollDate();
			FEBAUnboundInt payrollDay = new FEBAUnboundInt(date.getValue().getDate());
			FEBAUnboundString payrollAccountNumber = new FEBAUnboundString(StringUtils.leftPad(clpaInfo.getPayrollAccountNum().getValue(), 15, '0'));
			map.put("PAYROLL_ACCOUNT_NUMBER", payrollAccountNumber);
			map.put("MONTHLY_DUE_DATE", payrollDay); // Take this from CLPA
			map.put("CITY_OF_BIRTH", clpaInfo.getPlaceOfBirthWl());
			map.put("COMPANY_NAME", clpaInfo.getCompanyName());
			map.put("CUSTOMER_NAME", clpaInfo.getNameWl());

			if (null != clpaInfo.getBirthDateWl() && clpaInfo.getBirthDateWl().toString().length() >= 11) {
				map.put("DATE_OF_BIRTH", new FEBAUnboundString(clpaInfo.getBirthDateWl().toString().substring(0, 11)));
			}
			FEBAUnboundString gender = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(clpaInfo.getGenderWl())) {
				gender = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "GEN", new CommonCode(clpaInfo.getGenderWl().toString())));
			} else {
				gender = new FEBAUnboundString(clpaInfo.getGenderWl().getValue());
			}
			map.put("CUSTOMER_GENDER", gender);

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		} catch (CriticalException e) {
			e.printStackTrace();
		}
	}

	private void populateFromCLCD(FEBATransactionContext ebContext, ApplicationNumber applicationId, FEBAHashMap map) {
		try {
			final EBTransactionContext ebTranContext = (EBTransactionContext) ebContext;

			CLCDInfo clcdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(), applicationId);
			map.put("CUSTOMER_NO_OF_DEPENDENTS", clcdInfo.getNoOfDependents());
			map.put("EMERGENCY_CONTACT_ADDRESS", clcdInfo.getEmrConAddress());
			map.put("EMERGENCY_CONTACT_CITY", BLANK);
			FEBAUnboundString maritalStatus = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(clcdInfo.getMaritalStatus())) {
				maritalStatus = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "MST", clcdInfo.getMaritalStatus()));
			} else {
				maritalStatus = new FEBAUnboundString(clcdInfo.getMaritalStatus().getValue());
			}

			map.put("CUSTOMER_MARITAL_STATUS", maritalStatus);
			map.put("CUSTOMER_MOTHERS_MAIDEN_NAME", clcdInfo.getMotherName());
			map.put("EMERGENCY_CONTACT_NAME", clcdInfo.getEmergencyContact());
			//Modified for bug fix
			map.put("EMERGENCY_CONTACT_PHONE_NUMBER", new FEBAUnboundString(clcdInfo.getEmrConPhoneNum().toString()));
			//map.put("EMERGENCY_CONTACT_PHONE_NUMBER", new FEBAUnboundString("0" + clcdInfo.getEmrConPhoneNum()));
			map.put("EMERGENCY_CONTACT_POSTAL_CODE", clcdInfo.getEmrConPostalCd());

			FEBAUnboundString emrConRelation = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(clcdInfo.getMaritalStatus())) {
				emrConRelation = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "RLT", clcdInfo.getEmrConRelation()));
			} else {
				emrConRelation = new FEBAUnboundString(clcdInfo.getEmrConRelation().getValue());
			}

			map.put("EMERGENCY_CONTACT_RELATION", emrConRelation);
			map.put("EMERGENCY_CONTACT_RT_RW", BLANK);

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		} catch (CriticalException e) {
			e.printStackTrace();
		}
	}

	private void populateFromCLAD(FEBATransactionContext ebContext, ApplicationNumber applicationId, FEBAHashMap map) {
		try {
			CLADInfo cladInfo = CLADTAO.select(ebContext, ebContext.getBankId(), applicationId);
			CLATInfo clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(), applicationId);
			final EBTransactionContext ebTranContext = (EBTransactionContext) ebContext;

			FEBAUnboundString cardIssuerBank = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getCardIssuerBank())) {
				cardIssuerBank = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "CCBK", cladInfo.getCardIssuerBank()));
			} else {
				cardIssuerBank = new FEBAUnboundString(cladInfo.getCardIssuerBank().getValue());
			}

			FEBAUnboundString homeOwnershipStatus = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getHomeOwnershipStatus())) {
				homeOwnershipStatus = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "RES", cladInfo.getHomeOwnershipStatus()));
			} else {
				homeOwnershipStatus = new FEBAUnboundString(cladInfo.getHomeOwnershipStatus().getValue());
			}

			FEBAUnboundString state = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getState())) {
				state = new FEBAUnboundString(getStateCodeDescription(ebTranContext, cladInfo.getState().getValue()));
			} else {
				state = new FEBAUnboundString(cladInfo.getCity().getValue());
			}

			FEBAUnboundString city = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getCity())) {
				city = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, cladInfo.getState().getValue(), cladInfo.getCity()));
			} else {
				city = new FEBAUnboundString(cladInfo.getCity().getValue());
			}

			FEBAUnboundString lastEducation = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getLastEducation())) {
				lastEducation = new FEBAUnboundString(
						getCommonCodeDesc(ebTranContext, "EDU", cladInfo.getLastEducation()));
			} else {
				lastEducation = new FEBAUnboundString(cladInfo.getLastEducation().getValue());
			}

			FEBAUnboundString addLine4 = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getAddrLine4())) {
				addLine4 = new FEBAUnboundString(getCommonCodeDesc(ebTranContext, cladInfo.getCity().getValue(),
						new CommonCode(cladInfo.getAddrLine4().getValue())));
			} else {
				addLine4 = new FEBAUnboundString(cladInfo.getAddrLine4().getValue());
			}

			FEBAUnboundString addLine5 = new FEBAUnboundString();
			if (FEBATypesUtility.isNotNullOrBlank(cladInfo.getAddrLine5())) {
				addLine5 = new FEBAUnboundString(getCommonCodeDesc(ebTranContext, cladInfo.getAddrLine4().getValue(),
						new CommonCode(cladInfo.getAddrLine5().getValue())));
			} else {
				addLine5 = new FEBAUnboundString(cladInfo.getAddrLine5().getValue());
			}
			
			// Added for BRI AGRO payroll
			if(null != clatInfo){
				
				String bankCode = clatInfo.getBankCode().getValue();
				String appendBankCode= "BANKNAME_" + bankCode;
				
				String bankName = PropertyUtil.getProperty(appendBankCode, ebContext);
				map.put("BANK_NAME", new FEBAUnboundString(bankName));
			}
			
			map.put("CUSTOMER_ADDRESS",
					new FEBAUnboundString(cladInfo.getAddrLine1() + " " + cladInfo.getAddrLine2() + " "
							+ cladInfo.getAddrLine3() + " " + addLine4 + " " + addLine5 + " " + state + " " + city + " "
							+ cladInfo.getPostalCode()));
			map.put("CUSTOMER_HAND_PH_NUMBER", new FEBAUnboundString("0" + cladInfo.getMobileNo()));
			map.put("CUSTOMER_HOME_PH_NUMBER", new FEBAUnboundString("0" + cladInfo.getHomePhoneNum()));
			map.put("CUSTOMER_HOUSE_OWNERSHIP", homeOwnershipStatus);
			map.put("CUSTOMER_KECAMATAN", addLine4);
			map.put("CUSTOMER_KELURAHAN", addLine5);
			map.put("CUSTOMER_KOTA", city);
			map.put("CUSTOMER_KTP", cladInfo.getNationalId());
			map.put("CUSTOMER_LAST_EDUCATION", lastEducation);
			map.put("CUSTOMER_POSTAL_CODE", cladInfo.getPostalCode());
			map.put("CUSTOMER_RT_RW", new FEBAUnboundString(cladInfo.getAddrLine2() + "/" + cladInfo.getAddrLine3()));
			map.put("CUSTOMER_ADDRESS_LINE_1", cladInfo.getAddrLine1());
			map.put("CUSTOMER_EMAIL_ID", cladInfo.getEmailId());

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		} catch (FEBATypeSystemException e) {
			e.printStackTrace();
		} catch (CriticalException e) {
			e.printStackTrace();
		}
	}

	private void initializeArgMap(FEBAHashMap map) {
		Iterator<String> itr = Arrays.asList(DYN_PARMS).iterator();
		while (itr.hasNext()) {
			map.put(itr.next(), BLANK);
		}
	}

	private FEBAUnboundString getCurrentDate(FEBATransactionContext ctx, DateFormat dtFormat) {
		return new FEBAUnboundString(dtFormat.format(DateUtil.getCurrentDate(ctx)));
	}

	private FEBAHashMap getDocTextMap(FEBATransactionContext objTxnContext, String docName) throws BusinessException {
		CommonCodeCacheVO cocdCacheVO = (CommonCodeCacheVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.CommonCodeCacheVO);
		CommonCodeVO commonCodeVO = (CommonCodeVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CommonCodeVO);
		FEBAHashList<CommonCodeVO> cocdList = null;
		EBTransactionContext ebcontext = (EBTransactionContext) objTxnContext;
		commonCodeVO.setCodeType(new CommonCodeType(docName));// CustomEBankingConstants.QUICK_PAY_CODE_TYPE
		commonCodeVO.setBankID(ebcontext.getBankId());
		commonCodeVO.setLanguageID(ebcontext.getLangId());
		FEBAHashMap results = new FEBAHashMap();
		if (commonCodeVO.getCodeType() != null) {
			cocdCacheVO.setCriteria(commonCodeVO);
			try {
				cocdList = (FEBAHashList<CommonCodeVO>) AppDataManager.getList(objTxnContext,
						AppDataConstants.COMMONCODE_CACHE, cocdCacheVO);
				FEBAListIterator<FEBAHashList<CommonCodeVO>, CommonCodeVO> itr = cocdList.iterator();
				while (itr.hasNext()) {
					CommonCodeVO cmCode = itr.next();
					if (hasDefinition(cmCode)) {
						results.put(cmCode.getCommonCode().getValue(), getCustomDocTextResultVO(cmCode));
					}
				}
			} catch (CriticalException e) {
				e.printStackTrace();
			}
			if (cocdList.size() == 0) {
				throw new BusinessException(true, ebcontext, CustomEBankingIncidenceCodes.DOC_TEXT_DEFN_NOT_FOUND,
						"Document text definition is not found", null, CustomEBankingErrorCodes.DOC_TEXT_DEFN_NOT_FOUND,
						null);
			}
		}

		return results;
	}

	private FEBAArrayList<CustomDocTextResultVO> fetchDocumentText(FEBATransactionContext context,
			Set<String> txtNameVal) {
		QueryOperator queryOperator = QueryOperator.openHandle(context,
				CustomEBQueryIdentifiers.CUSTOM_DOCUMENT_TEXT_MAINTENANCE);
		queryOperator.associate("bankId", context.getBankId());
		queryOperator.associate("langId", context.getLangId());
		queryOperator.associate("textType", new FEBAUnboundString("TXM"));
		FEBAArrayList<FEBAUnboundString> txtNames = getTextNames(txtNameVal);
		queryOperator.associate("txtNames", txtNames);
		FEBAArrayList<CustomDocTextResultVO> list = new FEBAArrayList<CustomDocTextResultVO>();
		try {
			list = queryOperator.fetchList(context);
		} catch (DALException e) {
			e.printStackTrace();
		}
		queryOperator.closeHandle(context);
		return list;
	}

	private FEBAArrayList<FEBAUnboundString> getTextNames(Set<String> txtNameVal) {
		FEBAArrayList<FEBAUnboundString> txtNames = new FEBAArrayList<FEBAUnboundString>();
		Iterator<String> itr = txtNameVal.iterator();
		while (itr.hasNext()) {
			txtNames.addObject(new FEBAUnboundString(itr.next()));
		}
		return txtNames;
	}

	private FEBAValItem getMandatoryVal(String voField, FEBAAString fieldValue) {
		return new FEBAValItem(voField, fieldValue, FEBAValEngineConstants.MANDATORY,
				FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.MANDATORY_FIELDS_CANNOT_BE_BLANK);
	}

	private FEBAValItem getMandatoryVal(String voField, ApplicationNumber applicationId) {
		return new FEBAValItem(voField, applicationId, FEBAValEngineConstants.MANDATORY,
				FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.MANDATORY_FIELDS_CANNOT_BE_BLANK);
	}

	private boolean hasDefinition(CommonCodeVO cmCode) {
		if (FEBATypesUtility.isNotNullOrBlank(cmCode.getCodeDescription())) {
			String textDefn = cmCode.getCodeDescription().getValue();
			return textDefn.split("\\|").length == 5;
		}
		return false;
	}

	private CustomDocTextResultVO getCustomDocTextResultVO(CommonCodeVO cmCode) {
		String textDefn = cmCode.getCodeDescription().getValue();
		String[] defn = textDefn.split("\\|");
		CustomDocTextResultVO resultVO = (CustomDocTextResultVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomDocTextResultVO);
		resultVO.setTextName(cmCode.getCommonCode().getValue());
		resultVO.setRowNumber(defn[0]);
		resultVO.setTextType(defn[1]);
		resultVO.setSrlNumber(defn[2]);
		resultVO.setParentTextName(defn[3]);
		resultVO.setDynamicAttributes(defn[4]);
		return resultVO;
	}

	private String getCommonCodeDesc(EBTransactionContext context, String codeType, CommonCode code)
			throws CriticalException {
		DescriptionMedium codeDescription = null;
		if (null != code && code.toString().trim().length() != 0) {
			codeDescription = (DescriptionMedium) AppDataManager.getValue(context, FBAAppDataConstants.COMMONCODE_CACHE,
					FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + codeType
							+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
							+ code);
		}

		if (!(FEBATypesUtility.isNotNullOrBlank(codeDescription))) {
			if (FEBATypesUtility.isNotNullOrBlank(code)) {
				return code.toString();
			}
			return "";
		}
		return codeDescription.toString();
	}

	private String getStateCodeDescription(EBTransactionContext context, String stateCode) {
		FEBAArrayList filteredStcdList = new FEBAArrayList();
		StateCodesFilter stcdFilter = new StateCodesFilter();
		StateCodeCacheVO stateCodeCacheVO = (StateCodeCacheVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.StateCodeCacheVO);
		stateCodeCacheVO.getCriteria().setStateCode(stateCode);
		stateCodeCacheVO.getCriteria().setCountry("ID");
		stateCodeCacheVO.getCriteria().setLanguageID(context.getLangId());

		try {
			filteredStcdList = stcdFilter.getFilteredStcdList(context, stateCodeCacheVO);
		} catch (CriticalException e) {
			LogManager.logError(null, e);
		}

		if (filteredStcdList.get(0) != null) {
			return ((StateCodeVO) filteredStcdList.get(0)).getStateDescription().getValue();
		}
		return stateCode;
	}

}
