package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.tao.CLPMConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;

public class CustomLoanProductMaintenanceServiceDeleteImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		FBATransactionContext objContext = (FBATransactionContext) pObjContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLPM(pObjContext, customLoanProductDetailsVO,
					FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE);
			criteria.setDependencyFlag(FBAConstants.YES);
			addSuccessMessageToContext(pObjContext);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_DELETE + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.USER_DELETE_TAO_EXCEPTION, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_FETCH_DELETE,
					tblOperExp.getMessage(), tblOperExp.getErrorCode());
		}

	}

	/**
	 * Method populateWorkflowVO() Description Populates workflow value object
	 * from context and InputOutputVO Input objContext, workflowVO,
	 * objInputOutput, objTxnWM
	 *
	 * @param objContext
	 * @param populateWorkflowVOUtility
	 * @param objInputOutput
	 * @param objTxnWM
	 */
	@Override
	protected void populateWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility populateWorkflowVOUtility, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		EBTransactionContext ebcontext = (EBTransactionContext) objContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		// Setting Key Fields
		populateWorkflowVOUtility.setKeyField(CLPMConstants.BANK_ID, ebcontext.getBankId().toString().toUpperCase());
		populateWorkflowVOUtility.setKeyField(CLPMConstants.LN_REC_ID,
				customLoanProductDetailsVO.getLnRecId().toString().toUpperCase());

		populateWorkflowVOUtility.setEntityType("CLPM");
		populateWorkflowVOUtility.setModuleID(EBankingConstants.BANK_USER_ADMIN);
		populateWorkflowVOUtility.setCorpId(ebcontext.getCorpId().getValue());
		populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
		populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_DELETE).toString());
		populateWorkflowVOUtility.setTransactionType("LPM");
		populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
	}

	/**
	 *
	 * Adding Success Message to the Context to be displayed on the screen
	 *
	 * @param pObjContext
	 * @author Gautam_Kasukhela
	 * @since Feb 23, 2011 - 1:34:21 PM
	 */
	private void addSuccessMessageToContext(FEBATransactionContext pObjContext) {

		FBATransactionContext ebContext = (FBATransactionContext) pObjContext;
		// Getting Transaction Key generated for the transaction
		final TransactionKey txnKey = ebContext.getApprovalTxnKey();
		BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		// Checking if WorkFlow is enabled
		if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(FBAConstants.NO)) {
			infoDetails.setCode(new FEBAUnboundInt(FBAErrorCodes.RECORD_DELETE_SUCCESSFUL));
		} else {
			infoDetails.setCode(new FEBAUnboundInt(FBAErrorCodes.RECORD_SENT_FOR_APPROVAL));
			infoDetails.setParam(new AdditionalParam(FBAConstants.TRAN_KEY,
					new FEBAUnboundString(String.valueOf(txnKey.getValue()))));
		}
		infoDetails.setDispMessage("Loan Details Deleted Successfully.");
		infoDetails.setLogMessage("Loan Details Deleted Successfully.");
		pObjContext.addBusinessInfo(infoDetails);
	}

}
