
package com.infosys.custom.ebanking.user.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CAPWDTAO;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.CLEDTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CAPWDInfo;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;
import com.infosys.custom.ebanking.tao.info.CLEDInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoreCalculateVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;

/**
 * This service is used for credit score calculation..
 * 
 */

public class CustomCreditScoreServiceCalculateImpl extends AbstractHostUpdateTran {
	private static String privilegeRequest="CustomCreditScorePrivilegeRequest";

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[0];
	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", ebContext);
		CustomCreditScoreCalculateVO customCreditScoreCalculateVO = (CustomCreditScoreCalculateVO) objInputOutput;
		validateMandatory(objContext, customCreditScoreCalculateVO);
		ApplicationNumber applicationId = customCreditScoreCalculateVO.getApplicationId();
		String applicationIdStr = applicationId.toString();

		if (applicationIdStr != "" && !(applicationIdStr.isEmpty())) {
			try {

				if (pinangOrSahabat != null && pinangOrSahabat != "") {
					if (pinangOrSahabat.equalsIgnoreCase("PINANG")) {
						populatePinangCreditScoreCalculateVO(ebContext, customCreditScoreCalculateVO);
					}
					if (pinangOrSahabat.equalsIgnoreCase("SAHABAT")) {
						populateSahabatCreditScoreCalculateVO(ebContext, customCreditScoreCalculateVO);
					}
				}

			} catch (FEBATableOperatorException e) {
				throw new FatalException(ebContext, "Unable to select the record for credit score calculation",
						CustomEBankingIncidenceCodes.UNABLE_TO_SELECT_RECORD_FOR_CREDIT_SCORE_CALCULATION, e);
			}
		}

		try {
			if (pinangOrSahabat != null && pinangOrSahabat != "") {
				if (pinangOrSahabat.equalsIgnoreCase("PINANG")) {
					if (customCreditScoreCalculateVO.getAcctType().getValue().equalsIgnoreCase("PP")) {
						EBHostInvoker.processRequest(objContext,
								privilegeRequest, customCreditScoreCalculateVO);
					}else{
					EBHostInvoker.processRequest(objContext,
							CustomEBRequestConstants.CUSTOM_PINANG_CREDIT_SCORE_REQUEST, customCreditScoreCalculateVO);
					}
					// Changes for BRI AGRO Payroll rejection scenario - start
					String csStatus = "";
					if(FEBATypesUtility.isNotNullOrBlank(customCreditScoreCalculateVO.getStatus()))
						csStatus = customCreditScoreCalculateVO.getStatus().getValue();
					
						System.out.println("csStatus::"+csStatus);
					
					if(null != customCreditScoreCalculateVO) {
						
						if(csStatus.equals("success")){
							
							if(FEBATypesUtility.isNotNullOrBlank(customCreditScoreCalculateVO.getRespCode()) &&
									customCreditScoreCalculateVO.getRespCode().getValue().startsWith("20")){
									customCreditScoreCalculateVO.setApplicationStatus(new CommonCode("PAYROLL_REJ"));
							} else if(FEBATypesUtility.isNotNullOrBlank(customCreditScoreCalculateVO.getRespCode()) &&
									customCreditScoreCalculateVO.getRespCode().getValue().startsWith("00")){
								customCreditScoreCalculateVO.setApplicationStatus(new CommonCode("CR_SCORE_SUB"));
							}
						
						} else if(csStatus.equals("failed")){
								customCreditScoreCalculateVO.setApplicationStatus(new CommonCode("CR_SCORE_SUB"));									
					   }else{
						   		customCreditScoreCalculateVO.setApplicationStatus(new CommonCode("CR_SCORE_SUB")); 
						}
					}
					// Changes for BRI AGRO Payroll rejection scenario - End
					
				}
				if (pinangOrSahabat.equalsIgnoreCase("SAHABAT")) {
					EBHostInvoker.processRequest(objContext,
							CustomEBRequestConstants.CUSTOM_SAHABAT_CREDIT_SCORE_REQUEST, customCreditScoreCalculateVO);
				}
			}

			if (!FEBATypesUtility.isNotNullOrBlank(customCreditScoreCalculateVO.getUserId())) {
				throw getBusinessException(objContext, null, "Credit score calculation failed.");
			}

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}

	private void validateMandatory(FEBATransactionContext ebContext,
			CustomCreditScoreCalculateVO customCreditScoreCalculateVO) throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		if (!FEBATypesUtility.isNotBlankLong(customCreditScoreCalculateVO.getApplicationId())) {
			valError.add(getBusinessExceptionVO(ebContext, "Application Id is mandatory"));
		}
		if (!FEBATypesUtility.isNotNullOrBlank(customCreditScoreCalculateVO.getUserId())) {
			valError.add(getBusinessExceptionVO(ebContext, "User Id is mandatory"));
		}

		if (!valError.isEmpty()) {
			throw new BusinessException(ebContext, valError);
		}
	}

	private void populatePinangCreditScoreCalculateVO(EBTransactionContext ebContext,
			CustomCreditScoreCalculateVO customCreditScoreCalculateVO)
			throws FEBATableOperatorException, FEBATypeSystemException, CriticalException, BusinessException {

		
		DateFormat dateFormatPayrollDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");    
		
		ApplicationNumber applicationId = customCreditScoreCalculateVO.getApplicationId();
		String appId = applicationId.toString();
		UserId userId = customCreditScoreCalculateVO.getUserId();
		CLADInfo cladInfo = CLADTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLCDInfo clcdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLEDInfo cledInfo = CLEDTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLATInfo clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLPAInfo clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CAPWDInfo capwdInfo=CAPWDTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		
		if (!(clatInfo.getApplicationStatus().getValue().equalsIgnoreCase("CON_SAVED")
				|| clatInfo.getApplicationStatus().getValue().equalsIgnoreCase("CR_SCORE_SUB"))) {
			throw getBusinessException(ebContext, null, "Credit score calculation failed.");
		}

		FEBAUnboundInt experiance = null;
		/*
		 * Changes for Whitelist fields to be sent to DCS
		 */
		if (FEBATypesUtility.isNotBlankDate(clpaInfo.getEmpStartDateWl())
				&& FEBATypesUtility.isNotBlankDate(clpaInfo.getEmpEndDateWl())) {
			Date empStartDate = clpaInfo.getEmpStartDateWl().getValue();
			Date empEndDate = clpaInfo.getEmpEndDateWl().getValue();
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(empStartDate);
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(empEndDate);
			int expYears = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
			experiance = new FEBAUnboundInt(expYears);
			customCreditScoreCalculateVO.setEmploymentExperienceInYears(experiance);
		}
		FEBAUnboundInt finalAge = null;
		if (FEBATypesUtility.isNotBlankDate(clpaInfo.getBirthDateWl())) {
			int age = 0;
			Calendar now = Calendar.getInstance();
			Calendar dob = Calendar.getInstance();
			Date dateOfBirth = clpaInfo.getBirthDateWl().getValue();
			dob.setTime(dateOfBirth);
			if (now.after(dob)) {
				int year1 = now.get(Calendar.YEAR);
				int year2 = dob.get(Calendar.YEAR);
				age = year1 - year2;
				int month1 = now.get(Calendar.MONTH);
				int month2 = dob.get(Calendar.MONTH);
				if (month2 > month1) {
					age--;
				} else if (month1 == month2) {
					int day1 = now.get(Calendar.DAY_OF_MONTH);
					int day2 = dob.get(Calendar.DAY_OF_MONTH);
					if (day2 > day1) {
						age--;
					}
				}
			}
			finalAge = new FEBAUnboundInt(age);
			customCreditScoreCalculateVO.setBirthDate(clpaInfo.getBirthDateWl());
			customCreditScoreCalculateVO.setAge(finalAge);
		}

		String codeType = "LNTP";
		CommonCode code = new CommonCode("Pinang");
		String loanType = getCommonCodeDesc(ebContext, codeType, code);

		String longTimeDebtorStatus = PropertyUtil.getProperty("LONG_TIME_DEBTOR_STATUS", ebContext);
		String sideBusiness = PropertyUtil.getProperty("SIDE_BUSINESS", ebContext);
		String empNo = PropertyUtil.getProperty("PERSONAL_NUMBER", ebContext);

		String callBackUrl = PropertyUtil.getProperty("CREDT_SCORE_ROOT_URL", ebContext);
		callBackUrl = callBackUrl + "/cs_callback_pinang/" + appId + "/creditscoreupdate";

		customCreditScoreCalculateVO.setApplicationId(applicationId);
		customCreditScoreCalculateVO.setUserId(userId);
		customCreditScoreCalculateVO.setKTP(cladInfo.getNationalId());
		customCreditScoreCalculateVO.setFullName(clpaInfo.getNameWl());
		customCreditScoreCalculateVO.setFullNameKTP(clpaInfo.getNameWl());
		
		// Added for BRI AGRO Payroll - start
		String payrollAccountNum = StringUtils.leftPad(clpaInfo.getPayrollAccountNum().getValue(), 15, '0');
		customCreditScoreCalculateVO.setPayrollAccountId(payrollAccountNum);
		

		customCreditScoreCalculateVO.setEmployeeNumber(empNo);
		customCreditScoreCalculateVO.setBranchCode(clpaInfo.getPayrollAccBranch());
		customCreditScoreCalculateVO.setLoanType(loanType);
		customCreditScoreCalculateVO.setCallbackUrl(callBackUrl);
		customCreditScoreCalculateVO.setLoanTenure(clatInfo.getRequestedTenor());
		customCreditScoreCalculateVO.setLoanAmountRequested(clatInfo.getRequestedLoanAmt());
		customCreditScoreCalculateVO.setGender(new CommonCode(clpaInfo.getGenderWl().getValue()));
		customCreditScoreCalculateVO.setLastEducation(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_EDU_CODE_HOST_MAPPING, cladInfo.getLastEducation()));
		customCreditScoreCalculateVO.setMaritalStatus(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_MARITAL_STATUS_HOST_MAPPING, clcdInfo.getMaritalStatus()));
		customCreditScoreCalculateVO.setNumberOfDependents(clcdInfo.getNoOfDependents());
		customCreditScoreCalculateVO.setTypeOfHouseOwnership(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_HM_OWN_STATUS_HOST_MAPPING, cladInfo.getHomeOwnershipStatus()));
		customCreditScoreCalculateVO.setLongTimeDebtorStatus(longTimeDebtorStatus);

		customCreditScoreCalculateVO.setTotalSavings(clpaInfo.getSavingAmount());

		customCreditScoreCalculateVO.setNetIncome(clpaInfo.getNetIncomeWl());
		customCreditScoreCalculateVO.setTotalOfInstallmentPossessed(clatInfo.getMonhtlyInstallment());
		// Added for BRI AGRO payroll - start 
		customCreditScoreCalculateVO.setBankCode(clatInfo.getBankCode());
		customCreditScoreCalculateVO.setCompanyName(clpaInfo.getCompanyName());
		
		String payrollDate = clpaInfo.getAccPayrollDate().toString();
		// Strip zeros before payroll start date
		customCreditScoreCalculateVO.setPayrollDate(StringUtils.stripStart(payrollDate.substring(0,2),"0"));
		 
		customCreditScoreCalculateVO.getAccountPayrollDate().set(dateFormatPayrollDate.format(clpaInfo.getAccPayrollDate().getValue()));
		customCreditScoreCalculateVO.setStartDate(clpaInfo.getEmpStartDateWl());
		customCreditScoreCalculateVO.setEndDate(clpaInfo.getEmpEndDateWl());
		customCreditScoreCalculateVO.setEmployeeStatus(clpaInfo.getEmployeeStatus());
		// Added for BRI AGRO payroll - end 
		customCreditScoreCalculateVO.setIsSideBusiness(sideBusiness);
		
//		Added for Payroll reject Screen Start
		customCreditScoreCalculateVO.setNetIncomeWL(capwdInfo.getIncome());
		customCreditScoreCalculateVO.setRemainingWorkingPeriod(capwdInfo.getRemainingWorkingPeriod());
		customCreditScoreCalculateVO.setWorkingPeriod(capwdInfo.getWorkingPeriod());
		customCreditScoreCalculateVO.setMedianCredit(capwdInfo.getMedianCredit());
		customCreditScoreCalculateVO.setMedianBalance(capwdInfo.getMedianBalance());
	
//		Added for Payroll reject Screen End
		
//		Added for Payroll privilege Start
		customCreditScoreCalculateVO.setIsPrivilege(capwdInfo.getIsprivilege());
		customCreditScoreCalculateVO.setPreapprovedPlafond(capwdInfo.getPreapprovedplafond());
		customCreditScoreCalculateVO.setPreapprovedTenor(capwdInfo.getPreapprovedtenor());
		customCreditScoreCalculateVO.setAcctType(capwdInfo.getAccttype());
//		Added for Payroll privilege End
		
	}

	private void populateSahabatCreditScoreCalculateVO(EBTransactionContext ebContext,
			CustomCreditScoreCalculateVO customCreditScoreCalculateVO)
			throws FEBATableOperatorException, FEBATypeSystemException, CriticalException, BusinessException {

		ApplicationNumber applicationId = customCreditScoreCalculateVO.getApplicationId();
		String appId = applicationId.toString();
		UserId userId = customCreditScoreCalculateVO.getUserId();
		CLADInfo cladInfo = CLADTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLCDInfo clcdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLEDInfo cledInfo = CLEDTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLPAInfo clpaInfo = CLPATAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		CLATInfo clatInfo = CLATTAO.select(ebContext, ebContext.getBankId(),
				customCreditScoreCalculateVO.getApplicationId());
		FEBAUnboundInt experiance = null;

		if (!(clatInfo.getApplicationStatus().getValue().equalsIgnoreCase("EMP_SAVED")
				|| clatInfo.getApplicationStatus().getValue().equalsIgnoreCase("CR_SCORE_SUB"))) {
			throw getBusinessException(ebContext, null, "Credit score calculation failed.");
		}

		if (FEBATypesUtility.isNotBlankDate(clpaInfo.getEmpStartDateWl())
				&& FEBATypesUtility.isNotBlankDate(clpaInfo.getEmpStartDateWl())) {
			Date empStartDate = clpaInfo.getEmpStartDateWl().getValue();
			Date empEndDate = clpaInfo.getEmpStartDateWl().getValue();
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(empStartDate);
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(empEndDate);
			int expYears = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
			experiance = new FEBAUnboundInt(expYears);
			customCreditScoreCalculateVO.setEmploymentExperienceInYears(experiance);
		}
		FEBAUnboundInt finalAge = null;
		if (FEBATypesUtility.isNotBlankDate(clpaInfo.getBirthDateWl())) {
			int age = 0;
			Calendar now = Calendar.getInstance();
			Calendar dob = Calendar.getInstance();
			Date dateOfBirth = clpaInfo.getBirthDateWl().getValue();
			dob.setTime(dateOfBirth);
			if (now.after(dob)) {
				int year1 = now.get(Calendar.YEAR);
				int year2 = dob.get(Calendar.YEAR);
				age = year1 - year2;
				int month1 = now.get(Calendar.MONTH);
				int month2 = dob.get(Calendar.MONTH);
				if (month2 > month1) {
					age--;
				} else if (month1 == month2) {
					int day1 = now.get(Calendar.DAY_OF_MONTH);
					int day2 = dob.get(Calendar.DAY_OF_MONTH);
					if (day2 > day1) {
						age--;
					}
				}
			}
			finalAge = new FEBAUnboundInt(age);
			customCreditScoreCalculateVO.setBirthDate(clpaInfo.getBirthDateWl());
			customCreditScoreCalculateVO.setAge(finalAge);
		}

		DescriptionMedium codeDescription = null;
		String codeType = "LNTP";
		CommonCode code = new CommonCode("Sahabat");
		String loanType = getCommonCodeDesc(ebContext, codeType, code);

		String empNo = PropertyUtil.getProperty("PERSONAL_NUMBER", ebContext);

		String callBackUrl = PropertyUtil.getProperty("CREDT_SCORE_ROOT_URL", ebContext);
		callBackUrl = callBackUrl + "/cs_callback_ceria/" + appId + "/creditscoreupdate";

		customCreditScoreCalculateVO.setApplicationId(applicationId);
		customCreditScoreCalculateVO.setUserId(userId);
		customCreditScoreCalculateVO.setKTP(cladInfo.getNationalId());
		customCreditScoreCalculateVO.setFullName(clpaInfo.getNameWl());
		customCreditScoreCalculateVO.setBirthDate(clpaInfo.getBirthDateWl());
		customCreditScoreCalculateVO.setEmployeeNumber(empNo);
		customCreditScoreCalculateVO.setBranchCode(clpaInfo.getPayrollAccBranch());
		customCreditScoreCalculateVO.setLoanType(loanType);
		customCreditScoreCalculateVO.setCallbackUrl(callBackUrl);
		customCreditScoreCalculateVO.setLastEducation(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_EDU_CODE_HOST_MAPPING, cladInfo.getLastEducation()));
		customCreditScoreCalculateVO.setTypeOfHouseOwnership(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_HM_OWN_STATUS_HOST_MAPPING, cladInfo.getHomeOwnershipStatus()));
		customCreditScoreCalculateVO.setMaritalStatus(getCommonCodeDesc(ebContext,
				CustomEBConstants.CD_TYPE_MARITAL_STATUS_HOST_MAPPING, clcdInfo.getMaritalStatus()));
		customCreditScoreCalculateVO.setGender(new CommonCode(clpaInfo.getGenderWl().getValue()));
		customCreditScoreCalculateVO.setHouseOwnershipDuration(cladInfo.getHomeOwnershipDuration());
		customCreditScoreCalculateVO.setNetIncome(clpaInfo.getNetIncomeWl());
		customCreditScoreCalculateVO.setNumberOfDependents(clcdInfo.getNoOfDependents());
		customCreditScoreCalculateVO.setEmploymentExperienceInYears(experiance);
		customCreditScoreCalculateVO.setAge(finalAge);
		customCreditScoreCalculateVO.setCreFre3(clpaInfo.getCreditFrequency3());
		customCreditScoreCalculateVO.setCreFre6(clpaInfo.getCreditFrequency6());
		customCreditScoreCalculateVO.setCreFre12(clpaInfo.getCreditFrequency12());
		customCreditScoreCalculateVO.setDebFre3(clpaInfo.getDebitFrequency3());
		customCreditScoreCalculateVO.setDebFre6(clpaInfo.getDebitFrequency6());
		customCreditScoreCalculateVO.setDebFre12(clpaInfo.getDebitFrequency12());
		customCreditScoreCalculateVO.setCreTot3(clpaInfo.getCreditTotal3());
		customCreditScoreCalculateVO.setCreTot6(clpaInfo.getCreditTotal6());
		customCreditScoreCalculateVO.setCreTot12(clpaInfo.getCreditTotal12());
		customCreditScoreCalculateVO.setDebTot3(clpaInfo.getDebitTotal3());
		customCreditScoreCalculateVO.setDebTot6(clpaInfo.getDebitTotal6());
		customCreditScoreCalculateVO.setDebTot12(clpaInfo.getDebitTotal12());
		customCreditScoreCalculateVO.setCreAvg3(clpaInfo.getCreditAvg3());
		customCreditScoreCalculateVO.setCreAvg6(clpaInfo.getCreditAvg6());
		customCreditScoreCalculateVO.setCreAvg12(clpaInfo.getCreditAvg12());
		customCreditScoreCalculateVO.setDebAvg3(clpaInfo.getDebitAvg3());
		customCreditScoreCalculateVO.setDebAvg6(clpaInfo.getDebitAvg6());
		customCreditScoreCalculateVO.setDebAvg12(clpaInfo.getDebitAvg12());
		customCreditScoreCalculateVO.setMobileBanking(clpaInfo.getMobileBankingStatus());
		customCreditScoreCalculateVO.setNetBanking(clpaInfo.getInetBankingStatus());
		customCreditScoreCalculateVO.setSavingAccountOwnershipDate(clpaInfo.getSavAccountOwnershipDate());
		customCreditScoreCalculateVO.setSavingAccountOwnershipStatus(clpaInfo.getSavAccountOwnershipStatus());
		customCreditScoreCalculateVO.setPrimaryPhone(clpaInfo.getPrimaryPhoneStatus());
		customCreditScoreCalculateVO.setSecondaryPhone(clpaInfo.getSecondaryPhoneStatus());

	}

	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null, new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null, new AdditionalParam("message", msg));
	}

	private String getCommonCodeDesc(EBTransactionContext context, String codeType, CommonCode code)
			throws CriticalException {
		DescriptionMedium codeDescription = null;
		if (null != code && code.toString().trim().length() != 0) {
			codeDescription = (DescriptionMedium) AppDataManager.getValue(context, FBAAppDataConstants.COMMONCODE_CACHE,
					FBAAppDataConstants.COLUMN_CODE_TYPE + FBAConstants.EQUAL_TO + codeType
							+ FBAAppDataConstants.SEPERATOR + FBAAppDataConstants.COLUMN_CM_CODE + FBAConstants.EQUAL_TO
							+ code);
		}

		if (!(FEBATypesUtility.isNotNullOrBlank(codeDescription))) {
			if (FEBATypesUtility.isNotNullOrBlank(code)) {
				return code.toString();
			}
			return "";
		}
		return codeDescription.toString();
	}
}
