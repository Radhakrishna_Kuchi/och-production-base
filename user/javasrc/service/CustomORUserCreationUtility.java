/*

 * ORUserCreationUtility.java

 * @since Sep 14, 2009 - 6:49:28 PM

 *

 * COPYRIGHT NOTICE:

 * Copyright (c) 2007 Infosys Technologies Limited, Electronic City,

 * Hosur Road, Bangalore - 560 100, India.

 * All Rights Reserved.

 * This software is the confidential and proprietary information of

 * Infosys Technologies Ltd. ("Confidential Information"). You shall

 * not disclose such Confidential Information and shall use it only

 * in accordance with the terms of the license agreement you entered

 * into with Infosys.

 */

package com.infosys.custom.ebanking.user.service;

import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.tao.BRCMTAO;
import com.infosys.ebanking.tao.CULNTAO;
import com.infosys.ebanking.user.EBLoginAltFlowConstants;
import com.infosys.feba.framework.authentication.password.PasswordAuthEngine;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.FilePropertyManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.security.CryptManager;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.AuthorizationMode;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.DivisionId;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.HourValue;
import com.infosys.feba.framework.types.primitives.LanguageId;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.NumberOfAttempts;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserType;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.GenericFieldVO;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.types.primitives.AccessScheme;
import com.infosys.fentbase.types.primitives.AccountFormat;
import com.infosys.fentbase.types.primitives.AlertUserCategory;
import com.infosys.fentbase.types.primitives.AmountFormatCode;
import com.infosys.fentbase.types.primitives.AuthenticationMode;
import com.infosys.fentbase.types.primitives.BankCode;
import com.infosys.fentbase.types.primitives.BranchCode;
import com.infosys.fentbase.types.primitives.CalendarType;
import com.infosys.fentbase.types.primitives.CategoryCode;
import com.infosys.fentbase.types.primitives.CorporateUserType;
import com.infosys.fentbase.types.primitives.CorrespondenceType;
import com.infosys.fentbase.types.primitives.CustomerId;
import com.infosys.fentbase.types.primitives.CustomerStatus;
import com.infosys.fentbase.types.primitives.CustomerType;
import com.infosys.fentbase.types.primitives.DateFormat;
import com.infosys.fentbase.types.primitives.DivisionAccessIndicator;
import com.infosys.fentbase.types.primitives.EducationLevel;
import com.infosys.fentbase.types.primitives.IncomeRangeCode;
import com.infosys.fentbase.types.primitives.IndividualUserId;
import com.infosys.fentbase.types.primitives.LimitSchemeCode;
import com.infosys.fentbase.types.primitives.LocalAccessIndicator;
import com.infosys.fentbase.types.primitives.PageScheme;
import com.infosys.fentbase.types.primitives.Password;
import com.infosys.fentbase.types.primitives.RetailAuthModePrecedence;
import com.infosys.fentbase.types.primitives.SegmentName;
import com.infosys.fentbase.types.primitives.TotalNumberOfLogin;
import com.infosys.fentbase.types.primitives.UserEncryptionKey;
import com.infosys.fentbase.types.primitives.UserInterestsCode;
import com.infosys.fentbase.types.primitives.UserTxnTypes;
import com.infosys.fentbase.types.valueobjects.DBSectionVO;
import com.infosys.fentbase.user.LoginAltFlowConstants;

/**
 *CustomORUserCreationUtility is a new class called for insertion in tables
 * 
 *
 * @author Meena_Jain
 *
 * @version 1.0
 *
 * @since FEBA 2.0
 *
 */

public final class CustomORUserCreationUtility {
/*Modifying the default date to 01-01-1900*/
	private static final String DATE_01_01_1900 = "01-01-1900";
	private static final String DD_MM_YYYY = "dd-MM-yyyy";
	private static CustomORUserCreationUtility singleInstance;
	public static final String OR_CUST_ID = "OR_CUST_ID";

	private CustomORUserCreationUtility() {

	}

	public static CustomORUserCreationUtility
	getInstance() {

		if (singleInstance == null) {
			singleInstance = new CustomORUserCreationUtility();
		}
		return singleInstance;
	}

	/**
	 * This method is to insert in to CUSR table
	 * @param individualID
	 * @param objContext
	 *            the transaction context
	 * @param objInputOutput
	 *            the input output object
	 * @param objTxnWM
	 *            the object that is passed between methods of this class. - not
	 *            used.
	 * @throws CriticalException
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 */

	public static void insertCUSRTAO(FEBATransactionContext pObjContext,
			 String struserId, IndividualUserId individualID,DBSectionVO sectionVO, FEBAUnboundString segmentName)
			 {

		EBTransactionContext ebcontext = (EBTransactionContext) pObjContext;
		final CUSRTAO cusrTAO = new CUSRTAO(ebcontext);
		FEBAUnboundString phoneNo = null;
		String accessScheme=null;
		String alertUserCategory=null;
		String admId = null;
		String multiCrnTxnAllowed = null;
		String limitScheme = null;
		String authenticationMode = null;
		String authorizationMode = null;
		String retAuthModePrec = null;
		String rangeLimitScheme = null;
		final FilePropertyManager filePropertyManager = new FilePropertyManager();
	    FEBAHashList hashList=sectionVO.getBasicFields();
		if (hashList != null && hashList.size() > 0)
		{
			phoneNo=((GenericFieldVO) hashList.get("USERPHONENUMBER")).getSValue();
			cusrTAO.associateCMPhoneNo(new MobileNumber(phoneNo.toString()));
		}
		cusrTAO.associateBankId(ebcontext.getBankId());
		cusrTAO.associateOrgId(new CorporateId(struserId));
		cusrTAO.associateUserId(new UserId(struserId));
		String inputString = struserId.toUpperCase() + "." + struserId.toUpperCase();

		String encryptionKey = CryptManager.encrypt(inputString);
		cusrTAO.associateUserEncryptionKey(new UserEncryptionKey(encryptionKey));

		cusrTAO.associateUserType(new UserType(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_USER_TYPE, pObjContext)));
		// Changes made for the Online Registration enhancement 11010 and ticket 692689
		if ((FEBATypesUtility.isNotNullOrBlank(segmentName))){			
			alertUserCategory = filePropertyManager.getProperty((LoginAltFlowConstants.OR_ALERT_USER_CATEGORY+ "_" + segmentName), pObjContext);
			if (alertUserCategory.isEmpty()){
				alertUserCategory = null;				
			}
			cusrTAO.associateAlertUserCategory(new AlertUserCategory(alertUserCategory));
			
			accessScheme = filePropertyManager.getProperty(("OR_ACCESS_SCHEME"+ "_" + segmentName), pObjContext);
			if (accessScheme.isEmpty()){
				accessScheme = null;				
			}
			cusrTAO.associateAccessScheme(new AccessScheme(accessScheme));	

			
			admId = filePropertyManager.getProperty((LoginAltFlowConstants.OR_ADM_ID+ "_" + segmentName), pObjContext);
			if (admId.isEmpty()){
				admId = null;				
			}
			cusrTAO.associateAdmId(new UserId(admId));
			
			multiCrnTxnAllowed = filePropertyManager.getProperty((LoginAltFlowConstants.OR_MULTI_CRN_TXN_ALLOWED+ "_" + segmentName), pObjContext);
			if (multiCrnTxnAllowed.isEmpty()){
				multiCrnTxnAllowed = null;				
			}
			cusrTAO.associateMultiCrnTxnAllowed(new YNFlag(multiCrnTxnAllowed));
			
			limitScheme = filePropertyManager.getProperty((LoginAltFlowConstants.OR_LIMIT_SCHEME+ "_" + segmentName), pObjContext);
			if (limitScheme.isEmpty()){
				limitScheme = null;				
			}
			cusrTAO.associateLimitScheme(new LimitSchemeCode(limitScheme));
			
			authenticationMode = filePropertyManager.getProperty((LoginAltFlowConstants.OR_AUTHENTICATION_MODE+ "_" + segmentName), pObjContext);
			if (authenticationMode.isEmpty()){
				authenticationMode = null;				
			}
			cusrTAO.associateAuthenticationMode(new AuthenticationMode(authenticationMode));
			
			authorizationMode = filePropertyManager.getProperty((LoginAltFlowConstants.OR_AUTHORIZATION_MODE+ "_" + segmentName), pObjContext);
			if (authorizationMode.isEmpty()){
				authorizationMode = null;				
			}
			cusrTAO.associateAuthorizationMode(new AuthorizationMode(authorizationMode));
			
			retAuthModePrec = filePropertyManager.getProperty((EBLoginAltFlowConstants.OR_RET_AUTH_MODE_PREC+ "_" + segmentName), pObjContext);
			if (retAuthModePrec.isEmpty()){
				retAuthModePrec = null;				
			}
			cusrTAO.associateRetAuthModePrec(new RetailAuthModePrecedence(retAuthModePrec));
			
			rangeLimitScheme = filePropertyManager.getProperty((EBLoginAltFlowConstants.OR_RANGE_LIMIT_SCHEME+ "_" + segmentName), pObjContext);
			if (rangeLimitScheme.isEmpty()){
				rangeLimitScheme = null;				
			}
			cusrTAO.associateRangeLimitScheme(new LimitSchemeCode(rangeLimitScheme));				
		}
		else {			
			alertUserCategory = filePropertyManager.getProperty((LoginAltFlowConstants.OR_ALERT_USER_CATEGORY), pObjContext);
			if (alertUserCategory.isEmpty()){
				alertUserCategory = null;				
			}
			cusrTAO.associateAlertUserCategory(new AlertUserCategory(alertUserCategory));
			
			accessScheme = filePropertyManager.getProperty(("OR_ACCESS_SCHEME"), pObjContext);
			if (accessScheme.isEmpty()){
				accessScheme = null;				
			}
			cusrTAO.associateAccessScheme(new AccessScheme(accessScheme));
			
			admId = filePropertyManager.getProperty((LoginAltFlowConstants.OR_ADM_ID), pObjContext);
			if (admId.isEmpty()){
				admId = null;				
			}
			cusrTAO.associateAdmId(new UserId(admId));
			
			multiCrnTxnAllowed = filePropertyManager.getProperty((LoginAltFlowConstants.OR_MULTI_CRN_TXN_ALLOWED), pObjContext);
			if (multiCrnTxnAllowed.isEmpty()){
				multiCrnTxnAllowed = null;				
			}
			cusrTAO.associateMultiCrnTxnAllowed(new YNFlag(multiCrnTxnAllowed));
			
			limitScheme = filePropertyManager.getProperty((LoginAltFlowConstants.OR_LIMIT_SCHEME), pObjContext);
			if (limitScheme.isEmpty()){
				limitScheme = null;				
			}
			cusrTAO.associateLimitScheme(new LimitSchemeCode(limitScheme));
			
			authenticationMode = filePropertyManager.getProperty((LoginAltFlowConstants.OR_AUTHENTICATION_MODE), pObjContext);
			if (authenticationMode.isEmpty()){
				authenticationMode = null;				
			}
			cusrTAO.associateAuthenticationMode(new AuthenticationMode(authenticationMode));
			
			authorizationMode = filePropertyManager.getProperty((LoginAltFlowConstants.OR_AUTHORIZATION_MODE), pObjContext);
			if (authorizationMode.isEmpty()){
				authorizationMode = null;				
			}
			cusrTAO.associateAuthorizationMode(new AuthorizationMode(authorizationMode));
			
			//Added for the Ticket 663934-Start			
			retAuthModePrec = filePropertyManager.getProperty((EBLoginAltFlowConstants.OR_RET_AUTH_MODE_PREC), pObjContext);
			if (retAuthModePrec.isEmpty()){
				retAuthModePrec = null;				
			}
			cusrTAO.associateRetAuthModePrec(new RetailAuthModePrecedence(retAuthModePrec));			
			//Added for the Ticket 663934-End 
			/*fix for the ticket -619999 -start */
			rangeLimitScheme = filePropertyManager.getProperty((EBLoginAltFlowConstants.OR_RANGE_LIMIT_SCHEME), pObjContext);
			if (rangeLimitScheme.isEmpty()){
				rangeLimitScheme = null;				
			}
			cusrTAO.associateRangeLimitScheme(new LimitSchemeCode(rangeLimitScheme));			
			/*fix for the ticket -619999 -end */
		}
		cusrTAO.associateDdtFrom(new HourValue(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_DDT_FROM, pObjContext)));
		cusrTAO.associateDdtTo(new HourValue(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_DDT_TO, pObjContext)));
		
		cusrTAO.associateLangId(new LanguageId(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_LANG_ID, pObjContext)));

		cusrTAO.associateDtFmt(new DateFormat(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_DT_FMT, pObjContext)));
		cusrTAO.associateAmtFmtCd(new AmountFormatCode(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_AMT_FMT_CD, pObjContext)));
		cusrTAO.associateUsrIntCd(new UserInterestsCode(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_USR_INT_CD, pObjContext)));
		cusrTAO.associateIncRngCd(new IncomeRangeCode(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_INC_RNG_CD, pObjContext)));
		cusrTAO.associatePDivId(new DivisionId(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_P_DIV_ID, pObjContext)));
		cusrTAO.associateLocAccInd(new LocalAccessIndicator(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_LOC_ACC_IND, pObjContext)));
		cusrTAO.associateDivAccInd(new DivisionAccessIndicator(
				filePropertyManager.getProperty(
						LoginAltFlowConstants.OR_DIV_ACC_IND, pObjContext)));
		cusrTAO.associateLastTxnDate(new FEBADate(DateUtil.currentDate(ebcontext)));
		cusrTAO.associateSmsNoOfAtmpts(new NumberOfAttempts(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_SMS_NO_OF_ATMPTS,
						pObjContext)));
			cusrTAO.associateLoginDate(new FEBADate(DateUtil.currentDate(ebcontext)));
			cusrTAO.associateLogoffDate(new FEBADate(DateUtil.currentDate(ebcontext)));
		cusrTAO.associateLoginCertMatchReqd(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_LOGIN_CERT_MATCH_REQD,
						pObjContext)));
		cusrTAO.associateTxnCertMatchReqd(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_TXN_CERT_MATCH_REQD,
						pObjContext)));
		
		cusrTAO.associateAcMinBal(new FEBAAmount(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_AC_MIN_BAL, pObjContext)));
		cusrTAO.associateTxnLimit(new FEBAAmount(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_TXN_LIMIT, pObjContext)));
		cusrTAO.associateSmsAlert(new YNFlag(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_SMS_ALERT, pObjContext)));
		cusrTAO.associateExtAlert(new YNFlag(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_EXT_ALERT, pObjContext)));
		cusrTAO.associateCorrespondenceAddress(new CorrespondenceType(
				filePropertyManager.getProperty(
						LoginAltFlowConstants.OR_CORRESPONDENCE_ADDRESS,
						pObjContext)));
		cusrTAO
				.associateTotNumLogin(new TotalNumberOfLogin(
						filePropertyManager.getProperty(
								LoginAltFlowConstants.OR_TOT_NUM_LOGIN,
								pObjContext)));
		cusrTAO.associateAvailLang(new LanguageId(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_AVAIL_LANG, pObjContext)));
		cusrTAO.associateAcOpnDt(new FEBADate(DD_MM_YYYY, DATE_01_01_1900));
		cusrTAO.associateAnniversaryDate(new FEBADate(DD_MM_YYYY, DATE_01_01_1900));
		cusrTAO
				.associateCategoryCode(new CategoryCode(filePropertyManager
						.getProperty(LoginAltFlowConstants.OR_CATEGORY_CODE,
								pObjContext)));
		
		cusrTAO.associateEducationalLevel(new EducationLevel(
				filePropertyManager.getProperty(
						LoginAltFlowConstants.OR_EDUCATIONAL_LEVEL, pObjContext)));

		cusrTAO
				.associatePassportIssueDate(new FEBADate(DD_MM_YYYY,
						DATE_01_01_1900));
		cusrTAO.associatePassportExpiryDate(new FEBADate(DD_MM_YYYY,
				DATE_01_01_1900));
		cusrTAO.associateCustStatus(new CustomerStatus(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_CUST_STATUS, pObjContext)));
		cusrTAO.associateOofFlg(new YNFlag(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_OOF_FLG, pObjContext)));
		
		cusrTAO.associateLoginAllowed(new YNFlag(filePropertyManager
						.getProperty(LoginAltFlowConstants.OR_LOGIN_ALLOWED,
								pObjContext)));
		cusrTAO.associateTransactionAllowed(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_TRANSACTION_ALLOWED,
						pObjContext)));
		cusrTAO.associateAuthUser(new YNFlag(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_AUTH_USER, pObjContext)));
		cusrTAO.associateLastUnsuccessfulLoginTime(new FEBADate(DD_MM_YYYY,
				DATE_01_01_1900));
		
		cusrTAO.associateForceTermsFlag(new YNFlag(filePropertyManager
				.getProperty(LoginAltFlowConstants.OR_FORCE_TERMS_FLAG,
						pObjContext)));
		
		cusrTAO.associateUserIdType(new CorporateUserType(
						filePropertyManager.getProperty(
								LoginAltFlowConstants.OR_CORP_USER_TYPE,
								pObjContext)));
		cusrTAO.associateSegmentName(new SegmentName(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_OFFLINE_SEGMENT_NAME, pObjContext)));
		cusrTAO.associateVirtualUserFlg(new YNFlag(getTrimmedProperty(
				pObjContext, LoginAltFlowConstants.VIRTUAL_USER)));
		cusrTAO.associateConcurAccessFlg(new YNFlag(getTrimmedProperty(
				pObjContext, LoginAltFlowConstants.CONCURRENT_LOGIN)));
		cusrTAO.associateTranAuthScheme(new CommonCode(getTrimmedProperty(
				pObjContext, LoginAltFlowConstants.AUTHORIZATION_SCHEME)));
		//Added for Middle East Gap Project
		cusrTAO.associateCalType(new CalendarType(filePropertyManager.getProperty(
				LoginAltFlowConstants.OR_CAL_TYPE, pObjContext)));
		//Added for unified login
		cusrTAO.associateIndividualId(individualID);
		// Added for ticket id : 612678
		cusrTAO.associatePageScheme(new PageScheme(getTrimmedProperty(
				pObjContext, EBLoginAltFlowConstants.OR_PAGE_SCHEME)));
		cusrTAO.associateUserTxnTypes(new UserTxnTypes(getTrimmedProperty(
				pObjContext, EBLoginAltFlowConstants.OR_USER_TXN_TYPES)));
		//Start  added for Account display format changes
		cusrTAO.associateAccFmt(new AccountFormat(filePropertyManager.getProperty(
				EBLoginAltFlowConstants.OR_ACCOUNT_FORMAT, pObjContext)));
		//End  added for Account display format changes
		try {
			cusrTAO.insert(ebcontext);
		} catch (FEBATableOperatorException e) {
			throw new FatalException(ebcontext,
					EBIncidenceCodes.EXCEPTION_OCCURED_WHILE_INSERT_CUSR, e
							.getMessage(), e);
		}

	}

	private static String changeDateFormat(String dateExp)
	{
			dateExp=dateExp.replaceAll("/", "-");
		return dateExp;
	}
	/**
	 * Utility method which trims properties before returning from
	 * FilePropertyManager
	 *
	 * @param pObjContext
	 * @param virtualUser
	 * @return
	 */
	private static String getTrimmedProperty(
			FEBATransactionContext pObjContext, final String virtualUser) {
		final String property = new FilePropertyManager().getProperty(
				virtualUser, pObjContext);
		return property.trim();
	}

	/**
	 * This method is to insert in to BRCM table
	 *
	 * @param objContext
	 *            the transaction context
	 * @param objInputOutput
	 *            the input output object
	 * @param objTxnWM
	 *            the object that is passed between methods of this class. - not
	 *            used.
	 * @throws CriticalException
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 */
	public static void insertBRCMTAO(FEBATransactionContext pObjContext,String struserId)
			   {

		EBTransactionContext ebcontext = (EBTransactionContext) pObjContext;
		BRCMTAO brcmTAO = new BRCMTAO(ebcontext);
		BankCode bankCode = new BankCode(ebcontext.getBankId().toString());
		brcmTAO.associateBankId(bankCode);
		brcmTAO.associateCustId(new CustomerId(new FilePropertyManager().getProperty(OR_CUST_ID, ebcontext)));	
		brcmTAO.associateBayUserId(new CorporateId(struserId));
		brcmTAO.associateBranchId(new BranchCode(new FilePropertyManager()
				.getProperty(LoginAltFlowConstants.OR_P_BRANCH_ID, pObjContext)));
		brcmTAO.associateCustBankId(bankCode);
		brcmTAO.associateCustomerType(new CustomerType(
				new FilePropertyManager().getProperty(
						LoginAltFlowConstants.OR_USER_TYPE, pObjContext)));
		brcmTAO.associateUserType(new UserType(new FilePropertyManager()
				.getProperty(LoginAltFlowConstants.OR_USER_TYPE, pObjContext)));
		try {
			brcmTAO.insert(ebcontext);
		} catch (FEBATableOperatorException e) {
			throw new FatalException(ebcontext,
					EBIncidenceCodes.EXCEPTION_OCCURED_WHILE_INSERT_BRCM, e
							.getMessage(), e);
		}
	}

	/**
	 *
	 * Inserting an entry into CULN table for customer id <BR>
	 *
	 * @param pObjContext
	 * @param pObjInputOutput
	 * @param userId
	 * @author
	 * @since
	 */
	public static void insertCULNTAO(FEBATransactionContext pObjContext,
			String userId) {

		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;

		CULNTAO culnTao = new CULNTAO(ebContext);
		culnTao.associateBankId(new BankCode(ebContext.getBankId().getValue()
				.trim()));
		culnTao.associateBayUserId(new CorporateId(userId));
		culnTao.associateBranchId(new BranchCode(new FilePropertyManager().getProperty(
				LoginAltFlowConstants.OR_P_BRANCH_ID, ebContext)));
		culnTao.associateCorpUser(new UserId(userId));
		culnTao.associateCustId(new CustomerId(new FilePropertyManager().getProperty(OR_CUST_ID, ebContext)));
		// Added for ticket id : 619994 -start
		culnTao.associateCustomerType(new CustomerType(EBLoginAltFlowConstants.OR_CUSTOMER_TYPE));
		culnTao.associateCustBankId(new BankCode(ebContext.getBankId()
				.getValue().trim()));

		try {
			culnTao.insert(ebContext);
		} catch (FEBATableOperatorException e) {
			throw new FatalException(ebContext,
					EBIncidenceCodes.EXCEPTION_OCCURED_WHILE_INSERT_CULN, e
							.getMessage(), e);
		}

	}

	/**
	 *
	 * Encrypting the password <BR>
	 *
	 * @param tc
	 * @param newTxnPwd
	 * @return
	 * @author
	 * @since
	 */
	public static Password encryptPassword(EBTransactionContext tc,
			Password newTxnPwd) {

		return new Password(new PasswordAuthEngine().

		encryptCredential(tc,

		tc.getUserId().getValue() + "." + tc.getUserId().getValue(),

		newTxnPwd.getValue(),

		null, null));

	}


	/**
	 * Method to Populate Authentication Scheme.
	 *
	 * @author Harris_Jothikumar
	 * @param objContext
	 * @return AuthenticationScheme
	 */
	public static CommonCode populateAuthenticationScheme(
			FEBATransactionContext objContext) {

		return new CommonCode(new FilePropertyManager().getProperty(
				LoginAltFlowConstants.OR_AUTHENTICATION_SCHEME, objContext));

	}
}
