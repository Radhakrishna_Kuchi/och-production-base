package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanPayoffInOutVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.types.primitives.AccountId;

public class CustomLoanPaymentServicePayoffprocessImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext febatransactioncontext, IFEBAValueObject ifebavalueobject, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		CustomLoanPayoffInOutVO inoutvo = (CustomLoanPayoffInOutVO) ifebavalueobject;

		EBHostInvoker.processRequest(febatransactioncontext, "CustomLoanPaymentRequest", inoutvo);
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postProcess(FEBATransactionContext febatransactioncontext, IFEBAValueObject ifebavalueobject,
			IFEBAValueObject ifebavalueobject1) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanPayoffInOutVO inoutvo = (CustomLoanPayoffInOutVO) ifebavalueobject;
		CustomLoanApplnMasterDetailsVO loanAppDetails = null;
		QueryOperator operator = QueryOperator.openHandle(febatransactioncontext,
				CustomEBQueryIdentifiers.CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_LOAN_PAID);
		operator.associate("bankId", inoutvo.getBankId());
		operator.associate("userId", inoutvo.getUserId());
		operator.associate("applicationStatus", new CommonCode(CustomEBConstants.LOAN_CREATED));
		operator.associate("loanAccountId", new AccountId(inoutvo.getLoanAccountID().getValue()));

		FEBAArrayList<CustomLoanApplnMasterDetailsVO> loanAppDetailsResultList;
		try {
			loanAppDetailsResultList = operator.fetchList(febatransactioncontext);

			System.out.println("loanAppDetailsResultList - " + loanAppDetailsResultList);
			if (loanAppDetailsResultList.size() == 1) {
				loanAppDetails = loanAppDetailsResultList.get(0);

			}
			System.out.println("loanAppDetails-  " + loanAppDetails);
			if (null != loanAppDetails) {
				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
				loanApplnDetailsVO.setApplicationId(loanAppDetails.getApplicationId());
				loanApplnDetailsVO.setApplicationStatus(new CommonCode("LOAN_PAID"));
				LocalServiceUtil.invokeService(febatransactioncontext, "CustomLoanApplicationService",
						new FEBAUnboundString("modify"), loanApplnDetailsVO);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
