package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.applicationmaintenance.validators.CustomLoanMinAmtVal;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.tao.CLPMConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;

public class CustomLoanProductMaintenanceServiceUpdateImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem(customLoanProductDetailsVO.getMerchantID(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.MERCHANT_ID_MANDATORY),

				new FEBAValItem(customLoanProductDetailsVO.getProductCode(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.PROD_CODE_MANDATORY),

				new FEBAValItem(customLoanProductDetailsVO.getProductCategory(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.PROD_CAT_MANDATORY),

				new FEBAValItem(customLoanProductDetailsVO.getProductSubCategory(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.PROD_SUBCAT_MANDATORY),

				new FEBAValItem(customLoanProductDetailsVO.getConfigType(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.CONFIG_TYPE_MANDATORY),

				/*
				 * new FEBAValItem(customLoanProductDetailsVO .getLnPurpose(),
				 * FEBAValEngineConstants.MANDATORY,
				 * FEBAValEngineConstants.INDEPENDENT,
				 * CustomEBankingErrorCodes.LN_PURPOSE_MANDATORY),
				 */

				new FEBAValItem("minAmount", customLoanProductDetailsVO.getMinAmount(),
						FEBAValEngineConstants.MANDATORY, FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem(customLoanProductDetailsVO, new CustomLoanMinAmtVal(),
						FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem("maxAmount", customLoanProductDetailsVO.getMaxAmount(),
						FEBAValEngineConstants.MANDATORY, FEBAValEngineConstants.INDEPENDENT) };

	}

	@Override
	public void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		FBATransactionContext objContext = (FBATransactionContext) pObjContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLPM(pObjContext, customLoanProductDetailsVO,
					FEBATMOConstants.TMO_OPERATION_MODIFY);
			criteria.setDependencyFlag(FBAConstants.YES);
			addSuccessMessageToContext(pObjContext);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_MODIFY + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.USER_MODIFY_TMO_EXCEPTION, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_MODIFY, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}

	}

	/**
	 * Method populateWorkflowVO() Description Populates workflow value object
	 * from context and InputOutputVO Input objContext, workflowVO,
	 * objInputOutput, objTxnWM
	 *
	 * @param objContext
	 * @param populateWorkflowVOUtility
	 * @param objInputOutput
	 * @param objTxnWM
	 */
	@Override
	protected void populateWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility populateWorkflowVOUtility, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		EBTransactionContext ebcontext = (EBTransactionContext) objContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		// Setting Key Fields
		populateWorkflowVOUtility.setKeyField(CLPMConstants.BANK_ID, ebcontext.getBankId().toString().toUpperCase());
		populateWorkflowVOUtility.setKeyField(CLPMConstants.LN_REC_ID,
				customLoanProductDetailsVO.getLnRecId().toString().toUpperCase());
		populateWorkflowVOUtility.setEntityType("CLPM");
		populateWorkflowVOUtility.setModuleID(EBankingConstants.BANK_USER_ADMIN);
		populateWorkflowVOUtility.setCorpId(ebcontext.getCorpId().getValue());
		populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
		populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_MODIFY).toString());
		populateWorkflowVOUtility.setTransactionType("LPM");
		populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
	}

	private void addSuccessMessageToContext(FEBATransactionContext pObjContext) {

		FBATransactionContext ebContext = (FBATransactionContext) pObjContext;
		// Getting Transaction Key generated for the transaction
		final TransactionKey txnKey = ebContext.getApprovalTxnKey();
		BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		// Checking if WorkFlow is enabled
		if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(FBAConstants.NO)) {
			infoDetails.setCode(new FEBAUnboundInt(FBAErrorCodes.RECORD_MODIFICATION_SUCCESSFUL));
		} else {
			infoDetails.setCode(new FEBAUnboundInt(FBAErrorCodes.RECORD_SENT_FOR_APPROVAL));
			infoDetails.setParam(new AdditionalParam(FBAConstants.TRAN_KEY,
					new FEBAUnboundString(String.valueOf(txnKey.getValue()))));
		}
		infoDetails.setDispMessage("Loan Details Updated Successfully.");
		infoDetails.setLogMessage("Loan Details Updated Successfully.");
		pObjContext.addBusinessInfo(infoDetails);
	}

}
