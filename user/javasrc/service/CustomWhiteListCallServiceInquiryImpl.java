package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomWhiteListEnquiryVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomWhiteListCallServiceInquiryImpl extends AbstractHostInquiryTran {

	String pinang = "PINANG";
	String sahabat = "SAHABAT";

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomWhiteListEnquiryVO enquiryVO = (CustomWhiteListEnquiryVO) objInputOutput;
		String appProductId = PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, context);

		// Code Added Tushar Garg to remove leading zeros from account number as
		// required by whitelist Inquiry
		String accoutNumber = enquiryVO.getCriteria().getAccountNumber().toString();

		enquiryVO.getCriteria().setAccountNumber(accoutNumber.replaceFirst("^0+(?!$)", ""));

		if (appProductId.equals(pinang)) {
			try {
				EBHostInvoker.processRequest(context, CustomEBRequestConstants.WHITELIST_PINANG_INQUIRY, enquiryVO);
			} catch (BusinessException be) {
				if (be.getErrorCode() == 61007) {
					throw be;
				} else {
					throw new BusinessException(context, CustomEBankingIncidenceCodes.WHITELIST_PINANG_HOST_CALL_ERROR,
							"An unexpected exception occurred during whitelist inquiry",
							CustomEBankingErrorCodes.WHITELIST_PINANG_HOST_CALL_ERROR, be);
				}
			}
		} else if (appProductId.equals(sahabat)) {
			try {
				EBHostInvoker.processRequest(context, CustomEBRequestConstants.WHITELIST_SAHABAT_INQUIRY, enquiryVO);
			} catch (BusinessException be) {
				if (be.getErrorCode() == 61007) {
					throw be;
				} else {
					throw new BusinessException(context, CustomEBankingIncidenceCodes.WHITELIST_SAHABAT_HOST_CALL_ERROR,
							"An unexpected exception occurred during whitelist inquiry",
							CustomEBankingErrorCodes.WHITELIST_SAHABAT_HOST_CALL_ERROR, be);
				}
			}
		}

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

}
