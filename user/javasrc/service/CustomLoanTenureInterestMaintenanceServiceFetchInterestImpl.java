package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestCriteriaVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanTenureInterestMaintenanceServiceFetchInterestImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {
		CustomLoanTenureInterestCriteriaVO customLoanTenureInterestEnqVO = (CustomLoanTenureInterestCriteriaVO) objQueryCrit;
		queryOperator.associate("lnRecId",customLoanTenureInterestEnqVO.getLnRecId());
		queryOperator.associate("bankId", context.getBankId());
		queryOperator.associate("tenor", customLoanTenureInterestEnqVO.getTenor());
	}

	@Override
	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			throw new BusinessException(objContext,
                    EBIncidenceCodes.NO_RECORDS_FOUND,
                    "No Records Found",
                    EBankingErrorCodes.RECORDS_NOT_FOUND);

		}
	}
	
	/**
	 * 
	 * Associating DAL CustomLoanTenureInterestDetailsListDAL.xml
	 * @see CustomLoanTenureInterestDetailsListDAL.xml
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_TENURE_INTEREST_MAINTENANCE;

	}

	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {

			FEBAValItem[] vls=null;


		return vls;
	}}
