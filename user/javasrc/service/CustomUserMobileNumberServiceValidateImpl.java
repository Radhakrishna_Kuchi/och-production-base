package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBATransactionContext;

/**
 *
 * @author renita_pinto
 * @since 20127:27:16 PM
 */
public class CustomUserMobileNumberServiceValidateImpl extends AbstractLocalInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};

	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

		final DeviceDetailsVO deviceDetails = (DeviceDetailsVO) objInputOutput;

		final FBATransactionContext ebContext = (FBATransactionContext) objContext;

		UserPrincipal principalId = null;

			MobileNumber phoneNo = deviceDetails.getMobileNumber();

			principalId = new UserPrincipal(phoneNo.toString());

			try {
				QueryOperator pQueryOperator = QueryOperator.openHandle(ebContext,
						CustomEBQueryIdentifiers.CUSTOM_PRINCIPAL_ID_INQUIRY);
				pQueryOperator.associate("bankId", ebContext.getBankId());
				pQueryOperator.associate("principalId", principalId);
				FEBAArrayList principalIdList = pQueryOperator.fetchList(ebContext);
				if (principalIdList.size() > 0) {
					throw new BusinessException(ebContext, EBIncidenceCodes.MOBILE_NUMBER_ALREADY_REGISTERED,
							"The mobile number is already registered.",
							EBankingErrorCodes.MOBILE_NUMBER_ALREADY_REGISTERED);
				}

			} catch (DALException ex) {
				if (ex.getErrorCode() != ErrorCodes.NO_RECORD_FOUND_IN_DAL) {
					throw new CriticalException(ebContext, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
							"Record not found", EBankingErrorCodes.RECORD_NOT_FOUND, ex);
				}

			}

		

	
	}

}
