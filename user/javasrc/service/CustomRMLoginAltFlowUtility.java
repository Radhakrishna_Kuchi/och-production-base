package com.infosys.custom.ebanking.user.service;

import java.util.Map;

import com.infosys.feba.framework.common.ApplicationConfig;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.TranCommitBusinessException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.util.ValidationsUtil;
import com.infosys.feba.framework.types.primitives.AuthorizationMode;
import com.infosys.feba.framework.types.primitives.ChannelId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.framework.common.FBAFrameWorkConstants;
import com.infosys.fentbase.framework.directbanking.DBValUtil;
import com.infosys.fentbase.framework.directbanking.meta.DBMetaInfo;
import com.infosys.fentbase.framework.directbanking.processor.DBFactory;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.entityreferences.PrincipalIDER;
import com.infosys.fentbase.types.primitives.AuthenticationMode;
import com.infosys.fentbase.types.primitives.IndividualUserId;
import com.infosys.fentbase.types.valueobjects.DBSectionVO;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.IUserProfileVO;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.user.CSIPTableUtility;
import com.infosys.fentbase.user.CUSRTableUtility;
import com.infosys.fentbase.user.IUSRTableUtility;
import com.infosys.fentbase.user.UserConstants;
import com.infosys.fentbase.user.service.AuthenticationUtility;
import com.infosys.fentbase.user.service.IRMLoginFlowUtility;
import com.infosys.fentbase.user.service.OTPUtility;

/**
 *
 * @author renita_pinto
 * @since 20127:29:57 PM
 */
public class CustomRMLoginAltFlowUtility {

	private static CustomRMLoginAltFlowUtility instance = null;

	/**
	 *
	 * @author Shilpa_S09
	 * @return
	 * @return LoginAltFlowUtility
	 */
	public static CustomRMLoginAltFlowUtility getInstance() {

		if (null == instance) {
			instance = new CustomRMLoginAltFlowUtility();
		}
		return instance;
	}

	/**
	 *
	 * @author renita_pinto
	 * @param loginAltFlowVO
	 * @param ebContext
	 * @return
	 * @throws BusinessException
	 * @throws CriticalException
	 * @return boolean
	 */
	public boolean validateUser(ILoginAltFlowVO loginAltFlowVO,
			FBATransactionContext ebContext) throws BusinessException {

		boolean validationStatus = false;
		validationStatus = validateIfUserIdIsValid(loginAltFlowVO, ebContext);
		if (validationStatus)
			populateContext(loginAltFlowVO, ebContext);

		return validationStatus;
	}

	/**
	 *
	 * @author renita_pinto
	 * @param loginAltFlowVO
	 * @param ebContext
	 * @return
	 * @throws BusinessException
	 * @throws CriticalException
	 * @return boolean
	 */

	public boolean validateIfUserIdIsValid(ILoginAltFlowVO loginAltFlowVO,
			FBATransactionContext ebContext) throws BusinessException
			 {
		/*
		 * Added for 11010 user id case sensitive use case.
		 */
       String userIdCaseSensitive=PropertyUtil.getProperty("USER_ID_CASE_SENSITIVE", ebContext);
       if("".equals(userIdCaseSensitive)|| userIdCaseSensitive==null){
    	   userIdCaseSensitive=FBAConstants.NO;}
		PrincipalIDER principalIDER = loginAltFlowVO
				.getLoginAltFlowUserDetailsVO().getPrincipalIDER();
		/*shilpa: here i am assuming that channel id and bank id are already set along
		 with user principal in the principalIDER. or in context- check this*/
		boolean validationStatus = false;
		String contextUsertype = ebContext.getUserType().getValue();

		// Get individual's information based on the principal id and channel id
		// from the Channel Specific Table
		IUserProfileVO userProfileFromDB = CSIPTableUtility.getInstance()
				.getIndividualID(principalIDER, ebContext);
		if(userIdCaseSensitive.equalsIgnoreCase(FBAConstants.YES)&& !(userProfileFromDB.getPrincipalId().toString().equals(loginAltFlowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().getUserPrincipal().toString())))
		{
			throw new BusinessException(ebContext,
					FBAIncidenceCodes.USER_DETAILS_NOT_FOUND,
					"Invalid user id entered",
					FBAErrorCodes.USR_ID_INVALID);
		}
		// Get the default relationship (updates PrincipalIDER)
		IndividualUserId individualUserId = userProfileFromDB
				.getIndividualUserId();

		loginAltFlowVO.getLoginAltFlowUserDetailsVO().setIndividualUserId(
				individualUserId);
		AuthenticationMode primaryAuthenticationMode = userProfileFromDB
				.getPrimaryAuthenticationMode();
		AuthenticationMode secondaryAuthenticationMode = userProfileFromDB
				.getSecondaryAuthenticationMode();
		AuthorizationMode authorizationMode = userProfileFromDB.getTranAuthMode();

		loginAltFlowVO.getLoginAltFlowUserDetailsVO()
				.setPrimaryAuthenticationMode(primaryAuthenticationMode);
		loginAltFlowVO.getLoginAltFlowUserDetailsVO()
				.setSecondaryAuthenticationMode(secondaryAuthenticationMode);

		// check if the mode of the user is password

		if (!primaryAuthenticationMode.equals(new AuthenticationMode(
				FBAConstants.SIGNON_PASSWORD_MODE))
				&& !secondaryAuthenticationMode.equals(new AuthenticationMode(
						FBAConstants.SIGNON_PASSWORD_MODE))) {
			// check for channel g as part of fix for ticketid 646460
					if(ebContext.getAccessChannelId().equals(new ChannelId(FBAConstants.DOWNLOADABLE_CLIENT_GPRS)) )
					{
								if(!authorizationMode.equals(new AuthorizationMode(FBAConstants.TRANSACTION_PASSWORD_MODE))){
			throw new BusinessException(ebContext,
											FBAIncidenceCodes.UNSUPPORTED_AUTHORIZATION_MODE,
											"Authorization mode is not pwd for the user",
											FBAErrorCodes.REQ_SETUP_MISSING_CONTACT_ADMIN);
								}
							}else{
								throw new BusinessException(ebContext,
					FBAIncidenceCodes.UNSUPPORTED_AUTHENTICATION_MODE,
					"Authentication mode not pwd for the user",
					FBAErrorCodes.INVALID_AUTHENTICATION_MODE);
				}
		}

		IUSRTableUtility.getInstance().getDefaultRelationship(individualUserId,
				principalIDER, ebContext);

		loginAltFlowVO.getLoginAltFlowUserDetailsVO().setPrincipalIDER(
				principalIDER);

		CUSRInfo cusrInfo = CUSRTableUtility.getInstance().getUserRecord(
				ebContext, principalIDER.getUserId(),
				principalIDER.getCorpId(), ebContext.getBankId());

		if (cusrInfo != null) {
			validationStatus = true;
			ebContext.setUserType(cusrInfo.getUserType());
			/* RENITA:need to check where to put this in case of offline
			 scenario. we want this only for online*/
			String cusrUsertype =cusrInfo.getUserType().getValue();

			if(!contextUsertype.equalsIgnoreCase(FBAConstants.STRING_RELATIONSHIP_MANAGER))
			{
				IRMLoginFlowUtility iRMLoginFlowUtility=(IRMLoginFlowUtility)ApplicationConfig.getImplementationInstance("RM_LOGIN_FLOW_UTILITY");
				validationStatus=iRMLoginFlowUtility.validateUserIsValid(ebContext,cusrInfo);
			}
			else {
				 if (!(contextUsertype.equalsIgnoreCase(cusrUsertype))) {
					validationStatus = false;
				}
			}
		}

		if (!validationStatus) {
			throw new BusinessException(ebContext,
					FBAIncidenceCodes.USER_DETAILS_NOT_FOUND,
					"Invalid user id entered",
					FBAErrorCodes.USR_ID_INVALID);
		}

		return validationStatus;
	}

	/**
	 *
	 * @author Shilpa_S09
	 * @param loginAltFlowVO
	 * @param ebContext
	 * @throws BusinessException
	 * @throws CriticalException
	 * @return void
	 */
	protected void generateOTP(ILoginAltFlowVO loginAltFlowVO,
			FBATransactionContext ebContext) throws BusinessException,
			CriticalException {

		String generatedOTP = OTPUtility.getInstance()
				.generateOTP(
						ebContext,
						loginAltFlowVO.getLoginAltFlowUserDetailsVO()
								.getPrincipalIDER());

		
		if (generatedOTP == null || generatedOTP.length() == -1) {
			throw new BusinessException(ebContext,
					FEBAIncidenceCodes.INVALID_ACCESS,
					"OTP Authentication mode not enabled for the given user",
					ErrorCodes.INVALID_ACCESS);
		}
		FEBAUnboundString message = new FEBAUnboundString(
				UserConstants.OTP_GEN_SUCCESS);
		int errorCode = FBAErrorCodes.OTP_GEN_SUCCESS_CODE;
		ebContext.addBusinessInfo(new FEBAUnboundInt(errorCode), message,
				message);

		loginAltFlowVO.getLoginAltFlowUserDetailsVO().setOtp(generatedOTP);

	}

	/**
	 *
	 * @author renita_pinto
	 * @param objContext
	 * @param objInputOutput
	 * @return
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @return ArrayList<BusinessExceptionVO>
	 */
	public ArrayList validateLoginAltFlowConfigData(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput)
			throws BusinessException, CriticalException {

		// prepare val item from the config
		// here section id should be FPUSR for offline and FPUSRON for online

		FEBAValItem[] febaList = null;

		DBMetaInfo dbMetaInfo = null;

		DBSectionVO fpSectionVO = (DBSectionVO) objInputOutput;

		dbMetaInfo = DBFactory.getInstance(fpSectionVO.getSectionId()
				.toString().split("_")[0]);
		Map valArraysMap = dbMetaInfo.getValArrays();

		febaList = DBValUtil.prepareValItemArray(fpSectionVO, valArraysMap,
				FBAFrameWorkConstants.MAIN);

		ArrayList<BusinessExceptionVO> pExceptionList = new ArrayList<>();

		try {
			if (febaList != null) {
				ValidationsUtil
						.callValEngine(objContext, fpSectionVO, febaList);
			}
		} catch (BusinessException be) {
		/* DONT DELETE ArrayList is strongly typed with value BusinessExceptionVO , Done part of .NET Automation */
			ArrayList<BusinessExceptionVO> exceptionList = be.getExceptionList();
			for (int j = 0; j < exceptionList.size(); j++) {
			/* DONT DELETE removed type casting as exceptionList is already of type BusinessExceptionVO, Done part of .NET Automation */
				BusinessExceptionVO bevo = exceptionList.get(j);
				if (bevo.getErrorCode() == FBAErrorCodes.INVALID_ANSWERS_ENTERED){
					throw new TranCommitBusinessException(objContext,
							FBAIncidenceCodes.INVALID_ANSWERS_LAFA,
							"One or more data entered in text is Invalid.",
							FBAErrorCodes.INVALID_ANSWERS_ENTERED);
				}
				if (bevo.getErrorCode() == FBAErrorCodes.USER_ID_LOCKED){
					throw new TranCommitBusinessException(objContext,
							FBAIncidenceCodes.EXCEEDED_MAXIMUM_ATTEMPTS_LAFA,
							"Disabling user for reaching maximum retry attempts",
							FBAErrorCodes.USER_ID_LOCKED);
				}
				/* Added as a part of defect fixing for ticket #773816 start*/
				String temp = bevo.getErrorObjField();
				if((temp!= null)&& temp.length()>0){
					String tempFormField = temp.replace("basicFields['","").replace("'].sValue", "").trim();
					bevo.setFormField(tempFormField);
				}
				/* Added as a part of defect fixing for ticket #773816 end*/
				pExceptionList.add(bevo);
			}
		}

		return pExceptionList;

	}

	/**
	 * populate context variables for online forgot pwd check
	 */
	private void populateContext(ILoginAltFlowVO loginAltFlowVO,
			FBATransactionContext ebContext) throws BusinessException {

		ebContext.setIndividualId(loginAltFlowVO.getLoginAltFlowUserDetailsVO()
				.getIndividualUserId());
		PrincipalIDER principalIDER = loginAltFlowVO
				.getLoginAltFlowUserDetailsVO().getPrincipalIDER();

		// Setting corpid and user id of default relationship to context
		ebContext.setDefaultRelationshipCorpId(principalIDER.getCorpId());
		ebContext.setDefaultRelationshipUserId(principalIDER.getUserId());
		ebContext.setCorpId(principalIDER.getCorpId());
		ebContext.setUserId(principalIDER.getUserId());

		ebContext.setUserPrincipal(new UserPrincipal(principalIDER
				.getUserPrincipal().toString()));

		// Setting isUnifiedLoginSession value to N by default
		ebContext
				.setIsUnifiedLoginSession(new YNFlag(FBAConstants.CHAR_N));
		// if the user is a unified Login user, then setting
		// setIsUnifiedLoginSession to Y

		if (IUSRTableUtility.getInstance().isUnifiedLoginUser(
				loginAltFlowVO.getLoginAltFlowUserDetailsVO()
						.getIndividualUserId(), ebContext)) {

			ebContext.setIsUnifiedLoginSession(new YNFlag(
					FBAConstants.CHAR_Y));
		}

		ebContext.setRecordUserId(AuthenticationUtility.getInstance()
				.getRecordUserID(principalIDER.getUserId(),
						principalIDER.getCorpId()));

		ebContext.setActualUserId(AuthenticationUtility.getInstance()
				.getRecordUserID(principalIDER.getUserId(),
						principalIDER.getCorpId()));

	}

	/**
	 * this method is used to validate the fields that are not part of config
	 * file, but exist in the jsp directly.
	 *
	 * @author renita_pinto
	 * @param objContext
	 * @param loginAltFlowVO
	 * @param valitems
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 * @return void
	 */
	public void validateLoginAltFlowData(FEBATransactionContext objContext,
			LoginAltFlowVO loginAltFlowVO, FEBAValItem[] valitems)
			throws BusinessException, CriticalException {

		ArrayList<BusinessExceptionVO> pExceptionList = new ArrayList<>();

		try {
			if (valitems != null) {
				ValidationsUtil
						.callValEngine(objContext, loginAltFlowVO, valitems);
			}
		} catch (BusinessException be) {
				/* DONT DELETE ArrayList is strongly typed with value BusinessExceptionVO , Done part of .NET Automation */
			ArrayList<BusinessExceptionVO> exceptionList = be.getExceptionList();
			for (int j = 0; j < exceptionList.size(); j++) {
				/* DONT DELETE removed type casting as exception List is already defined BusinessExceptionVO , Done part of .NET Automation */
				pExceptionList.add(exceptionList.get(j));
			}
		}
		DBSectionVO fpSectionVO = loginAltFlowVO.getForgotPasswordOfflineVO()
				.getFpSectionVO();

		ArrayList<BusinessExceptionVO> pExceptionListConfig = CustomRMLoginAltFlowUtility
				.getInstance().validateLoginAltFlowConfigData(objContext,
						fpSectionVO);
		pExceptionList.addAll(pExceptionListConfig);

		if (pExceptionList.isEmpty()) {
			throw new BusinessException(objContext, pExceptionList);
		}
	}
}