package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.DVDTTAO;
import com.infosys.custom.ebanking.tao.info.DVDTInfo;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.FreeText;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;


public class CustomDeviceRegistrationServiceCreateImpl extends AbstractLocalUpdateTran {

	@SuppressWarnings("rawtypes")
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		final DeviceDetailsVO deviceDetailsVO = (DeviceDetailsVO) objInputOutput;

		return new FEBAValItem[] {
				new FEBAValItem(deviceDetailsVO.getUserID(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.RALTR_INPUT_DATA_INSUFFICIENT),
				// Mandatory Check for Channel ID
				new FEBAValItem(deviceDetailsVO.getDeviceId(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.DEVICE_ID_MANDATORY)
		};

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		DeviceDetailsVO deviceDetailsVO = (DeviceDetailsVO) objInputOutput;

		try {
			updateDVDT(ebContext,deviceDetailsVO);
		} catch (FEBATableOperatorException ftoExcetption) {
			ftoExcetption.printStackTrace();
			insertDVDT(ebContext, deviceDetailsVO);
		}

	}
	private void insertDVDT(FEBATransactionContext objContext, DeviceDetailsVO deviceDetailsVO)
			throws BusinessException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		DVDTTAO dvdtTAO = new DVDTTAO(objContext);

		try {
			dvdtTAO.associateBankId(ebContext.getBankId());
			dvdtTAO.associateUserId(objContext.getUserId());
			dvdtTAO.associateDeviceToken(new FreeText(deviceDetailsVO.getDeviceId().getValue()));
			dvdtTAO.associateDeviceName(deviceDetailsVO.getDeviceName());
			dvdtTAO.insert(objContext);

		} catch (FEBATableOperatorException e) {
			throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
					"Record Insertion failed in DVDT", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
		}
	}
	private void updateDVDT(FEBATransactionContext objContext, DeviceDetailsVO deviceDetailsVO)
			throws FEBATableOperatorException {

		EBTransactionContext ebContext = (EBTransactionContext) objContext;

		DVDTTAO dvdtTAO = new DVDTTAO(objContext);

			DVDTInfo dvdtInfo = DVDTTAO.select(ebContext, objContext.getBankId(), objContext.getUserId());

			dvdtTAO.associateBankId(dvdtInfo.getBankId());
			dvdtTAO.associateUserId(dvdtInfo.getUserId());
			dvdtTAO.associateDeviceToken(new FreeText(deviceDetailsVO.getDeviceId().getValue()));
			dvdtTAO.associateDeviceName(deviceDetailsVO.getDeviceName());
			dvdtTAO.associateCookie(dvdtInfo.getCookie());
			dvdtTAO.update(objContext);
	}
}
