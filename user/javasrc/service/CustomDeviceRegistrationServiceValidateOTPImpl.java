package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.types.valueobjects.CustomDeviceDetailsVO;
import com.infosys.custom.ebanking.user.validator.CustomOTPValidator;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomDeviceRegistrationServiceValidateOTPImpl extends AbstractLocalUpdateTran{

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext tc, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		DeviceDetailsVO deviceDetVO = (DeviceDetailsVO) objInputOutput;
		CustomDeviceDetailsVO customVO = (CustomDeviceDetailsVO)deviceDetVO.getExtensionVO();

		return new FEBAValItem[] {

				new FEBAValItem("otp", customVO.getOtp(),
						new CustomOTPValidator(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT),

		};
	}

	@Override
	public void process(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// Not doing anything here
	}

}
