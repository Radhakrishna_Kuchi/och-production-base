/**
 * CustomRMLoginAltFlowServiceGenerateAuthAccessCodePostProcess.java
 * @since Oct 10, 2018 - 2:21:19 PM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.user.custom;

import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.common.ICustomHook;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;

/**
 * @author Tarun_Kumar18
 *
 */
public class CustomRMLoginAltFlowServiceGenerateAuthAccessCodePostProcess implements ICustomHook {

	@Override
	public void execute(IContext objContext, Object objInputOutput)
			throws BusinessException, BusinessConfirmation, CriticalException {

		FEBATransactionContext febaTxnContext = (FEBATransactionContext) objContext;
		CustomSendSMSUtil smsUtil = new CustomSendSMSUtil();
		smsUtil.sendSMS(febaTxnContext, objInputOutput);
	}

}
