
package com.infosys.custom.ebanking.contentmanagement.util;

import com.infosys.custom.ebanking.tmo.CACMTMO;
import com.infosys.custom.ebanking.tmo.CMGTTMO;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBADeleteFlag;
import com.infosys.feba.framework.types.primitives.FEBAMaintenanceRequired;
import com.infosys.feba.framework.types.primitives.FEBATMOOperation;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.ModuleName;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBACookie;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.common.util.OperationModeUtil;
import com.infosys.fentbase.types.primitives.EntityKey;
import com.infosys.fentbase.types.primitives.FreeText2;
import com.infosys.fentbase.user.UserConstants;
import com.infosys.fentbase.usermaintenance.common.UserMaintenanceConstants;

public final class CustomContentMgmtUtil {	
	
  
	private CustomContentMgmtUtil() {
		
	}
	
    public static void processCACOM(FEBATransactionContext pObjContext,
    		CustomContentMgmtDetailsVO detVO, int tmoMode)
            throws FEBATMOException, FEBATableOperatorException {

        FEBATMOCriteria criteria = new FEBATMOCriteria();
        if ((tmoMode == FEBATMOConstants.TMO_OPERATION_INSERT)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_MODIFY)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_MODIFY)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_DELETE)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_DELETE)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE)) {
            criteria.setMaintenanceRequired(MaintenanceReqdUtility
                    .getMaintenanceReqd(pObjContext));
        } else {
            if ((tmoMode == FEBATMOConstants.TMO_OPERATION_APPROVE)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_APPROVE)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_REJECT)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_REJECT)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_CANCEL)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_CANCEL)) {
                criteria.setDependencyFlag(FBAConstants.YES);
            }
            criteria.setMaintenanceRequired(new FEBAMaintenanceRequired("Y"));
        }
        
      	if (OperationModeUtil.isWorkflowOperation(tmoMode)) {
			criteria.setMaintenanceRequired(EBankingConstants.YES);
		} else {
			criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility
					.getMaintenanceReqd(pObjContext));
		}
        
        CACMTMO acomTMO = new CACMTMO(pObjContext, criteria);
        acomTMO.associateBankId(pObjContext.getBankId());     
        acomTMO.associatePhotoKey(detVO.getObjectId());
        acomTMO.associateContentType(detVO.getContType());
        acomTMO.associateHeader(detVO.getHeader());
        acomTMO.associateDescription(detVO.getDescription());
        acomTMO.associateTextType(detVO.getTextType());
        acomTMO.associateStartDate(detVO.getStartDate());
        acomTMO.associateEndDate(detVO.getEndDate());
        acomTMO.associateMerchantId(detVO.getMerchantId());
        acomTMO.associateAndAppUrl(detVO.getAndAppURL());
        acomTMO.associateIosAppUrl(detVO.getIosAppURL());
        acomTMO.associateIosStoreUrl(detVO.getIosStoreURL());
        acomTMO.associatePlayStoreUrl(detVO.getPlayStoreURL());
        acomTMO.associatePriority(detVO.getPriority());
        acomTMO.associateFilename(detVO.getFileName());
               
		final FEBADeleteFlag delFlag = new FEBADeleteFlag(
				UserMaintenanceConstants.CHAR_N);
		acomTMO.associateDelFlg(delFlag);
      
        if (detVO.getCacomCookie() == null) {
        	detVO.getCacomCookie().set(new FEBACookie());
        }
        acomTMO.associateCookie(detVO.getCacomCookie());
        acomTMO.process(pObjContext, new FEBATMOOperation(tmoMode));
    }

    
    public static void processCMGT(FEBATransactionContext pObjContext,
    		CustomContentMgmtDetailsVO detVO, int tmoMode)
            throws FEBATMOException, FEBATableOperatorException {

        FEBATMOCriteria criteria = new FEBATMOCriteria();
        if ((tmoMode == FEBATMOConstants.TMO_OPERATION_INSERT)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_MODIFY)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_MODIFY)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_DELETE)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_DELETE)
                || (tmoMode == FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE)) {
            criteria.setMaintenanceRequired(MaintenanceReqdUtility
                    .getMaintenanceReqd(pObjContext));
        } else {
            if ((tmoMode == FEBATMOConstants.TMO_OPERATION_APPROVE)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_APPROVE)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_REJECT)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_REJECT)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_CANCEL)
                    || (tmoMode == FEBATMOConstants.TMO_OPERATION_SELECT_FOR_CANCEL)) {
                criteria.setDependencyFlag(FBAConstants.YES);
            }
            criteria.setMaintenanceRequired(new FEBAMaintenanceRequired("Y"));
        }
        CMGTTMO cmgtTMO = new CMGTTMO(pObjContext, criteria);   
        
        cmgtTMO.associateObjectKey(new FreeText2(detVO.getObjectId().toString()));
        cmgtTMO.associateObjectId(detVO.getObjectId());
        cmgtTMO.associateFileData(detVO.getProfilePhoto());
        cmgtTMO.associateFileMime(detVO.getContentType());
        cmgtTMO.associateDataLength(detVO.getFileSize());  
        cmgtTMO.associateOrgId(new CorporateId(pObjContext.getBankId().toString()));
        cmgtTMO.associateUserId(new UserId("ACOM"));
        cmgtTMO.associateEntityId(new EntityKey(UserConstants.IUSR));
        cmgtTMO.associateModuleId(new ModuleName("ACOM"));
        cmgtTMO.associateBankId(pObjContext.getBankId());
               
		final FEBADeleteFlag delFlag = new FEBADeleteFlag(
				UserMaintenanceConstants.CHAR_N);
		cmgtTMO.associateDelFlg(delFlag);
      
        if (detVO.getCmgtCookie() == null) {
        	detVO.getCmgtCookie().set(new FEBACookie());
        }
        cmgtTMO.associateCookie(detVO.getCmgtCookie());
        cmgtTMO.process(pObjContext, new FEBATMOOperation(tmoMode));
    }      
    
    
    public static BusinessInfoVO getSuccessMessage(EBTransactionContext objContext,
			int errorCodes) {
		FEBAUnboundInt errorCode = null;
		// fetching the approval id
		TransactionKey transactionKey = objContext.getApprovalTxnKey();
		// Set the error code
		final BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		if (MaintenanceReqdUtility.getMaintenanceReqd(objContext).equals(
				EBankingConstants.YES)
				&& (errorCodes == EBankingErrorCodes.CAFW_INSERT_SUCCESS)
				|| (errorCodes == EBankingErrorCodes.RECORD_UPDATED_SUCCESSFULLY)
				|| (errorCodes == EBankingErrorCodes.RECORD_DELETED_SUCCESSFULLY)) {
			errorCode = new FEBAUnboundInt(
					EBankingErrorCodes.ADMIN_RECORD_SENT_FOR_APPROVAL);
			infoDetails.setParam(new AdditionalParam(TxnTypeConstants.REF_ID,
					new FEBAUnboundString(String.valueOf(transactionKey
							.getValue()))));
		} else {
			errorCode = new FEBAUnboundInt(errorCodes);
		}
		// create an instance of businessinfo object and set the error code
		infoDetails.setCode(errorCode);

		// return business info object
		return infoDetails;
	}
    
}
