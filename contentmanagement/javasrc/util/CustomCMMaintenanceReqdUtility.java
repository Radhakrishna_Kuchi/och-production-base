package com.infosys.custom.ebanking.contentmanagement.util;

import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.fentbase.types.primitives.WorkflowMode;
import com.infosys.fentbase.types.primitives.WorkflowOperation;

public class CustomCMMaintenanceReqdUtility {
	
private CustomCMMaintenanceReqdUtility() {
		
	}
	
	public static String getMaintenanceReqd(FEBATransactionContext pObjContext) {		
		final WorkflowMode wfMode = ((EBTransactionContext) pObjContext)
				.getWorkflowVO().getWorkflowOutputVO().getWorkflowMode();
		boolean isconfirm = isConfirmFlow(pObjContext, wfMode);
		String mntcRequired = EBankingConstants.NO;

		if (FEBATypesUtility.isNotNullOrBlank(wfMode)) {
			mntcRequired = wfModeFAP(wfMode.getValue(), pObjContext, isconfirm) ? EBankingConstants.YES
					: EBankingConstants.NO;
		}
		return mntcRequired;
	}
	private static boolean isConfirmFlow(FEBATransactionContext pObjContext,
			final WorkflowMode wfMode) {
		FEBAUnboundChar isConfirmFlow = ((EBTransactionContext) pObjContext)
				.getIsConfirmFlow();
		// Setting the default value as 'N' when it is not a confirmation flow
		if (!FEBATypesUtility.isNotBlankChar(isConfirmFlow)) {
			isConfirmFlow = new FEBAUnboundChar(EBankingConstants.CHAR_N);
		}
		boolean isconfirm = false;
		// Setting boolean value when confirm flow
		if (FEBATypesUtility.isNotBlankChar(isConfirmFlow)
				&& isConfirmFlow.equals(new FEBAUnboundChar(
						EBankingConstants.CHAR_Y))
				&& !wfMode.toString().equals(
						EBankingConstants.PENDING_FOR_CONFIRMATION)) {
			isconfirm = true;
		}
		return isconfirm;
	}
	private static boolean wfModeFAP(String workFlowMode,
			FEBATransactionContext pObjContext, boolean isconfirm) {
		boolean mntcRequired;
		// checking if it is the confirm mode and workflow mode is Final
		// Approval
		if (isconfirm
				&& (workFlowMode.equals(EBankingConstants.FINAL_APPROVAL) || workFlowMode
						.equals(EBankingConstants.REJECTED))) {
			mntcRequired = true;
		} else if (isconfirm
				&& !(workFlowMode.equals(EBankingConstants.FINAL_APPROVAL) || workFlowMode
						.equals(EBankingConstants.REJECTED))) {
			mntcRequired = false;
		} else {
			mntcRequired = utilityMethod(workFlowMode, pObjContext,
					false);
		}
		return mntcRequired;

	}
	private static boolean utilityMethod(String workFlowMode,
			FEBATransactionContext pObjContext, boolean mntcRequired) {
		if (utilityConditionalMethod(workFlowMode)) {
			/*
			 * If workflow mode is set to Final Approval/Rejected/Recalled/Sent
			 * for Repair/Initiated it returns maintenance required
			 */
			mntcRequired = true;
		} else if (workFlowMode.equals(EBankingConstants.WORKFLOW_NOT_REQD)) {
			/*
			 * During initiate if transaction goes for RM Approval then
			 * Maintenance required will be Yes else false
			 */
			mntcRequired = false;
		} else if (((EBTransactionContext) pObjContext).getWorkflowVO()
				.getWorkflowInputVO().getWfOperation().equals(
						new WorkflowOperation(EBankingConstants.INITIATE))
				&& workFlowMode.equals(EBankingConstants.WAITING_FOR_RM_APPR)) {
			/*
			 * During initiate if transaction goes for RM Approval then
			 * Maintenance required will be Yes else false
			 */
			mntcRequired = true;
		}
		return mntcRequired;
	}
	
	private static boolean utilityConditionalMethod(String workFlowMode) {
		return workFlowMode.equals(EBankingConstants.FINAL_APPROVAL)
				|| workFlowMode.equals(EBankingConstants.REJECTED)
				|| workFlowMode.equals(EBankingConstants.RECALLED)
				|| workFlowMode.equals(EBankingConstants.SENT_FOR_REPAIR)
				|| workFlowMode.equals(EBankingConstants.WORKFLOW_INIT)
				|| workFlowMode
						.equals(EBankingConstants.PENDING_FOR_CONFIRMATION);
	}
	
}
