/**
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.ebanking.contentmanagement.validators;



import java.util.StringTokenizer;

import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.valengine.IFEBAExtendedValidator;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.feba.utils.insulate.ArrayList;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.user.UserConstants;


public class CustomContentUploadFormatValidator implements IFEBAStandardValidator {

	
	public void validate(FEBATransactionContext arg0, IFEBAType arg1)
			throws BusinessException, CriticalException {
	
		CustomContentMgmtDetailsVO vo = (CustomContentMgmtDetailsVO) arg1;
		FBATransactionContext ctx = (FBATransactionContext) arg0;
		Boolean isValidFormat = false;
		String extension = vo.getFileName().toString()
				.substring(vo.getFileName().toString().lastIndexOf(FBAConstants.DOT) + 1,
                  vo.getFileName().toString().length());

		ArrayList supportedFormatList = new ArrayList();

		/* Getting the list of supported formats into a token */

		StringTokenizer token = new StringTokenizer(PropertyUtil.getProperty(
				UserConstants.PHOTO_FORMAT_SUPPORTED, ctx), FBAConstants.COMMA);

		/* Getting the list of supported formats into a token */

		while (token.hasMoreTokens()) {
			supportedFormatList.add(token.nextToken());
		}

		for (int count = 0; count < supportedFormatList.size(); count++) {
			if (extension.equalsIgnoreCase(supportedFormatList.get(count)
					.toString())) {
				isValidFormat = true;
				break;
			}
		}
		if (!isValidFormat) {
			throw new BusinessException(ctx,
					FBAIncidenceCodes.INVALID_PHOTO_FORMAT,
					"Format of uploaded photo not supported.",
					FBAErrorCodes.INVALID_PHOTO_FORMAT);
		}
	}

	

}

