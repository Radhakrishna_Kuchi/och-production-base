/**
 * UserMaintenanceServiceModifyImpl.java
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.contentmanagement.service;
import com.infosys.custom.ebanking.contentmanagement.util.CustomContentMgmtUtil;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentTypeFieldsValidator;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentUploadDateValidator;
import com.infosys.custom.ebanking.tao.CACMConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.FEBATypesConstants;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.tao.CMGTConstants;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;
import com.infosys.fentbase.usermaintenance.common.UserMaintenanceConstants;


public class CustomContentMgmtServiceDeleteImpl extends AbstractLocalUpdateTran {
       

    @Override
    public void process(FEBATransactionContext pObjContext,
            IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
            throws BusinessException, BusinessConfirmation, CriticalException {

        FBATransactionContext objContext = (FBATransactionContext) pObjContext;
        CustomContentMgmtDetailsVO detVO = (CustomContentMgmtDetailsVO) objInputOutput;
        FEBATMOCriteria criteria = new FEBATMOCriteria();
        criteria.setMaintenanceRequired(MaintenanceReqdUtility
                .getMaintenanceReqd(pObjContext));
        criteria.setDependencyFlag(FBAConstants.NO);
     
        try {                                           
            CustomContentMgmtUtil.processCACOM(pObjContext,
                    detVO,
                    FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE);
            criteria.setDependencyFlag(FBAConstants.YES);                                   	         		               
            addSuccessMessageToContext(pObjContext);
            
            
            CustomContentMgmtUtil.processCMGT(pObjContext,
                    detVO,
                    FEBATMOConstants.TMO_OPERATION_LOGICAL_DELETE);
            criteria.setDependencyFlag(FBAConstants.YES);                                   	         		               
            addSuccessMessageToContext(pObjContext);
            
        } catch (FEBATMOException tmoExp) {
            int errorCode = CAFrameworkErrorMappingHelper
                    .getMappedErrorCode(OperationModeConstants.CA_MODIFY
                            + tmoExp.getErrorCode());
            throw new BusinessException(objContext,
                    FBAIncidenceCodes.USER_MODIFY_TMO_EXCEPTION, errorCode,
                    tmoExp);
        } catch (FEBATableOperatorException tblOperExp) {
            throw new CriticalException(objContext,
                    FBAIncidenceCodes.TAO_EXP_USER_MODIFY, tblOperExp
                            .getMessage(), tblOperExp.getErrorCode());
        }
    }
    /**
     * Method populateWorkflowVO() Description Populates workflow value object
     * from context and InputOutputVO Input objContext, workflowVO,
     * objInputOutput, objTxnWM
     *
     * @param objContext
     * @param populateWorkflowVOUtility
     * @param objInputOutput
     * @param objTxnWM
     */
    @Override
    protected void populateWorkflowVO(FEBATransactionContext objContext,
            IPopulateWorkflowVOUtility populateWorkflowVOUtility,
            IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

    	EBTransactionContext ebcontext = (EBTransactionContext) objContext;
    	CustomContentMgmtDetailsVO detailsVO = (CustomContentMgmtDetailsVO) objInputOutput;
		// Setting Key Fields
		populateWorkflowVOUtility.setKeyField(CACMConstants.BANK_ID, ebcontext.getBankId().toString().toUpperCase());
		populateWorkflowVOUtility.setKeyField(CACMConstants.PHOTO_KEY,detailsVO.getObjectId().toString().toUpperCase());
		populateWorkflowVOUtility.setKeyField(CMGTConstants.OBJECT_KEY,detailsVO.getObjectId().toString().toUpperCase());		
		populateWorkflowVOUtility.setEntityType("CACM");
		populateWorkflowVOUtility.setModuleID("ACOM");
		populateWorkflowVOUtility.setCorpId(ebcontext.getCorpId().getValue());
		populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
		populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_DELETE).toString());
		populateWorkflowVOUtility.setTransactionType("ACM");
		populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
        }
                     
    /**
     *
     * Adding Success Message to the Context to be displayed on the screen
     *
     * @param pObjContext
     * @author Gautam_Kasukhela
     * @since Feb 23, 2011 - 1:34:21 PM
     */
    private void addSuccessMessageToContext(FEBATransactionContext pObjContext) {

        FBATransactionContext ebContext = (FBATransactionContext) pObjContext;
        // Getting Transaction Key generated for the transaction
        final TransactionKey txnKey = ebContext.getApprovalTxnKey();
        BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
                .createInstance(TypesCatalogueConstants.BusinessInfoVO);
        // Checking if WorkFlow is enabled
        if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(
                FBAConstants.NO)) {
            infoDetails.setCode(new FEBAUnboundInt(
                    FBAErrorCodes.RECORD_DELETE_SUCCESSFUL));
        } else {
            infoDetails.setCode(new FEBAUnboundInt(
                    FBAErrorCodes.RECORD_SENT_FOR_APPROVAL));
            infoDetails.setParam(new AdditionalParam(
                    FBAConstants.TRAN_KEY, new FEBAUnboundString(String
                            .valueOf(txnKey.getValue()))));
        }
        infoDetails.setDispMessage("Content Deleted Successfully.");
        infoDetails.setLogMessage("Content Deleted Successfully.");
        pObjContext.addBusinessInfo(infoDetails);
    }
    @Override
    public FEBAValItem[] prepareValidationsList(
            FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
            IFEBAValueObject objTxnWM) throws BusinessException,
            BusinessConfirmation, CriticalException {

    	CustomContentMgmtDetailsVO photoVO = (CustomContentMgmtDetailsVO) objInputOutput;
		return new FEBAValItem[] {												
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO,
						new CustomContentTypeFieldsValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				  
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO,
						new CustomContentUploadDateValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				};
		
    }
}
