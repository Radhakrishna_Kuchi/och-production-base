package com.infosys.custom.ebanking.contentmanagement.service;


import com.infosys.custom.ebanking.contentmanagement.util.CustomCMMaintenanceReqdUtility;
import com.infosys.custom.ebanking.contentmanagement.util.CustomContentMgmtUtil;
import com.infosys.custom.ebanking.tao.CACMConstants;
import com.infosys.custom.ebanking.tao.CMGTConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;


/**

 * This class is responsible for uploading content photo to CMS system or CMGT
 * @author Vidhi_Kapoor01
 */

public class CustomContentMgmtServiceAddImpl extends AbstractLocalUpdateTran {

	/**
	 * This method performs necessary validations on the inputs
	 * @author Vidhi_Kapoor01
	 * @since Sept 4, 2018	
	 */ 

	@Override

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0,
			IFEBAValueObject arg1, IFEBAValueObject arg2)

			throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {}; 
	}

			
	 @Override
	    public void process(FEBATransactionContext pObjContext,
	            IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
	            throws BusinessException, BusinessConfirmation, CriticalException {

		 CustomContentMgmtDetailsVO detailsVO = (CustomContentMgmtDetailsVO) objInputOutput;
	        FEBATMOCriteria criteria = new FEBATMOCriteria();
	        criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility
					.getMaintenanceReqd(pObjContext));
	       	       try {
	        	CustomContentMgmtUtil.processCACOM(pObjContext,
	        			detailsVO,
						FEBATMOConstants.TMO_OPERATION_INSERT);	   
	        	
	        	CustomContentMgmtUtil.processCMGT(pObjContext,
	        			detailsVO,
						FEBATMOConstants.TMO_OPERATION_INSERT);	 
	        	 addSuccessMessageToContext(pObjContext);
	            } 
	        catch (FEBATMOException tmoExp) {
	            int errorCode = CAFrameworkErrorMappingHelper
	                    .getMappedErrorCode(OperationModeConstants.CA_INSERT
	                            + tmoExp.getErrorCode());
	            throw new BusinessException(pObjContext,
	                    FBAIncidenceCodes.TMO_EXP_USER_CREATE, errorCode, tmoExp);
	        } catch (FEBATableOperatorException tblOperExp) {
	            throw new CriticalException(pObjContext,
	                    FBAIncidenceCodes.TAO_EXP_USER_CREATE, tblOperExp
	                            .getMessage(), tblOperExp.getErrorCode());
	        }
	        
	    }
	

	 @Override
		protected void populateWorkflowVO(FEBATransactionContext pObjContext,
				IPopulateWorkflowVOUtility populateWorkflowVOUtility,
				IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		 CustomContentMgmtDetailsVO detVO = (CustomContentMgmtDetailsVO) objInputOutput;

			EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
			
			populateWorkflowVOUtility.setKeyField(CACMConstants.PHOTO_KEY,detVO.getObjectId().toString().toUpperCase());
			populateWorkflowVOUtility.setKeyField(CMGTConstants.OBJECT_KEY,detVO.getObjectId().toString().toUpperCase());		
			populateWorkflowVOUtility.setKeyField(TxnTypeConstants.BANK_ID, ebContext.getBankId().toString());					
			populateWorkflowVOUtility.setModuleKey("CACM");
			populateWorkflowVOUtility.setEntityType("CACM");
			populateWorkflowVOUtility.setModuleID((EBankingConstants.BANK_USER_MODULE_ID).toString());
			populateWorkflowVOUtility.setCorpId(ebContext.getCorpId().getValue());
			populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
			populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_INSERT).toString());
			populateWorkflowVOUtility.setTransactionType("ACM");
			populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
		}

		/**
		 * 
		 * Adding Success Message to the Context to be displayed on the screen
		 * 
		 * @param pObjContext
		 * @author Vidhi_Kapoor01
		 * @since Feb 23, 2011 - 1:34:21 PM
		 */
		public static void addSuccessMessageToContext(
				FEBATransactionContext pObjContext) {
			EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
			// Getting Transaction Key generated for the transaction
			final TransactionKey txnKey = ebContext.getApprovalTxnKey();
			BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.BusinessInfoVO);
			// Checking if WorkFlow is enabled
			if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(
					EBankingConstants.NO)) {
				infoDetails.setCode(new FEBAUnboundInt(
						EBankingErrorCodes.RECORD_INSERTION_SUCCESSFUL));
			} else {
				infoDetails.setCode(new FEBAUnboundInt(
						EBankingErrorCodes.RECORD_SENT_FOR_APPROVAL));
				infoDetails.setParam(new AdditionalParam(
						EBankingConstants.TRAN_KEY, new FEBAUnboundString(String
								.valueOf(txnKey.getValue()))));
			}
			infoDetails.setDispMessage("Bulk Type Create Successfully.");
			infoDetails.setLogMessage("Log:Bulk Type Created Successfully.");
			pObjContext.addBusinessInfo(infoDetails);
		}
}