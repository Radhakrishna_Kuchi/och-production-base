package com.infosys.custom.ebanking.contentmanagement.service;


import com.infosys.custom.ebanking.tao.S_CACMTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtCritVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtEnqVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.dal.QueryRecord;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;

import io.jsonwebtoken.lang.Collections;

public class CustomContentListServiceFetchImpl extends AbstractLocalListInquiryTran  {

	@Override
	protected void associateQueryParameters(FEBATransactionContext pTxnContext,
			IFEBAValueObject criteriaVO, IFEBAValueObject arg2, QueryOperator pQueryOperator)
			throws CriticalException {
		CustomContentMgmtCritVO critVO=(CustomContentMgmtCritVO)criteriaVO;
		EBTransactionContext ebcontext = (EBTransactionContext) pTxnContext;
      
     	pQueryOperator.associate("bankId", ebcontext.getBankId());
     	pQueryOperator.associate("startDate",critVO.getStartDate());
     	pQueryOperator.associate("endDate",critVO.getEndDate());
     	pQueryOperator.associate("contType",critVO.getContType());
		
	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0,
			IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};  		
	}
	/**
	 * This method used to return the Query Identifier which is used for DAL
	 * execution
	 *
	 * @param pObjContext
	 * @param objInputOutput
	 * @param pObjTxnWM
	 * @return queryIdentifier
	 *
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {
        
		return "CustomContentListFetch";		
	}

	/**
	 *
	 * Executes the Query based criteria ( ORG_ID ) if the Result is NULL, then
	 * throws the exception
	 *
	 * @param objContext
	 * @param objInputOutput
	 * @param pObjTxnWM
	 * @return null
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws CriticalException
	 *
	 */

	@Override
	protected void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
		
		CustomContentMgmtEnqVO enquiryVO = (CustomContentMgmtEnqVO) objInputOutput;
	        try {
	            super.executeQuery(objContext, enquiryVO, objTxnWM);
	        } catch (BusinessException be) {
	            if (be.getErrorCode() != ErrorCodes.RECORD_NOT_FOUND) {
	                throw new CriticalException(objContext,
	                        FBAIncidenceCodes.CRIT_EXP_NO_USERS_FETCHED,
	                        "Critical Exception on fetching users list", be
	                                .getErrorCode(), be);
	            }
	        }		        	        
	}
	
	@Override
	 public boolean postQuery(FEBATransactionContext objContext,
	            IFEBAValueObject objQueryCrit, IFEBAValueObject objSummary,
	            QueryRecord queryRecord, IFEBAValueObject outputVO,
	            IFEBAValueObject objTxnWM) throws BusinessException,
	            BusinessConfirmation, CriticalException {

	        // return true by default-outputVO will get added to the result list if
	        // this method returns true.
		 CustomContentMgmtDetailsVO detailsVO = (CustomContentMgmtDetailsVO) outputVO;
		    // Check for user type and call method to handle shadow records
		 if(detailsVO!=null)
		 { try {
				// Check in S_CACOM to see if record is present
				S_CACMTAO.select(objContext, detailsVO.getObjectId());			
				 detailsVO.setIsShadowRecord("Y");
			  } catch (FEBATableOperatorException tableEx) {
				detailsVO.setIsShadowRecord("N");
			  }
		 }
	       	                 
	        return true;
	    }
	

}
