<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>
<feba2:Set value="${feba2:getValue('CustomLoanProdMaintenanceFG.DEL_MULTIPLE','formfield', pageContext)}" var="deleteMode"></feba2:Set>
<feba2:Set value="${feba2:select('deleteMode#true@@!deleteMode#false',pageContext)}" var="allChk"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'true')}" var="delete"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'false')}" var="search"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('deleteMode#DeleteCountryParameter@@!deleteMode#CountryParameterMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "Product Configuration"/>
<feba2:Page action="Finacle" name="CustomLoanProdMaintenanceFG" forcontrolIDs="Product Code:=CustomLoanProdMaintenanceFG.PRODUCT_CODE@@Product Category:=CustomLoanProdMaintenanceFG.PROD_CATEGORY@@Configuration Type:=CustomLoanProdMaintenanceFG.CONFIG_TYPE@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  
  <feba2:Set value="${feba2:select('delete#false@@search#true',pageContext)}" var="visible" />
  <feba2:Set value="${feba2:select('delete#Delete Currency Parameter@@search#Currency Parameter Maintenance',pageContext)}" var="heading" />
  <feba2:Set value="${feba2:getValue('CustomLoanProdMaintenanceFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize" />
  <feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible" />
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="title" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeading" text="Loan Product Maintenance" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
            <feba2:Col displaymode="N" identifier="C3" style="right">
              <feba2:field caption="Create New" id="CREATE_NEW" name="CREATE_NEW" style="formbtn_top" title="Create New" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC73" titleLC="LC73" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay17845338" style="alerttable" />
  <feba2:Set value="true" var="SearchPanel.visible" />
  <feba2:Section id="SearchPanel" collapsibleDisplay="allCollapse" displaymode="N" identifier="SearchPanel" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="TableHeader" style="whtboldtxt" text="Search Criteria" visible="true" isDownloadLink="false" isMenu="false" textLC="LC74" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.PRODUCT_CODE" id="productCode" name="productCode" text="Product Code:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC14249" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <%-- <feba2:field combomode="complex" id="productCode" key="CODE_TYPE=PCO" name="CustomLoanProdMaintenanceFG.PRODUCT_CODE" style="simpletext" tagHelper="CommonCodeDescTagHelper" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" /> --%>
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.PRODUCT_CODE" key="CODE_TYPE=PCO" name="CustomLoanProdMaintenanceFG.PRODUCT_CODE" select="true" title="Product Code" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.PROD_CATEGORY" id="productCategory" name="productCategory" text="Product Category:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC20000" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <%-- <feba2:field id="productCategory" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.PROD_CATEGORY" style="simpletext" tagHelper="CommonCodeDescTagHelper" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" /> --%>
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.PROD_CATEGORY" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.PROD_CATEGORY" select="true" title="Product Code" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.CONFIG_TYPE" id="configType" name="merchant" text="Configuration Type:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC9001054" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <%-- <feba2:field id="configType" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.CONFIG_TYPE" style="simpletext" tagHelper="CommonCodeDescTagHelper" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" /> --%>
              <feba2:field combomode="complex" id="CustomLoanProdMaintenanceFG.CONFIG_TYPE" key="CODE_TYPE=CFT" name="CustomLoanProdMaintenanceFG.CONFIG_TYPE" select="true" title="Configuration Type" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>
         
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section> 
  <feba2:Set value="true" var="NavPanel.visible" />
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Search" id="SEARCH" name="SEARCH" style="formbtn_top" title="Search" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC75" titleLC="LC75" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Clear" id="CLEAR" name="CLEAR" style="formbtn_last" title="Clear" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC76" titleLC="LC76" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isVisible}" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Loan Product List" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="listtopbigbg" headinglevel="h3" hideEmptyTable="true" id="CustomLoanProdListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="CustomLoanProdListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="${allChk}" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC20005" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
                <feba2:Header>
                  <feba2:Heading id="column7145547" sort="true" sortOrder="ASC" style="slno" visible="true" text="Select" textLC="LC38" />
                  <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Merchant ID" textLC="LC14250" />
                  <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Product Code" textLC="LC14250" />
                  <feba2:Heading id="column28061596" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Product Category" textLC="LC20001" />
                  <feba2:Heading id="column6744085" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Product SubCategory" textLC="LC20002" />
                  <feba2:Heading id="column1073282" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Configuration Type" textLC="LC14252" />
                  <feba2:Heading id="column1073288" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Minimum Amount" textLC="LC20003" />
                  <feba2:Heading id="column1073288" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Maximum Amount" textLC="LC20004" />
                </feba2:Header>
                <feba2:field id="CustomLoanProdMaintenanceFG.SELECTED_INDEX" name="CustomLoanProdMaintenanceFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Currency Parameter" visible="true" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" titleLC="LC319" />
                <feba2:field id="CustomLoanProdMaintenanceFG.MERCHANT_ID_ARRAY" key="CODE_TYPE=MID" name="CustomLoanProdMaintenanceFG.MERCHANT_ID_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" tagHelper="GenericDescriptionTagHelper" visible="true"/>
                <feba2:field id="CustomLoanProdMaintenanceFG.PRODUCT_CODE_ARRAY" key="CODE_TYPE=PCO" name="CustomLoanProdMaintenanceFG.PRODUCT_CODE_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" tagHelper="GenericDescriptionTagHelper" visible="true"/>
                <feba2:field id="CustomLoanProdMaintenanceFG.PROD_CATEGORY_ARRAY" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.PROD_CATEGORY_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" tagHelper="GenericDescriptionTagHelper" visible="true" />
                <feba2:field id="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY_ARRAY" name="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="CustomLoanProdMaintenanceFG.CONFIG_TYPE_ARRAY" key="CODE_TYPE=CFT" name="CustomLoanProdMaintenanceFG.CONFIG_TYPE_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" tagHelper="GenericDescriptionTagHelper" visible="true" />
                <feba2:field id="CustomLoanProdMaintenanceFG.MIN_AMOUNT_ARRAY" name="CustomLoanProdMaintenanceFG.MIN_AMOUNT_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="CustomLoanProdMaintenanceFG.MAX_AMOUNT_ARRAY" name="CustomLoanProdMaintenanceFG.MAX_AMOUNT_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="CustomLoanProdListing.GOTO_PAGE__" prevPage="CustomLoanProdListing.GOTO_PREV__" nextPage="CustomLoanProdListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
   <feba2:Set value="${!isVisible}" var="NavPanel1.visible" />
  <feba2:Section id="NavPanel1" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="stage3_downloadnav">
          	 <feba2:Col identifier="C1" style="">
              <feba2:field caption="Add Loan Interest Details" id="INTEREST_DETAILS" name="INTEREST_DETAILS" style="formbtn_top" title="Update" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC78" titleLC="LC78" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Update" id="UPDATE" name="UPDATE" style="formbtn_top" title="Update" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC78" titleLC="LC78" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Delete" id="DELETE" name="DELETE" style="formbtn_last" title="Delete" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC13" titleLC="LC13" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
    </feba2:Section>
          
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanProdMaintenanceFG" />
 <feba2:hidden name="CustomLoanProdMaintenanceFG.REPORTTITLE" id="CustomLoanProdMaintenanceFG.REPORTTITLE" value="CustomLoanProductList" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>