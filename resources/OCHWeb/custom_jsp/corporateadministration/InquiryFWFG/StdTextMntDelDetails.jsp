<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('InquiryFWFG.DISP_MODE','formfield', pageContext)}" var="dispMode"></feba2:Set>
<feba2:Set value="${feba2:equals(dispMode,'STATUS')}" var="isDelMultipleStatus"></feba2:Set>
<feba2:Set value="${feba2:equals(dispMode,'STATUS')}" var="isDelMultipleStatus"></feba2:Set>
<feba2:Set value="${feba2:getValue('InquiryFWFG.DISP_MODE','formfield', pageContext)}" var="dispMode"></feba2:Set>
<feba2:Set value="${feba2:select('isDelMultipleStatus#btn-printscreen.gif',pageContext)}" var="prntScreen"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isDelMultipleStatus#DeleteStandardText@@!isDelMultipleStatus#BankUserPreviewConf',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isDelMultipleStatus#Delete Multiple Status||LC66@@!isDelMultipleStatus#Preview Confirmation Details||LC64',pageContext)}"/>
<feba2:Page action="Finacle" name="InquiryFWFG" forcontrolIDs="">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb1" printScreen="${prntScreen}" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeader" text="${feba2:select('isDelMultipleStatus#Delete Multiple Status||LC66@@!isDelMultipleStatus#Preview Confirmation Details||LC64',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay27341365" style="alerttable" />
  <feba2:Section id="ListTable" identifier="ListingTable" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Standard Text List" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="gradientbgonelinewithouttxt" headinglevel="" hideEmptyTable="true" id="StdTextMntInqConfListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="StdTextMntInqConfListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="false" summary="" tableDisplayStyle="" tableStyle="fourlinbrd_100percent_feba" visible="true" width="100" captionLC="LC7254" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
                <feba2:Header>
                  <feba2:Heading id="column2583282" sortOrder="ASC" style="slno" text="Sl. No." textLC="LC68" />
                  <feba2:Heading id="column381299" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Bank ID" textLC="LC69" />
                  <feba2:Heading id="column15232416" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Text Name" textLC="LC7251" />
                  <feba2:Heading id="column25377109" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Text Type" textLC="LC7255" />
                  <feba2:Heading id="column11245030" sort="true" sortOrder="ASC" style="listboldtxtwithline" visible="${isDelMultipleStatus}" text="Status" textLC="LC8" />
                </feba2:Header>
                <feba2:field id="InquiryFWFG.STATUS_SELECTED_INDEX_ARRAY" name="InquiryFWFG.STATUS_SELECTED_INDEX_ARRAY" tableColStyle="listgreyrowtxtwithoutline" style="simpletext" tagHelper="TableSerialNumberTagHelper" />
                <feba2:field id="InquiryFWFG.BANK_ID_ARRAY" name="InquiryFWFG.BANK_ID_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="InquiryFWFG.TEXT_NAME_ARRAY" name="InquiryFWFG.TEXT_NAME_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="InquiryFWFG.TEXT_TYPE_ARRAY" key="CODE_TYPE=STXT" name="InquiryFWFG.TEXT_TYPE_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" />
                <feba2:field id="InquiryFWFG.STATUS_ARRAY" name="InquiryFWFG.STATUS_ARRAY" tableColStyle="listgreyrowtxtleftline" visible="${isDelMultipleStatus}" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" />
                <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="StdTextMntInqConfListing.GOTO_PAGE__" prevPage="StdTextMntInqConfListing.GOTO_PREV__" nextPage="StdTextMntInqConfListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${!isDelMultipleStatus}" var="Authorization.visible" />
  <feba2:Set value="${!isDelMultipleStatus}" var="Authorization.visible" />
  <feba2:Section id="Authorization" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails12074486" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper" visible="true">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization19174213" name="auth" tagHelper="AuthenticationTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Submit" id="DELETE_SUBMIT" name="DELETE_SUBMIT" style="formbtn_last" title="Submit" visible="${!isDelMultipleStatus}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC21" titleLC="LC21" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Back" id="CONF_BACK" name="CONF_BACK" style="formbtn_last" title="Back" visible="${!isDelMultipleStatus}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
            <feba2:Col identifier="C4" style="">
              <feba2:field caption="Back" id="GOTO_LISTSCREEN" name="GOTO_LISTSCREEN" style="formbtn_last" title="Back" visible="${isDelMultipleStatus}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="InquiryFWFG" />
 <feba2:hidden name="InquiryFWFG.REPORTTITLE" id="InquiryFWFG.REPORTTITLE" value="StdTextMntDelDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

