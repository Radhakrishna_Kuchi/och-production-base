<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@ page import="com.infosys.feba.framework.common.util.resource.PropertyUtil"%>
<%@include file="/jsp/topbar.jsp"%>
<feba2:Set value="True" var="DispForm.SubSection2.visible"></feba2:Set>
<feba2:Set value="True" var="DispForm.SubSection4.visible"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.USER_TYPE', 'FormField', pageContext)}" var="userType"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'1')}" var="isRet"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'2')}" var="isCorp"></feba2:Set>
<feba2:Set value="${feba2:equals(userType,'4')}" var="isBnkUsr"></feba2:Set>
<feba2:Set value="${feba2:equals(isChannelListVisible,'Y')}" var="channelVisible"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.CALL_MODE', 'FormField', pageContext)}" var="callMode"></feba2:Set>
<feba2:Set value="${feba2:equals(callMode,'5') or feba2:equals(callMode,'6') or feba2:equals(callMode,'7') or feba2:equals(callMode,'8') or feba2:equals(callMode,'9')}" var="isOption"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'APPROVALS')}" var="isApproval"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'CONF')}" var="isConf"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'VIEW_DETAILS')}" var="viewDetails"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CONF') or feba2:equalsIgnoreCase(viewMode,'APPROVALS')}" var="isConfOrIsApprovals"></feba2:Set>
<feba2:Set value="${!viewDetails}" var="Authorization.visible"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.PROVINCE', 'FormField', pageContext)}" var="stateId"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.CITY', 'FormField', pageContext)}" var="cityId"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.KECAMATAN', 'FormField', pageContext)}" var="kecId"></feba2:Set>

<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.FILE_SEQUENCE_NUMBER', 'FormField', pageContext)}" var="fileNo"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('isConfOrIsApprovals#BankUserPreviewConf@@isRet#RetailUserMaintenance@@isCorp#CorporateUserMaintenance@@isBnkUsr#BankUserMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Details||LC255',pageContext)}"/>
<feba2:Page action="Finacle" name="CustomLoanOriginationFG" forcontrolIDs="Download Details As=CustomLoanOriginationFG.OUTFORMAT@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar16082044" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="arrow" id="Image9349517" name="Image9349517" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC1788" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title26742489" text="${feba2:select('isConfOrIsApprovals#Preview Confirmation Details||LC64@@!isConf#View Details||LC255',pageContext)}" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay21624724" style="" />
  <feba2:Set value="true" var="DispForm.SubSection5.visible" />
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:SubSectHeader identifier="Header1">
          <feba2:caption id="Caption7210546" text="Loan Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1591" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset1" style="width100percent">
		  <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Application Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.APPLICATION_ID" name="CustomLoanOriginationFG.APPLICATION_ID" readonly="readonly" title="Application Id" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="App Created On:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.APP_CREATED_ON" name="CustomLoanOriginationFG.APP_CREATED_ON" readonly="readonly" title="App Created On" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Last Action Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.LAST_ACTION_DATE" name="CustomLoanOriginationFG.LAST_ACTION_DATE" readonly="readonly" title="Last Action Date" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Application Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.APPLICATION_STATUS" key="CODE_TYPE=APS" name="CustomLoanOriginationFG.APPLICATION_STATUS"  displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />           
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Days in Current Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.DAYS_IN_CURRENT_STATUS" name="CustomLoanOriginationFG.DAYS_IN_CURRENT_STATUS" readonly="readonly" title="Days in Current Status" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Credit Scoring Result:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.CREDIT_SCORING_RESULT" name="CustomLoanOriginationFG.CREDIT_SCORING_RESULT" readonly="readonly" title="Credit Scoring Result" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>

          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756932" name="Caption23756932" text="Requested Loan Amount:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.BALANCE_REQUESTED" name="CustomLoanOriginationFG.BALANCE_REQUESTED" readonly="readonly" title="Balance Requested" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23756933" name="Caption23756933" text="Approved Loan Amount:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1478" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
				<feba2:field id="CustomLoanOriginationFG.BALANCE_APPROVED" name="CustomLoanOriginationFG.BALANCE_APPROVED" readonly="readonly" title="Balance Approved" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" />            
				</feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2224752" name="Caption2224752" text="Loan Tenure:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1800" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.LOAN_TENURE" name="CustomLoanOriginationFG.LOAN_TENURE" readonly="readonly" title="Loan Tenure" visible="true" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1801" maxlength="32" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24776745" name="Caption24776745" text="Installment Per Month:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.INSTALLMENT_PER_MONTH" name="CustomLoanOriginationFG.INSTALLMENT_PER_MONTH" readonly="readonly" title="Installment Per Month" visible="true" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1803" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24776745" name="Caption24776745" text="Loan Type:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
            	<feba2:field id="CustomLoanOriginationFG.LOAN_TYPE" key="CODE_TYPE=PCO" name="CustomLoanOriginationFG.LOAN_TYPE" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
             </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24776745" name="Caption24776745" text="Keterangan Loan Type:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.KETETRANGAN_LOAN_TYPE" name="CustomLoanOriginationFG.KETETRANGAN_LOAN_TYPE" readonly="readonly" title="Keterangan Loan Type" visible="true" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1803" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption24776745" name="Caption24776745" text="Staff Name:" visible="true" isDownloadLink="false" isMenu="false" style="simpletext" textLC="LC1802" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.STAFF_NAME" name="CustomLoanOriginationFG.STAFF_NAME" readonly="readonly" title="Staff Name" visible="true" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" titleLC="LC1803" />
            </feba2:Col>
          </feba2:row>
                 
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:SubSectHeader identifier="Header2">
          <feba2:caption id="Caption21529100" text="Payroll Account Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1294" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption26522547" name="Caption26522547" text="Payroll Account Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1858" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field fldFormatter="CustomPayrollAccountNumberFormatter" id="CustomLoanOriginationFG.PAYROLL_ACCOUNT_NUMBER" name="CustomLoanOriginationFG.PAYROLL_ACCOUNT_NUMBER" title="CustomLoanOriginationFG.PAYROLL_ACCOUNT_NUMBER" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC3100" maxlength="" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption20751912" name="Caption20751912" text="Payroll Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1860" />
            </feba2:Col>
     		  <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.PAYROLL_DATE" name="CustomLoanOriginationFG.PAYROLL_DATE" readonly="readonly" title="Payroll Date" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1956" fldFormatter="CustomDateFormatter"/>
            </feba2:Col> 
          </feba2:row>
          <feba2:row identifier="Re3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption7603710" name="Caption7603710" text="E-KTP Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1862" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.KTP_NUMBER" name="CustomLoanOriginationFG.KTP_NUMBER" readonly="readonly" title="E-KTP Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1859" maxlength="" />
            </feba2:Col>
          </feba2:row>        
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection3" style="width100percent">
        <feba2:SubSectHeader identifier="Header3">
          <feba2:caption id="Caption25108396" text="Personal Information" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1303" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset9" style="width100percent">
          <feba2:row identifier="Ri1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23284554" name="Caption23284554" text="Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1878" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.NAME" name="CustomLoanOriginationFG.NAME" readonly="readonly" title="Name" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1958" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ri2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Email Address:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.EMAIL_ID" name="CustomLoanOriginationFG.EMAIL_ID" readonly="readonly" title="Email Address" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Education Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
			<feba2:field id="CustomLoanOriginationFG.EDUCATION_STATUS" key="CODE_TYPE=EDU" name="CustomLoanOriginationFG.EDUCATION_STATUS" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Total Income per month:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.TOTAL_INCOME_PER_MONTH" name="CustomLoanOriginationFG.TOTAL_INCOME_PER_MONTH" readonly="readonly" title="Total Income per month" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Total Expenses per month:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.TOTAL_EXPENSES_PER_MONTH" name="CustomLoanOriginationFG.TOTAL_EXPENSES_PER_MONTH" readonly="readonly" title="Total Expenses per month" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Credit Card ownership status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
            	<feba2:field id="CustomLoanOriginationFG.CREDIT_CARD_OWNERSHIP_STATUS" key="CODE_TYPE=YNF" name="CustomLoanOriginationFG.CREDIT_CARD_OWNERSHIP_STATUS" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />           
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Credit Card Operator:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomLoanOriginationFG.CREDIT_CARD_OPERATOR" key="CODE_TYPE=CCBK" name="CustomLoanOriginationFG.CREDIT_CARD_OPERATOR" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />                       
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Address Information:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.KTP_ADDRESS" name="CustomLoanOriginationFG.KTP_ADDRESS" readonly="readonly" title="Address Information" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Province:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomLoanOriginationFG.PROVINCE" name="CustomLoanOriginationFG.PROVINCE" tagHelper="GenericDescriptionTagHelper">
                <feba2:map>
                  <feba2:param name="key" value="CNTRY=ID" />
                  <feba2:param name="displayEvenIfAbsent" value="false" />
                  <feba2:param name="resourceName" value="StateCode" />
                </feba2:map>
              </feba2:field> 
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="City:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
			<feba2:field id="CustomLoanOriginationFG.CITY" key="CODE_TYPE=${stateId}" name="CustomLoanOriginationFG.CITY" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Kecamatan:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.KECAMATAN" key="CODE_TYPE=${cityId}" name="CustomLoanOriginationFG.KECAMATAN" readonly="readonly" title="Kecamatan" disable="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Kelurahan:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.KELURAHAN" name="CustomLoanOriginationFG.KELURAHAN" key="CODE_TYPE=${kecId}" readonly="readonly" title="Kelurahan" disable="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="RW:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.RW" name="CustomLoanOriginationFG.RW" readonly="readonly" title="RW" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri14" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="RT:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.RT" name="CustomLoanOriginationFG.RT" readonly="readonly" title="RT" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri15" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Home Ownership status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
			<feba2:field id="CustomLoanOriginationFG.HOUSE_OWNERSHIP_STATUS" key="CODE_TYPE=RES" name="CustomLoanOriginationFG.HOUSE_OWNERSHIP_STATUS" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri16" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Home telephone number availability:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
                <feba2:field id="CustomLoanOriginationFG.HOME_TELEPHONE_NUMBER_AVAILABILTY" key="CODE_TYPE=YNF" name="CustomLoanOriginationFG.HOME_TELEPHONE_NUMBER_AVAILABILTY" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />                                 
            </feba2:Col>
          </feba2:row>
		  <feba2:row identifier="Ri17" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption4506235" name="Caption4506235" text="Home telephone number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1880" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.HOME_TELEPHONE_NUMBER" name="CustomLoanOriginationFG.HOME_TELEPHONE_NUMBER" readonly="readonly" title="Home telephone number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1881" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection4" style="width100percent">
        <feba2:SubSectHeader identifier="Header4">
          <feba2:caption id="Caption7024328" text="Relative's Information" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1889" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset13" style="width100percent">
          <feba2:row identifier="Rm1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28405560" name="Caption28405560" text="Mother's Maiden Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.MOTHER_NAME" name="CustomLoanOriginationFG.MOTHER_NAME" readonly="readonly" title="Mother's Maiden Name" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Number of dependents:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.NO_OF_DEPENDENTS" name="CustomLoanOriginationFG.NO_OF_DEPENDENTS" readonly="readonly"  title="Number of dependents" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1893" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row>  
			<feba2:row identifier="Rm3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Marital Status:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
			<feba2:field id="CustomLoanOriginationFG.MARITAL_STATUS" key="CODE_TYPE=MST" name="CustomLoanOriginationFG.MARITAL_STATUS" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
            </feba2:Col>
          </feba2:row> 
		<feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Spouse's E-KTP number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.SPOUSE_KTP_NUMBER" name="CustomLoanOriginationFG.SPOUSE_KTP_NUMBER" readonly="readonly" title="Spouse's E-KTP number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1893" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>
          </feba2:row> 
		<feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Partner Nik:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.PARTNER_NIK" name="CustomLoanOriginationFG.PARTNER_NIK" readonly="readonly" title="Partner Nik" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1893" maxlength=""  />
            </feba2:Col>
          </feba2:row> 
		<feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Partner Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.PARTNER_NAME" name="CustomLoanOriginationFG.PARTNER_NAME" readonly="readonly" title="Partner Name" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1893" maxlength="" />
            </feba2:Col>
          </feba2:row>	      
        </feba2:rowset>
      </feba2:SubSection>
      <feba2:SubSection identifier="SubSection5" style="width100percent">
        <feba2:SubSectHeader identifier="Header4">
          <feba2:caption id="Caption7024328" text="Contact Information" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1889" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset13" style="width100percent">
          <feba2:row identifier="Rm1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28405560" name="Caption28405560" text="Relatives' Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.RELATIVE_NAME" name="CustomLoanOriginationFG.RELATIVE_NAME" readonly="readonly"  title="Relatives' Name" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Relationship with Relatives:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
				<feba2:field id="CustomLoanOriginationFG.RELATIVE_RELATIONSHIP" key="CODE_TYPE=RLT" name="CustomLoanOriginationFG.RELATIVE_RELATIONSHIP" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
            </feba2:Col>
          </feba2:row>
		<feba2:row identifier="Rm3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28405560" name="Caption28405560" text="Relative's Phone Number:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.RELATIVE_PHONE_NUMBER" name="CustomLoanOriginationFG.RELATIVE_PHONE_NUMBER" readonly="readonly" title="Relative's Phone Number" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />
            </feba2:Col>
          </feba2:row>               
        </feba2:rowset>
      </feba2:SubSection>
        <feba2:SubSection identifier="SubSection6" style="width100percent">
        <feba2:SubSectHeader identifier="Header4">
          <feba2:caption id="Caption7024328" text="Document Information" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1889" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset13" style="width100percent">
           <feba2:row identifier="Rm1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption28405560" name="Caption28405560" text="KTP:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1890" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomLoanOriginationFG.FU_FILE_NAME1" name="CustomLoanOriginationFG.FU_FILE_NAME1"  link="FORMSGROUP_ID__=CustomLoanOriginationFG#__EVENT_ID__= DOWNLOAD_FILE__#CustomLoanOriginationFG.FU_SEQUENCE_NUMBER={CustomLoanOriginationFG.FU_SEQUENCE_NUMBER1}#CustomLoanOriginationFG.FU_FILE_NAME={CustomLoanOriginationFG.FU_FILE_NAME1}"  linkstyle="bluelink" readonly="false" title="KTP" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />                       
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rm2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Terms & Condition:" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
             <feba2:field id="CustomLoanOriginationFG.FU_FILE_NAME1" name="CustomLoanOriginationFG.FU_FILE_NAME1"  link="FORMSGROUP_ID__=CustomLoanOriginationFG#__EVENT_ID__= DOWNLOAD_FILE__#CustomLoanOriginationFG.FU_SEQUENCE_NUMBER={CustomLoanOriginationFG.FU_SEQUENCE_NUMBER1}#CustomLoanOriginationFG.FU_FILE_NAME={CustomLoanOriginationFG.FU_FILE_NAME1}"  linkstyle="bluelink" readonly="false" title="Terms & Condition" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="false" titleLC="LC1891" />            
           </feba2:Col>
          </feba2:row> 
		<feba2:row identifier="Rm3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Surat Pengakuan Hutang:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
             <feba2:field id="CustomLoanOriginationFG.FU_FILE_NAME3" name="CustomLoanOriginationFG.FU_FILE_NAME3"  link="FORMSGROUP_ID__=CustomLoanOriginationFG#__EVENT_ID__= DOWNLOAD_FILE__#CustomLoanOriginationFG.FU_SEQUENCE_NUMBER={CustomLoanOriginationFG.FU_SEQUENCE_NUMBER3}#CustomLoanOriginationFG.FU_FILE_NAME={CustomLoanOriginationFG.FU_FILE_NAME3}"  linkstyle="bluelink" readonly="false" title="Permohonan Kredit" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />            
            </feba2:Col>
          </feba2:row>
		<feba2:row identifier="Rm4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption2012335" name="Caption2012335" text="Permohonan Fasilitas Pinang:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1892" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
            <feba2:field id="CustomLoanOriginationFG.FU_FILE_NAME2" name="CustomLoanOriginationFG.FU_FILE_NAME2"  link="FORMSGROUP_ID__=CustomLoanOriginationFG#__EVENT_ID__= DOWNLOAD_FILE__#CustomLoanOriginationFG.FU_SEQUENCE_NUMBER={CustomLoanOriginationFG.FU_SEQUENCE_NUMBER2}#CustomLoanOriginationFG.FU_FILE_NAME={CustomLoanOriginationFG.FU_FILE_NAME2}"  linkstyle="bluelink" readonly="false" title="Perjanjian Kredit" disable="false" isDownloadLink="false" isMenu="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC1891" />                        
            </feba2:Col>
          </feba2:row>		  
        </feba2:rowset>
      </feba2:SubSection>
   </feba2:SubSectionSet>
  </feba2:Section> 
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">          
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>          
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanOriginationFG" />
 <feba2:hidden name="CustomLoanOriginationFG.REPORTTITLE" id="CustomLoanOriginationFG.REPORTTITLE" value="CustomLoanOriginationDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>
