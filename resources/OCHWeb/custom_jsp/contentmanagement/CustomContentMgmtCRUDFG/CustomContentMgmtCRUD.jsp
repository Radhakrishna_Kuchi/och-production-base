<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomContentMgmtCRUDFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CREATE')}" var="isCreate"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'UPDATE')}" var="isUpdate"></feba2:Set>

<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "${feba2:select('isCreate#Add Content||LC9938@@isUpdate#Modify Content||LC9939',pageContext)}"/>
<feba2:Page action="Finacle" name="CustomContentMgmtCRUDFG" forcontrolIDs="Start Date:=CustomContentMgmtCRUDFG.START_DATE@@End Date:=CustomContentMgmtCRUDFG.END_DATE@@Content Type:=CustomContentMgmtCRUDFG.CONT_TYPE@@" enctype="multipart/form-data">
<% if(!isGroupletView){%>
	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
<feba2:script id="script18449348" language="JavaScript" src="scripts/module/contentmanagement/customDisplayFieldsCRUDCM.js" />
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar17474161" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="arrow" id="Image4237311" name="Arrow" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC1788" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title16538590" text="${feba2:select('isCreate#Add Content||LC9938@@isUpdate#Modify Content||LC9939',pageContext)}" upperCase="True" headinglevel="h1" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay31435030" style="" />
  <feba2:Section id="DispFormCollapsible" identifier="DispFormCollapsible" style="section_blackborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection collapsible="true" identifier="SubSection1" style="width100percent">
        <feba2:SubSectHeader identifier="Header1" style="header">
          <feba2:caption id="Caption30687916" style="whiteboldlink" text="Content Maintenance" title="Content Maintenance" isDownloadLink="false" isMenu="false" visible="true" textLC="LC1590" titleLC="LC1590" />
        </feba2:SubSectHeader>
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:row identifier="Ra1" style="formrow">          
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="CustomContentMgmtCRUDFG.START_DATE" id="LabelForControl30837569" required="True" name="LabelForControl30837569" text="Start Date:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9952" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.START_DATE" name="CustomContentMgmtCRUDFG.START_DATE" title="Start Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC9953" maxlength="" fldFormatter="DateFormatter" />
            </feba2:Col>  
          </feba2:row>
          <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
             <feba2:labelforcontrol displayDateFormat="True" forControlId="CustomContentMgmtCRUDFG.END_DATE" id="LabelForControl30837569" name="LabelForControl30837569" required="True" text="End Date:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9952" />
             </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.END_DATE" name="CustomContentMgmtCRUDFG.END_DATE" title="End Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC9953" maxlength="" fldFormatter="DateFormatter" />
             </feba2:Col>
          </feba2:row>
            <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.CONT_TYPE" id="LabelForControl25696146" name="LabelForControl25696146" required="True" text="Content Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1838" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.CONT_TYPE" key="CODE_TYPE=CTYP" name="CustomContentMgmtCRUDFG.CONT_TYPE" function="onChange=&quot;CheckCont();&quot;" select="True" title="Content Type" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1839" maxlength="56" />
            </feba2:Col>
          </feba2:row>
           <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.HEADER" id="LabelForControl18453554" name="LabelForControl18453554" text="Header:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.HEADER" name="CustomContentMgmtCRUDFG.HEADER"  style="querytextboxmedium" title="Heading" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="512" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.DESCRIPTION" id="LabelForControl18453554" name="LabelForControl18453554" text="Description:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.DESCRIPTION" name="CustomContentMgmtCRUDFG.DESCRIPTION"  style="querytextboxmedium" title="Description" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="512" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.TEXT_TYPE" id="LabelForControl18453554" name="LabelForControl18453554" text="Text Type:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
             <feba2:field id="CustomContentMgmtCRUDFG.TEXT_TYPE" key="CODE_TYPE=STXT" name="CustomContentMgmtCRUDFG.TEXT_TYPE"  select="True" title="Text Type" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1839" maxlength="56" />
            </feba2:Col>			
          </feba2:row>
            <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.MERCHANT_ID" id="LabelForControl18453554" name="LabelForControl18453554" text="Merchant Id:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtCRUDFG.MERCHANT_ID" key="CODE_TYPE=MRTY" name="CustomContentMgmtCRUDFG.MERCHANT_ID" select="false" title="Merchant Id" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1839" maxlength="56" />
           </feba2:Col>
          </feba2:row> 
          <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.AND_APP_URL" id="LabelForControl18453554" name="LabelForControl18453554" text="Android App URL:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.AND_APP_URL" name="CustomContentMgmtCRUDFG.AND_APP_URL"  style="querytextboxmedium" title="URL" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="150" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.PLAY_STORE_URL" id="LabelForControl18453554" name="LabelForControl18453554" text="Play Store URL:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.PLAY_STORE_URL" name="CustomContentMgmtCRUDFG.PLAY_STORE_URL"  style="querytextboxmedium" title="URL" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="150" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.IOS_APP_URL" id="LabelForControl18453554" name="LabelForControl18453554" text="IOS App URL:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.IOS_APP_URL" name="CustomContentMgmtCRUDFG.IOS_APP_URL"  style="querytextboxmedium" title="URL" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="150" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.IOS_STORE_URL" id="LabelForControl18453554" name="LabelForControl18453554" text="IOS Store URL:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.IOS_STORE_URL" name="CustomContentMgmtCRUDFG.IOS_STORE_URL"  style="querytextboxmedium" title="URL" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" visible="true" titleLC="LC9959" maxlength="150" />
            </feba2:Col>
          </feba2:row>          
           <feba2:row identifier="Ra11" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.FILE_PATH" id="LabelForControl18453554" name="LabelForControl18453554" text="Image:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
             <feba2:Col identifier="C2" style="querytextright">
             <feba2:field id="CustomContentMgmtCRUDFG.FILE_PATH" name="CustomContentMgmtCRUDFG.FILE_PATH" title="File Path" disable="false" style="txtfield_fl" tagHelper="FileBrowseTagHelper" titleLC="LC3625" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomContentMgmtCRUDFG.PRIORITY" id="LabelForControl18453554" name="LabelForControl18453554" text="Priority:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC9958" />
            </feba2:Col>
             <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtCRUDFG.PRIORITY" key="CODE_TYPE=PRI" name="CustomContentMgmtCRUDFG.PRIORITY" select="false" title="Merchant Id" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="false" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" visible="true" titleLC="LC1839" maxlength="56" />
            </feba2:Col>
          </feba2:row>
       </feba2:rowset>
      </feba2:SubSection>   
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C6" style="">
              <feba2:field caption="Continue" id="CONTINUE" name="CONTINUE" style="formbtn_last" title="Continue" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC62" titleLC="LC62" />
            </feba2:Col>
            <feba2:Col identifier="C7" style="">
              <feba2:field caption="Reset" id="ResetButton594870" style="formbtn_last" title="Reset" disable="false" name="Reset" tagHelper="ButtonTagHelper" visible="true" captionLC="LC2" titleLC="LC2" />
            </feba2:Col>
            <feba2:Col identifier="C8" style="">
              <feba2:field caption="Back" id="BACK_TO_RETRIEVE_LIST" name="BACK_TO_RETRIEVE_LIST" style="formbtn_last" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" visible="true" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomContentMgmtCRUDFG" />
 <feba2:hidden name="CustomContentMgmtCRUDFG.REPORTTITLE" id="CustomContentMgmtCRUDFG.REPORTTITLE" value="CustomContentMgmtCRUD" />  
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

