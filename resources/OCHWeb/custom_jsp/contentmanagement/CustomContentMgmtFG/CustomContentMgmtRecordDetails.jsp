<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomContentMgmtFG.CONT_TYPE', 'FormField', pageContext)}" var="contType"></feba2:Set>

<feba2:Set value="${feba2:equals(contType,'MRC')}" var="isMerchant"></feba2:Set>
<feba2:Set value="${feba2:equals(contType,'PRO')}" var="isPromo"></feba2:Set>
<feba2:Set value="${feba2:equals(contType,'SPL')}" var="isSplash"></feba2:Set>


<feba2:Set value="${!isViewDetails}" var="Authorization1.visible"></feba2:Set>

<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "View Content Details"/>
<feba2:Page action="Finacle" name="CustomContentMgmtFG" forcontrolIDs="">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar29368658" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow" id="Image33330951" name="Image33330951" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC228" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
            <feba2:title id="title26742489" text="View Content Details" headinglevel="h1" upperCase="false" visible="true" />
             </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay25251687" style="" /> 
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="Caption30764024" text="View Content Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">
          <feba2:RowSetHeader identifier="RowSetHeader1" style="notopborder">
            <feba2:caption id="Caption24223076" text="Content Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1590" />
          </feba2:RowSetHeader>
          <feba2:row identifier="Ra1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Start Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.START_DATE" name="CustomContentMgmtFG.START_DATE" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="End Date:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.END_DATE" name="CustomContentMgmtFG.END_DATE" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Content Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtFG.CONT_TYPE" key="CODE_TYPE=CTYP" name="CustomContentMgmtFG.CONT_TYPE" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>  
           <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Header:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isSplash}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.HEADER" name="CustomContentMgmtFG.HEADER" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isSplash}" maxlength="" />
            </feba2:Col>
          </feba2:row> 
           <feba2:row identifier="Ra5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Description:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isSplash}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.DESCRIPTION" name="CustomContentMgmtFG.DESCRIPTION" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isSplash}" maxlength="" />
            </feba2:Col>            
          </feba2:row>  
           <feba2:row identifier="Ra6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Text Type:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isPromo}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtFG.TEXT_TYPE" key="CODE_TYPE=STXT" name="CustomContentMgmtFG.TEXT_TYPE" visible="${isPromo}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>    
                   
           <feba2:row identifier="Ra7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Merchant Id:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtFG.MERCHANT_ID" key="CODE_TYPE=MRTY" name="CustomContentMgmtFG.MERCHANT_ID" visible="${isMerchant}" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>   
           <feba2:row identifier="Ra8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Andriod App URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.AND_APP_URL" name="CustomContentMgmtFG.AND_APP_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}" maxlength="" />
            </feba2:Col>            
          </feba2:row>
          <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Play Store URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.PLAY_STORE_URL" name="CustomContentMgmtFG.PLAY_STORE_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}" maxlength="" />
            </feba2:Col>            
          </feba2:row>
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="IOS App URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.IOS_APP_URL" name="CustomContentMgmtFG.IOS_APP_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}" maxlength="" />
            </feba2:Col>            
          </feba2:row>  
          <feba2:row identifier="Ra10" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="IOS Store URL:" isDownloadLink="false" isMenu="false" style="simpletext" visible="${isMerchant}" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.IOS_STORE_URL" name="CustomContentMgmtFG.IOS_STORE_URL" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="${isMerchant}" maxlength="" />
            </feba2:Col>            
          </feba2:row> 
            <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="File Name:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomContentMgmtFG.FILE_NAME" name="CustomContentMgmtFG.FILE_NAME" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
            </feba2:Col>
          </feba2:row>          
           <feba2:row identifier="Ra12" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field alt="front image" id="CustomContentMgmtFG.FILEOUTPUTSTREAM" name="CustomContentMgmtFG.FILEOUTPUTSTREAM" tagHelper="CardImageTagHelper" visible="false" />          
             </feba2:Col>
          </feba2:row> 
            <feba2:row identifier="Ra13" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23473562" name="Caption23473562" text="Priority:" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
               <feba2:field id="CustomContentMgmtFG.PRIORITY" key="CODE_TYPE=PRI" name="CustomContentMgmtFG.PRIORITY" visible="true" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="56" />
            </feba2:Col>
          </feba2:row>                    
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">           
            <feba2:Col identifier="C11" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>          
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomContentMgmtFG" />
 <feba2:hidden name="CustomContentMgmtFG.REPORTTITLE" id="CustomContentMgmtFG.REPORTTITLE" value="CustomContentMgmtRecordDetails" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>

