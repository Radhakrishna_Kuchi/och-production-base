var scripts = document.getElementsByTagName('script');
var myScript ="";
if(document.head){
	
	for(var i=0;i<document.head.childNodes.length;i++){
		var scripts1 = document.head.childNodes[i]; 

		if(scripts1.id && scripts1.id=="customDisplayFieldsCRUDCM"){
			myScript = scripts1;
			break;
		}
	}
	//myScript=document.head.childNodes[document.head.childNodes.length-1];
}
else{
	myScript=document.getElementsByTagName('head')[0].childNodes[document.getElementsByTagName('head')[0].childNodes.length-1];
	
	for(i=0;i<document.getElementsByTagName('head')[0].childNodes.length;i++){
		var scripts2 = document.getElementsByTagName('head')[0].childNodes[i]; 
		if(scripts2.id && scripts2.id=="customDisplayFieldsCRUDCM"){
			myScript = scripts2;
			break;
		}
	}
}

var queryString="";
if(myScript!=null && myScript.src!=null){
	queryString = myScript.src.replace(/^[^\?]+\??/,'');
}

var params = parseQuery( queryString );
function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      
      Params[key] = val;
       
   }

   return Params;
}

var groupletId = params.groupletId;	
var elementId1 = 'CustomContentMgmtCRUDFG';
var elementId2 = 'DispFormCollapsible';	
if(!isGroupletExecution(groupletId)){
groupletId="";
}	
if(groupletId && groupletId!=null && groupletId.length>0 && groupletId!="null" && groupletId!=undefined){
      elementId1 = groupletId+":"+elementId1;
      elementId2 = groupletId+":"+elementId2;
}

window.onload = function() { 
	      CheckCont();	
}

function CheckCont(){				
	var cntType = document.getElementById("CustomContentMgmtCRUDFG.CONT_TYPE").value;				
	if(cntType=="MRC")		
		{
		jQuery("[id='"+elementId2+".Ra12']").show();
		jQuery("[id='"+elementId2+".Ra7']").show();
		jQuery("[id='"+elementId2+".Ra8']").show();
		jQuery("[id='"+elementId2+".Ra9']").show();
		jQuery("[id='"+elementId2+".Ra10']").show();
		
		jQuery("[id='"+elementId2+".Ra4']").hide();
		jQuery("[id='"+elementId2+".Ra5']").hide();
		jQuery("[id='"+elementId2+".Ra6']").hide();
		}
	else if(cntType=='SPL')
		{
		jQuery("[id='"+elementId2+".Ra4']").show();
		jQuery("[id='"+elementId2+".Ra5']").show();
		
		jQuery("[id='"+elementId2+".Ra12']").hide();
		jQuery("[id='"+elementId2+".Ra7']").hide();
		jQuery("[id='"+elementId2+".Ra8']").hide();
		jQuery("[id='"+elementId2+".Ra9']").hide();
		jQuery("[id='"+elementId2+".Ra10']").hide();
		jQuery("[id='"+elementId2+".Ra6']").hide();
		}
	else if(cntType=='PRO')
		{
		jQuery("[id='"+elementId2+".Ra6']").show();
		
		jQuery("[id='"+elementId2+".Ra12']").hide();
		jQuery("[id='"+elementId2+".Ra7']").hide();
		jQuery("[id='"+elementId2+".Ra8']").hide();
		jQuery("[id='"+elementId2+".Ra9']").hide();
		jQuery("[id='"+elementId2+".Ra10']").hide();
		jQuery("[id='"+elementId2+".Ra4']").show();
		jQuery("[id='"+elementId2+".Ra5']").show();
		}
	else
	{
	jQuery("[id='"+elementId2+".Ra6']").hide();	
	jQuery("[id='"+elementId2+".Ra12']").hide();
	jQuery("[id='"+elementId2+".Ra7']").hide();
	jQuery("[id='"+elementId2+".Ra8']").hide();
	jQuery("[id='"+elementId2+".Ra9']").hide();
	jQuery("[id='"+elementId2+".Ra10']").hide();
	jQuery("[id='"+elementId2+".Ra4']").hide();
	jQuery("[id='"+elementId2+".Ra5']").hide();
	}
	
}