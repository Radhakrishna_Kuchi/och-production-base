package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomMerchantInquiryResponseVO extends RestHeaderFooterHandler{
	
private CustomMerchantInquiryOutputVO data;
	
	public CustomMerchantInquiryOutputVO getData() {
		return data;
	}

	public void setData(CustomMerchantInquiryOutputVO data) {
		this.data = data;
	}


}
