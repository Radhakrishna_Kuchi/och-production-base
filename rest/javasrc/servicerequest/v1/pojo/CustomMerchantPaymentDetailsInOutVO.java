package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value="Merchant payment details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomMerchantPaymentDetailsInOutVO {
	
	@ApiModelProperty(value = "The loan account id", required=true)
	private String merchantId;
	
	@ApiModelProperty(value = "The loan account id", required=true)
	private String merchantRefId;
	
	@ApiModelProperty(value = "The loan account id")
	private String promoCode;
	
	@ApiModelProperty(value = "The loan account id", required=true)
	private String selectedTenor;
	
	@ApiModelProperty(value = "The loan account id", required=true)
	private Double netPurchaseAmount;
	
	@ApiModelProperty(value = "The loan account id", required=true)
	private Double fullPurchaseAmount;
	
	@ApiModelProperty(value = "The loan account id")
	private Double feeAmount;
	
	@ApiModelProperty(value = "The loan account id")
	private Double totalPurchaseAmount;
	
	@ApiModelProperty(value = "The loan account id")
	private String purchaseRemarks;
	
	@ApiModelProperty(value = "The loan account id")
	private String txnRefNumber;
	
	@ApiModelProperty(value = "Payment Mode")
	private String paymentMode;
	
	@ApiModelProperty(value = "Refund Amount", required=true)
	private Double refundAmount;
	
	@ApiModelProperty(value = "Refund Remarks")
	private String refundRemarks;
	
	@ApiModelProperty(value = "Refund Ref Id")
	private String refundRefId;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantRefId() {
		return merchantRefId;
	}

	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getSelectedTenor() {
		return selectedTenor;
	}

	public void setSelectedTenor(String selectedTenor) {
		this.selectedTenor = selectedTenor;
	}

	public Double getNetPurchaseAmount() {
		return netPurchaseAmount;
	}

	public void setNetPurchaseAmount(Double netPurchaseAmount) {
		this.netPurchaseAmount = netPurchaseAmount;
	}

	public Double getFullPurchaseAmount() {
		return fullPurchaseAmount;
	}

	public void setFullPurchaseAmount(Double fullPurchaseAmount) {
		this.fullPurchaseAmount = fullPurchaseAmount;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Double getTotalPurchaseAmount() {
		return totalPurchaseAmount;
	}

	public void setTotalPurchaseAmount(Double totalPurchaseAmount) {
		this.totalPurchaseAmount = totalPurchaseAmount;
	}

	public String getPurchaseRemarks() {
		return purchaseRemarks;
	}

	public void setPurchaseRemarks(String purchaseRemarks) {
		this.purchaseRemarks = purchaseRemarks;
	}

	public String getTxnRefNumber() {
		return txnRefNumber;
	}

	public void setTxnRefNumber(String txnRefNumber) {
		this.txnRefNumber = txnRefNumber;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundRemarks() {
		return refundRemarks;
	}

	public void setRefundRemarks(String refundRemarks) {
		this.refundRemarks = refundRemarks;
	}

	public CustomMerchantPaymentDetailsInOutVO(String merchantId, String merchantRefId, String promoCode,
			String selectedTenor, Double netPurchaseAmount, Double fullPurchaseAmount, Double feeAmount,
			Double totalPurchaseAmount, String purchaseRemarks, String txnRefNumber) {
		super();
		this.merchantId = merchantId;
		this.merchantRefId = merchantRefId;
		this.promoCode = promoCode;
		this.selectedTenor = selectedTenor;
		this.netPurchaseAmount = netPurchaseAmount;
		this.fullPurchaseAmount = fullPurchaseAmount;
		this.feeAmount = feeAmount;
		this.totalPurchaseAmount = totalPurchaseAmount;
		this.purchaseRemarks = purchaseRemarks;
		this.txnRefNumber = txnRefNumber;
		this.paymentMode = paymentMode;
		this.refundAmount = refundAmount;
		this.refundRemarks = refundRemarks;
	}

	public CustomMerchantPaymentDetailsInOutVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRefundRefId() {
		return refundRefId;
	}

	public void setRefundRefId(String refundRefId) {
		this.refundRefId = refundRefId;
	}

	
	
}
