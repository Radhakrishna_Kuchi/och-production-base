package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanProductReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanProductReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomTenureDetailsVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Product Maintenance")
@ApiResponses(value = {
		@ApiResponse(code=400, message= ResourcePaths.HTTP_ERROR_400 , response = Header.class),
		@ApiResponse(code=401, message= ResourcePaths.HTTP_ERROR_401 , response = Header.class),
		@ApiResponse(code=403, message= ResourcePaths.HTTP_ERROR_403 , response = Header.class),
		@ApiResponse(code=405, message= ResourcePaths.HTTP_ERROR_405 , response = Header.class),
		@ApiResponse(code=415, message= ResourcePaths.HTTP_ERROR_415 , response = Header.class),
		@ApiResponse(code=422, message= ResourcePaths.HTTP_ERROR_422 , response = Header.class),
		@ApiResponse(code=500, message= ResourcePaths.HTTP_ERROR_500 , response = Header.class),
})
public class CustomLoanProductMaintenanceResource extends ResourceAuthenticator{
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	/**
	 * Authenticates if the corpid and bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the corpid and bankid is authenticated it returns false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");
		String userId = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userIdFromContext = (UserId) opcontext.getFromContextData(EBankingConstants.USER_ID);


		boolean isInValid;

		if (bank.getValue().equals(bankId) && userIdFromContext.getValue().equals(userId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}



	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_PRODUCT_MAINTENANCE_URL, auditFields = {"configType : config type input",
			"selectedAmount: selected amount input"
	})
	@ApiOperation(value="Fetch Loan Product Maintenance details based on config type and selected amount" , response=CustomLoanProductReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "Successfull Retrieval of loan Product  Maintenance Details" , response = CustomLoanProductReferencesResponseVO.class),
			@ApiResponse(code=404, message= "Loan Product Details do not exist"),
			@ApiResponse(code=500, message= "Internal Server Error")
	})
	public Response fetchRequest(
			@ApiParam(value = "Loan Product Maintenance configType input" ,required=true) @QueryParam("configType") String configType,
			@ApiParam(value = "Loan Product Maintenance selectedAmount input") @QueryParam("selectedAmount") String selectedAmount

			){
		CustomLoanProductReferencesResponseVO responseVO = new CustomLoanProductReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
			List<CustomLoanProductReferencesVO> outputVO = fetchLoanProductDetails(configType,selectedAmount, context);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO	, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request,response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomLoanProductReferencesVO> fetchLoanProductDetails(String configType, String selectedAmount,  IOpContext context)
			throws FEBAException {
		List<CustomLoanProductReferencesVO> outputVO = null;

		CustomLoanTenureEnquiryVO enquiryVO = (CustomLoanTenureEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanTenureEnquiryVO);
		if(null!=configType){
			enquiryVO.getCriteria().setConfigType(configType);
		}
		if(null!=selectedAmount){
			enquiryVO.getCriteria().setSelectedAmount(new FEBAUnboundString(selectedAmount));
		}
		outputVO = new ArrayList<>();
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanProductMaintenanceService"));
		enquiryVO = (CustomLoanTenureEnquiryVO)serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomLoanTenureDetailsVO> detailsListVO = enquiryVO.getResultList();

		ArrayList<CustomTenureDetailsVO> customTenDetListVO = new ArrayList<>();
		//comment

		
		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomLoanTenureDetailsVO detailsVO = detailsListVO.get(i);
			CustomTenureDetailsVO customTenDetVO = new CustomTenureDetailsVO();
			
			CustomLoanProductReferencesVO lnProdDetails = new CustomLoanProductReferencesVO();
			String currentLnRecId = "";
			String nextLnTexId = "1";
			
			if(i!=detailsListVO.size()-1){
				currentLnRecId=detailsListVO.get(i).getLnRecId().getValue();
				nextLnTexId=detailsListVO.get(i+1).getLnRecId().getValue();
			}
			
			customTenDetVO.setInterest(String.valueOf(detailsVO.getInterest().getValue()));
			customTenDetVO.setTenure(detailsVO.getTenor().getValue());
			
			if(nextLnTexId.equals(currentLnRecId)){
				customTenDetListVO.add(customTenDetVO);
				continue;
			}
			else{
				customTenDetListVO.add(customTenDetVO);
				lnProdDetails.setTenureDetails(customTenDetListVO);
				customTenDetListVO=new ArrayList<>();
				lnProdDetails.setProductCode(detailsVO.getProductCode().toString());
				lnProdDetails.setProductCategory(detailsVO.getProductCategory().toString());
				lnProdDetails.setProductSubCategory(detailsVO.getProductSubCategory().toString());
				lnProdDetails.setConfigType(detailsVO.getConfigType().toString());
				lnProdDetails.setMinAmount(detailsVO.getMinAmount().getAmount().getValue());
				lnProdDetails.setMaxAmount(detailsVO.getMaxAmount().getAmount().getValue());
				
				ArrayList<CodeReferencesVO> codeRefListVO = new ArrayList<>();
				FEBAUnboundString loanPurpose =detailsVO.getLnPurpose();
				if(loanPurpose.getLength()!=0){
					String [] lnPurpose = loanPurpose.toString().split("\\|");

					for (int j = 0; j < lnPurpose.length; j++) {
						CodeReferencesVO codeRefVO = new CodeReferencesVO();

						codeRefVO.setCmCode(lnPurpose[j]);

						String cmCodeDesc = RestCommonUtils.getCodeDescription(context, CustomResourceConstants.LOAN_PURPOSE_CODE_TYPE, lnPurpose[j]);
						codeRefVO.setCodeDescription(cmCodeDesc);

						codeRefListVO.add(codeRefVO);
					}
				}
				lnProdDetails.setLoanPurpose(codeRefListVO);
			}
			
			outputVO.add(lnProdDetails);
		}


		return outputVO;
	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();

		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211030, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211031, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));

		
		return restResponceErrorCode;
	}}

