package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanRepaymentListReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanRepaymentListReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanAccountDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanAccountEnquiryVO;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Repayment List")
@Path("/v1/banks/{bankid}/users/{userid}/custom/loans/{accountId}")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomFetchLoanRepaymentScheduleResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_REPAYMENT_SCHEUDLE_LIST_URL)
	@ApiOperation(value = "Loan Repayment Schedule List", response = CustomLoanRepaymentListReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Loan Details", response = CustomLoanRepaymentListReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Loan details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	public Response fetchRequest(@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Account Id input", required = true) @PathParam("accountId") String accountId) {

		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		CustomLoanRepaymentListReferencesResponseVO responseVO = new CustomLoanRepaymentListReferencesResponseVO();
		CustomLoanRepaymentListReferencesVO inOutVO = new CustomLoanRepaymentListReferencesVO();
		try {
			isUnAuthenticatedRequest();
			/*
			 * CustomLoanAccountDetailsVO detailsVO =
			 * (CustomLoanAccountDetailsVO)
			 * FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.
			 * CustomLoanAccountDetailsVO);
			 * 
			 * detailsVO.setAccountID(inOutVO.getAccountId());
			 * 
			 * final IClientStub serviceStub = ServiceUtil.getService(new
			 * FEBAUnboundString("CustomLoanRepaymentService")); detailsVO =
			 * (CustomLoanAccountDetailsVO) serviceStub.callService(opcontext,
			 * detailsVO, new FEBAUnboundString("fetch"));
			 */

			List<CustomLoanRepaymentListReferencesVO> outputVO = fetchLoanRepaymentList(accountId, opcontext);

			responseVO.setList(outputVO);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());

		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomLoanRepaymentListReferencesVO> fetchLoanRepaymentList(String accountID, IOpContext context)
			throws FEBAException {

		List<CustomLoanRepaymentListReferencesVO> outputVO = new ArrayList<CustomLoanRepaymentListReferencesVO>();

		CustomLoanAccountEnquiryVO enquiryVO = (CustomLoanAccountEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanAccountEnquiryVO);

		// enquiryVO.getAccountID().set(accountID);

		enquiryVO.getAccountID().set(accountID);

		final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanRepaymentService"));
		enquiryVO = (CustomLoanAccountEnquiryVO) serviceStub.callService(context, enquiryVO,
				new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomLoanAccountDetailsVO> detailsListVO = enquiryVO.getResultList();
		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomLoanAccountDetailsVO detailsVO = detailsListVO.get(i);
			CustomLoanRepaymentListReferencesVO pojoVO = new CustomLoanRepaymentListReferencesVO();

			pojoVO.setInstallmentAmount(new BigDecimal(detailsVO.getInstallmentAmount().getValue())
					.setScale(0, RoundingMode.HALF_UP).toString());

			Date duedate;
			try {
				/*
				 * 			05-08-2020
				 */
				System.out.println("detailsVO.getInstallmentDate().getValue() - "+detailsVO.getInstallmentDate().getValue());
				duedate = new SimpleDateFormat("dd-MM-yyyy")
						.parse(detailsVO.getInstallmentDate().getValue().substring(0, 10));
				System.out.println("duedate - "+duedate);
				FEBADate installmentDate = new FEBADate(duedate);
				System.out.println("installmentDate - "+installmentDate);
				installmentDate.setDateFormat("yyyy-MM-dd HH:mm:ss");
				System.out.println("installmentDate - "+installmentDate.toString());
				pojoVO.setInstallmentDate(installmentDate.toString());
			} catch (ParseException e) {
				throw new BusinessException(true, context, "LNINQ01", "Error in Date format in Repayment details.",
						null, 211303, null);
			}

			pojoVO.setInstallmentNo(detailsVO.getInstallmentNumber().getValue());
			pojoVO.setCurrency(detailsVO.getAccountCurrency().getValue());
			pojoVO.setInstallmentId(detailsVO.getInstallmentId().toString());
			pojoVO.setSrlNo(detailsVO.getSrlNo().toString());

			outputVO.add(pojoVO);

		}
		return outputVO;
	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();

		return restResponceErrorCode;
	}

}
