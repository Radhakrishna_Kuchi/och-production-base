package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAmountVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanHistoryReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanHistoryReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanHistoryDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanHistoryEnquiryVO;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import ch.qos.logback.classic.net.SyslogAppender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan History")
@Path("/v1/banks/{bankid}/users/{userid}/custom/loanHist/{accountId}")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomFetchLoanHistoryResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_HISTORY_URL)
	@ApiOperation(value = "Loan History Details", response = CustomLoanHistoryReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Loan History", response = CustomLoanHistoryReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Loan details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	public Response fetchRequest(@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Account Id input", required = true) @PathParam("accountId") String accountId,
			@ApiParam(value = "Start Date input") @QueryParam("startDate") String startDate,
			@ApiParam(value = "End Date input") @QueryParam("endDate") String endDate) {

		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		CustomLoanHistoryReferencesResponseVO responseVO = new CustomLoanHistoryReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			List<CustomLoanHistoryReferencesVO> outputVO = fetchLoanHistory(accountId, startDate, endDate, opcontext);
			responseVO.setData(outputVO);
		} catch (Exception e) {
			LogManager.logError(null, e);
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());

		}
		return Response.ok().entity(responseVO).build();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<CustomLoanHistoryReferencesVO> fetchLoanHistory(String accountID, String startDt, String endDt,
			IOpContext context) throws Exception {
		List<CustomLoanHistoryReferencesVO> outputVO = new ArrayList<>();
		FEBAUnboundString startDate = null;
		FEBAUnboundString endDate = null;
		try{
			System.out.println("fetchLoanHistory - accountID - "+accountID);
			CustomLoanHistoryEnquiryVO enquiryVO = (CustomLoanHistoryEnquiryVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomLoanHistoryEnquiryVO);
			System.out.println("startDt - "+startDt);
			System.out.println("endDt - "+endDt);
			if(null!=startDt)
			{
				startDate = new FEBAUnboundString(startDt);
			}
			if(null!=endDt)
			{
				endDate = new FEBAUnboundString(endDt);
			}
			enquiryVO.getCriteria().setAccountID(accountID);
			if ((FEBATypesUtility.isNotNullOrBlank(startDate)&& !FEBATypesUtility.isNotNullOrBlank(endDate)) 
					|| (!FEBATypesUtility.isNotNullOrBlank(startDate)&& FEBATypesUtility.isNotNullOrBlank(endDate))) {
				System.out.println("In if part");
				throw new BusinessException(context, "LNHS001",
						"If date is provided then both start and end date should be provided", 211085);
			} else if (FEBATypesUtility.isNotNullOrBlank(startDate) && FEBATypesUtility.isNotNullOrBlank(endDate)) {
				System.out.println("In else part");
				try {
					String userDate = context.getFromContextData("DATE_FORMAT").toString();
					DateFormat df = new SimpleDateFormat(userDate);
					Date sdate = df.parse(startDt);
					Date edate = df.parse(endDt);

					SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					String stDate = dateFormatter.format(sdate);
					String edDate = dateFormatter.format(edate);
					if (sdate.after(edate)) {
						throw new BusinessException(context, "LNHS002", "End date cannot be prior to the start date",
								103321);
					}

					else {
						enquiryVO.getCriteria().setStartDate(stDate);
						enquiryVO.getCriteria().setEndDate(edDate);
					}

				} catch (ParseException e) {
					e.printStackTrace();
					throw e;
				}

			}
			System.out.println("Before calling history service - ");
			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanHistoryService"));
			enquiryVO = (CustomLoanHistoryEnquiryVO) serviceStub.callService(context, enquiryVO,
					new FEBAUnboundString("fetch"));
			System.out.println("After calling history service - enquiryVO - "+enquiryVO);
			FEBAArrayList<CustomLoanHistoryDetailsVO> detailsListVO = enquiryVO.getResultList();
			int count = 1;

			int countOfPayoffTrans = 0;
			String payoffTranId = "";
			Double tranAmount =0.0;
			CustomAmountVO payOffTranAmountVO = new CustomAmountVO();

			for (Iterator iterator = detailsListVO.iterator(); iterator.hasNext();) {
				CustomLoanHistoryDetailsVO historyDetailsVO = (CustomLoanHistoryDetailsVO) iterator
						.next();
				if(historyDetailsVO.getTrnParticulars().toString().contains("Payoff")){
					++countOfPayoffTrans;
					payoffTranId=historyDetailsVO.getTrnId().getValue();
				}
			}
			System.out.println("countOfPayoffTrans = "+countOfPayoffTrans);
			System.out.println("payoffTranId = "+payoffTranId);
			if(countOfPayoffTrans==2){
				for (Iterator iterator = detailsListVO.iterator(); iterator.hasNext();) {
					CustomLoanHistoryDetailsVO historyDetailsVO = (CustomLoanHistoryDetailsVO) iterator
							.next();
					if(historyDetailsVO.getTrnId().getValue().equalsIgnoreCase(payoffTranId)){
						tranAmount=tranAmount+Double.valueOf(historyDetailsVO.getAmountValue().getValue());
					}
				}
				System.out.println("tranAmount = "+tranAmount);
				payOffTranAmountVO.setAmount(
						new BigDecimal(String.valueOf(tranAmount)).setScale(0, RoundingMode.HALF_UP).toString());
			}
			System.out.println("payOffTranAmountVO - "+payOffTranAmountVO);

			for (int i = 0; i < detailsListVO.size(); i++) {
				System.out.println("Inside loop number - "+i+" detailsListVO - "+detailsListVO);
				CustomLoanHistoryDetailsVO detailsVO = detailsListVO.get(i);
				CustomLoanHistoryReferencesVO pojoVO = new CustomLoanHistoryReferencesVO();
				System.out.println("detailsVO - "+detailsVO);
				CustomAmountVO amntVO = new CustomAmountVO();
				amntVO.setAmount(
						new BigDecimal(detailsVO.getAmountValue().toString()).setScale(0, RoundingMode.HALF_UP).toString());
				System.out.println("amntVO - "+amntVO);
				System.out.println("detailsVO.getCurrencyCode().toString() - "+detailsVO.getCurrencyCode().toString());

				amntVO.setCurrency(detailsVO.getCurrencyCode().toString());
				System.out.println("amntVO - "+amntVO);

				pojoVO.setPaymentAmount(amntVO);
				System.out.println("detailsVO.getTrnDt().toString() - "+detailsVO.getTrnDt().toString());

				pojoVO.setTxnDate(detailsVO.getTrnDt().toString());
				System.out.println("detailsVO.getValueDt().toString()"+detailsVO.getValueDt().toString());

				//pojoVO.setPaymentMethod(detailsVO.getTrnParticulars().toString());
				System.out.println("detailsVO.getValueDt().toString() - "+detailsVO.getValueDt().toString());

				pojoVO.setValueDate(detailsVO.getValueDt().toString());
				System.out.println("detailsVO.getTrnId().toString() - "+detailsVO.getTrnId().toString());

				pojoVO.setTrnId(detailsVO.getTrnId().toString());
				System.out.println("detailsVO.getTrnType().toString() - "+detailsVO.getTrnType().toString());

				pojoVO.setTrnType(detailsVO.getTrnType().toString());
				/*
				 * Changing the static Txn Particular to bahasa as per the banks request
				 * by Pratik_shah07
				 */
				System.out.println("detailsVO.getTrnType().toString() - "+detailsVO.getTrnType().toString());



				// Added below condition to skip adding disbursement transaction
				// Entry to the output list
				if (!detailsVO.getPartTrnType().toString().equalsIgnoreCase(CustomEBConstants.PART_TRAN_TYPE_DEBIT)) {
					
					if(detailsVO.getTrnParticulars().toString().contains("Payoff")){
						switch(countOfPayoffTrans) {
						case 1:
							pojoVO.setInstallmentNo(count++);
							pojoVO.setTxnParticulars("Pelunasan");
							outputVO.add(pojoVO);
							break;
						case 2:
							if(detailsVO.getTrnParticulars().toString().contains("Loan Payoff Payment")){
								pojoVO.setPaymentAmount(payOffTranAmountVO);
								pojoVO.setInstallmentNo(count++);
								pojoVO.setTxnParticulars("Pelunasan");
								outputVO.add(pojoVO);
							}
							break;
						}
					}else{
						pojoVO.setInstallmentNo(count++);
						pojoVO.setTxnParticulars("Autodebet");
						outputVO.add(pojoVO);
					}
					System.out.println("pojoVO - "+pojoVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return outputVO;
	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(103321, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211085, "BE", 400));
		return restResponceErrorCode;
	}
}


