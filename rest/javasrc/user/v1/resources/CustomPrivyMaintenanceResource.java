package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPrivyRegistrationPOJO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPrivyRegistrationResponsePOJO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.utils.insulate.ArrayList;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Privy Maintenance")
@Path("/v1/banks/{bankid}/users/{userid}/custom/privyregistrations")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomPrivyMaintenanceResource extends ResourceAuthenticator {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	@POST
	@MethodInfo(uri = CustomResourcePaths.PRIVY_MAINTENANCE_URL)
	@ApiOperation(value = "Register Privy Maintenance details")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfull Registeration of Privy Maintenance Details", response = CustomPrivyRegistrationResponsePOJO.class),
			@ApiResponse(code = 404, message = "Registeration of Privy Maintenance Details failed"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response registerPrivy(CustomPrivyRegistrationPOJO registerPOJO,
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId) {
		System.out.println("**inside registerPrivy method");
		CustomPrivyRegistrationResponsePOJO responseVO = new CustomPrivyRegistrationResponsePOJO();
		try {	
			isUnAuthenticatedRequest();
			CustomPrivyRegistrationVO enquiryVO = getCustomPrivyRegistrationVO(registerPOJO);
			responseVO = getSingleResponse(enquiryVO, "register");
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request, 0);
		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	@GET
	@MethodInfo(uri = CustomResourcePaths.PRIVY_MAINTENANCE_URL, auditFields = {
	"privyId : Privy Maintenance privyId input" })
	@ApiOperation(value = "Fetch Privy Maintenance details based on text name and text type")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfull Retrieval of Privy Maintenance Details", response = CustomPrivyRegistrationResponsePOJO.class),
			@ApiResponse(code = 404, message = "Privy Details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchByUserId(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Loan Application ID") @QueryParam("applicationId") String applicationId) {
		System.out.println("**inside fetchrequest method");
		CustomPrivyRegistrationResponsePOJO pojo = new CustomPrivyRegistrationResponsePOJO();
		FEBATransactionContext objTxnContext  = null;
		try {
			isUnAuthenticatedRequest();
			CustomPrivyRegistrationVO enquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);
			enquiryVO.setUserId(userId);
			IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
			objTxnContext = RestCommonUtils.getTransactionContext(context);

			if(RestCommonUtils.isNotNullorEmpty(applicationId)){
				enquiryVO.setApplicationId(new ApplicationNumber(applicationId));
			}else{
				throw new BusinessException(true, context, CustomEBankingIncidenceCodes.LN_APP_ID_MANDATORY,
						"Loan Application Id is null or invalid", null, CustomEBankingErrorCodes.LN_APP_ID_MANDATORY, null);
			}

			final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
			CustomPrivyRegistrationVO regVO = (CustomPrivyRegistrationVO)serviceCStub.callService(context, enquiryVO,
					new FEBAUnboundString("fetchByUserId"));

			pojo.setUserId(regVO.getUserId().getValue());
			pojo.setKtpNum(regVO.getKTP().getValue());
			pojo.setPrivyId(regVO.getPrivyId().getValue());
			pojo.setUserName(regVO.getName().getValue());
			
			if(RestCommonUtils.isNotNullorEmpty(pojo.getPrivyId())){
				new CustomLoanHistoryUpdateUtil().updateHistory(enquiryVO.getApplicationId(),
						"DIG_SIGN_COM", "", objTxnContext);
			}

			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
		return Response.ok().entity(pojo).build();
	}

	private CustomPrivyRegistrationResponsePOJO getSingleResponse(CustomPrivyRegistrationVO enquiryVO, String method)
			throws BusinessException, BusinessConfirmation, FEBAException {
		IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
		return getResponsePOJO((CustomPrivyRegistrationVO) serviceCStub.callService(context, enquiryVO,
				new FEBAUnboundString(method)));
	}

	private CustomPrivyRegistrationResponsePOJO getResponsePOJO(CustomPrivyRegistrationVO regVO) {
		CustomPrivyRegistrationResponsePOJO pojo = new CustomPrivyRegistrationResponsePOJO();
		if(regVO.getStage().toString().equals(CustomEBConstants.PRIVY_COM)){
			pojo.setUserId(regVO.getUserId().getValue());
			pojo.setKtpNum(regVO.getKTP().getValue());
			pojo.setPrivyId(regVO.getPrivyId().getValue());
			pojo.setUserName(regVO.getName().getValue());
		}else{
			pojo.setStatus("true");
			pojo.setMessage("EKYC registration completed successfully");
		}

		return pojo;
	}

	private CustomPrivyRegistrationVO getCustomPrivyRegistrationVO(CustomPrivyRegistrationPOJO registerPOJO) {
		CustomPrivyRegistrationVO enquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);
		enquiryVO.setApplicationId(registerPOJO.getApplicationId());
		enquiryVO.setSelfie(registerPOJO.getFile1().getFileData().replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", ""));
		enquiryVO.setFile2(registerPOJO.getFile2().getFileData().replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", ""));
		return enquiryVO;
	}

	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211102, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211103, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211104, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211105, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211106, "BE", 401));
		return restResponceErrorCode;
	}

}
