package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAuthenticationDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomForgotPasswordOnlineInputDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.RestResourceConstants;
import com.infosys.ebanking.rest.common.v1.pojo.ForgotPasswordOnlineResponseVO;
import com.infosys.ebanking.rest.common.v1.pojo.PasswordDetailsVO;
import com.infosys.ebanking.rest.common.v1.pojo.SecurityQuestionnaireVO;
import com.infosys.ebanking.rest.common.v1.resources.AbstractDirectBankingResource;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.AuthorizationInputVO;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.FrameworkTypesCatalogueConstants;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.Status;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.GenericFieldVO;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.types.entityreferences.PrincipalIDER;
import com.infosys.fentbase.types.primitives.LAFRequestType;
import com.infosys.fentbase.types.valueobjects.CUSRInoutVO;
import com.infosys.fentbase.types.valueobjects.DBNavListVO;
import com.infosys.fentbase.types.valueobjects.DBSectionVO;
import com.infosys.fentbase.types.valueobjects.ForgotPasswordOfflineVO;
import com.infosys.fentbase.types.valueobjects.LAFDisplayQuestionnaireVO;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.user.LoginAltFlowConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author Anju_AL Request will be raised for Forgot Password online for retail
 *         users
 */
@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApiResponses(value = { @ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 404, message = ResourcePaths.HTTP_ERROR_404, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })
@Api(value = "Forgot Password")

public class CustomForgotPasswordResource extends AbstractDirectBankingResource {
	public static final String USER_PRINCIPAL = "USER_PRINCIPAL";
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String CUSTOMRMLOGINALTFLOWSERVICE = "CustomRMLoginAltFlowService";
	private static final String CUSTOMFORGOTPSWDSERVICE = "CustomForgotPasswordService";
	public static final String CAMS_CALLS_SERVICE = "CustomCAMSCallService";
	@Context
	HttpServletRequest request;

	/**
	 * This method is used to create forgot password online request.
	 * 
	 * @param inputDetails
	 * @return Response
	 */

	@POST
	@ApiOperation(value = "Resets the password online incase the user has forgotten the password.", response = RestCommonSuccessMessageHandler.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/custom/users/forgot-password/reset")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successfully reset password", response = RestCommonSuccessMessageHandler.class),
			@ApiResponse(code = 401, message = "The authentication details required", response = RestCommonSuccessMessageHandler.class) })
	public Response forgotPasswordOnline(@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			CustomForgotPasswordOnlineInputDetailsVO inputDetails) {

		ForgotPasswordOnlineResponseVO wrapper = new ForgotPasswordOnlineResponseVO();
		Header header = new Header();
		int responseStatus = 201;

		RestCommonSuccessMessageHandler headerResponse = new RestCommonSuccessMessageHandler();

		IOpContext opcontext = null;
		LoginAltFlowVO loginAltflowVO = null;
		try {
			opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
			isValidBankId();

			settingContextData(opcontext, inputDetails);
			opcontext.setInContextData("REQUEST_TYPE",
					new LAFRequestType(RestResourceConstants.FORGOT_PASSWORD_ONLINE_REQST_ID));
			String mode = RestResourceConstants.FORGOT_PASSWORD_ONLINE_REQST_ID;
			String status = "";
			String navigation = mode + "Navigation.xml";

			loginAltflowVO = (LoginAltFlowVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.LoginAltFlowVO);
			loginAltflowVO.setNavigationSource(navigation);
			loginAltflowVO = getNavigationSections(loginAltflowVO, opcontext);

			DBNavListVO dbNavListVO = loginAltflowVO.getLoginAltFlowNavListVO();

			FEBAArrayList navigatinList = getNavigationList(dbNavListVO.getNavList());

			if (inputDetails != null) {

				if (inputDetails.getLoginUserId() != null) {
					loginAltflowVO = fetchFormFieldDetails(opcontext,
							RestResourceConstants.FORGOT_PASSWORD_ONLINE_USER_DETAILS);
					loginAltflowVO = populateUserDetails(loginAltflowVO, opcontext, inputDetails);

					loginAltflowVO.setIsServiceExRequest(FBAConstants.YNFLAG_Y);
					loginAltflowVO = createLAFRValidateUser(loginAltflowVO, opcontext);

					CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
							.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
					CustomLoanStatusVO statusVO = (CustomLoanStatusVO) FEBAAVOFactory
							.createInstance(CustomLoanStatusVO.class.getName());
					
					final IClientStub customLoanInquiryService = ServiceUtil
							.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
					enquiryVO.getCriteria().setApplicationStatus(CustomEBConstants.LOAN_CREATED);
					customLoanInquiryService.callService(opcontext, enquiryVO,
							new FEBAUnboundString("fetchForInquiry"));
					FEBAArrayList<CustomLoanApplnMasterDetailsVO> loanDetailsList = enquiryVO.getResultList();
				
					if (loanDetailsList.size() > 0) {
						try {
							final IClientStub customLoanStatusService = ServiceUtil
									.getService(new FEBAUnboundString("CustomLoanStatusService"));
							customLoanStatusService.callService(opcontext, statusVO,
									new FEBAUnboundString("fetchStatus"));
							status = CustomEBConstants.WITH_FACILITY;
						} catch (FEBAException e) {
							if(e.getErrorCode()==8504){
								status = CustomEBConstants.WITHOUT_FACILITY;
							}else{
								throw new BusinessException(opcontext, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
										EBankingErrorCodes.HOST_NOT_AVAILABLE);
							}
						}
						status = CustomEBConstants.WITH_FACILITY;
					} else if (loanDetailsList.size() == 0) {
						status = CustomEBConstants.WITHOUT_FACILITY;
					}

				} else {

					throw new BusinessException(opcontext, EBIncidenceCodes.USER_ID_BLANK, "Enter the user ID",
							EBankingErrorCodes.USR_ID_MANDATORY);
				}

//				custom pin validation check--start
				if (inputDetails.getPasswordDetails() != null) {
					if (RestCommonUtils.isNotNullorEmpty(inputDetails.getPasswordDetails().getSignOnPassword())) {
						customCheckPinforPattern(inputDetails.getPasswordDetails().getSignOnPassword(), opcontext);
					}
				}
//				custom pin validation check--send
				
				if (inputDetails.getAuthenticationDetails() != null) {
					if (inputDetails.getAuthenticationDetails().getDebitCardDetails() != null) {
						navigatinList.add(new FEBAUnboundString(CustomEBConstants.CLGDC));
					} else {
						navigatinList.add(new FEBAUnboundString(CustomEBConstants.CLGOTP));
					}

					loginAltflowVO = validateAuthenticationDetails(navigatinList,
							inputDetails.getAuthenticationDetails(), opcontext, loginAltflowVO);

				} else {
					responseStatus = 401;

					String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", opcontext);

					if ((pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.PINANG)
							|| pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.SAHABAT))
							&& (status.equalsIgnoreCase(CustomEBConstants.WITHOUT_FACILITY))) {

						navigatinList.add(new FEBAUnboundString(CustomEBConstants.CLGOTP));
						loginAltflowVO.setRegistrationStatus("N");
						AuthorizationInputVO authmodeDetails = getAuthenticationDetails(navigatinList, opcontext,
								loginAltflowVO);
						header.setAuthorization(authmodeDetails);
						headerResponse.setHeader(header);
					} else if ((pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.PINANG))
							&& (status.equalsIgnoreCase(CustomEBConstants.WITH_FACILITY))) {

						navigatinList.add(new FEBAUnboundString(CustomEBConstants.CLGDC));
						loginAltflowVO.setRegistrationStatus("Y");
						AuthorizationInputVO authmodeDetails = getAuthenticationDetails(navigatinList, opcontext,
								loginAltflowVO);
						header.setAuthorization(authmodeDetails);
						headerResponse.setHeader(header);
					} else if (pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.SAHABAT)
							&& (status.equalsIgnoreCase(CustomEBConstants.WITH_FACILITY))) {

						navigatinList.add(new FEBAUnboundString(CustomEBConstants.CLGOTP));
						loginAltflowVO.setRegistrationStatus("Y");
						AuthorizationInputVO authmodeDetails = getAuthenticationDetails(navigatinList, opcontext,
								loginAltflowVO);
						header.setAuthorization(authmodeDetails);
						headerResponse.setHeader(header);
					}

				}

				if (responseStatus != 401) {

					loginAltflowVO = validatePasswordDetails(loginAltflowVO, opcontext,
							inputDetails.getPasswordDetails());
					loginAltflowVO.getForgotPasswordOfflineVO()
							.setRequestStatus(new Status(LoginAltFlowConstants.REQUEST_STATUS_PENDING));
					loginAltflowVO = submitRequestDetails(loginAltflowVO, opcontext);

				}

			} else {
				throw new BusinessException(opcontext, EBIncidenceCodes.USER_ID_BLANK, "Enter the user ID",
						EBankingErrorCodes.USR_ID_MANDATORY);
			}

		} catch (Exception ex) {
			Logger.logError("ex" + ex.getStackTrace());
			RestResourceManager.handleFatalErrorOutput(request, response, ex, restResponseErrorCodeMapping());
		}

		if (responseStatus == 401) {
			forgotPasswordOutputDetail(wrapper, loginAltflowVO, responseStatus);
			RestResourceManager.updateHeaderFooterResponseData(headerResponse, request, response, 0);
			return Response.status(responseStatus).entity(wrapper).build();
		} else {
			RestResourceManager.updateHeaderFooterResponseData(headerResponse, request, response, 0);
			return Response.status(responseStatus).entity(headerResponse).build();
		}

	}

	private void settingContextData(IOpContext opcontext, CustomForgotPasswordOnlineInputDetailsVO inputDetails)
			throws CriticalException, BusinessException {

		opcontext.setInContextData(EBankingConstants.USER_PRINCIPAL,
				new UserPrincipal(inputDetails.getLoginUserId().toUpperCase()));
		CUSRInoutVO cusrInoutvo = (CUSRInoutVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CUSRInoutVO);
		try {
			IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CUSRInfoService"));
			cusrInoutvo = (CUSRInoutVO) serviceCStub.callService(opcontext, cusrInoutvo,
					new FEBAUnboundString("fetchPrimaryInfo"));
			if (cusrInoutvo != null) {
				opcontext.setInContextData(EBankingConstants.USER_ID, cusrInoutvo.getCusrOutputVO().getUserId());
				opcontext.setInContextData(EBankingConstants.CORPORATE_ID, cusrInoutvo.getCusrOutputVO().getOrgId());
				opcontext.getFromContextData(EBankingConstants.USER_TYPE);
				opcontext.setInContextData("USER_TYPE", cusrInoutvo.getCusrOutputVO().getUserType());
			}
		} catch (BusinessConfirmation e) {
			Logger.logError("e" + e.getStackTrace());
		}

	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();

	}

	/**
	 * Populate the response
	 * 
	 * @param opcontext
	 * @param data
	 * @param loginAltflowVO
	 * @param requestStatus
	 */
	private void forgotPasswordOutputDetail(ForgotPasswordOnlineResponseVO data, LoginAltFlowVO loginAltflowVO,
			int requestStatus) {

		DBSectionVO userDetails = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();

		if (userDetails != null) {
			FEBAHashList hashList = userDetails.getBasicFields();
			GenericFieldVO genericFieldVOObj = (GenericFieldVO) hashList.get(USER_PRINCIPAL);
			if (genericFieldVOObj != null && FEBATypesUtility.isNotNullOrBlank(genericFieldVOObj.getSValue())) {
				data.setLoginUserId(genericFieldVOObj.getSValue().getValue());
			}

			genericFieldVOObj = (GenericFieldVO) hashList.get("PIN");
			if (genericFieldVOObj != null && FEBATypesUtility.isNotNullOrBlank(genericFieldVOObj.getSValue())) {
				data.setSecurePin(genericFieldVOObj.getSValue().getValue());
			}

		}
		if (requestStatus == 401) {
			FEBAArrayList<LAFDisplayQuestionnaireVO> questionsList = loginAltflowVO.getQuestionDisplayVO()
					.getResultList();
			if (questionsList != null) {
				List<SecurityQuestionnaireVO> questions = new ArrayList<>();
				SecurityQuestionnaireVO securityQuestionnaireVO = null;
				LAFDisplayQuestionnaireVO configuredQs = null;
				for (int i = 0; i < questionsList.size(); i++) {
					securityQuestionnaireVO = new SecurityQuestionnaireVO();

					configuredQs = questionsList.get(i);

					securityQuestionnaireVO.setQuestion(configuredQs.getQuestion().toString());
					securityQuestionnaireVO.setQuestionId(configuredQs.getQuestionID().toString());
					questions.add(securityQuestionnaireVO);
				}

				data.setSecurityQuestions(questions);

			}
		}

	}

	/**
	 * 
	 * @param loginAltflowVO
	 * @param opcontext
	 * @param inputDetails
	 * @return
	 * @throws BusinessException
	 */
	protected LoginAltFlowVO populateUserDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext,
			CustomForgotPasswordOnlineInputDetailsVO inputDetails) throws BusinessException {

		DBSectionVO userDBSectionVO = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();

		FEBAHashList genericFieldList = userDBSectionVO.getBasicFields();

		GenericFieldVO genericFieldVOObj = (GenericFieldVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.GenericFieldVO);

		if (inputDetails.getLoginUserId() != null) {
			String fieldValue = null;
			if (RestCommonUtils.isNotNullorEmpty(inputDetails.getLoginUserId())) {
				fieldValue = inputDetails.getLoginUserId();
			} else {
				fieldValue = EBankingConstants.BLANK;
			}
			genericFieldVOObj.initialize();
			genericFieldVOObj.setSName(USER_PRINCIPAL);
			genericFieldVOObj.setSValue(fieldValue);
			genericFieldList.cloneAndPut(USER_PRINCIPAL, genericFieldVOObj);
			loginAltflowVO.getForgotPasswordOfflineVO().setUserPrincipal(fieldValue);

		}
		userDBSectionVO.setBasicFields(genericFieldList);
		userDBSectionVO.setSectionId(RestResourceConstants.FORGOT_PASSWORD_ONLINE_USER_DETAILS);

		loginAltflowVO.getForgotPasswordOfflineVO().setFpSectionVO(userDBSectionVO);
		loginAltflowVO.getForgotPasswordOfflineVO()
				.setRequestType((LAFRequestType) opcontext.getFromContextData(new FEBAUnboundString("REQUEST_TYPE")));

		return loginAltflowVO;
	}

	/**
	 * Validates the password details
	 * 
	 * @param loginAltflowVO
	 * @param opcontext
	 * @param passwordDetails
	 * @return
	 * @throws CriticalException
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws FEBATypeSystemException
	 */
	public LoginAltFlowVO validatePasswordDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext,
			PasswordDetailsVO passwordDetails) throws BusinessException {
		PasswordVO passwordVO = loginAltflowVO.getChangePwdVO();
		if (passwordDetails != null) {

			if (RestCommonUtils.isNotNullorEmpty(String.valueOf(passwordDetails.getPasswordType()))
					&& (passwordDetails.getPasswordType().equals("SPWD")
							|| passwordDetails.getPasswordType().equals("TPWD")
							|| passwordDetails.getPasswordType().equals("BOTH"))) {
				if ((String.valueOf(passwordDetails.getPasswordType()).equals("SPWD"))
						|| (String.valueOf(passwordDetails.getPasswordType()).equals("BOTH"))) {
					passwordVO.setSignOnFlg(new YNFlag(EBankingConstants.YES));

				}
				if ((String.valueOf(passwordDetails.getPasswordType()).equals("TPWD"))
						|| (String.valueOf(passwordDetails.getPasswordType()).equals("BOTH"))) {
					passwordVO.setTxnFlg(new YNFlag(EBankingConstants.YES));
				}
			} else {
				throw new BusinessException(opcontext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
						"Password Type is invalid. Valid values are BOTH, SPWD, TPWD.",
						EBankingErrorCodes.PASSWORD_TYPE_INVALID);
			}

			if (RestCommonUtils.isNotNullorEmpty(String.valueOf(passwordVO.getSignOnFlg()))) {
				if ((String.valueOf(passwordVO.getSignOnFlg()).equals(EBankingConstants.YES))
						&& !(String.valueOf(passwordDetails.getSignOnPassword())
								.equals(passwordDetails.getReTypesignOnPassword()))) {
					throw new BusinessException(opcontext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
							"Enter the sign on flag not valid.", EBankingErrorCodes.BLANK_SIGNON_PASSWORD_VALIDATION);
				}

				if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getSignOnPassword())) {
					passwordVO.setSignOnNewPwd(passwordDetails.getSignOnPassword());

				}

				if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getReTypesignOnPassword())) {
					passwordVO.setSignOnDupNewPwd(passwordDetails.getReTypesignOnPassword());

				}

			} else {
				throw new BusinessException(opcontext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
						"Enter the sign on password.", EBankingErrorCodes.BLANK_SIGNON_PASSWORD_VALIDATION);
			}
//			custom pin validation check--start
			if (passwordDetails!=null) {
				if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getSignOnPassword())) {
					customCheckPinforPattern(passwordDetails.getSignOnPassword(), opcontext);
				}
			}
//			custom pin validation check--send

			loginAltflowVO.setChangePwdVO(passwordVO);

		}
		return loginAltflowVO;
	}

	/**
	 * This method is used to validate the input user details section
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@Override
	protected LoginAltFlowVO createLAFRValidateUser(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMRMLOGINALTFLOWSERVICE));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("validateUserInputs"));

		return loginAltflowVO;
	}

	public LoginAltFlowVO validateAuthenticationDetails(FEBAArrayList navigationList,
			CustomAuthenticationDetailsVO authenticationDetails, IOpContext opcontext, LoginAltFlowVO loginAltflowVO)
					throws CriticalException, BusinessException, BusinessConfirmation {

		String authSection = null;
		for (int section = 0; section < navigationList.size(); section++) {
			if (navigationList.get(section).toString().equals("CLGOTP")) {

				authenticationOTPSection(navigationList.get(section).toString(), authenticationDetails, opcontext);
				authSection = navigationList.get(section).toString();
			} else if (navigationList.get(section).toString().equals("CLGDC")) {
				authenticationDebitCardSection(navigationList.get(section).toString(), authenticationDetails,
						opcontext);
				authSection = navigationList.get(section).toString();
			}
		}

		if (authSection == null) {

			throw new BusinessException(opcontext, EBIncidenceCodes.NAVIGATION_FILE_MISSING,
					"The setup is invalid. Contact the bank administrator.", EBankingErrorCodes.INVALID_SETUP);

		}

		return loginAltflowVO;
	}

	/**
	 * Get the authentication details input
	 * 
	 * @param navigationList
	 * @param opcontext
	 * @return
	 * @throws CriticalException
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws FEBATypeSystemException
	 */
	@Override
	public AuthorizationInputVO getAuthenticationDetails(FEBAArrayList navigationList, IOpContext opcontext,
			LoginAltFlowVO loginAltflowVO) throws CriticalException, BusinessException, BusinessConfirmation {
		AuthorizationInputVO authmodeDetails = new AuthorizationInputVO();
		ArrayList authenticationList = new ArrayList();

		for (int section = 0; section < navigationList.size(); section++) {

			if (navigationList.get(section).toString().equals("CLGOTP")) {

				authenticationList.add(navigationList.get(section).toString());
				generateOTP(opcontext, navigationList.get(section).toString(), loginAltflowVO);

			} else if (navigationList.get(section).toString().equals("CLGDC")) {

				authenticationList.add(navigationList.get(section).toString());
			}
		}

		if (authenticationList.isEmpty()) {
			throw new BusinessException(opcontext, EBIncidenceCodes.NAVIGATION_FILE_MISSING,
					"Authentication mode is not set properly in navigation xls.", EBankingErrorCodes.INVALID_SETUP);
		}

		if (authenticationList.size() == 1) {
			authmodeDetails.setFirstAuthorizationMode(authenticationList.get(0).toString());
		} else {
			authmodeDetails.setFirstAuthorizationMode(authenticationList.get(0).toString());
			authmodeDetails.setSecondAuthorizationMode(authenticationList.get(1).toString());
		}
		authmodeDetails.setWorkFlowRequired('N');
		authmodeDetails.setUserIdRequired('N');

		return authmodeDetails;

	}

	public void authenticationOTPSection(String sectionId, CustomAuthenticationDetailsVO authenticationDetails,
			IOpContext opcontext) throws CriticalException, BusinessException, BusinessConfirmation {
		LoginAltFlowVO loginAltflowVO = fetchFormFieldDetails(opcontext, sectionId);

		DBSectionVO userDBSectionVO = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();
		FEBAHashList genericFieldList = userDBSectionVO.getBasicFields();

		PrincipalIDER principalidER = (PrincipalIDER) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.PrincipalIDER);
		principalidER.setCorpId(opcontext.getFromContextData("CORPORATE_ID").toString());
		principalidER.setUserId(opcontext.getFromContextData("USER_ID").toString());
		loginAltflowVO.getLoginAltFlowUserDetailsVO().setPrincipalIDER(principalidER);

		GenericFieldVO genericFieldVOObj = (GenericFieldVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.GenericFieldVO);

		String fieldValue = null;
		if (RestCommonUtils.isNotNullorEmpty(authenticationDetails.getOneTimePassword())) {
			fieldValue = authenticationDetails.getOneTimePassword();
		} else {
			fieldValue = EBankingConstants.BLANK;
		}
		genericFieldVOObj.initialize();
		genericFieldVOObj.setSName("OTP");
		genericFieldVOObj.setSValue(fieldValue);
		genericFieldList.cloneAndPut("OTP", genericFieldVOObj);

		userDBSectionVO.setBasicFields(genericFieldList);
		userDBSectionVO.setSectionId(sectionId);

		loginAltflowVO.getForgotPasswordOfflineVO().setFpSectionVO(userDBSectionVO);

		getAuthenticateDetails(loginAltflowVO, opcontext);
	}

	@SuppressWarnings("unchecked")
	public void authenticationDebitCardSection(String sectionId, CustomAuthenticationDetailsVO authenticationDetails,
			IOpContext opcontext) throws CriticalException, BusinessException, BusinessConfirmation {
		LoginAltFlowVO loginAltflowVO = fetchFormFieldDetails(opcontext, sectionId);

		DBSectionVO userDBSectionVO = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();
		FEBAHashList<GenericFieldVO> genericFieldList = userDBSectionVO.getBasicFields();

		GenericFieldVO genericFieldVOObj = (GenericFieldVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.GenericFieldVO);

		String fieldValue = null;

		if (authenticationDetails.getDebitCardDetails() != null
				&& RestCommonUtils.isNotNullorEmpty(authenticationDetails.getDebitCardDetails().getDebitCardNumber())) {
			fieldValue = authenticationDetails.getDebitCardDetails().getDebitCardNumber();
		} else {
			fieldValue = EBankingConstants.BLANK;
		}
		genericFieldVOObj.initialize();
		genericFieldVOObj.setSName("CARD_NUMBER");
		genericFieldVOObj.setSValue(fieldValue);
		genericFieldList.cloneAndPut("CARD_NUMBER", genericFieldVOObj);

		if (authenticationDetails.getDebitCardDetails() != null && RestCommonUtils
				.isNotNullorEmpty(authenticationDetails.getDebitCardDetails().getDebitCardExpiryDate())) {
			fieldValue = authenticationDetails.getDebitCardDetails().getDebitCardExpiryDate();
		} else {
			fieldValue = EBankingConstants.BLANK;
		}
		genericFieldVOObj.initialize();
		genericFieldVOObj.setSName("CARD_EXP_DATE");
		genericFieldVOObj.setSValue(fieldValue);
		genericFieldList.cloneAndPut("CARD_EXP_DATE", genericFieldVOObj);

		if (authenticationDetails.getDebitCardDetails() != null
				&& RestCommonUtils.isNotNullorEmpty(authenticationDetails.getDebitCardDetails().getDebitKtpNo())) {
			fieldValue = authenticationDetails.getDebitCardDetails().getDebitKtpNo();
		} else {
			fieldValue = EBankingConstants.BLANK;
		}
		genericFieldVOObj.initialize();
		genericFieldVOObj.setSName("KTP_NUMBER");
		genericFieldVOObj.setSValue(fieldValue);
		genericFieldList.cloneAndPut("KTP_NUMBER", genericFieldVOObj);

		userDBSectionVO.setBasicFields(genericFieldList);
		userDBSectionVO.setSectionId(sectionId);

		loginAltflowVO.getForgotPasswordOfflineVO().setFpSectionVO(userDBSectionVO);

		CustomCAMSCallEnquiryVO camsEnqVO = (CustomCAMSCallEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO");
		camsEnqVO.getCriteria().setCardNumber(authenticationDetails.getDebitCardDetails().getDebitCardNumber());
		camsEnqVO.getCriteria().setExpiryDate(authenticationDetails.getDebitCardDetails().getDebitCardExpiryDate());
		camsEnqVO.getCriteria().setKtp(authenticationDetails.getDebitCardDetails().getDebitKtpNo());
		// Added for AGRO payroll
		camsEnqVO.getCriteria().setBankCode(authenticationDetails.getDebitCardDetails().getBankCode());
		camsEnqVO.getCriteria().setProductCode(authenticationDetails.getDebitCardDetails().getProductCode());
		
		camsEnqVO.getCriteria().setIsKTPCheckRequired(FEBAConstants.YES);

		// Cams Host Call
		final IClientStub camsServiceCStub = ServiceUtil.getService(new FEBAUnboundString(CAMS_CALLS_SERVICE));
		camsServiceCStub.callService(opcontext, camsEnqVO, new FEBAUnboundString("fetchAccountNumber"));
		getAuthenticateDetails(loginAltflowVO, opcontext);

	}

	public void generateOTP(IOpContext opcontext, String sectionId, LoginAltFlowVO loginAltflowVO)
			throws CriticalException, BusinessException, BusinessConfirmation {

		PrincipalIDER principalidER = (PrincipalIDER) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.PrincipalIDER);

		ForgotPasswordOfflineVO frtPwdOfflineVO = loginAltflowVO.getForgotPasswordOfflineVO();
		FEBAHashList hashList = frtPwdOfflineVO.getFpSectionVO().getBasicFields();
		MobileNumber phoneNumber = null;

		if (hashList.size() > 0) {
			phoneNumber = new MobileNumber(((GenericFieldVO) hashList.get(USER_PRINCIPAL)).getSValue().toString());
			loginAltflowVO.getForgotPasswordOfflineVO().setMobileNumber(phoneNumber);
			principalidER.setCorpId(opcontext.getFromContextData("CORPORATE_ID").toString());
			principalidER.setUserId(opcontext.getFromContextData("USER_ID").toString());
		}

		loginAltflowVO.getLoginAltFlowUserDetailsVO().setPrincipalIDER(principalidER);
		loginAltflowVO.getForgotPasswordOfflineVO().getFpFieldDetailsVO().setSectionId(sectionId);

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMFORGOTPSWDSERVICE));
		serviceCStub.callService(opcontext, loginAltflowVO, new FEBAUnboundString("generateAuthAccessCode"));

	}

	/**
	 * This method is used to insert the offline data to request details table
	 * 
	 * @param loginAltflowVO
	 * @param opcontext
	 * @return
	 * @throws CriticalException
	 * @throws BusinessException
	 * @throws BusinessConfirmation
	 * @throws FEBATypeSystemException
	 */
	protected LoginAltFlowVO insertUserRecordDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoginAltFlowService"));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("insertOfflineRequestDetails"));

		return loginAltflowVO;
	}

	/**
	 * This method is used to set the new passwords
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@Override
	protected LoginAltFlowVO setPassword(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMRMLOGINALTFLOWSERVICE));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("setPassword"));

		return loginAltflowVO;
	}

	/**
	 * This method is used to submit the Forgot Password online request
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@Override
	protected LoginAltFlowVO submitRequestDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		loginAltflowVO = updateLAFRStatusValidateUser(loginAltflowVO, opcontext);
		loginAltflowVO = setPassword(loginAltflowVO, opcontext);
		loginAltflowVO.getForgotPasswordOfflineVO()
				.setRequestStatus(RestResourceConstants.FORGOT_PASSWORD_ONLINE_REQUEST_STATUS);
		logout(opcontext);

		return loginAltflowVO;
	}
//	added for custom pin validation -start
	private void customCheckPinforPattern(String password,IOpContext opcontext)
			throws BusinessException {
		if (password.matches("\\d+")) {
			int first = Integer.valueOf(String.valueOf(password.charAt(0)));
			String patern;
			if (first>0) {
				patern = String.valueOf(first * 111111);
			}else{
				patern = "000000";
			}
			if (password.equalsIgnoreCase(patern)) {
				System.out.println("same pin");
				throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
						"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
			}

			for (int i = 1; i < password.length(); i++) {
				if (Integer.valueOf(String.valueOf(password.charAt(i))) == (first + 1)) {
					first++;
				} else {
					return;
				}
			}			
			throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
					"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
		}
	}
//	added for custom pin validation -end
}
