package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCAMSCallReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDeviceRegistrationRetailInputVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDeviceDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationOutputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationResponseVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationRetailInputVO;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.AuthorizationInputVO;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.transaction.common.TransactionConstants;
import com.infosys.feba.framework.transaction.util.TransactionContextFactory;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.types.primitives.OTP;
import com.infosys.fentbase.types.valueobjects.AuthenticationVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Device Registration")

@ApiResponses(value = { @ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 404, message = ResourcePaths.HTTP_ERROR_404, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })
public class CustomDeviceRegistrationRetailResource extends ResourceAuthenticator {

	@Context
	protected HttpServletRequest request;

	@Context
	UriInfo uriInformation;

	RestCommonSuccessMessageHandler headerResponse = new RestCommonSuccessMessageHandler();
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String DEVICE_REGISTRATION_SERVICE = "CustomDeviceRegistrationService";
	public static final String MODIFY = "modify";
	public static final String CAMS_CALLS_SERVICE = "CustomCAMSCallService";

	public static final String CUSTOMRMLOGINALTFLOWSERVICE = "CustomRMLoginAltFlowService";
	public static final String USER_PHONE_NUM_SERVICE = "CustomUserPhoneNumService";
	public static final String FETCH = "fetch";
	String status = "";

	@PUT
	@ApiOperation(value = "Device Details updation,when device is reinstalled or device is changed", response = DeviceRegistrationResponseVO.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/deviceregistration", auditFields = {
			"deviceId:deviceId" })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The device details registered successfully.", response = DeviceRegistrationResponseVO.class),
			@ApiResponse(code = 401, message = "The authorization and user details Required", response = DeviceRegistrationResponseVO.class) })

	public Response updateDeviceDetails(
			@ApiParam(value = "Retail user id", required = true) @PathParam("userid") String userId,
			CustomDeviceRegistrationRetailInputVO inputVO) {

		DeviceRegistrationResponseVO wrapper = new DeviceRegistrationResponseVO();
		DeviceRegistrationOutputVO detailsVO = new DeviceRegistrationOutputVO();
		int responseStatus = 200;
		Header header = new Header();

		AuthorizationInputVO authmodeDetails = new AuthorizationInputVO();
		try {
			isUnAuthenticatedRequest();

			IOpContext opContext = (IOpContext) request.getAttribute("OP_CONTEXT");

			String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", opContext);
			FEBATransactionContext txnContext = null;
			txnContext = TransactionContextFactory.getInstance(TransactionConstants.EB_TXN_CONTEXT);

			txnContext.setBankId((BankId) opContext.getFromContextData(FBAConstants.STRING_BANK_ID));
			txnContext.setUserId(new UserId(userId));

			DeviceDetailsVO deviceDetailsVO = (DeviceDetailsVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.DeviceDetailsVO);
			
			/*
			 * To check if Authorization Details are null and isHPNumberFlag is Y and the User is without Facility 
			 */
			String isHPNumberChanged ="N";
			if(RestCommonUtils.isNotNull(inputVO.getIsHPNumberChanged())){
				isHPNumberChanged=inputVO.getIsHPNumberChanged();
			}

			if (inputVO.getDeviceDetails() == null) {
				throw new BusinessException(true, opContext, "Device details mandatory", "error", null, 211053, null);
			}
			
			
			if (inputVO.getDeviceDetails() != null) {
				deviceDetailsVO.setUserID(userId);
				if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceDetails().getDeviceId())) {
					deviceDetailsVO.setDeviceId(inputVO.getDeviceDetails().getDeviceId());
				}
				if(RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceDetails().getMobileNo())){
					deviceDetailsVO.setMobileNumber(new MobileNumber(inputVO.getDeviceDetails().getMobileNo()));
				}

				CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
						.createInstance(CustomLoanApplicationEnquiryVO.class.getName());

				CustomLoanStatusVO statusVO = (CustomLoanStatusVO) FEBAAVOFactory
						.createInstance(CustomLoanStatusVO.class.getName());

				final IClientStub customLoanInquiryService = ServiceUtil
						.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
				enquiryVO.getCriteria().setApplicationStatus(CustomEBConstants.LOAN_CREATED);
				customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
				FEBAArrayList<CustomLoanApplnMasterDetailsVO> loanDetailsList = enquiryVO.getResultList();

				if (inputVO.getAuthorizationDetails() != null) {

					if (inputVO.getAuthorizationDetails().getCardDetails() != null) {

						validateCardDetails(opContext, inputVO.getAuthorizationDetails().getCardDetails());

						CustomCAMSCallEnquiryVO camsEnqVO = (CustomCAMSCallEnquiryVO) FEBAAVOFactory.createInstance(
								"com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO");
						camsEnqVO.getCriteria()
								.setCardNumber(inputVO.getAuthorizationDetails().getCardDetails().getCardNumber());
						camsEnqVO.getCriteria()
								.setExpiryDate(inputVO.getAuthorizationDetails().getCardDetails().getExpiryDate());
						camsEnqVO.getCriteria().setKtp(inputVO.getAuthorizationDetails().getCardDetails().getKtp());
						
						// Added for AGRO payroll
						camsEnqVO.getCriteria().setBankCode(inputVO.getAuthorizationDetails().getCardDetails().getBankCode());
						camsEnqVO.getCriteria().setProductCode(inputVO.getAuthorizationDetails().getCardDetails().getProductCode());
						

						final IClientStub camsServiceCStub = ServiceUtil
								.getService(new FEBAUnboundString(CAMS_CALLS_SERVICE));
						camsEnqVO = (CustomCAMSCallEnquiryVO) camsServiceCStub.callService(opContext, camsEnqVO,
								new FEBAUnboundString("fetchAccountNumber"));
						CustomCAMSCallDetailsVO camsDetVO = camsEnqVO.getDetails();

						if (camsDetVO != null) {

							mapDeviceDetails(inputVO.getDeviceDetails(), deviceDetailsVO, opContext);
						}
					} else {
						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)) {

							validateOTP(opContext, deviceDetailsVO,
									inputVO.getAuthorizationDetails().getOneTimePassword());

						}
						mapDeviceDetails(inputVO.getDeviceDetails(), deviceDetailsVO, opContext);
					}

				} else {

					if (loanDetailsList.size() > 0) {
						try {
							final IClientStub customLoanStatusService = ServiceUtil
									.getService(new FEBAUnboundString("CustomLoanStatusService"));
							customLoanStatusService.callService(opContext, statusVO,
									new FEBAUnboundString("fetchStatus"));
							status = CustomEBConstants.WITH_FACILITY;
						} catch (FEBAException e) {
							if(e.getErrorCode()==8504){
								status = CustomEBConstants.WITHOUT_FACILITY;
							}else{
								throw new BusinessException(opContext, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
										EBankingErrorCodes.HOST_NOT_AVAILABLE);
							}
						}
						if(isHPNumberChanged.equalsIgnoreCase("Y")
								&& status.equalsIgnoreCase(CustomEBConstants.WITHOUT_FACILITY)){
							throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.HP_NUBER_CHANGED,
									"User HP Number has Changed. Call HP Number Update Service", null, CustomEBankingErrorCodes.HP_NUMBER_CHANGED, null);
						}
						responseStatus = 401;

						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)
								&& status.equals(CustomEBConstants.WITH_FACILITY)) {
							authmodeDetails.setFirstAuthorizationMode("KTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
						}
						else{
							authmodeDetails.setFirstAuthorizationMode("OTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
							generateOTP(opContext, deviceDetailsVO);

							responseStatus = 401;
						}
						

					} else {
						if(isHPNumberChanged.equalsIgnoreCase("Y")){
							throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.HP_NUBER_CHANGED,
									"User HP Number has Changed. Call HP Number Update Service", null, CustomEBankingErrorCodes.HP_NUMBER_CHANGED, null);
						}
						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)
								|| pinangOrSahabat.equals(CustomEBConstants.SAHABAT)) {
							status = CustomEBConstants.WITHOUT_FACILITY;
							authmodeDetails.setFirstAuthorizationMode("OTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
							generateOTP(opContext, deviceDetailsVO);

							responseStatus = 401;
						}
					}
					
				}

				detailsVO.setUserId(userId);
				detailsVO.setMobileNo(inputVO.getDeviceDetails().getMobileNo());
				detailsVO.setDeviceType(inputVO.getDeviceDetails().getDeviceType());
				detailsVO.setDeviceName(inputVO.getDeviceDetails().getDeviceName());
				detailsVO.setDeviceId(inputVO.getDeviceDetails().getDeviceId());
				detailsVO.setEncryptedLicense(deviceDetailsVO.getEncryptedLicense().toString());
			}

			wrapper.setData(detailsVO);

		} catch (Exception e) {
			
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}

		RestResourceManager.updateHeaderFooterResponseData(wrapper, request, response, 0);

		return Response.status(responseStatus).entity(detailsVO).build();

	}

	protected static void mapDeviceDetails(DeviceRegistrationRetailInputVO inputVO, DeviceDetailsVO deviceDetailsVO,
			IOpContext opContext) throws CriticalException, BusinessException, BusinessConfirmation {

		if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceName())) {
			deviceDetailsVO.setDeviceName(inputVO.getDeviceName());

		}
		if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceId())) {
			deviceDetailsVO.setDeviceId(inputVO.getDeviceId());

		}

		final IClientStub customDeviceRegService = ServiceUtil
				.getService(new FEBAUnboundString(DEVICE_REGISTRATION_SERVICE));

		customDeviceRegService.callService(opContext, deviceDetailsVO, new FEBAUnboundString(MODIFY));

	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();
	}

	public boolean validateCardDetails(IOpContext opContext, CustomCAMSCallReferencesVO cardDetails)
			throws BusinessException {
		if (cardDetails.getCardNumber() == null || cardDetails.getCardNumber() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.IDENTIFICATION_CARD_MANDATORY,
					"Enter the card number.", EBankingErrorCodes.CARD_NO_MANDATORY);
		}
		if (cardDetails.getExpiryDate() == null || cardDetails.getExpiryDate() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.INVALID_EXPIRY_DATE, "Enter the Card Expiry date",
					EBankingErrorCodes.ENTER_EXP_DATE);

		}
		if (cardDetails.getKtp() == null || cardDetails.getKtp() == "") {
			throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.KTP_CARD_NUMBER_EMPTY,
					"KTP Number empty.", null, CustomEBankingErrorCodes.KTP_CARD_NUMBER_EMPTY, null);

		}
		return true;
	}

	/**
	 * This method generateOTP autherization mode.
	 *
	 * @param IOpContext
	 * @return TransactionAuthorizationModeVO
	 *
	 * @throws BusinessException,CriticalException,BusinessConfirmation,
	 *             FEBATypeSystemException
	 *
	 */

	public AuthenticationVO generateOTP(IOpContext opContext, DeviceDetailsVO detailsVO)
			throws CriticalException, BusinessException, BusinessConfirmation {

		AuthenticationVO authenticationVO = (AuthenticationVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.AuthenticationVO);
		//Populate authentication VO
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setUserPrincipal(new UserPrincipal(detailsVO.getMobileNumber().getValue()));
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setUserId(detailsVO.getUserID());
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setCorpId(detailsVO.getUserID().toString());
		authenticationVO.setBankID((BankId) opContext.getFromContextData(FBAConstants.STRING_BANK_ID));

		opContext.setOperatingMode(new FEBAOperatingMode("C"));
		if(status !=null){
			if(status.equals(CustomEBConstants.WITH_FACILITY)){
				authenticationVO.setIsFallbackRequired("Y");
			}
			else{
				authenticationVO.setIsFallbackRequired("N");
			}
		}

		final IClientStub service = ServiceUtil.getService(new FEBAUnboundString("CustomRMAuthenticationService"));
		service.callService(opContext, authenticationVO, new FEBAUnboundString("generateOTP"));
		return authenticationVO;

	}

	public void validateOTP(IOpContext opContext, DeviceDetailsVO detailsVO, String userEnteredOTP)
			throws BusinessException, CriticalException, BusinessConfirmation {

		CustomDeviceDetailsVO customVO = (CustomDeviceDetailsVO) detailsVO.getExtensionVO();
		customVO.setOtp(new OTP(userEnteredOTP));

		final IClientStub service = ServiceUtil.getService(new FEBAUnboundString(DEVICE_REGISTRATION_SERVICE));
		service.callService(opContext, detailsVO, new FEBAUnboundString("validateOTP"));

	}

}
