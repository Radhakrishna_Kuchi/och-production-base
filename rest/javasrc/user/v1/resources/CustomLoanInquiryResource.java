package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAmountVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanInquiryCallReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanInquiryCallReferencesVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;
import com.infosys.ebanking.rest.retail.servicerequest.v1.resources.AbstractServiceRetailRequest;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Inquiry Call")
@Path("/v1/banks/{bankid}/users/{userid}/custom/loans")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomLoanInquiryResource extends AbstractServiceRetailRequest<CustomLoanInquiryCallReferencesVO> {

	final IClientStub lonListServiceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanListService"));

	private String userId;

	@Context
	HttpServletRequest request1;

	@Context
	HttpServletResponse httpResponse1;

	@Context
	UriInfo uriInfo1;

	/**
	 * Authenticates if the corpid and bankid who is trying to access the
	 * resource is same as the corpid and bankid which is associated with the
	 * context.
	 *
	 * @return boolean if the corpid and bankid is authenticated it returns
	 *         false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {

		IOpContext opcontext = (OpContext) request1.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo1.getPathParameters();

		String bankId = pathParams.getFirst("bankid");
		userId = pathParams.getFirst("userid");
		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		UserId user = (UserId) opcontext.getFromContextData(EBankingConstants.USER_ID);
		boolean isInValid;
		if (bank.getValue().equals(bankId)) {
			isInValid = false;
		} else {
			if (user.getValue().equals(userId)) {
				isInValid = false;
			} else {
				isInValid = true;
			}
		}
		if (isInValid) {
			throw new BusinessException(opcontext, EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					FBAErrorCodes.UNATHORIZED_USER);
		}
		return isInValid;
	}

	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_INQ_URL, auditFields = { "userid : userid" })
	@ApiOperation(value = "Fetch loan accounts,loan status and balance details based on cif id of the user ", response = CustomLoanInquiryCallReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Loan Details", response = CustomLoanInquiryCallReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Loan details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchRequest(@ApiParam(value = "userid", required = true) @QueryParam("userid") String userid,
			@ApiParam(value = "Bank Id", required = true) @QueryParam("bankid") String bankid) {
		CustomLoanInquiryCallReferencesResponseVO responseVO = new CustomLoanInquiryCallReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		
			List<CustomLoanInquiryCallReferencesVO> outputVO = fetchDetails(userId, context);
			Collections.sort(
					outputVO, Comparator.comparing(CustomLoanInquiryCallReferencesVO::getLoanSrlNum).reversed());	
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request1, 0);
		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomLoanInquiryCallReferencesVO> fetchDetails(String userid, IOpContext context)
			throws FEBAException {

		List<CustomLoanInquiryCallReferencesVO> outputVO = null;

		CustomLoanCallEnquiryVO enquiryVO = (CustomLoanCallEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO");
		if (null != userid) {
			enquiryVO.getCriteria().setUserId(userid);
		}

		if (userid == null || userid.equals("")) {
			throw new BusinessException(true, context, "LNINQ01", "User Id empty.", null, 211081, null);
		}

		outputVO = new ArrayList<>();

		enquiryVO = (CustomLoanCallEnquiryVO) lonListServiceCStub.callService(context, enquiryVO,
				new FEBAUnboundString("fetch"));
		String custId = enquiryVO.getCriteria().getCifId().toString();
		FEBAArrayList<CustomLoanCallDetailsVO> detailsListVO = enquiryVO.getResultList();
		int count = 0;

		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomLoanCallDetailsVO detVO = detailsListVO.get(i);
			CustomLoanInquiryCallReferencesVO addVO = new CustomLoanInquiryCallReferencesVO();

			addVO.setAccountId(detVO.getAccountId().toString());
			addVO.setCustomerId(custId);
			enquiryVO.setDetails(detVO);
			String bal = detVO.getAccountBalance().toString();
			String sign = bal.substring(0, 1);

			if (sign.equalsIgnoreCase("-") && count == 0) {
				CodeReferencesVO codeRefVO = new CodeReferencesVO();
				codeRefVO.setCmCode("A");
				String cmCodeDesc = RestCommonUtils.getCodeDescription(context, "LTP", "A");
				codeRefVO.setCodeDescription(cmCodeDesc);
				addVO.setAccountStatus(codeRefVO);
				count++;
			} else {
				CodeReferencesVO codeRefVO = new CodeReferencesVO();
				codeRefVO.setCmCode("P");
				String cmCodeDesc = RestCommonUtils.getCodeDescription(context, "LTP", "P");
				codeRefVO.setCodeDescription(cmCodeDesc);
				addVO.setAccountStatus(codeRefVO);
			}

			CodeReferencesVO codeRefCurVO = new CodeReferencesVO();
			codeRefCurVO.setCmCode(detVO.getAccountCurrency().toString());
			String cmCodeDesc = RestCommonUtils.getCodeDescription(context, "CUR", codeRefCurVO.getCmCode());
			codeRefCurVO.setCodeDescription(cmCodeDesc);
			addVO.setAccountCurrency(codeRefCurVO);

			
				lonListServiceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetchBalance"));
				CustomLoanCallDetailsVO balDetailsVO = enquiryVO.getDetails();
				
				System.out.println("enquiryVO.getDetails() - "+enquiryVO.getDetails());

				CustomAmountVO amntVO1 = new CustomAmountVO();
				amntVO1.setCurrency(detVO.getAccountCurrency().toString());
				amntVO1.setAmount(balDetailsVO.getTotalOutstandingAmount().toString());
				addVO.setTotalOutstandingAmount(amntVO1);

				CustomAmountVO amntVO2 = new CustomAmountVO();
				amntVO2.setCurrency(detVO.getAccountCurrency().toString());
				amntVO2.setAmount(balDetailsVO.getSactionAmount().toString());
				addVO.setSactionAmount(amntVO2);

				addVO.setCountofTotalInstallement(balDetailsVO.getCountofTotalInstallement().toString());
				addVO.setCountOfPendingInstallement(balDetailsVO.getCountOfPendingInstallement().toString());

				CustomAmountVO amntVO3 = new CustomAmountVO();
				amntVO3.setCurrency(detVO.getAccountCurrency().toString());
				amntVO3.setAmount(balDetailsVO.getInstallmentAmountPerMonth().toString());
				addVO.setInstallmentAmountPerMonth(amntVO3);

				CustomAmountVO amntVO4 = new CustomAmountVO();
				amntVO4.setCurrency(detVO.getAccountCurrency().toString());
				amntVO4.setAmount(balDetailsVO.getCurrentInstallementAmount().toString());
				addVO.setCurrentInstallementAmount(amntVO4);

				CustomAmountVO amntVO5 = new CustomAmountVO();
				amntVO5.setCurrency(detVO.getAccountCurrency().toString());
				amntVO5.setAmount(balDetailsVO.getCurrentInstallementPrincipal().toString());
				addVO.setCurrentInstallementPrincipal(amntVO5);

				CustomAmountVO amntVO6 = new CustomAmountVO();
				amntVO6.setCurrency(detVO.getAccountCurrency().toString());
				amntVO6.setAmount(balDetailsVO.getCurrentInstallmentInterest().toString());
				addVO.setCurrentInstallmentInterest(amntVO6);

				CustomAmountVO amntVO7 = new CustomAmountVO();
				amntVO7.setCurrency(detVO.getAccountCurrency().toString());
				amntVO7.setAmount(balDetailsVO.getOverdueInstallementAmount().toString());
				addVO.setOverdueInstallementAmount(amntVO7);

				CustomAmountVO amntVO8 = new CustomAmountVO();
				amntVO8.setCurrency(detVO.getAccountCurrency().toString());
				amntVO8.setAmount(balDetailsVO.getOverdueInstallementPrincipal().toString());
				addVO.setOverdueInstallementPrincipal(amntVO8);

				CustomAmountVO amntVO9 = new CustomAmountVO();
				amntVO9.setCurrency(detVO.getAccountCurrency().toString());
				amntVO9.setAmount(balDetailsVO.getOverdueInstallmentInterest().toString());
				addVO.setOverdueInstallmentInterest(amntVO9);

				CustomAmountVO amntVO10 = new CustomAmountVO();
				amntVO10.setCurrency(detVO.getAccountCurrency().toString());
				amntVO10.setAmount(balDetailsVO.getOverdueInstallmentPenality().toString());
				addVO.setOverdueInstallmentPenality(amntVO10);
				
				CustomAmountVO amntVO11 = new CustomAmountVO();
				amntVO11.setCurrency(detVO.getAccountCurrency().toString());
				amntVO11.setAmount(balDetailsVO.getTotalOutstandingPriAmount().toString());
				addVO.setTotalOutstandingPriAmount(amntVO11);
				

				addVO.setOverdueDays(balDetailsVO.getOverdueDays().toString());
				//Fix Added for calculating the Loan Serial Number. 
				addVO.setLoanSrlNum(balDetailsVO.getLoanSrlNum().toString());
				System.out.println("Calculating Due Date -");

				try {

					/*long daystoBlockPaymentBeforeDueDays = Long
							.parseLong(PropertyUtil.getProperty("BLOCK_DAYS_BEFORE_PAYMENT_DUE_DATE", context));

					String coreBODDate = PropertyUtil.getProperty("CORE_BOD_DATE", context);

					long daysToBlockAfterLoanStartDate = Long
							.parseLong(PropertyUtil.getProperty("BLOCK_DAYS_AFTER_LOAN_START_DATE", context));
					 */
					long daysToDueDateInstOne = Long
							.parseLong(PropertyUtil.getProperty("DAYS_TO_DUE_DATE_INST_ONE", context));

					String coreBODDate = PropertyUtil.getProperty("CORE_BOD_DATE", context);

					long daysToDueDateInstTwoOrMore = Long
							.parseLong(PropertyUtil.getProperty("DAYS_TO_DUE_DATE_INST_TWO_OR_MORE", context));

					
					FEBADate curDate = null;
					if (null != coreBODDate && coreBODDate.length() > 0) {
						curDate = new FEBADate(coreBODDate);
					} else {
						curDate = new FEBADate(DateUtil.currentDate(context));
					}
					Date loanStartDate = new Date();
					if(FEBATypesUtility.isNotNullOrBlank(balDetailsVO.getLoanStartDate())){
						System.out.println("balDetailsVO.loanStartDate()" + balDetailsVO.getLoanStartDate());
						loanStartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.parse(balDetailsVO.getLoanStartDate().toString());
						addVO.setLoanStartDate(loanStartDate);

					}
					
					System.out.println("FEBATypesUtility.isNotNull(balDetailsVO.getPmtDueDate()) - "+FEBATypesUtility.isNotNullOrBlank(balDetailsVO.getPmtDueDate()));
					if(FEBATypesUtility.isNotNullOrBlank(balDetailsVO.getPmtDueDate())){
						System.out.println("balDetailsVO.getPmtDueDate()" + balDetailsVO.getPmtDueDate());
						Date duedate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.parse(balDetailsVO.getPmtDueDate().toString());
						FEBADate dateToBeValidated = new FEBADate(duedate);
						long daysToDueDate = DateUtil.dateDiff(dateToBeValidated.getTimestampValue(),curDate.getTimestampValue());
						
						FEBADate formattedLoanDate = new FEBADate(loanStartDate);
						long loanAge = DateUtil.dateDiff(curDate.getTimestampValue(), formattedLoanDate.getTimestampValue());

						long totalInstallments = Long.valueOf(balDetailsVO.getCountofTotalInstallement().getValue());
						long totalPendingInstallments = Long.valueOf(balDetailsVO.getCountOfPendingInstallement().getValue());

						long currentInstallmentNumber = totalInstallments-totalPendingInstallments+1;

						addVO.setDueDate(dateToBeValidated.toString());
						addVO.setDueDays(daysToDueDate);

						System.out.println("PaymentAllowed - ");
						System.out.println("addVO.getAccountStatus().getCmCode() - "+addVO.getAccountStatus().getCmCode());
						System.out.println("currentInstallmentNumber - "+currentInstallmentNumber);
						System.out.println("daysToDueDate - "+daysToDueDate);
						System.out.println("daysToDueDateInstOne - "+daysToDueDateInstOne);
						System.out.println("daysToDueDateInstOne - "+daysToDueDateInstTwoOrMore);
						if (addVO.getAccountStatus().getCmCode().equalsIgnoreCase("A")
								&& totalPendingInstallments==0) {
							addVO.setPaymentAllowed("Y");
						}else if (addVO.getAccountStatus().getCmCode().equalsIgnoreCase("A")
								&& currentInstallmentNumber==1 
								&& daysToDueDate==daysToDueDateInstOne) {
							addVO.setPaymentAllowed("Y");
						}else if (addVO.getAccountStatus().getCmCode().equalsIgnoreCase("A")
								&& currentInstallmentNumber>1 
								&& daysToDueDate>=daysToDueDateInstTwoOrMore) {
							addVO.setPaymentAllowed("Y");
						}else {
							addVO.setPaymentAllowed("N");
						}
					}
					

				} catch (Exception e) {
					e.printStackTrace();
					throw new BusinessException(true, context, "LNINQ01", "Error in Date format in Balance details.", null,
							211303, null);
				}

				addVO.setLoanSrlNum(balDetailsVO.getLoanSrlNum().toString());

			
			
			
			outputVO.add(addVO);
		}
		return outputVO;
	}

	@Override
	public Response createRequest(CustomLoanInquiryCallReferencesVO arg0) {
		return null;
	}

	@Override
	public Response fetchRequest(String arg0) {
		return null;
	}
}

