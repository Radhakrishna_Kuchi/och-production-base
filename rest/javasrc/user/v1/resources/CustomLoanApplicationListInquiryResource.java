package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicationStatusVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApprovalDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomIncompleteApplicationDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationListInqOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationListInqResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanLimitDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomMonthlyPayDetails;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomOneMonthlyPaymentDetails;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPayrollRejDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomRejectionDetailsVO;
import com.infosys.custom.ebanking.tao.CLADTAO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.info.CLADInfo;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomCreditScoreCalculateVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnLimitInqVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestEnquiryVO;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAAString;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Application")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applicationInquiry")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomLoanApplicationListInquiryResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest servletrequest;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInformation;

	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String LOAN_APPLICATION_TENURE_SERVICE = "CustomLoanTenureMaintenanceService";

	private static final String MODIFY = "modify";
	public static final String MESSAGE = "No records fetched";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GET
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/applicationInquiry", auditFields = {
	"applicationId: Application Id" })
	@ApiOperation(value = "Fetch the Application details", response = CustomLoanApplicationResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful Retrieval", response = CustomLoanApplicationResponseVO.class) })
	public Response fetchDetails(@ApiParam(value = "user id", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Record Count", required = true) @QueryParam("recordCount") String recordCount,
			@ApiParam(value = "Application status", required = true) @QueryParam("applicationStatus") String applicationStatus,
			@ApiParam(value = "Application Id input") @QueryParam("applicationId") String applicationId) {
		CustomLoanApplicationListInqOutputVO outputVO = new CustomLoanApplicationListInqOutputVO();
		CustomLoanApplicationListInqResponseVO responseVO = new CustomLoanApplicationListInqResponseVO();
		CustomApplicationStatusVO applnStatusVO = new CustomApplicationStatusVO();
		FEBATransactionContext objTxnContext = null;
		try {
			isUnAuthenticatedRequest();
			IOpContext opContext = (IOpContext) servletrequest.getAttribute("OP_CONTEXT");

			// calling inquiry service
			CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
					.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
			if (applicationStatus != null) {
				enquiryVO.getCriteria().setApplicationStatus(applicationStatus);
			}
			if (applicationId != null) {
				enquiryVO.getCriteria().setApplicationId(applicationId);
			}
			final IClientStub customLoanInquiryService = ServiceUtil
					.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
			customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
			if (enquiryVO.getResultList().size() == 0) {
				throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}

			CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList()
					.get(0);
			Date rModTime = detailsVO.getRModTime().getValue();
			String status = detailsVO.getApplicationStatus().toString();
			
			String crRespCode = detailsVO.getCrRespCode().toString();

			outputVO.setApplicationId(Long.valueOf(detailsVO.getApplicationId().toString()));

			outputVO.setLastActionDate(rModTime);

			Date currentTime = new java.util.Date(System.currentTimeMillis());
			long diff = currentTime.getTime() - rModTime.getTime();
			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			long minutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
			long timeLeftForScoreCompletion = 0;
			
			// Start logic to check If customer has already LOAN_PAID status then set isSecond Apply Flag value to Y
			// Otherwise isSecond Apply Flag to N.
			// Changes for JIRA ticket # PD-141.
			outputVO.setIsSecondApply('N');
			if(enquiryVO.getResultList() !=null && enquiryVO.getResultList().size() != 0){
				for(int i=0; i<enquiryVO.getResultList().size(); i++){
					CustomLoanApplnMasterDetailsVO detailsVO1 = (CustomLoanApplnMasterDetailsVO) enquiryVO.getResultList()
							.get(i);
						if(FEBATypesUtility.isNotNullOrBlank(detailsVO1.getApplicationStatus())){
						if(detailsVO1.getApplicationStatus().getValue().equals("LOAN_PAID")){
							outputVO.setIsSecondApply('Y');
							outputVO.setPreviousLoanPaidAppId(detailsVO1.getApplicationId().getValue());
							outputVO.setPreviousLoanAppStatus(detailsVO1.getApplicationStatus().getValue());
							break;
						}
					}
				}
			}
			// end logic
			
			if (status.equals("CR_SCORE_SUB")) {
				String creditScoreCalTimeInterval = PropertyUtil.getProperty("CREDIT_SCR_CAL_TIME_INTRVL", opContext);
				timeLeftForScoreCompletion = Long.valueOf(creditScoreCalTimeInterval) - minutes;
				if (timeLeftForScoreCompletion > 0.0) {
					// Do nothing
				} else {
					boolean isReSubmitRequired = true;
					CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) 	FEBAAVOFactory
							.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());

					System.out.println("detailsVO.getStatusDesc() - "+detailsVO.getStatusDesc());
					/*
					 * Added below logic to handle the case where credit score
					 * submission failed use to invalid data. In this case
					 * re-submission is not required
					 */
					if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getStatusDesc())) {
						IFEBAType isUnhandledFailure = AppDataManager.getValue(opContext, "CommonCodeCache",
								"CODE_TYPE=CSFS|CM_CODE=" + detailsVO.getStatusDesc());
						System.out.println("isUnhandledFailure - "+isUnhandledFailure);
						if (null != isUnhandledFailure && (isUnhandledFailure.toString().equals("Y"))) {
							loanApplnDetailsVO.setApplicationStatus("APP_EXPIRED");
							System.out.println("loanApplnDetailsVO - "+loanApplnDetailsVO);
							isReSubmitRequired = false;
							status = "CR_SUB_FAILED";
							System.out.println("loanApplnDetailsVO - "+loanApplnDetailsVO);
						}
					}
					if (isReSubmitRequired) {
						CustomCreditScoreCalculateVO creditScoreCalculateVO = (CustomCreditScoreCalculateVO) FEBAAVOFactory
								.createInstance(CustomCreditScoreCalculateVO.class.getName());
						creditScoreCalculateVO.setApplicationId(detailsVO.getApplicationId().toString());
						creditScoreCalculateVO.setUserId(userId);
						final IClientStub customCreditScoreService = ServiceUtil
								.getService(new FEBAUnboundString("CustomCreditScoreService"));
						customCreditScoreService.callService(opContext, creditScoreCalculateVO,
								new FEBAUnboundString("calculate"));
						loanApplnDetailsVO.setUid(creditScoreCalculateVO.getUid());
						loanApplnDetailsVO.setStatusDesc(creditScoreCalculateVO.getDesc());
						// Fix for Payroll Rejection production issue.
						loanApplnDetailsVO.setApplicationStatus(creditScoreCalculateVO.getApplicationStatus());
					}
					
					loanApplnDetailsVO.setApplicationId(detailsVO.getApplicationId().toString());
					final IClientStub customLoanApplicationService = ServiceUtil
							.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
					customLoanApplicationService.callService(opContext, loanApplnDetailsVO,
							new FEBAUnboundString(MODIFY));
					if (!isReSubmitRequired) {
						objTxnContext = RestCommonUtils.getTransactionContext(opContext);
						new CustomLoanHistoryUpdateUtil().updateHistory(loanApplnDetailsVO.getApplicationId(), "APP_EXPIRED",
								"", objTxnContext);
					}
					timeLeftForScoreCompletion = Long.valueOf(creditScoreCalTimeInterval);
				}
				outputVO.setTimeLeftForScoreCompletion(timeLeftForScoreCompletion);
			}

			outputVO.setIsEmailVerified(detailsVO.getIsEmailVerified().getValue());

			if (status.equals("CR_SCORE_REJ")) {
				
				if(crRespCode != null && crRespCode.equals("2102")){
					
					String coolDownPeriodForRejection = PropertyUtil.getProperty("COOLDOWN_PERIOD_FOR_MISSING_SIMPANAN",
							opContext);
					Calendar c = Calendar.getInstance();
					c.setTime(rModTime);
					// Fix added for setting hours, minutes and seconds for calculate the rejection date start.
					int hours = c.get(Calendar.HOUR_OF_DAY);
					int mintes = c.get(Calendar.MINUTE);
					int seconds = c.get(Calendar.SECOND);
					c.add(Calendar.DATE, Integer.valueOf(coolDownPeriodForRejection));
					c.add(Calendar.HOUR, hours);
					c.add(Calendar.MINUTE, mintes);
					c.add(Calendar.SECOND, seconds);
					// End here.
					if (c.getTime().after(currentTime)) {
						// if less than sysdate send rej details else exception
						CustomRejectionDetailsVO rejectionDetails = new CustomRejectionDetailsVO();
						rejectionDetails.setNextEligibilityDate(c.getTime());
						rejectionDetails.setResponseCode(crRespCode);
						outputVO.setRejectionDetails(rejectionDetails);
					} else {
						//Changes for JIRA ticket # PD-160
						if(outputVO.getIsSecondApply() == 'Y'){
							outputVO.setApplicationId(outputVO.getPreviousLoanPaidAppId());
							status = outputVO.getPreviousLoanAppStatus();
						}else{
							throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
								EBankingErrorCodes.NO_RECORDS_FOUND);
						}
					}
					
				}else {
					
					String coolDownPeriodForRejection = PropertyUtil.getProperty("COOLDOWN_PERIOD_FOR_CREDIT_REJECTION",
							opContext);
					Calendar c = Calendar.getInstance();
					c.setTime(rModTime);
					// Fix added for setting hours, minutes and seconds for calculate the rejection date start.
					int hours = c.get(Calendar.HOUR_OF_DAY);
					int mintes = c.get(Calendar.MINUTE);
					int seconds = c.get(Calendar.SECOND);
					c.add(Calendar.DATE, Integer.valueOf(coolDownPeriodForRejection));
					c.add(Calendar.HOUR, hours);
					c.add(Calendar.MINUTE, mintes);
					c.add(Calendar.SECOND, seconds);
					// End here.
					if (c.getTime().after(currentTime)) {
						// if less than sysdate send rej details else exception
						CustomRejectionDetailsVO rejectionDetails = new CustomRejectionDetailsVO();
						rejectionDetails.setNextEligibilityDate(c.getTime());
						outputVO.setRejectionDetails(rejectionDetails);
					} else {
						//Changes for JIRA ticket # PD-160
						if(outputVO.getIsSecondApply() == 'Y'){
							outputVO.setApplicationId(outputVO.getPreviousLoanPaidAppId());
							status = outputVO.getPreviousLoanAppStatus();
						}else{
							throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
									EBankingErrorCodes.NO_RECORDS_FOUND);
						}
					}
				}
			}
			
			// Added for BRI AGRO Payroll payroll rejection start
			if (status.equals("PAYROLL_REJ")) {
				String coolDownPeriodForPayrollRejection = PropertyUtil.getProperty("COOLDOWN_PERIOD_FOR_PAYROLL_REJECTION",
						opContext);
				Calendar c = Calendar.getInstance();
				c.setTime(rModTime);
				c.add(Calendar.DATE, Integer.valueOf(coolDownPeriodForPayrollRejection));
				if (c.getTime().after(currentTime)) {
					// if less than sysdate send rej details else exception
					CustomPayrollRejDetailsVO payRollRejectionDetails = new CustomPayrollRejDetailsVO();
					payRollRejectionDetails.setNextEligibilityDate(c.getTime());
					outputVO.setPayrollrejdetails(payRollRejectionDetails);
				} else {					
					//Changes for JIRA ticket # PD-160
					if(outputVO.getIsSecondApply() == 'Y'){
						outputVO.setApplicationId(outputVO.getPreviousLoanPaidAppId());
						status = outputVO.getPreviousLoanAppStatus();
					}else{
						throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
								EBankingErrorCodes.NO_RECORDS_FOUND);
					}
				}
			}
			// Added for BRI AGRO Payroll payroll rejection end
			if (status.equals("USR_REJECT")) {
				String allowedConsecutiveUserRejection = PropertyUtil.getProperty("ALLOWED_CONSECUTIVE_USER_REJ",
						opContext);
				String banPeriodForUserRejection = PropertyUtil.getProperty("BAN_PERIOD_FOR_USER_REJECTION", opContext);
				Calendar c = Calendar.getInstance();
				c.setTime(rModTime);
				
				// Fix added for setting hours, minutes and seconds for calculate the rejection date start.
				int hours = c.get(Calendar.HOUR_OF_DAY);
				int mintes = c.get(Calendar.MINUTE);
				int seconds = c.get(Calendar.SECOND);				
				c.add(Calendar.DATE, Integer.valueOf(banPeriodForUserRejection));
				c.add(Calendar.HOUR, hours);
				c.add(Calendar.MINUTE, mintes);
				c.add(Calendar.SECOND, seconds);
				// End here.
				
				if (days > Integer.valueOf(banPeriodForUserRejection)) {
					//Changes for JIRA ticket # PD-160
					if(outputVO.getIsSecondApply() == 'Y'){
						outputVO.setApplicationId(outputVO.getPreviousLoanPaidAppId());
						status = outputVO.getPreviousLoanAppStatus();
					}else{
						throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
								EBankingErrorCodes.NO_RECORDS_FOUND);
					}
					
				} else {
					// check ban period first. if its greater than 7 days then
					// do inquiry
					enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
							.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
					enquiryVO.getCriteria().setApplicationStatus(detailsVO.getApplicationStatus());
					customLoanInquiryService.callService(opContext, enquiryVO,
							new FEBAUnboundString("fetchForInquiry"));
					if (enquiryVO.getResultList().size() >= Integer.valueOf(allowedConsecutiveUserRejection)) {

						CustomRejectionDetailsVO rejectionDetails = new CustomRejectionDetailsVO();
						rejectionDetails.setNextEligibilityDate(c.getTime());
						outputVO.setRejectionDetails(rejectionDetails);
					} else {
						//Changes for JIRA ticket # PD-160
						if(outputVO.getIsSecondApply() == 'Y'){
							outputVO.setApplicationId(outputVO.getPreviousLoanPaidAppId());
							status = outputVO.getPreviousLoanAppStatus();
						}else{
							throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
									EBankingErrorCodes.NO_RECORDS_FOUND);
						}
					}
				}
			}
			if (status.equals("CR_SCORE_APR")) {
				String creditOfferValidityPeriod = PropertyUtil.getProperty("CREDIT_OFFER_VALIDITY_PERIOD", opContext);

				if (days < Integer.valueOf(creditOfferValidityPeriod)) {

					CustomApprovalDetailsVO approvalDetails = new CustomApprovalDetailsVO();
					
					approvalDetails.setAppliedAmount(detailsVO.getRequestedLoanAmt().getAmountValue());

					approvalDetails.setApprovalAmount(detailsVO.getApprovedAmount().getAmountValue());

					objTxnContext = RestCommonUtils.getTransactionContext(opContext);
					CLPAInfo clpainfo = CLPATAO.select(objTxnContext,
							new BankId(opContext.getFromContextData("BANK_ID").toString()),
							detailsVO.getApplicationId());
					CLADInfo cladinfo = CLADTAO.select(objTxnContext,
							new BankId(opContext.getFromContextData("BANK_ID").toString()),
							detailsVO.getApplicationId());
					CLATInfo clatInfo = CLATTAO.select(objTxnContext,
							new BankId(opContext.getFromContextData("BANK_ID").toString()),
							detailsVO.getApplicationId());
					
					
					 // Credit Scoring Enhanced Details being mapped to approvalDetails here					 	
					 	approvalDetails.setPlafond1(clatInfo.getPlafond1().getAmountValue());
						approvalDetails.setPlafond2(clatInfo.getPlafond2().getAmountValue());
						
						approvalDetails.setTenor1(clatInfo.getTenor1().toString());
						approvalDetails.setTenor2(clatInfo.getTenor2().toString());
					
					approvalDetails.setIsKTPVerified(clatInfo.getIsKtpVerified().toString());
					// added for PINANG DASHBOARD KTP rejection case start
					approvalDetails.setKtpRejectCnt(clatInfo.getKtpRejCnt().getValue());
					approvalDetails.setIsKTPReUploaded(clatInfo.getIsKtpReuploaded().toString());	
					// added for PINANG DASHBOARD KTP rejection case end

					approvalDetails.setDisbursementAccount(
							StringUtils.leftPad(clpainfo.getPayrollAccountNum().getValue(), 15, '0'));
					approvalDetails.setAccountName(cladinfo.getName().toString());
					IFEBAType branchName = (IFEBAType) AppDataManager.getValue(objTxnContext, "BranchCodeCache",
							"BRANCH_ID=" + clpainfo.getPayrollAccBranch().toString());
					if (FEBATypesUtility.isNotNullOrBlank((FEBAAString) branchName)) {
						approvalDetails.setAccountBranch(branchName.toString());
					} else {
						approvalDetails.setAccountBranch(clpainfo.getPayrollAccBranch().toString());
					}
					Calendar c = Calendar.getInstance();
					c.setTime(rModTime);
					c.add(Calendar.DATE, Integer.valueOf(creditOfferValidityPeriod)); // Adding
					// 21
					// days
					approvalDetails.setValidityDate(c.getTime());
					// start logic to fetch the maximum TENURE value from CLTI table.
					CustomLoanTenureInterestEnquiryVO enquiryTenureVO = (CustomLoanTenureInterestEnquiryVO) FEBAAVOFactory
							.createInstance(CustomLoanTenureInterestEnquiryVO.class.getName());
					enquiryTenureVO.getCriteria().setLnRecId("1");
					//System.out.println("enquiryTenureVO::"+enquiryTenureVO);
					//System.out.println("calling LOAN_APPLICATION_TENURE_SERVICE service ");
					final IClientStub customLoanInqTenureService = ServiceUtil
							.getService(new FEBAUnboundString(LOAN_APPLICATION_TENURE_SERVICE));
					customLoanInqTenureService.callService(opContext, enquiryTenureVO, new FEBAUnboundString("fetchTenures"));
					if (enquiryTenureVO.getResultList().size() == 0) {
						throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
								EBankingErrorCodes.NO_RECORDS_FOUND);
					}
					CustomLoanTenureInterestDetailsVO tenureDetailsVO = (CustomLoanTenureInterestDetailsVO) enquiryTenureVO.getResultList().get(0);
					//System.out.println("tenureDetailsVO::"+tenureDetailsVO);

					FEBAArrayList<CustomLoanTenureInterestDetailsVO> tenureList = new FEBAArrayList<>();
					tenureList = enquiryTenureVO.getResultList();
					//System.out.println("tenureList::"+tenureList);
					
					String maxTenorValue = String.valueOf(clatInfo.getTenorMax().getValue());

					// end logic here.
					
					// The calling method will do monthly payment calculation for PLAFOND-2 amount and 
					// one month payment calculation for PLAFOND-1 and PLAFOND-2 amounts.
					caluculateMonthlyPayments(approvalDetails, clatInfo, maxTenorValue, tenureList);
					outputVO.setApprovalDetails(approvalDetails);
				} else {
					status = "APP_EXPIRED";

				}

			}
			if (status.equals("ACT_LINK_CONF") || status.equals("DISB_ACC_CONF") || status.equals("EKYC_COM")
					|| status.equals("DIG_SIGN_COM") || status.equals("DOCUMENT_SIGNED")) {
				String incompleteAppValidityPeriod = PropertyUtil.getProperty("ALLOWED_PERIOD_FOR_INCOMPLETE_APP",
						opContext);

				if (days < Integer.valueOf(incompleteAppValidityPeriod)) {
					CustomIncompleteApplicationDetailsVO incompleteApplicationDetails = new CustomIncompleteApplicationDetailsVO();
					incompleteApplicationDetails.setApprovalAmount(detailsVO.getApprovedAmount().getAmountValue());
					Calendar c = Calendar.getInstance();
					c.setTime(rModTime);
					c.add(Calendar.DATE, Integer.valueOf(incompleteAppValidityPeriod)); // Adding
					// 10
					// days

					incompleteApplicationDetails.setValidityDate(c.getTime());
					outputVO.setIncompleteApplicationDetails(incompleteApplicationDetails);
				} else {
					status = "APP_EXPIRED";
				}
			}
			// Status Service integration by Vidhi start
			else if (status.equals("LOAN_CREATED")) {
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);

				String installationProductId = PropertyUtil.getProperty(CustomResourceConstants.INSTALLATION_PRODUCT_ID,
						opContext);	
				if (installationProductId.equalsIgnoreCase(CustomResourceConstants.PINANG)) {
					CustomLoanStatusVO statusVO = (CustomLoanStatusVO) FEBAAVOFactory
							.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO");
					statusVO.setAccountId(detailsVO.getLoanAccountId());
					try {
						final IClientStub customLoanStatusService = ServiceUtil
								.getService(new FEBAUnboundString("CustomLoanStatusService"));
						customLoanStatusService.callService(opContext, statusVO, new FEBAUnboundString("fetchStatus"));
					} catch (Exception e) {
							throw new BusinessException(opContext, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
									EBankingErrorCodes.HOST_NOT_AVAILABLE);
					}
					
					try{
						
						if(FEBATypesUtility.isNotNullOrBlank(statusVO.getLoanStatus())?statusVO.getLoanStatus().getValue().equalsIgnoreCase("LOAN_PAID"):false){
							status="LOAN_PAID";
							System.out.println("As part of Application Loan Inq, updating the status to LOAN _PAID");
							updateApplicationStatus(opContext, detailsVO.getApplicationId().toString(), "LOAN_PAID");
							new CustomLoanHistoryUpdateUtil().updateHistory(detailsVO.getApplicationId(), "LOAN_PAID",
									"ApplicationListInq", objTxnContext);
						}
						
					}catch (Exception e) {
						e.printStackTrace();
					} finally{
						if(objTxnContext != null){
							try {
								objTxnContext.cleanup();
							} catch (CriticalException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			// Status Service integration by Vidhi end

			if (status.equals("LOAN_CREATED")) {
				String installationProductId = PropertyUtil.getProperty(CustomResourceConstants.INSTALLATION_PRODUCT_ID,
						opContext);
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);
				if (installationProductId.equalsIgnoreCase(CustomResourceConstants.SAHABAT)) {
					CustomLoanApplnLimitInqVO loanApplnLimitInqVO = (CustomLoanApplnLimitInqVO) FEBAAVOFactory
							.createInstance(CustomLoanApplnLimitInqVO.class.getName());
					loanApplnLimitInqVO.setApplicationId(detailsVO.getApplicationId());
					final IClientStub customLoanLimitInqService = ServiceUtil
							.getService(new FEBAUnboundString("CustomLoanLimitInqService"));
					customLoanLimitInqService.callService(opContext, loanApplnLimitInqVO,
							new FEBAUnboundString("fetch"));

					if (loanApplnLimitInqVO.getStatus().toString().equals("S")) {

						CustomLoanLimitDetailsVO loanLimitDetails = new CustomLoanLimitDetailsVO();
						loanLimitDetails.setSanctionedLimit(loanApplnLimitInqVO.getSanctionLimit().getAmountValue());
						loanLimitDetails.setAvailableLimit(loanApplnLimitInqVO.getAvailableLimit().getAmountValue());
						outputVO.setLoanLimitDetails(loanLimitDetails);

					}
				}
				CLPAInfo clpainfo = CLPATAO.select(objTxnContext,
						new BankId(opContext.getFromContextData("BANK_ID").toString()), detailsVO.getApplicationId());
				CustomApprovalDetailsVO approvalDetails = new CustomApprovalDetailsVO();
				approvalDetails.setDisbursementAccount(
						StringUtils.leftPad(clpainfo.getPayrollAccountNum().getValue(), 15, '0'));
				approvalDetails.setNama(clpainfo.getNameWl().getValue()); 
				outputVO.setApprovalDetails(approvalDetails);

			}

			if (status.equals("OCR_VER_FAIL")) {
				throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}
			if (status.equals("KTP_REJECT")) {
				CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
						.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
				loanApplnDetailsVO.setApplicationId(detailsVO.getApplicationId().toString());
				loanApplnDetailsVO.setApplicationStatus("APP_EXPIRED");
				final IClientStub customLoanApplicationService = ServiceUtil
						.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
				customLoanApplicationService.callService(opContext, loanApplnDetailsVO,
						new FEBAUnboundString(MODIFY));
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);
				new CustomLoanHistoryUpdateUtil().updateHistory(loanApplnDetailsVO.getApplicationId(), "APP_EXPIRED",
						"KTP Reject Application Marked as Expired", objTxnContext);
				status = "KTP_REJECT";
			}

			// Added KTP in Applciation inq Resposne--bug fix (for apk cache) -->START
			if (status.equals("APP_CREATED") ||status.equals("PAYROLL_APP") ) {
				
				objTxnContext = RestCommonUtils.getTransactionContext(opContext);
				CUSRInfo cusrInfo=CUSRTAO.select(objTxnContext,
						new BankId(opContext.getFromContextData("BANK_ID").toString()), new CorporateId(userId), new UserId(userId));
				outputVO.setKtpNum(cusrInfo.getPanNationalId().getValue());
			}

			// Added KTP in Applciation inq Resposne--bug fix (for apk cache) -->END
			applnStatusVO.setCodeType("ASTG");
			applnStatusVO.setCmCode(status);

			IFEBAType statusDesc = AppDataManager.getValue(opContext, "CommonCodeCache",
					"CODE_TYPE=ASTG|CM_CODE=" + status);
			if (null != statusDesc) {
				applnStatusVO.setCodeDescription(statusDesc.toString());
			}

			outputVO.setApplicationStatus(applnStatusVO);
			ArrayList finalList = new ArrayList();
			finalList.add(outputVO);
			responseVO.setData(finalList);
		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(servletrequest, e, restResponseErrorCodeMapping());
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}

		return Response.ok().entity(outputVO).build();
	}
	protected static void updateApplicationStatus(IOpContext opContext, String applicationId, String applicationStatus)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomLoanApplnMasterDetailsVO loanApplnDetailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
				.createInstance(CustomLoanApplnMasterDetailsVO.class.getName());
		loanApplnDetailsVO.setApplicationId(new ApplicationNumber(applicationId));
		final IClientStub customLoanApplicationService = ServiceUtil
				.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
		loanApplnDetailsVO.setApplicationStatus(new CommonCode(applicationStatus));
		customLoanApplicationService.callService(opContext, loanApplnDetailsVO, new FEBAUnboundString(MODIFY));
		// mapping main application details:: end

	}
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(EBankingErrorCodes.NO_RECORDS_FOUND, "BE", 400));

		return restResponceErrorCode;
	}
	
	protected static void caluculateMonthlyPayments(CustomApprovalDetailsVO approvalDetails, CLATInfo clatInfo, String maxTenorVal, FEBAArrayList<CustomLoanTenureInterestDetailsVO> tenureList)
	{
	
		List<CustomMonthlyPayDetails> monthlyPayDetails = new ArrayList<>();
		List<Integer> tenorList = new ArrayList<>();
		if(null != approvalDetails){
			
			int tenor1 = 0;
			int tenor2 = 0;
	
			// Fetching complete Tenure List from CLTI table.
			for(CustomLoanTenureInterestDetailsVO tenture : tenureList){
				tenorList.add(Integer.parseInt(tenture.getTenor().toString()));
			}
			
			// read tenor-1 value
			if(null != approvalDetails.getTenor1())
				tenor1 = Integer.parseInt(approvalDetails.getTenor1());
			// read tenor-2 value
			if(null != approvalDetails.getTenor2())
				tenor2 = Integer.parseInt(approvalDetails.getTenor2());
			
			// Check If tenor-2 is more than zero then calculate monthly payments and set in a list.
			if(tenor2>0){
				for(int i=tenor2; i<=Integer.parseInt(maxTenorVal); i++){
					CustomMonthlyPayDetails customMonthlyPaymts = new CustomMonthlyPayDetails();
										
					if(null != tenorList && tenorList.contains(i)){
						
						double intPerMonth = (double) (approvalDetails.getPlafond2()
								* i * clatInfo.getLoanIntRate().getValue()) / 1200;
						double prinAmountPerMonth = (double) approvalDetails.getPlafond2()
								/i;
						double emi = (double) intPerMonth/i + prinAmountPerMonth;
						
						customMonthlyPaymts.setLoanTenor(i);
						customMonthlyPaymts.setMonthlyPayment(new BigDecimal(emi).setScale(0, RoundingMode.CEILING).doubleValue());
						monthlyPayDetails.add(customMonthlyPaymts);
					}
				}
			}else{
				CustomMonthlyPayDetails customMonthlyPaymts = new CustomMonthlyPayDetails();
				monthlyPayDetails.add(customMonthlyPaymts);
			}
			
			CustomOneMonthlyPaymentDetails customOneMonthPayDetailsO = new CustomOneMonthlyPaymentDetails();
			
			// Check If tenor-1 is more than zero then calculate one month payment.
			if(tenor1>0){
				double totalIntAmt = (double) (approvalDetails.getPlafond1()
						* tenor1 * clatInfo.getLoanIntRate().getValue()) / 1200;
				double prinAmountPerMonth = (double) approvalDetails.getPlafond1()
						/tenor1;
				
				double intPerMonth = (double) totalIntAmt/tenor1;
				
				double emi = intPerMonth + prinAmountPerMonth;
				customOneMonthPayDetailsO.setOneMonthPayForPlafond1(new BigDecimal(emi).setScale(0, RoundingMode.CEILING).doubleValue());
			}else{
				customOneMonthPayDetailsO.setOneMonthPayForPlafond1(0.00);
			}
			
			// Check If tenor-2 is more than zero then calculate one month payment.
			if(tenor2>0){
				
				double totalIntAmt = (double) (approvalDetails.getPlafond2()
						* tenor2 * clatInfo.getLoanIntRate().getValue()) / 1200;
				double prinAmountPerMonth = (double) approvalDetails.getPlafond1()
						/tenor2;
				double intPerMonth = (double)totalIntAmt/tenor2;

				double emi = intPerMonth + prinAmountPerMonth;

				customOneMonthPayDetailsO.setOneMonthPayForPlafond2(new BigDecimal(emi).setScale(0, RoundingMode.CEILING).doubleValue());
			}else{
				customOneMonthPayDetailsO.setOneMonthPayForPlafond2(0.00);
			}		
			
			approvalDetails.setMonthlyPayDetails(monthlyPayDetails);
			approvalDetails.setOneMonthPayDetails(customOneMonthPayDetailsO);
			
		}
		
		
	}
}