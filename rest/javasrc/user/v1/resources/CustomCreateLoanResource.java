package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCreateLoanReferenceResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCreateLoanReferenceVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanInquiryCallReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocBinaryListVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentUploadUtil;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.Flag;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Vidhi_Kapoor01
 *
 *         This is the Resource through which Loan creation maintenance is done.
 *
 */
@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Loan Creation Maintenance")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/createloan")
public class CustomCreateLoanResource extends ResourceAuthenticator {

	@Context
	protected HttpServletRequest request;
	
	final IClientStub loanListServiceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanListService"));


	/**
	 * This method is used for creating loan
	 * 
	 * @author Vidhi_Kapoor01
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Create Loan based applicationId and user id", response = CustomCreateLoanReferenceResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_CREATE_LOAN_RESOURCE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Record Updated successfully.", response = CustomCreateLoanReferenceResponseVO.class) })
	public Response updateRequest(
			@ApiParam(value = "The ID of the user", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "The Application ID", required = true) @Size(min = 0, max = 32) @PathParam("applicationId") String applicationId) {

		CustomCreateLoanReferenceResponseVO responseVO = new CustomCreateLoanReferenceResponseVO();

		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");

		try {
			isUnAuthenticatedRequest();
			CustomLoanApplicationEnquiryVO enqVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO");
			enqVO.getCriteria().setApplicationId(new ApplicationNumber(applicationId));
			enqVO.getCriteria().setUserId(new UserId(userid));
			List<CustomCreateLoanReferenceVO> outputVO = createLoan(enqVO, opcontext);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
		return Response.ok().entity(responseVO).build();
	}

	private List<CustomCreateLoanReferenceVO> createLoan(CustomLoanApplicationEnquiryVO inOutVO, IOpContext context)
			throws CriticalException, BusinessException, BusinessConfirmation {

		List<CustomCreateLoanReferenceVO> output = new ArrayList<>();
		FEBATransactionContext objTxnContext = null;
		
		try{
			
			checkDuplicateLoan(inOutVO, context);
			
			objTxnContext = RestCommonUtils.getTransactionContext(context); 
			
			IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomCIFAndLoanAcctService"));
			serviceCStub.callService(context, inOutVO, new FEBAUnboundString("create"));
			System.out.println("Inserting entry in CLHT for Loan Creation : "+inOutVO.getCriteria().getApplicationStatus().getValue());
			new CustomLoanHistoryUpdateUtil().updateHistory(inOutVO.getCriteria().getApplicationId(),
					"LOAN_CREATED", "Loan Created", objTxnContext);
			
		}catch (Exception e) {
			e.printStackTrace();
			LogManager.logError(null, e);
			throw e;
		} finally{
			if(objTxnContext != null){
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}

		

		String installationProductId = PropertyUtil.getProperty(CustomResourceConstants.INSTALLATION_PRODUCT_ID,
				context);
		if (installationProductId.equalsIgnoreCase(CustomResourceConstants.SAHABAT)) {
			CustomApplicationDocEnquiryVO enqVO = (CustomApplicationDocEnquiryVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO");
			enqVO.getCriteria().setApplicationId(inOutVO.getCriteria().getApplicationId());

			try {
				final IClientStub serviceCStub2 = ServiceUtil
						.getService(new FEBAUnboundString("CustomCreateWelcomePageService"));
				serviceCStub2.callService(context, enqVO, new FEBAUnboundString("createPDF"));

				FEBAArrayList binaryList = enqVO.getBinaryList();

				CustomDocumentInputDetailsVO documentDetails = new CustomDocumentInputDetailsVO();
				documentDetails.setApplicationId(Long.valueOf(enqVO.getCriteria().getApplicationId().toString()));

				for (int i = 0; i < binaryList.size(); i++) {
					CustomApplicationDocBinaryListVO binaryVO = (CustomApplicationDocBinaryListVO) binaryList.get(i);
					documentDetails.setFile(binaryVO.getDocString().toString());
					documentDetails.setFileName(binaryVO.getDocTitle().toString() + ".pdf");

					documentDetails = new CustomDocumentUploadUtil()
							.createFileInSharedDirAndUploadEncryptedFile(documentDetails, context, request);

					CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
							.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnDocumentsDetailsVO);
					docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
					docDetailsVO.setDocType(new DocumentType(binaryVO.getDocumentId().toString()));

					docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
					docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());

					final IClientStub customDocDetailsService = ServiceUtil
							.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));

					customDocDetailsService.callService(context, docDetailsVO, new FEBAUnboundString("createOrUpdate"));

					CustomEmailDetailsVO emailDetVO = (CustomEmailDetailsVO) FEBAAVOFactory
							.createInstance(CustomTypesCatalogueConstants.CustomEmailDetailsVO);

					emailDetVO.setAttachment(docDetailsVO.getDocStorePath());

					final IClientStub customEmailService = ServiceUtil
							.getService(new FEBAUnboundString("CustomSendEmailService"));
					customEmailService.callService(context, emailDetVO, new FEBAUnboundString("sendMail"));

				}
			} catch (CriticalException | BusinessException | BusinessConfirmation | FEBATypeSystemException
					| IOException e) {
				// do nothing
			} 

		}

		FEBAArrayList<CustomLoanApplicationDetailsVO> detailsListVO = inOutVO.getResultList();
		// for (int i = 0; i < detailsListVO.size(); i++) {
		CustomLoanApplicationDetailsVO detailsVO = detailsListVO.get(0);
		CustomCreateLoanReferenceVO pojoVO = new CustomCreateLoanReferenceVO();

		pojoVO.setPayrollAccount(detailsVO.getPayrollAccntDetails().getPayrollAccountId().toString());
		pojoVO.setLoanAccountId(detailsVO.getAcctId().toString());
		pojoVO.setAccountLimit(
				new BigDecimal(detailsVO.getLoanApplnMasterDetails().getApprovedAmount().getAmount().getValue())
				.setScale(0, RoundingMode.HALF_UP).toString());
		// Added for loan double credit production issue
		pojoVO.setStatus(detailsVO.getLoanApplnMasterDetails().getApplicationStatus().toString());

		output.add(pojoVO);
		// }

		return output;

	}

	private void checkDuplicateLoan(CustomLoanApplicationEnquiryVO inOutVO, IOpContext context) throws CriticalException, BusinessException, BusinessConfirmation, FEBATypeSystemException{
		CustomLoanApplicationEnquiryVO enqVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO");

		enqVO.getCriteria().setUserId(inOutVO.getCriteria().getUserId());
		enqVO.getCriteria().setApplicationStatus("LOAN_CREATED");
		
		System.out.println("Checking Duplicate Loan status in local OCH - for user Id "+enqVO.getCriteria().getUserId());

		final IClientStub customLoanInquiryService = ServiceUtil
				.getService(new FEBAUnboundString("CustomLoanApplicationService"));
		customLoanInquiryService.callService(context, enqVO, new FEBAUnboundString("fetchForInquiry"));
		int loansInCreatedStatus = enqVO.getResultList().size();
		
		System.out.println("loansInCreatedStatus - "+loansInCreatedStatus);
		if(loansInCreatedStatus>0){
			throw new CriticalException(context, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
					"Loan exists in created status", EBankingErrorCodes.RECORD_NOT_FOUND);
		}

		CustomLoanCallEnquiryVO enquiryVO = (CustomLoanCallEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO");
		
		if (null != inOutVO.getCriteria().getUserId()) {
			enquiryVO.getCriteria().setUserId((inOutVO.getCriteria().getUserId().toString()));
		}
		
		System.out.println("Checking Duplicate Loan in Core using online FI call - for - "+enquiryVO.getCriteria().getUserId());
		int countOfActiveLoansInCore = 0;

		try{
			enquiryVO = (CustomLoanCallEnquiryVO) loanListServiceCStub.callService(context, enquiryVO,
					new FEBAUnboundString("fetch"));

			FEBAArrayList<CustomLoanCallDetailsVO> detailsListVO = enquiryVO.getResultList();
			System.out.println("DetailsListVO after Online call - "+detailsListVO);
			for (int i = 0; i < detailsListVO.size(); i++) {
				CustomLoanCallDetailsVO detVO = detailsListVO.get(i);
				CustomLoanInquiryCallReferencesVO addVO = new CustomLoanInquiryCallReferencesVO();
				System.out.println(i+" CustomLoanInquiryCallReferencesVO - "+addVO);
				addVO.setAccountId(detVO.getAccountId().toString());
				enquiryVO.setDetails(detVO);
				String bal = detVO.getAccountBalance().toString();
				String sign = bal.substring(0, 1);
				if (sign.equalsIgnoreCase("-") || detVO.getAcctClsFlg().equals(new Flag('N'))) {
					++countOfActiveLoansInCore;
				}
			}
		}catch(FEBAException fe){
			System.out.println("FEBA Exception::Error after online call to verify multiple loan application");
			fe.printStackTrace();
			if(fe.getErrorCode() == 14084 ){
				throw fe;
			}
		}catch(Exception e){
			System.out.println("Exception::Error after online call to verify multiple loan application");
			e.printStackTrace();
		}
		System.out.println("after online call - countOfActiveLoansInCore - "+countOfActiveLoansInCore);		

		if(countOfActiveLoansInCore>0){
			throw new CriticalException(context, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
					"Loan exists in created status", EBankingErrorCodes.RECORD_NOT_FOUND);
		}
	}
}
