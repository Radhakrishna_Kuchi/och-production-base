package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanLimitDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanLimitInqResponseVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnLimitInqVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Application")
@Path(CustomResourcePaths.LIMIT_INQUIRY_URL)
@ApiResponses(value = {
		@ApiResponse(code=400, message= ResourcePaths.HTTP_ERROR_400 , response = Header.class),
		@ApiResponse(code=401, message= ResourcePaths.HTTP_ERROR_401 , response = Header.class),
		@ApiResponse(code=403, message= ResourcePaths.HTTP_ERROR_403 , response = Header.class),
		@ApiResponse(code=405, message= ResourcePaths.HTTP_ERROR_405 , response = Header.class),
		@ApiResponse(code=415, message= ResourcePaths.HTTP_ERROR_415 , response = Header.class),
		@ApiResponse(code=422, message= ResourcePaths.HTTP_ERROR_422 , response = Header.class),
		@ApiResponse(code=500, message= ResourcePaths.HTTP_ERROR_500 , response = Header.class),
})
public class CustomLoanLimitInquiryResource extends ResourceAuthenticator{
	@Context
	HttpServletRequest servletrequest;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInformation;
	
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";
	public static final String MESSAGE = "No records fetched";
	@GET
	@MethodInfo(uri = CustomResourcePaths.LIMIT_INQUIRY_URL)
	@ApiOperation(value ="Fetch the limit details", response = CustomLoanLimitInqResponseVO.class)
	@ApiResponses (value = {@ApiResponse(code = 201,message = "Successful Retrieval",response = CustomLoanLimitInqResponseVO.class)})
	public Response fetchLimitDetails() {
		CustomLoanLimitInqResponseVO responseVO = new CustomLoanLimitInqResponseVO();
		CustomLoanLimitDetailsVO outputVO = new CustomLoanLimitDetailsVO();
		try {
			isUnAuthenticatedRequest();
			 IOpContext opContext = (IOpContext) servletrequest.getAttribute("OP_CONTEXT");
							
					//calling inquiry service
					CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
					
					final IClientStub customLoanInquiryService = ServiceUtil.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
					customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
					if(enquiryVO.getResultList().size()==0){
						throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
								MESSAGE, EBankingErrorCodes.NO_RECORDS_FOUND);
					}
					
					CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO)enquiryVO.getResultList().get(0);
					String status = detailsVO.getApplicationStatus().toString();
					 
					
						if(status.equals("LOAN_CREATED")){
							CustomLoanApplnLimitInqVO loanApplnLimitInqVO = (CustomLoanApplnLimitInqVO) FEBAAVOFactory.createInstance(CustomLoanApplnLimitInqVO.class.getName());
							loanApplnLimitInqVO.setApplicationId(detailsVO.getApplicationId());
							final IClientStub customLoanLimitInqService = ServiceUtil.getService(new FEBAUnboundString("CustomLoanLimitInqService"));
							customLoanLimitInqService.callService(opContext, loanApplnLimitInqVO, new FEBAUnboundString("fetch"));
							if(loanApplnLimitInqVO.getStatus().toString().equals("S")){
								CustomLoanLimitDetailsVO loanLimitDetails = new CustomLoanLimitDetailsVO();
								loanLimitDetails.setSanctionedLimit(loanApplnLimitInqVO.getSanctionLimit().getAmountValue());
								loanLimitDetails.setAvailableLimit(loanApplnLimitInqVO.getAvailableLimit().getAmountValue());
								responseVO.setData(loanLimitDetails);
							}
						}else{
							throw new BusinessException(opContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
									MESSAGE, EBankingErrorCodes.NO_RECORDS_FOUND);
						}
						
						
		}
		catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(servletrequest, e, restResponseErrorCodeMapping());
		}

	
	return Response.ok().entity(responseVO).build();
	}
	
	private List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(EBankingErrorCodes.NO_RECORDS_FOUND, "BE", 400));
				
		return restResponceErrorCode;
	}

}
