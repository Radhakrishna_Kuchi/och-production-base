package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomBankCodFetchReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomBankCodeFetchReferencesVO;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Fetch Bank code input application ID")
public class CustomBankCodeFetchResource extends ResourceAuthenticator {

	
	public static final String BANK_ID="BANK_ID";
	

	@Context  
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;
	
	@Context
	UriInfo uriInfo;
	
	/**
	 * Authenticates if the corpid and bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the corpid and bankid is authenticated it returns false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");
		String userId = pathParams.getFirst("userid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		UserId userIdFromContext = (UserId) opcontext.getFromContextData(EBankingConstants.USER_ID);


		boolean isInValid;

		if (bank.getValue().equals(bankId) && userIdFromContext.getValue().equals(userId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}

    @GET
    @ApiOperation(value = "Fetch Bank Code for input applicatio ID", response = CustomBankCodFetchReferencesResponseVO.class)
    @MethodInfo(uri = CustomResourcePaths.BANK_CODE_FETCH_URL)
    @ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Bank List", response = CustomBankCodFetchReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Bank List do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
    
    
    public Response fetchAccountList (
    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
		@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
		@PathParam("applicationId") String applicationId){
    	IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
    	CustomBankCodFetchReferencesResponseVO responseVO= new CustomBankCodFetchReferencesResponseVO();
    	
    	
    	try {
    		isUnAuthenticatedRequest();	
    		List <CustomBankCodeFetchReferencesVO> outputVO = fetchBankCode (userId,applicationId,opcontext);
    		responseVO.setData(outputVO); 	
    		RestResourceManager.updateHeaderFooterResponseData(responseVO	, request, 0);
    	}
    	
    	catch (Exception e) {
    		e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
    		
    	}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
    	
    	return Response.ok().entity(responseVO).build();
    }
    
    public List <CustomBankCodeFetchReferencesVO> fetchBankCode (String userid,String applicationId,IOpContext context) throws FEBAException {
    	
    		List <CustomBankCodeFetchReferencesVO> outputVO = new ArrayList<CustomBankCodeFetchReferencesVO>();
    	
    		FEBATransactionContext objTxnContext = null;
		
			objTxnContext = RestCommonUtils.getTransactionContext(context);
    	    	
			CustomBankCodeFetchReferencesVO  outVO  = new  CustomBankCodeFetchReferencesVO(); 
    	
    		CLATInfo clatinfo = null;
			try {
				clatinfo = CLATTAO.select(objTxnContext,
					new BankId(objTxnContext.getFromContextData(BANK_ID).toString()),
					new ApplicationNumber(applicationId));
				
				
			} catch (FEBATypeSystemException | FEBATableOperatorException e) {
				
				LogManager.logError(objTxnContext, e);
				
			} finally{
				if(objTxnContext != null){
					try {
						objTxnContext.cleanup();
					} catch (CriticalException e) {
						e.printStackTrace();
					}
				}
			}
			
			if(null != clatinfo){
				outVO.setBankCode(clatinfo.getBankCode().toString());
			}
    		
			outputVO.add(outVO);

    	return outputVO;
    }
    
    protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211100, "BE", 400));
		return restResponceErrorCode;
	}
    
    
}
