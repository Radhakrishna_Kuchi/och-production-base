package com.infosys.custom.ebanking.rest.user.v1.resources;
         

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCompanyDetailsInOutVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCompanyDetailsReferencesResponseVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCompanyDetailsVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoListVO;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.types.TypesCatalogueConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "companydetails")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Company Details")

@Path("/v1/banks/{bankid}/users/{userid}/custom/companydetails")


public class CustomCompanyDetailsManagerResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;
	@Context
	UriInfo uriInfo;
    
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {
		
		


		IOpContext opcontext = (OpContext)request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);

		
		boolean isInValid;

		if (bank.getValue().equals(bankId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}

	@POST
	@MethodInfo(uri= CustomResourcePaths.CUSTOM_USER_COMPANY_DETAILS, auditFields={"dummy: dummy_val"})
	@ApiOperation(value="Post Company DEtails", response=CustomCompanyDetailsReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=400, message= ResourcePaths.HTTP_ERROR_400 , response = Header.class),
			@ApiResponse(code=401, message= ResourcePaths.HTTP_ERROR_401 , response = Header.class),
			@ApiResponse(code=403, message= ResourcePaths.HTTP_ERROR_403 , response = Header.class),
			@ApiResponse(code=405, message= ResourcePaths.HTTP_ERROR_405 , response = Header.class),
			@ApiResponse(code=415, message= ResourcePaths.HTTP_ERROR_415 , response = Header.class),
			@ApiResponse(code=422, message= ResourcePaths.HTTP_ERROR_422 , response = Header.class),
			@ApiResponse(code=500, message= ResourcePaths.HTTP_ERROR_500 , response = Header.class),
	})
	public Response createRequest(CustomCompanyDetailsInOutVO inputVO) {
		
		
		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		CustomCompanyDetailsReferencesResponseVO responseVO =new CustomCompanyDetailsReferencesResponseVO();
		RestCommonSuccessMessageHandler wrapper = new RestCommonSuccessMessageHandler();
		
		CustomCompanyDetailsVO detailsVO= (CustomCompanyDetailsVO) FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomCompanyDetailsVO);
		try {
			isUnAuthenticatedRequest();
		    detailsVO.setCompanyName(inputVO.getCompanyName());
			detailsVO.setCompanyAddress(inputVO.getCompanyAddress());
			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomCompanyDetailsService"));
			detailsVO = (CustomCompanyDetailsVO) serviceStub.callService(opcontext, detailsVO, new FEBAUnboundString("create"));
			 BusinessInfoListVO businessListVO = opcontext.getBusinessInfo();
			 if(businessListVO !=null){
				 
	        		opcontext.clearBusinessInfo();
	        		BusinessInfoListVO businessVO = (BusinessInfoListVO)FEBAAVOFactory.createInstance(TypesCatalogueConstants.BusinessInfoListVO);
	        		BusinessInfoVO businessInfoVO = (BusinessInfoVO)FEBAAVOFactory.createInstance(TypesCatalogueConstants.BusinessInfoVO);
	        		FEBAArrayList<BusinessInfoVO> list =businessListVO.getInfoList();
	        		FEBAArrayList<BusinessInfoVO> infoList = new FEBAArrayList<BusinessInfoVO>(); 
	        		for(int i=0;i<list.size();i++){
	        			BusinessInfoVO bussinessInfo= list.get(i);
	        			if(bussinessInfo.getCode().getValue() == 105411){
	        				FEBAUnboundString message = new FEBAUnboundString(
	        						"Company Details Added successfully");
	        				businessInfoVO.setDispMessage(message);
	        				infoList.add(businessInfoVO);
	        			}
	        			else{
	        				infoList.add(bussinessInfo);        				
	        			}
	        		}
	        		
	        				
	        		RestResourceManager.updateHeaderFooterResponseData(wrapper, request, response, 0);

			 }
		}
		catch (Throwable e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request, e, restResponseErrorCodeMapping());
		}

		 return Response.ok().build();
	}


	public Response fetchRequest(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();



		return restResponceErrorCode;
	}



}
