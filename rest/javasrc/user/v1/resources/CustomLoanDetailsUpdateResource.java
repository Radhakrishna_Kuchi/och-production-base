package com.infosys.custom.ebanking.rest.user.v1.resources;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomLoanDetailsUpdateInputReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomApplicationStatusVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCreateLoanReferenceResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationCreationOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomLoanApplicationCreationResponseVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBAAmount;
import com.infosys.feba.framework.types.primitives.FEBACurrencyCode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundDouble;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Pratik Shah
 *
 *         This is the Resource through which Loan creation maintenance is done.
 *
 */
@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Loan Details Maintenance")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/updateLoanDetails")
public class CustomLoanDetailsUpdateResource extends ResourceAuthenticator {

	@Context
	protected HttpServletRequest request;

	final IClientStub loanUpdateServiceStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanApplicationService"));


	/**
	 * This method is used for creating loan
	 * 
	 * @author Vidhi_Kapoor01
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Create Loan based applicationId and user id", response = CustomCreateLoanReferenceResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_LOAN_DETAILS_UPDATE_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Record Updated successfully.", response = CustomCreateLoanReferenceResponseVO.class) })
	public Response updateRequest(
			CustomLoanDetailsUpdateInputReferencesVO customLoanDetailsUpdateReferencesVO,
			@ApiParam(value = "The ID of the user", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "The Application ID", required = true) @Size(min = 0, max = 32) @PathParam("applicationId") String applicationId) {

		CustomLoanApplicationCreationResponseVO responseVO = new CustomLoanApplicationCreationResponseVO();

		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");

		try {
			isUnAuthenticatedRequest();
			CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO");
			
			if(null != customLoanDetailsUpdateReferencesVO){
				
				String plafond = customLoanDetailsUpdateReferencesVO.getSelectedPlafond();
				String tenor = customLoanDetailsUpdateReferencesVO.getSelectedTenor();
				FEBAAmount plafondAmount = new FEBAAmount();
				String homeCurrency = PropertyUtil.getProperty("HOME_CUR_CODE", opcontext);

				if((null!=plafond && !plafond.equalsIgnoreCase(""))
						&& (null!=tenor && !tenor.equalsIgnoreCase(""))){
					if(plafond.equalsIgnoreCase("0") && tenor.equalsIgnoreCase("0")){
						detailsVO.setApplicationStatus(new CommonCode("CR_SCORE_REJ")); 
					}else{
						detailsVO.setApplicationStatus(new CommonCode("CR_SCORE_APR")); 
						plafondAmount.setAmount(new FEBAUnboundDouble(plafond));
						plafondAmount.setCurrencyCode(new FEBACurrencyCode(homeCurrency));
						detailsVO.setApprovedAmount(plafondAmount);
						detailsVO.setRequestedTenor(tenor);
					}
				}
			} else{
				throw new BusinessException(true, opcontext, CustomEBankingIncidenceCodes.CREDIT_SCORE_CALLBACK_TABLE_EXC,
						"Plafond and Tenor must not be null", null, CustomEBankingErrorCodes.CREDIT_SCORE_CALLBACK_TABLE_EXC, null); 
			}
			
			
			detailsVO.setApplicationId(new ApplicationNumber(applicationId));
			detailsVO.setUserId(new UserId(userid));
			
			CustomLoanApplicationCreationOutputVO outputVO = updateLoanDetails(detailsVO, opcontext);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
		return Response.ok().entity(responseVO).build();
	}
	private CustomLoanApplicationCreationOutputVO updateLoanDetails(CustomLoanApplnMasterDetailsVO detailsVO, IOpContext context)
			throws CriticalException, BusinessException, BusinessConfirmation, FEBATypeSystemException{
		CustomLoanApplicationCreationOutputVO outputVO = new CustomLoanApplicationCreationOutputVO();
		loanUpdateServiceStub.callService(context, detailsVO, new FEBAUnboundString("modifyLoanDetails"));
		outputVO.setApplicationId(detailsVO.getApplicationId().getValue());
		
		return outputVO;
	}

}
