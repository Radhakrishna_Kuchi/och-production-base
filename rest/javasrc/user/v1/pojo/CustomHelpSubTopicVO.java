package com.infosys.custom.ebanking.rest.user.v1.pojo;

import io.swagger.annotations.ApiModelProperty;

public class CustomHelpSubTopicVO {

	@ApiModelProperty(value = "Sub Topic")
	private String subTopic;

	@ApiModelProperty(value = "Sub Topic details")
	private String details;

	public String getSubTopic() {
		return subTopic;
	}

	public void setSubTopic(String subTopic) {
		this.subTopic = subTopic;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
