package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Customer one month payment details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomOneMonthlyPaymentDetails {
	
	@ApiModelProperty(value = "monthly payment for planfond1 with one month")
	private double oneMonthPayForPlafond1;
	
	@ApiModelProperty(value = "monthly payment for planfond2 with one month")
	private double oneMonthPayForPlafond2;

	public double getOneMonthPayForPlafond1() {
		return oneMonthPayForPlafond1;
	}


	public void setOneMonthPayForPlafond1(double oneMonthPayForPlafond1) {
		this.oneMonthPayForPlafond1 = oneMonthPayForPlafond1;
	}


	public double getOneMonthPayForPlafond2() {
		return oneMonthPayForPlafond2;
	}


	public void setOneMonthPayForPlafond2(double oneMonthPayForPlafond2) {
		this.oneMonthPayForPlafond2 = oneMonthPayForPlafond2;
	}


	public CustomOneMonthlyPaymentDetails() {
		super();

	}

}
