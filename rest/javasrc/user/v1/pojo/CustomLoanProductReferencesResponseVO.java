package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanProductReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomLoanProductReferencesVO> data;
	
	public List<CustomLoanProductReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomLoanProductReferencesVO> data) {
		this.data = data;
	}

	
	
}
