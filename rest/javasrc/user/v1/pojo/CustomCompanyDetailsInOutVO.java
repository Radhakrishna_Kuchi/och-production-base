package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Company Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomCompanyDetailsInOutVO {
	
	@ApiModelProperty(value = "The name of the company")
	private String companyName;
	
	@ApiModelProperty(value = "The address of the company")
	private String companyAddress;
	
	@ApiModelProperty(value = "The address of the company")
	private String bankId;
	
	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public CustomCompanyDetailsInOutVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomCompanyDetailsInOutVO(String companyName, String companyAddress) {
		super();
		this.companyName = companyName;
		this.companyAddress = companyAddress;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
}
