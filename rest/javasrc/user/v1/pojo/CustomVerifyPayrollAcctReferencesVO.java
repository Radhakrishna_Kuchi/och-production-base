package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value ="Verify Payroll account details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)

public class CustomVerifyPayrollAcctReferencesVO {

	@ApiModelProperty(value = "CIF No")
	private String cifNo;
	
	@ApiModelProperty(value = "Status Rekening")
	private String statusRekening;
	
	@ApiModelProperty(value = "Savings Amount")
	private String savingsAmount;

	public String getCifNo() {
		return cifNo;
	}

	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}

	public String getStatusRekening() {
		return statusRekening;
	}

	public void setStatusRekening(String statusRekening) {
		this.statusRekening = statusRekening;
	}

	public String getSavingsAmount() {
		return savingsAmount;
	}

	public void setSavingsAmount(String savingsAmount) {
		this.savingsAmount = savingsAmount;
	}

}
