package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "ATM Inquiry CAMS Call")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomCAMSCallReferencesVO {

	
	@ApiModelProperty(value = "KTP")
	private String ktp;

	@ApiModelProperty(value = "Card Number")
	private String cardNumber;
	
	@ApiModelProperty(value = "Expiry Date")
	private String expiryDate;

	@ApiModelProperty(value = "Account Number")
	private String accountNumber;

	@ApiModelProperty(value = "Branch Code")
	private String branchCode;
	
	@ApiModelProperty(value = "Bank Code", required  = true)
	private String bankCode;
	
	@ApiModelProperty(value = "product Code", required  = true)
	private String productCode;
	
	public String getKtp() {
		return ktp;
	}
	public void setKtp(String ktp) {
		this.ktp = ktp;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public CustomCAMSCallReferencesVO(){
		super();
	}
	
	
	public CustomCAMSCallReferencesVO(String ktp, String cardNumber, String expiryDate, String accountNumber,
			String branchCode, String bankCode, String productCode) {
		super();
		this.ktp = ktp;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
		this.accountNumber = accountNumber;
		this.branchCode = branchCode;
		this.bankCode = bankCode;
		this.productCode = productCode;
	}
	
	
}
