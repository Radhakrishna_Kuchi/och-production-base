package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.retail.user.v1.pojo.RetailUserOutputDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Device registration output")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomDeviceRegistrationOutputVO {

	public RetailUserOutputDetailsVO getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(RetailUserOutputDetailsVO userDetail) {
		this.userDetail = userDetail;
	}

	@ApiModelProperty(value="Specify the online registration retail output details.", required=false)
	public RetailUserOutputDetailsVO userDetail;
}
