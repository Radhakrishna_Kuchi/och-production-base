package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomCreateLoanReferenceResponseVO extends RestHeaderFooterHandler {
	private List<CustomCreateLoanReferenceVO> data;

	public List<CustomCreateLoanReferenceVO> getData() {
		return data;
	}

	public void setData(List<CustomCreateLoanReferenceVO> data) {
		this.data = data;
	}
}
