package com.infosys.custom.ebanking.rest.user.v1.pojo;


import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomDocTextResponseReferencesResponsePOJO extends RestHeaderFooterHandler{
	private List<CustomDocTextResponsePOJO> data;
	
	public List<CustomDocTextResponsePOJO> getData() {
		return data;
	}

	public void setData(List<CustomDocTextResponsePOJO> data) {
		this.data = data;
	}

}
