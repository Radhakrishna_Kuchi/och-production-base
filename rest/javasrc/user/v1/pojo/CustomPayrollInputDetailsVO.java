package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan application input details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomPayrollInputDetailsVO {
	@ApiModelProperty(value = "Application id", required = true)
	private Long applicationId;
	
	@ApiModelProperty(value = "Payroll account number", required = true)
	private String payrollAccount;
	
	@ApiModelProperty(value = "Payroll account branch", required = true)
	private String payrollAccountBranch;
	
	@ApiModelProperty(value = "Agent code", required = true)
	private String agentCode;
	
	@ApiModelProperty(value = "Payroll account date", required = true)
	private Date payrollDate;
	
	@ApiModelProperty(value = "Referral Code", required = true)
	private String referralCode;
	
	@ApiModelProperty(value = "Saving amount", required = true)
	private double savingAmount;
	
	@ApiModelProperty(value = "Employee Status", required = true)
	private String employeeStatus;
	
	@ApiModelProperty(value = "Credit Frequency3")
	private int creditFre3;	

	@ApiModelProperty(value = "Credit Frequency6")
	private int creditFre6;
	
	@ApiModelProperty(value = "Credit Frequency12")
	private int creditFre12;
	
	@ApiModelProperty(value = "Debit Frequency3")
	private int debitFre3;
	
	@ApiModelProperty(value = "Debit Frequency6")
	private int debitFre6;
	
	@ApiModelProperty(value = "Debit Frequency12")
	private int debitFre12;
	
	@ApiModelProperty(value = "Credit Total3")
	private double creditTotal3;
	
	@ApiModelProperty(value = "Credit Total6")
	private double creditTotal6;
	
	@ApiModelProperty(value = "Credit Total12")
	private double creditTotal12;
	
	@ApiModelProperty(value = "Debit Total3")
	private double debitTotal3;
	
	@ApiModelProperty(value = "Debit Total6")
	private double debitTotal6;
	
	@ApiModelProperty(value = "Debit Total12")
	private double debitTotal12;
	
	@ApiModelProperty(value = "CreditAvg3")
	private double creditAvg3;
	
	@ApiModelProperty(value = "CreditAvg6")
	private double creditAvg6;
	
	@ApiModelProperty(value = "CreditAvg12")
	private double creditAvg12;
	
	@ApiModelProperty(value = "DebitAvg3")
	private double debitAvg3;
	
	@ApiModelProperty(value = "DebitAvg6")
	private double debitAvg6;
	
	@ApiModelProperty(value = "DebitAvg12")
	private double debitAvg12;
	
	@ApiModelProperty(value = "Internet Banking")
	private char netBanking;
	
	@ApiModelProperty(value = "Mobile Banking")
	private char mobileBanking;
	
	@ApiModelProperty(value = "Primary Phone")
	private char primaryPhone;
	
	@ApiModelProperty(value = "Secondary Phone")
	private char secPhone;	

	@ApiModelProperty(value = "Saving Account Ownership Status")
	private char savingAccOwnshpSts;
	
	@ApiModelProperty(value = "Saving Account Ownership Date")
	private Date savingAccOwnshpDate;
	
	@ApiModelProperty(value = "Company Name")
	private String companyName;	

	@ApiModelProperty(value = "Personel Name")
	private String personelName;
	
	@ApiModelProperty(value = "Personel Number")
	private Long personelNumber;
	
	/*
	 * Added fields to map WhiteList data
	 */
	
	@ApiModelProperty(value = "Whitelist Name")
	private String nameWL;
	
	@ApiModelProperty(value = "Whitelist Net Income")
	private double netIncomeWL;
	
	@ApiModelProperty(value = "Whitelist Birth Date")
	private Date birthDateWL;
	
	@ApiModelProperty(value = "Whitelist employement start Date")
	private Date empStartDateWL;
	
	@ApiModelProperty(value = "Whitelist employement end Date")
	private Date empEndDateWL;
	
	@ApiModelProperty(value = "Whitelist Gender")
	private String genderWL;
	
	@ApiModelProperty(value = "Whitelist Address")
	private String addressWL;
	
	@ApiModelProperty(value = "Whitelist Place of Birth")
	private String placeOfBirthWL;
	
	@ApiModelProperty(value = "Expenditure amount")
	private double expenditureAmount;
		
	public String getGenderWL() {
		return genderWL;
	}

	public void setGenderWL(String genderWL) {
		this.genderWL = genderWL;
	}

	public String getAddressWL() {
		return addressWL;
	}

	public void setAddressWL(String addressWL) {
		this.addressWL = addressWL;
	}

	public String getPlaceOfBirthWL() {
		return placeOfBirthWL;
	}

	public void setPlaceOfBirthWL(String placeOfBirthWL) {
		this.placeOfBirthWL = placeOfBirthWL;
	}

	public String getNameWL() {
		return nameWL;
	}

	public void setNameWL(String nameWL) {
		this.nameWL = nameWL;
	}

	public double getNetIncomeWL() {
		return netIncomeWL;
	}

	public CustomPayrollInputDetailsVO() {
		super();
	}

	public void setNetIncomeWL(double netIncomeWL) {
		this.netIncomeWL = netIncomeWL;
	}

	public Date getBirthDateWL() {
		return birthDateWL;
	}

	public void setBirthDateWL(Date birthDateWL) {
		this.birthDateWL = birthDateWL;
	}

	public Date getEmpStartDateWL() {
		return empStartDateWL;
	}

	public void setEmpStartDateWL(Date empStartDateWL) {
		this.empStartDateWL = empStartDateWL;
	}

	public Date getEmpEndDateWL() {
		return empEndDateWL;
	}

	public void setEmpEndDateWL(Date empEndDateWL) {
		this.empEndDateWL = empEndDateWL;
	}

	public void setPersonelNumber(Long personelNumber) {
		this.personelNumber = personelNumber;
	}

	
	
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getPayrollAccount() {
		return payrollAccount;
	}

	public void setPayrollAccount(String payrollAccount) {
		this.payrollAccount = payrollAccount;
	}

	public String getPayrollAccountBranch() {
		return payrollAccountBranch;
	}

	public void setPayrollAccountBranch(String payrollAccountBranch) {
		this.payrollAccountBranch = payrollAccountBranch;
	}
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}

	
	public double getSavingAmount() {
		return savingAmount;
	}

	public void setSavingAmount(double savingAmount) {
		this.savingAmount = savingAmount;
	}

	public int getCreditFre3() {
		return creditFre3;
	}

	public void setCreditFre3(int creditFre3) {
		this.creditFre3 = creditFre3;
	}

	public int getCreditFre6() {
		return creditFre6;
	}

	public void setCreditFre6(int creditFre6) {
		this.creditFre6 = creditFre6;
	}

	public int getCreditFre12() {
		return creditFre12;
	}

	public void setCreditFre12(int creditFre12) {
		this.creditFre12 = creditFre12;
	}

	public int getDebitFre3() {
		return debitFre3;
	}

	public void setDebitFre3(int debitFre3) {
		this.debitFre3 = debitFre3;
	}

	public int getDebitFre6() {
		return debitFre6;
	}

	public void setDebitFre6(int debitFre6) {
		this.debitFre6 = debitFre6;
	}

	public int getDebitFre12() {
		return debitFre12;
	}

	public void setDebitFre12(int debitFre12) {
		this.debitFre12 = debitFre12;
	}

	public double getCreditTotal3() {
		return creditTotal3;
	}

	public void setCreditTotal3(double creditTotal3) {
		this.creditTotal3 = creditTotal3;
	}

	public double getCreditTotal6() {
		return creditTotal6;
	}

	public void setCreditTotal6(double creditTotal6) {
		this.creditTotal6 = creditTotal6;
	}

	public double getCreditTotal12() {
		return creditTotal12;
	}

	public void setCreditTotal12(double creditTotal12) {
		this.creditTotal12 = creditTotal12;
	}

	public double getDebitTotal3() {
		return debitTotal3;
	}

	public void setDebitTotal3(double debitTotal3) {
		this.debitTotal3 = debitTotal3;
	}

	public double getDebitTotal6() {
		return debitTotal6;
	}

	public void setDebitTotal6(double debitTotal6) {
		this.debitTotal6 = debitTotal6;
	}

	public double getDebitTotal12() {
		return debitTotal12;
	}

	public void setDebitTotal12(double debitTotal12) {
		this.debitTotal12 = debitTotal12;
	}

	public double getCreditAvg3() {
		return creditAvg3;
	}

	public void setCreditAvg3(double creditAvg3) {
		this.creditAvg3 = creditAvg3;
	}

	public double getCreditAvg6() {
		return creditAvg6;
	}

	public void setCreditAvg6(double creditAvg6) {
		this.creditAvg6 = creditAvg6;
	}

	public double getCreditAvg12() {
		return creditAvg12;
	}

	public void setCreditAvg12(double creditAvg12) {
		this.creditAvg12 = creditAvg12;
	}

	public double getDebitAvg3() {
		return debitAvg3;
	}

	public void setDebitAvg3(double debitAvg3) {
		this.debitAvg3 = debitAvg3;
	}

	public double getDebitAvg6() {
		return debitAvg6;
	}

	public void setDebitAvg6(double debitAvg6) {
		this.debitAvg6 = debitAvg6;
	}

	public double getDebitAvg12() {
		return debitAvg12;
	}

	public void setDebitAvg12(double debitAvg12) {
		this.debitAvg12 = debitAvg12;
	}

	public char getNetBanking() {
		return netBanking;
	}

	public void setNetBanking(char netBanking) {
		this.netBanking = netBanking;
	}

	public char getMobileBanking() {
		return mobileBanking;
	}

	public void setMobileBanking(char mobileBanking) {
		this.mobileBanking = mobileBanking;
	}

	public char getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(char primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	
	public char getSecPhone() {
		return secPhone;
	}

	public void setSecPhone(char secPhone) {
		this.secPhone = secPhone;
	}

	public char getSavingAccOwnshpSts() {
		return savingAccOwnshpSts;
	}

	public void setSavingAccOwnshpSts(char savingAccOwnshpSts) {
		this.savingAccOwnshpSts = savingAccOwnshpSts;
	}

	public Date getSavingAccOwnshpDate() {
		return savingAccOwnshpDate;
	}

	public void setSavingAccOwnshpDate(Date savingAccOwnshpDate) {
		this.savingAccOwnshpDate = savingAccOwnshpDate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPersonelName() {
		return personelName;
	}

	public void setPersonelName(String personelName) {
		this.personelName = personelName;
	}

	public long getPersonelNumber() {
		return personelNumber;
	}

	public void setPersonelNumber(long personelNumber) {
		this.personelNumber = personelNumber;
	}

	public String getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public double getExpenditureAmount() {
		return expenditureAmount;
	}

	public void setExpenditureAmount(double expenditureAmount) {
		this.expenditureAmount = expenditureAmount;
	}

}
