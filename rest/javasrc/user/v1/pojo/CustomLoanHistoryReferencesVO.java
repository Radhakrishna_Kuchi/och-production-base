package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan History List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanHistoryReferencesVO {

	
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public int getInstallmentNo() {
		return installmentNo;
	}

	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}

	public CustomAmountVO getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(CustomAmountVO paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@ApiModelProperty(value = "Payment Sl Number")
	private int installmentNo;
	
	@ApiModelProperty(value = "Payment amount")
	private CustomAmountVO paymentAmount;		

	@ApiModelProperty(value = "Txn Date")
	private String txnDate;
	
	@ApiModelProperty(value = "Payment Method")
	private String paymentMethod;
	
	@ApiModelProperty(value = "Txn Id")
	private String trnId;
	
	public String getTrnId() {
		return trnId;
	}

	public void setTrnId(String trnId) {
		this.trnId = trnId;
	}

	public String getTrnType() {
		return trnType;
	}

	public void setTrnType(String trnType) {
		this.trnType = trnType;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getTxnParticulars() {
		return txnParticulars;
	}

	public void setTxnParticulars(String txnParticulars) {
		this.txnParticulars = txnParticulars;
	}

	@ApiModelProperty(value = "Txn Type")
	private String trnType;
	
	@ApiModelProperty(value = "Value Date")
	private String valueDate;
	
	@ApiModelProperty(value = "Transaction Particulars")
	private String txnParticulars;
	
	@ApiModelProperty(value = "Loan Account Id")
	private String accountId;

	
}
