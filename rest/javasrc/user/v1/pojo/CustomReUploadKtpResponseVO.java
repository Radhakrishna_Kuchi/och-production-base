package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomReUploadKtpResponseVO extends RestHeaderFooterHandler {
	private CustomReUploadKtpOutputVO data = null;
	
	public void setData(CustomReUploadKtpOutputVO data) {
		this.data = data;
	}

	public CustomReUploadKtpOutputVO getData() {
		return data;
	}

}
