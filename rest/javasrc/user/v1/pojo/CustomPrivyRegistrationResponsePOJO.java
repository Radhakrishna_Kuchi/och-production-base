package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Privy Registration details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomPrivyRegistrationResponsePOJO {

	@ApiModelProperty(value = "User Id")
	String userId;
	@ApiModelProperty(value = "Privy Id")
	String privyId;
	@ApiModelProperty(value = "KTP Number")
	String ktpNum;
	@ApiModelProperty(value = "User Name")
	String userName;

	@ApiModelProperty(value = "messsage")
	String message;
	
	@ApiModelProperty(value = "status")
	String status;

	
	
	public CustomPrivyRegistrationResponsePOJO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomPrivyRegistrationResponsePOJO(String userId, String privyId, String ktpNum, String userName,
			String message, String status) {
		super();
		this.userId = userId;
		this.privyId = privyId;
		this.ktpNum = ktpNum;
		this.userName = userName;
		this.message = message;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPrivyId() {
		return privyId;
	}

	public void setPrivyId(String privyId) {
		this.privyId = privyId;
	}

	public String getKtpNum() {
		return ktpNum;
	}

	public void setKtpNum(String ktpNum) {
		this.ktpNum = ktpNum;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
