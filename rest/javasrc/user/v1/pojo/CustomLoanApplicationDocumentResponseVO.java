/**
 * CustomLoanApplicationDocumentResponseVO.java
 * @since Oct 4, 2018 - 11:21:16 AM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

/**
 * @author Tarun_Kumar18
 *
 */
public class CustomLoanApplicationDocumentResponseVO extends RestHeaderFooterHandler {

	private List<CustomLoanApplicationDocumentListReferenceVO> data = null;

	public List<CustomLoanApplicationDocumentListReferenceVO> getData() {
		return data;
	}

	public void setData(List<CustomLoanApplicationDocumentListReferenceVO> data) {
		this.data = data;
	}

}
