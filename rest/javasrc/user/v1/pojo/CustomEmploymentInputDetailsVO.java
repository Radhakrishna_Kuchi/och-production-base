package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Employment details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomEmploymentInputDetailsVO {
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "npwp")
	private String npwp;
	
	@ApiModelProperty(value = "Monthly income")
	private double monthlyIncome;
	
	@ApiModelProperty(value = "monthly expenditure")
	private double monthlyExpenditure;
	
	@ApiModelProperty(value = "financial source")
	private String financialSource;
	
	@ApiModelProperty(value = "Work Type")
	private String workType;

	@ApiModelProperty(value = "Employer name")
	private String employerName;

	@ApiModelProperty(value = "Employer status")
	private String employerStatus;
	
	@ApiModelProperty(value = "Employee start date")
	private Date empStartDate;
	
	@ApiModelProperty(value = "Employee end date")
	private Date empEndDate;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public double getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(double monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public double getMonthlyExpenditure() {
		return monthlyExpenditure;
	}

	public void setMonthlyExpenditure(double monthlyExpenditure) {
		this.monthlyExpenditure = monthlyExpenditure;
	}

	public String getFinancialSource() {
		return financialSource;
	}

	public void setFinancialSource(String financialSource) {
		this.financialSource = financialSource;
	}

	public String getWorkType() {
		return workType;
	}

	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getEmployerStatus() {
		return employerStatus;
	}

	public void setEmployerStatus(String employerStatus) {
		this.employerStatus = employerStatus;
	}

	public Date getEmpStartDate() {
		return empStartDate;
	}

	public void setEmpStartDate(Date empStartDate) {
		this.empStartDate = empStartDate;
	}

	public Date getEmpEndDate() {
		return empEndDate;
	}

	public void setEmpEndDate(Date empEndDate) {
		this.empEndDate = empEndDate;
	}

}
