package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomRejectionDetailsVO {
	@ApiModelProperty(value = "Next eligibility date")
	private Date nextEligibilityDate;
	
	@ApiModelProperty(value = "Missing Simpanan response code")
	private String responseCode;

	public Date getNextEligibilityDate() {
		return nextEligibilityDate;
	}

	public void setNextEligibilityDate(Date nextEligibilityDate) {
		this.nextEligibilityDate = nextEligibilityDate;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
}
