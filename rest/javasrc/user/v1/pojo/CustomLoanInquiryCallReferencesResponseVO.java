package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanInquiryCallReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomLoanInquiryCallReferencesVO> data;
	
	public List<CustomLoanInquiryCallReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomLoanInquiryCallReferencesVO> data) {
		this.data = data;
	}

	
	
}
