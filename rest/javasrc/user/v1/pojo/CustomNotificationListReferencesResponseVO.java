package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomNotificationListReferencesResponseVO extends RestHeaderFooterHandler {
	
	
	
	public CustomNotificationListReferencesResponseVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private List<CustomNotificationListReferencesVO> list;

	public List<CustomNotificationListReferencesVO> getList() {
		return list;
	}

	public CustomNotificationListReferencesResponseVO(List<CustomNotificationListReferencesVO> list) {
		super();
		this.list = list;
	}

	public void setList(List<CustomNotificationListReferencesVO> list) {
		this.list = list;
	}

}
