package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Offline KTP Verififcation")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomOfflineKTPVerificationReferencesVO {

	@ApiModelProperty(value = "status")
	private String isKTPVerified;

	@ApiModelProperty(value = "responseStatus")
	private String responseStatus;
	
	@ApiModelProperty(value = "responseCode")
	private String responseCode;

	public String getIsKTPVerified() {
		return isKTPVerified;
	}

	public void setIsKTPVerified(String isKTPVerified) {
		this.isKTPVerified = isKTPVerified;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public CustomOfflineKTPVerificationReferencesVO(String isKTPVerified, String responseStatus, String responseCode) {
		super();
		this.isKTPVerified = isKTPVerified;
		this.responseStatus = responseStatus;
		this.responseCode = responseCode;
	}

	public CustomOfflineKTPVerificationReferencesVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	
}
