package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Loan Inquiry Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanInquiryCallReferencesVO {

	@ApiModelProperty(value = "accountId")
	private String accountId;

	@ApiModelProperty(value = "userid")
	private String userid;

	@ApiModelProperty(value = "customerId")
	private String customerId;

	@ApiModelProperty(value = "loanSrlNum")
	private String loanSrlNum;

	@ApiModelProperty(value = "loanStartDate")
	private Date loanStartDate;

	@ApiModelProperty(value = "overdueDays")
	private String overdueDays;

	@ApiModelProperty(value = "countofTotalInstallement")
	private String countofTotalInstallement;

	@ApiModelProperty(value = "countOfPendingInstallement")
	private String countOfPendingInstallement;

	@ApiModelProperty(value = "accountCurrency")
	private CodeReferencesVO accountCurrency;

	@ApiModelProperty(value = "accountStatus")
	private CodeReferencesVO accountStatus;

	@ApiModelProperty(value = "totalOutstandingAmount")
	private CustomAmountVO totalOutstandingAmount;

	@ApiModelProperty(value = "sactionAmount")
	private CustomAmountVO sactionAmount;

	@ApiModelProperty(value = "installmentAmountPerMonth")
	private CustomAmountVO installmentAmountPerMonth;

	@ApiModelProperty(value = "currentInstallementAmount")
	private CustomAmountVO currentInstallementAmount;

	@ApiModelProperty(value = "currentInstallementPrincipal")
	private CustomAmountVO currentInstallementPrincipal;

	@ApiModelProperty(value = "currentInstallmentInterest")
	private CustomAmountVO currentInstallmentInterest;

	@ApiModelProperty(value = "overdueInstallementAmount")
	private CustomAmountVO overdueInstallementAmount;

	@ApiModelProperty(value = "overdueInstallementPrincipal")
	private CustomAmountVO overdueInstallementPrincipal;

	@ApiModelProperty(value = "overdueInstallmentInterest")
	private CustomAmountVO overdueInstallmentInterest;

	@ApiModelProperty(value = "overdueInstallmentPenality")
	private CustomAmountVO overdueInstallmentPenality;

	@ApiModelProperty(value = "dueDays")
	private long dueDays;

	@ApiModelProperty(value = "dueDate")
	private String dueDate;

	@ApiModelProperty(value = "paymentAllowed")
	private String paymentAllowed;
	
	@ApiModelProperty(value = "totalOutstandingPriAmount")
	private CustomAmountVO totalOutstandingPriAmount;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getLoanSrlNum() {
		return loanSrlNum;
	}

	public void setLoanSrlNum(String loanSrlNum) {
		this.loanSrlNum = loanSrlNum;
	}

	public Date getLoanStartDate() {
		return loanStartDate;
	}

	public void setLoanStartDate(Date loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	public String getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(String overdueDays) {
		this.overdueDays = overdueDays;
	}

	public String getCountofTotalInstallement() {
		return countofTotalInstallement;
	}

	public void setCountofTotalInstallement(String countofTotalInstallement) {
		this.countofTotalInstallement = countofTotalInstallement;
	}

	public String getCountOfPendingInstallement() {
		return countOfPendingInstallement;
	}

	public void setCountOfPendingInstallement(String countOfPendingInstallement) {
		this.countOfPendingInstallement = countOfPendingInstallement;
	}

	public CodeReferencesVO getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(CodeReferencesVO accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public CodeReferencesVO getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(CodeReferencesVO accountStatus) {
		this.accountStatus = accountStatus;
	}

	public CustomAmountVO getTotalOutstandingAmount() {
		return totalOutstandingAmount;
	}

	public void setTotalOutstandingAmount(CustomAmountVO totalOutstandingAmount) {
		this.totalOutstandingAmount = totalOutstandingAmount;
	}

	public CustomAmountVO getSactionAmount() {
		return sactionAmount;
	}

	public void setSactionAmount(CustomAmountVO sactionAmount) {
		this.sactionAmount = sactionAmount;
	}

	public CustomAmountVO getInstallmentAmountPerMonth() {
		return installmentAmountPerMonth;
	}

	public void setInstallmentAmountPerMonth(CustomAmountVO installmentAmountPerMonth) {
		this.installmentAmountPerMonth = installmentAmountPerMonth;
	}

	public CustomAmountVO getCurrentInstallementAmount() {
		return currentInstallementAmount;
	}

	public void setCurrentInstallementAmount(CustomAmountVO currentInstallementAmount) {
		this.currentInstallementAmount = currentInstallementAmount;
	}

	public CustomAmountVO getCurrentInstallementPrincipal() {
		return currentInstallementPrincipal;
	}

	public void setCurrentInstallementPrincipal(CustomAmountVO currentInstallementPrincipal) {
		this.currentInstallementPrincipal = currentInstallementPrincipal;
	}

	public CustomAmountVO getCurrentInstallmentInterest() {
		return currentInstallmentInterest;
	}

	public void setCurrentInstallmentInterest(CustomAmountVO currentInstallmentInterest) {
		this.currentInstallmentInterest = currentInstallmentInterest;
	}

	public CustomAmountVO getOverdueInstallementAmount() {
		return overdueInstallementAmount;
	}

	public void setOverdueInstallementAmount(CustomAmountVO overdueInstallementAmount) {
		this.overdueInstallementAmount = overdueInstallementAmount;
	}

	public CustomAmountVO getOverdueInstallementPrincipal() {
		return overdueInstallementPrincipal;
	}

	public void setOverdueInstallementPrincipal(CustomAmountVO overdueInstallementPrincipal) {
		this.overdueInstallementPrincipal = overdueInstallementPrincipal;
	}

	public CustomAmountVO getOverdueInstallmentInterest() {
		return overdueInstallmentInterest;
	}

	public void setOverdueInstallmentInterest(CustomAmountVO overdueInstallmentInterest) {
		this.overdueInstallmentInterest = overdueInstallmentInterest;
	}

	public CustomAmountVO getOverdueInstallmentPenality() {
		return overdueInstallmentPenality;
	}

	public void setOverdueInstallmentPenality(CustomAmountVO overdueInstallmentPenality) {
		this.overdueInstallmentPenality = overdueInstallmentPenality;
	}

	public long getDueDays() {
		return dueDays;
	}

	public void setDueDays(long dueDays) {
		this.dueDays = dueDays;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getPaymentAllowed() {
		return paymentAllowed;
	}

	public void setPaymentAllowed(String paymentAllowed) {
		this.paymentAllowed = paymentAllowed;
	}

	public CustomAmountVO getTotalOutstandingPriAmount() {
		return totalOutstandingPriAmount;
	}

	public void setTotalOutstandingPriAmount(CustomAmountVO totalOutstandingPriAmount) {
		this.totalOutstandingPriAmount = totalOutstandingPriAmount;
	}
}
