package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanLimitInqResponseVO extends RestHeaderFooterHandler {
	private CustomLoanLimitDetailsVO data = null;
	
	public void setData(CustomLoanLimitDetailsVO data) {
		this.data = data;
	}

	public CustomLoanLimitDetailsVO getData() {
		return data;
	}
}
