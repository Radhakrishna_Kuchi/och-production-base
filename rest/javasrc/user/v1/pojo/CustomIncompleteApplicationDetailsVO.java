package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomIncompleteApplicationDetailsVO {
	@ApiModelProperty(value = "approval amount")
	private double approvalAmount;
	
	@ApiModelProperty(value = "validity date")
	private Date validityDate;

	public double getApprovalAmount() {
		return approvalAmount;
	}

	public void setApprovalAmount(double approvalAmount) {
		this.approvalAmount = approvalAmount;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}
}
