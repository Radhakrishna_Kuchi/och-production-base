package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomBankCodFetchReferencesResponseVO extends RestHeaderFooterHandler{
	
	private List<CustomBankCodeFetchReferencesVO> data;

	public List<CustomBankCodeFetchReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomBankCodeFetchReferencesVO> data) {
		this.data = data;
	}


	

}
