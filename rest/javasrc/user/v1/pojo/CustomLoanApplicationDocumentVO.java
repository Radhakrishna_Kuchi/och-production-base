/**
 * CustomLoanApplicationDocumentVO.java
 * @since Oct 4, 2018 - 11:24:05 AM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Tarun_Kumar18
 *
 */
public class CustomLoanApplicationDocumentVO {
	
	@ApiModelProperty(value = "Document Type Code Reference") 
	private CodeReferencesVO documentType;

	@ApiModelProperty(value = "File Sequence Number of document") 
	private String fileSeqNum;

	public CodeReferencesVO getDocumentType() {
		return documentType;
	}

	public void setDocumentType(CodeReferencesVO documentType) {
		this.documentType = documentType;
	}

	public String getFileSeqNum() {
		return fileSeqNum;
	}

	public void setFileSeqNum(String fileSeqNum) {
		this.fileSeqNum = fileSeqNum;
	}

}
