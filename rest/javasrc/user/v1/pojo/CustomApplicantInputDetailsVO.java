package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan applicant details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomApplicantInputDetailsVO {
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "email Id")
	private String emailId;
	
	@ApiModelProperty(value = "mobile number")
	private Long mobileNo;
	
	@ApiModelProperty(value = "name")
	private String name;
	
	@ApiModelProperty(value = "gender")
	private String gender;
	
	@ApiModelProperty(value = "place of birth")
	private String placeOfBirth;
	
	@ApiModelProperty(value = "date of birth")
	private Date dateOfBirth;
	
	@ApiModelProperty(value = "national id")
	private String nationalId;
	
	@ApiModelProperty(value = "last education")
	private String lastEducation;
	
	@ApiModelProperty(value = "credit card issuer")
	private String creditCardIssuer;

	@ApiModelProperty(value = "Address line 1")
	private String addressLine1;
	
	@ApiModelProperty(value = "Address line 2")
	private String addressLine2;
	
	@ApiModelProperty(value = "Address line 3")
	private String addressLine3;
	
	@ApiModelProperty(value = "Address line 4")
	private String addressLine4;
	
	@ApiModelProperty(value = "Address line 5")
	private String addressLine5;
	
	@ApiModelProperty(value = "Province")
	private String province;
	
	@ApiModelProperty(value = "city")
	private String city;
	
	@ApiModelProperty(value = "Postal code")
	private Long postalCode;
	
	@ApiModelProperty(value = "home ownership status")
	private String homeOwnershipStatus;
	
	@ApiModelProperty(value = "home ownership duration")
	private int homeOwnershipDuration;
	
	@ApiModelProperty(value = "home phone number")
	private Long homePhoneNumber;
	
	@ApiModelProperty(value = "religion")
	private String religion;

	@ApiModelProperty(value = "is address same as ktp")
	private String isAddressSameAsKTP;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(String lastEducation) {
		this.lastEducation = lastEducation;
	}

	public String getCreditCardIssuer() {
		return creditCardIssuer;
	}

	public void setCreditCardIssuer(String creditCardIssuer) {
		this.creditCardIssuer = creditCardIssuer;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getAddressLine5() {
		return addressLine5;
	}

	public void setAddressLine5(String addressLine5) {
		this.addressLine5 = addressLine5;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}

	public String getHomeOwnershipStatus() {
		return homeOwnershipStatus;
	}

	public void setHomeOwnershipStatus(String homeOwnershipStatus) {
		this.homeOwnershipStatus = homeOwnershipStatus;
	}

	public int getHomeOwnershipDuration() {
		return homeOwnershipDuration;
	}

	public void setHomeOwnershipDuration(int homeOwnershipDuration) {
		this.homeOwnershipDuration = homeOwnershipDuration;
	}


	public Long getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(Long homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}
	
	public String getIsAddressSameAsKTP() {
		return isAddressSameAsKTP;
	}

	public void setIsAddressSameAsKTP(String isAddressSameAsKTP) {
		this.isAddressSameAsKTP = isAddressSameAsKTP;
	}



}
