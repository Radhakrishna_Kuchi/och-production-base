package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.ebanking.rest.common.v1.pojo.BaseFileDetailsVO;

public class CustomPrivyRegistrationPOJO {

	String applicationId;
		
	BaseFileDetailsVO file1;
	
	BaseFileDetailsVO file2;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public BaseFileDetailsVO getFile1() {
		return file1;
	}

	public void setFile1(BaseFileDetailsVO file1) {
		this.file1 = file1;
	}

	public BaseFileDetailsVO getFile2() {
		return file2;
	}

	public void setFile2(BaseFileDetailsVO file2) {
		this.file2 = file2;
	}

	public CustomPrivyRegistrationPOJO(String applicationId, BaseFileDetailsVO file1, BaseFileDetailsVO file2) {
		super();
		this.applicationId = applicationId;
		this.file1 = file1;
		this.file2 = file2;
	}

	public CustomPrivyRegistrationPOJO() {
		super();
	}
	
	

}
