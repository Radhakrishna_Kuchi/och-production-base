package com.infosys.custom.ebanking.rest.user.v1.pojo;

import io.swagger.annotations.ApiModelProperty;

public class CustomReUploadKtpOutputVO {
	
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "status of the request")
	private String status;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
