package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Custom Loan Details Update Input References VO - update plafond and tenure in DB as per users selection")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomLoanDetailsUpdateInputReferencesVO {
	
	@ApiModelProperty(value = "selectedPlafond")
	private String selectedPlafond;
	
	public CustomLoanDetailsUpdateInputReferencesVO() {
		super();
	}

	public CustomLoanDetailsUpdateInputReferencesVO(String selectedPlafond, String selectedTenor) {
		super();
		this.selectedPlafond = selectedPlafond;
		this.selectedTenor = selectedTenor;
	}

	public String getSelectedPlafond() {
		return selectedPlafond;
	}

	public void setSelectedPlafond(String selectedPlafond) {
		this.selectedPlafond = selectedPlafond;
	}

	public String getSelectedTenor() {
		return selectedTenor;
	}

	public void setSelectedTenor(String selectedTenor) {
		this.selectedTenor = selectedTenor;
	}

	@ApiModelProperty(value = "selectedTenor")
	private String selectedTenor;
	
	
	
	

}
