package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "App Status Notification Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomAppStatusNotificationResponseVO extends RestHeaderFooterHandler{

	private CustomAppStatusNotificationOutputVO data;

	public CustomAppStatusNotificationOutputVO getData() {
		return data;
	}

	public CustomAppStatusNotificationResponseVO() {
		super();
	}

	public CustomAppStatusNotificationResponseVO(CustomAppStatusNotificationOutputVO data) {
		super();
		this.data = data;
	}

	public void setData(CustomAppStatusNotificationOutputVO data) {
		this.data = data;
	}

	
}
