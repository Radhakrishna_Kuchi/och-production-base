package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.types.primitives.Flag;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Loan application input details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomLoanApplicationInputDetailsVO {
	@ApiModelProperty(value = "Application id", required = true)
	private Long applicationId;
	
	@ApiModelProperty(value = "last action date")
	private Date lastActionDate;
	
	@ApiModelProperty(value = "Application status")
	private CustomCommonCodeDetailsVO applicationStatus;
	
	@ApiModelProperty(value = "Product type", required = true)
	private String productType;
	
	@ApiModelProperty(value = "Product code", required = true)
	private String productCode;
	
	@ApiModelProperty(value = "Product category", required = true)
	private String productCategory;
	
	@ApiModelProperty(value = "Product sub category", required = true)
	private String productSubCategory;
	
	@ApiModelProperty(value = "Requested Loan amount", required = true)
	private double requestedLoanAmount;

	@ApiModelProperty(value = "Requested Tenor")
	private int requestedTenor;

	@ApiModelProperty(value = "Interest rate")
	private double interestRate;
		
	@ApiModelProperty(value = "Monthly installment")
	private double monthlyInstallment;
	
	@ApiModelProperty(value = "legacy cif")
	private String legacyCif;
	
	@ApiModelProperty(value = "is submitted")
	private Flag isSubmitted;
	
	@ApiModelProperty(value = "new status")
	private String newStatus;
	
	@ApiModelProperty(value = "feedback star")
	private String feedback;
	
	@ApiModelProperty(value = "remarks")
	private String remarks;
	
	// Added for BRI AGRO
	@ApiModelProperty(value = "Bank Code", required = true)
	private String bankCode;
	
	@ApiModelProperty(value = "Customer Ktp Number")
	private String ktpNum;
	
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public Date getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	public CustomCommonCodeDetailsVO getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(CustomCommonCodeDetailsVO applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}

	public double getRequestedLoanAmount() {
		return requestedLoanAmount;
	}

	public void setRequestedLoanAmount(double requestedLoanAmount) {
		this.requestedLoanAmount = requestedLoanAmount;
	}

	public int getRequestedTenor() {
		return requestedTenor;
	}

	public void setRequestedTenor(int requestedTenor) {
		this.requestedTenor = requestedTenor;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	
	public double getMonthlyInstallment() {
		return monthlyInstallment;
	}

	public void setMonthlyInstallment(double monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}

	public String getLegacyCif() {
		return legacyCif;
	}

	public void setLegacyCif(String legacyCif) {
		this.legacyCif = legacyCif;
	}

	public Flag getIsSubmitted() {
		return isSubmitted;
	}

	public void setIsSubmitted(Flag isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getKtpNum() {
		return ktpNum;
	}

	public void setKtpNum(String ktpNum) {
		this.ktpNum = ktpNum;
	}

}
