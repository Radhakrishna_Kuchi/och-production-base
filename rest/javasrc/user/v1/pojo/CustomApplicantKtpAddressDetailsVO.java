package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Customer KTP address details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomApplicantKtpAddressDetailsVO {
	@ApiModelProperty(value = "Application id")
	private Long applicationId;
	
	@ApiModelProperty(value = "Address line 1")
	private String address;
	
	@ApiModelProperty(value = "Address line 2")
	private String address2;
	
	@ApiModelProperty(value = "Address line 3")
	private String address3;
	
	@ApiModelProperty(value = "Address line 4")
	private String address4;
	
	@ApiModelProperty(value = "religion")
	private String agama;
	
	@ApiModelProperty(value = "date of birth")
	private Date birthDate;
	
	@ApiModelProperty(value = "Legacy CIF no")
	private String cifNo;
	
	@ApiModelProperty(value = "city")
	private String city;
	
	@ApiModelProperty(value = "KTP number")
	private String idNo;
	
	@ApiModelProperty(value = "ID Type")
	private String idType;
	
	@ApiModelProperty(value = "name of the customer")
	private String nama;
	
	@ApiModelProperty(value = "place of birth")
	private String placeOfBirth;
	
	@ApiModelProperty(value = "Province")
	private String province;
	
	@ApiModelProperty(value = "Customer Gender")
	private String sex;
	
	@ApiModelProperty(value = "Postal code")
	private Long zipCode;
	
	@ApiModelProperty(value = "Bank Code")
	private String bankCode;

	@ApiModelProperty(value = "Customer Address")
	private String isKTPAddess;
	
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCifNo() {
		return cifNo;
	}

	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Long getZipCode() {
		return zipCode;
	}

	public void setZipCode(Long zipCode) {
		this.zipCode = zipCode;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getIsKTPAddess() {
		return isKTPAddess;
	}

	public void setIsKTPAddess(String isKTPAddess) {
		this.isKTPAddess = isKTPAddess;
	}
	
}
