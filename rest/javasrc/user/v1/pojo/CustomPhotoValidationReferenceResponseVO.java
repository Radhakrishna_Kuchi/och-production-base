package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomPhotoValidationReferenceResponseVO extends RestHeaderFooterHandler {
	private List<CustomPhotoValidationReferenceVO> data;

	public List<CustomPhotoValidationReferenceVO> getData() {
		return data;
	}

	public void setData(List<CustomPhotoValidationReferenceVO> data) {
		this.data = data;
	}
}
