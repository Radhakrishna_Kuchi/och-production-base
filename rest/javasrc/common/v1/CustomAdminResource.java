package com.infosys.custom.ebanking.rest.common.v1;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.infosys.custom.ebanking.rest.general.v1.resources.CustomApplicationStatusUpdateResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomBanksListFetchForForgotPasswordResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomChangePayrollDateResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomContentManagementResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomCreditScoringCallbackResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomOfflineKTPFetchResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomOfflineKTPVerificationResource;
import com.infosys.custom.ebanking.rest.general.v1.resources.CustomStandardTextMaintenanceResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantInquiryResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantRefundResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomForgotPasswordResource;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;

import io.swagger.annotations.ApiParam;

@Path(ResourcePaths.RETAILREQUEST+CustomResourcePaths.CUSTOM)

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomAdminResource {
	
	@Context
	HttpServletRequest request;
	
	@Inject
	CustomStandardTextMaintenanceResource customStdTxtResource;
	
	@Path("/standard-text")
	public CustomStandardTextMaintenanceResource customStandardTxtResource
	(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId){
	return customStdTxtResource;
	}
	
	@Inject
	CustomContentManagementResource contentManagementResource;
	@Path("/contents")
	public CustomContentManagementResource contentManagementResource
	(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId){
	return contentManagementResource;
	}
	
	
	@Inject
	CustomCreditScoringCallbackResource customCreditScoringCallbackResource;

	@Path("/applications/{appId}/creditscoreupdate")
	public CustomCreditScoringCallbackResource customCreditScoringCallbackResource
	(
			@ApiParam(value = "The ID of the application.")  
			@PathParam("appId") String bankId
			){
		return customCreditScoringCallbackResource;
	}
	
	 @Inject
	 CustomForgotPasswordResource customForgotPasswordResource;
	 
	@Path("/users/forgot-password/reset")
	public CustomForgotPasswordResource customForgotPasswordResource
	(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId){
	return customForgotPasswordResource;
	}

	@Inject
	CustomApplicationStatusUpdateResource customAppStatusUpdateResource;

	@Path("/applicationStatusUpdate")
	public CustomApplicationStatusUpdateResource customAppStatusUpdateResource
	(
			@ApiParam(value = "Application Status Update")  
			@PathParam("bankId") String bankId
			){
		return customAppStatusUpdateResource;
	}
	
		@Inject
	CustomMerchantRefundResource custoMerchantRefundResource;

	@Path("/servicerequests/make-refund")
	public CustomMerchantRefundResource custoMerchantRefundResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid) {
		return custoMerchantRefundResource;
	}
	
	@Inject
	CustomMerchantInquiryResource custoMerchantInquiryResource;

	@Path("/servicerequests/merchant-inquiry")
	public CustomMerchantInquiryResource custoMerchantInquiryResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid) {
		return custoMerchantInquiryResource;
	}
	
	@Inject
	CustomOfflineKTPVerificationResource customOfflineKTPVerificationResource;

	@Path("/applications/{appId}/verifyKTP")
	public CustomOfflineKTPVerificationResource customOfflineKTPVerificationResource
	(
			@ApiParam(value = "The ID of the application.")  
			@PathParam("appId") String bankId
			){
		return customOfflineKTPVerificationResource;
	}
	
	@Inject
	CustomBanksListFetchForForgotPasswordResource customBankListFetchForForgotPasswordResource;
	@Path("/bankcodes/{productCode}")
	public CustomBanksListFetchForForgotPasswordResource customBankListFetchForForgotPasswordResource(
	    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@PathParam("productCode") String productCode) {
		return customBankListFetchForForgotPasswordResource;
	}
	
	@Inject
	CustomOfflineKTPFetchResource customOfflineKTPFetchResource;

	@Path("/applications/{appId}/fetchKTP")
	public CustomOfflineKTPFetchResource customOfflineKTPFetchResource
	(
			@ApiParam(value = "The ID of the application.")  
			@PathParam("appId") String bankId
			){
		return customOfflineKTPFetchResource;
	}
	@Inject
	CustomChangePayrollDateResource changePayrollDateResource ;

	@Path("/applications/changepayrolldate")
	public CustomChangePayrollDateResource changePayrollDateResource(@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid){
		return changePayrollDateResource;
	}
}
