package com.infosys.custom.ebanking.rest.common.v1;

public class CustomResourceConstants {

	private CustomResourceConstants(){
		super();
	}

	/***************** Custom Resource Paths - Start *****************/
	public static final String STANDARD_TEXT_CODE_TYPE = "STXT";
	public static final String LOAN_PURPOSE_CODE_TYPE = "LNPP";
	public static final String LOAN_CONFIG_TYPE = "LNCT";
	public static final String LOAN_CONFIG_CM_CODE_PRIN = "PRIN";
	public static final String LOAN_MERCH_ID_CODE_TYPE = "MID";
	public static final String LOAN_TENURE_CODE_TYPE = "LTEN";
	
	public static final String LOAN_CONTENT_TYPE_PROMO = "PRO";
	public static final String LOAN_CONTENT_TYPE_SLP = "SPL";
	public static final String LOAN_CONTENT_TYPE_MER = "MRC";
	public static final String LOAN_CONTENT_TYPE = "CTYP";
	
	public static final String SIGN_ON_OLD_PASS = "Signon Old Password";
	public static final String SIGN_ON_NEW_PASS = "Signon New Password";
	public static final String SIGN_ON_CONFIRM_NEW_PASS = "Signon Confirm New Password";
	
	public static final String TXN_ON_OLD_PASS = "Transaction Old Password";
	public static final String TXN_ON_NEW_PASS = "Transaction New Password";
	public static final String TXN_ON_CONFIRM_NEW_PASS = "Transaction Confirm New Password";
	
	public static final String INSTALLATION_PRODUCT_ID = "INSTALLATION_PRODUCT_ID";
	public static final String PINANG = "PINANG";
	public static final String SAHABAT = "SAHABAT";
	public static final String LOAN_APP_STATUS_LOAN_CREATED = "LOAN_CREATED";

	public static final String SUBJECT = "Selamat! Pinjaman Ceriamu sudah aktif. Dapatkan kemudahan belanja dan Promo di Merchant terbaik";
	
	
	
	
}

