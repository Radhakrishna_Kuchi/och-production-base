package com.infosys.custom.ebanking.rest.common.v1;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomLoanPaymentResource;
import com.infosys.custom.ebanking.rest.servicerequest.v1.resources.CustomMerchantPaymentResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomATMInquiryCallResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomAccountListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomAppStatusNotificationMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomBankCodeFetchResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomBanksListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomCompanyDetailsManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomCreateLoanResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomDeviceRegUpdateHPNumberResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomDeviceRegistrationRetailResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFetchLoanHistoryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFetchLoanRepaymentScheduleResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomGenerateOtpManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationDocumentListResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationListInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanApplicationResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanDetailsUpdateResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanLimitInquiryResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanProductMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomLoanSimulationResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomNotificationMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomOnlineRegistrationRetailResources;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomPasswordRetailResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomReUploadKtpResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomStandardTextResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomUserProfileDetailsResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomValidatePhotoMaintenanceResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomVerifyOtpManagerResource;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomVerifyPayrollResource;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.resources.CustomFileAttachmentResource;
import io.swagger.annotations.ApiParam;

@Path(ResourcePaths.RETAILREQUEST_URL + CustomResourcePaths.CUSTOM)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomUserResource {

	@Context
	HttpServletRequest request;

	@Inject
	CustomLoanProductMaintenanceResource customLoanProdMnt;

	@Path("/loanproducts")
	public CustomLoanProductMaintenanceResource customLoanProductMaintenanceResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {
		return customLoanProdMnt;
	}

	@Inject
	CustomLoanSimulationResource customLoanSimulationResource;

	@Path("loansimulation")
	public CustomLoanSimulationResource customLoanSimulationResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {
		return customLoanSimulationResource;
	}

	@Inject
	CustomATMInquiryCallResource customATMInquiryCallResource;

	@Path("/atminquiry")
	public CustomATMInquiryCallResource customATMInquiryCallResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {
		return customATMInquiryCallResource;
	}

	@Inject
	CustomCompanyDetailsManagerResource customCompanyDetailsResource;

	@Path("/companydetails")
	public CustomCompanyDetailsManagerResource customCompanyDetailsManagerResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId) {
		return customCompanyDetailsResource;
	}

	@Inject
	CustomLoanApplicationResource customLoanApplnResource;

	@Path("/applications")
	public CustomLoanApplicationResource customLoanApplnResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id.") @PathParam("userid") String userid) {
		return customLoanApplnResource;
	}

	@Inject
	CustomOnlineRegistrationRetailResources customOnlineRegistrationResource;

	@Path("users")
	public CustomOnlineRegistrationRetailResources customOnlineRegistrationResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId) {
		return customOnlineRegistrationResource;
	}

	@Inject
	CustomLoanApplicationListInquiryResource customLoanApplistInqResource;

	@Path("/applicationInquiry")
	public CustomLoanApplicationListInquiryResource customLoanApplistInqResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id.") @PathParam("userid") String userid) {
		return customLoanApplistInqResource;
	}

	@Inject
	CustomGenerateOtpManagerResource customGenerateOtpResource;

	@Path("generateOtp")
	public CustomGenerateOtpManagerResource customGenerateOtpResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId,
			@ApiParam(value = "The ID of the bank.") @PathParam("applicationId") String applicationId) {
		return customGenerateOtpResource;
	}

	@Inject
	CustomVerifyOtpManagerResource customVerifyOtpResource;

	@Path("verifyOTP")
	public CustomVerifyOtpManagerResource customVerifyOtpResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId,
			@ApiParam(value = "The ID of the application.") @PathParam("applicationId") String applicationId) {
		return customVerifyOtpResource;
	}

	@Inject
	CustomLoanInquiryResource loanInquiry;

	@Path("/loans")
	public CustomLoanInquiryResource loanInquiryResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid) {
		return loanInquiry;
	}

	@Inject
	CustomStandardTextResource customStandardTextResource;

	@Path("/helptopics")
	public CustomStandardTextResource customStandardTextResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userid) {
		return customStandardTextResource;
	}

	@Inject
	CustomPasswordRetailResource customPasswordRetailResource;

	@Path("/password")
	public CustomPasswordRetailResource customPasswordRetailResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {
		return customPasswordRetailResource;
	}

	@Inject
	CustomValidatePhotoMaintenanceResource customPhotoVal;

	@Path("/applications/{applicationId}/valdiatePhoto")
	public CustomValidatePhotoMaintenanceResource customPhotoMaintenanceResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid,
			@ApiParam(value = "The Application id") @PathParam("applicationId") String applicationId) {
		return customPhotoVal;
	}

	@Inject
	CustomUserProfileDetailsResource customUserProfileDetailsResource;

	@Path("/userprofile")
	public CustomUserProfileDetailsResource customUserProfileDetailsResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {
		return customUserProfileDetailsResource;
	}

	@Inject
	CustomNotificationMaintenanceResource customNotificationMaintenanceUpdateResource;

	@Path("/notifications/{notificationId}")
	public CustomNotificationMaintenanceResource customNotificationMaintenanceUpdateResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId,
			@ApiParam(value = "The notification id") @PathParam("notificationId") String notificationId) {
		return customNotificationMaintenanceUpdateResource;
	}

	@Inject
	CustomFetchLoanRepaymentScheduleResource customFetchLoanRepaymentScheduleResource;

	@Path("/accountId")
	public CustomFetchLoanRepaymentScheduleResource customFetchLoanRepaymentScheduleResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId,
			@ApiParam(value = "Account Id input", required = true) @PathParam("accountId") String accountId) {
		return customFetchLoanRepaymentScheduleResource;
	}

	@Inject
	CustomDeviceRegistrationRetailResource customDeviceRegResource;

	@Path("/deviceregistration")
	public CustomDeviceRegistrationRetailResource customDeviceRegResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id.") @PathParam("userid") String userid) {
		return customDeviceRegResource;
	}

	@Inject
	CustomDeviceRegUpdateHPNumberResource customDeviceRegUpdateHPNumResource;

	@Path("/deviceregistration/updateHpNumber")
	public CustomDeviceRegUpdateHPNumberResource customDeviceRegUpdateHPNumResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id.") @PathParam("userid") String userid) {
		return customDeviceRegUpdateHPNumResource;
	}

	@Inject
	CustomFetchLoanHistoryResource customLoanHistoryResource;

	@Path("/loanHist/{accountId}")
	public CustomFetchLoanHistoryResource customLoanHistoryResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId,
			@ApiParam(value = "Account Id input", required = true) @PathParam("accountId") String accountId) {
		return customLoanHistoryResource;

	}

	@Inject
	CustomNotificationMaintenanceResource customNotificationMaintenanceResource;

	@Path("/notifications")
	public CustomNotificationMaintenanceResource customNotificationMaintenanceResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId) {
		return customNotificationMaintenanceResource;
	}

	@Inject
	CustomLoanPaymentResource customLoanPmtResource;

	@Path("/servicerequests/loan-payment")
	public CustomLoanPaymentResource customLoanPaymentResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid) {
		return customLoanPmtResource;
	}

	@Inject
	CustomLoanApplicationDocumentListResource customLoanApplicationDocumentListResource;

	@Path("applications/document-list")
	public CustomLoanApplicationDocumentListResource customLoanApplicationDocumentListResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId) {

		return customLoanApplicationDocumentListResource;
	}

	@Inject
	CustomCreateLoanResource customCreateLoan;

	@Path("/applications/{applicationId}/createloan")
	public CustomCreateLoanResource customCreateLoanResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid,
			@ApiParam(value = "The Application id") @PathParam("applicationId") String applicationId) {
		return customCreateLoan;
	}
	@Inject
	CustomMerchantPaymentResource custoMerchantPmtResource;

	@Path("/servicerequests/make-purchase")
	public CustomMerchantPaymentResource customMerchantPaymentResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid) {
		return custoMerchantPmtResource;
	}
	
	@Inject
	CustomAccountListInquiryResource customAccountListInquiryResource;
			
			@Path("/loanaccounts")
			public CustomAccountListInquiryResource customAccountListInquiryResource
			(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId,
					@ApiParam(value = "The ID of the retail user.")  @PathParam("userid") String userId) {
			 return customAccountListInquiryResource;
	}
	
	CustomLoanLimitInquiryResource customLoanLimitResource;

	@Path("/limitInquiry")
	public CustomLoanLimitInquiryResource customLoanLimitResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid) {
		return customLoanLimitResource;
	}
	
	@Inject
	CustomLoanDetailsUpdateResource customLoanDeatilsUpdate;

	@Path("/applications/{applicationId}/updateLoanDetails")
	public CustomLoanDetailsUpdateResource customLoanDeatilsUpdate(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid,
			@ApiParam(value = "The Application id") @PathParam("applicationId") String applicationId) {
		return customLoanDeatilsUpdate;
	}
	
	
	@Inject
	CustomBanksListInquiryResource customBanksListInquiryResource;

	@Path("/bankcodes/{productCode}")
	public CustomBanksListInquiryResource customBanksListInquiryResource(
	    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@PathParam("productCode") String productCode) {
		return customBanksListInquiryResource;
	}
	
	@Inject
	CustomBankCodeFetchResource customBankCodeFetchResource;

	@Path("/bankcode/{applicationId}")
	public CustomBankCodeFetchResource customBankCodeFetchResource(
	    	@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@PathParam("applicationId") String applicationId) {
		return customBankCodeFetchResource;
	}
	
	@Inject
	CustomReUploadKtpResource customReUploadKtpResource;

	@Path("/applications/{applicationId}/reuploadktp")
	public CustomReUploadKtpResource customReUploadKtpResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The Application id") @PathParam("applicationId") String applicationId) {
		return customReUploadKtpResource;
	}
	
	@Inject
	CustomVerifyPayrollResource customVerifyPayrollResource;

	@Path("/verifyPayroll")
	public CustomVerifyPayrollResource customVerifyPayrollResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankid,
			@ApiParam(value = "The user id") @PathParam("userid") String userid) {
		return customVerifyPayrollResource;
	}
	
	@Inject
	CustomAppStatusNotificationMaintenanceResource customAppStatusNotificationMaintenanceResource;

	@Path("/appstatusnotifications")
	public CustomAppStatusNotificationMaintenanceResource customAppStatusNotificationMaintenanceResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.") @PathParam("userid") String userId) {
		return customAppStatusNotificationMaintenanceResource;
	}
	
	@Inject
	CustomAppStatusNotificationMaintenanceResource customAppStatusNotificationMaintenanceUpdateResource;

	@Path("/appstatusnotifications/{notificationId}")
	public CustomAppStatusNotificationMaintenanceResource customAppStatusNotificationMaintenanceUpdateResource(
			@ApiParam(value = "The ID of the bank.") @PathParam("bankid") String bankId,
			@ApiParam(value = "The user id") @PathParam("userid") String userId,
			@ApiParam(value = "The notification id") @PathParam("notificationId") String notificationId) {
		return customAppStatusNotificationMaintenanceUpdateResource;
	}
	
	@Inject
	CustomFileAttachmentResource fileAttachmentResource;

	@Path("fileattachment/{fileseqno}")
	public CustomFileAttachmentResource fileAttachmentResource(@ApiParam(value = "The ID of the bank.")  @PathParam("bankid") String bankId,
			@ApiParam(value = "The ID of the retail user.")  @PathParam("userid") String userId,
			@ApiParam(value = "The Sequence Number of the file to be downloaded.")  @PathParam("fileseqno") String fileSeqNo) {
		return fileAttachmentResource;
	}
}
