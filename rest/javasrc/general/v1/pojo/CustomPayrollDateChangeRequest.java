package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
public class CustomPayrollDateChangeRequest {
	@ApiModelProperty(value = "Customer details List")
	private List<CustomPayrollDateInput> customerList;

	public List<CustomPayrollDateInput> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomPayrollDateInput> customerList) {
		this.customerList = customerList;
	}

}