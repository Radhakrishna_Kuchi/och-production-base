package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomStandardTxtMaintReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomStandardTxtMaintReferencesVO> data;
	
	public List<CustomStandardTxtMaintReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomStandardTxtMaintReferencesVO> data) {
		this.data = data;
	}
}
