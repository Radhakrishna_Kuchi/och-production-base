package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CustomPayrollDateChangeStatusDetail {
	@ApiModelProperty(value = "Customer mobile number")
	private String mobileNo;
	
	@ApiModelProperty(value = "Customer application ID")
	private String applicationId;
	
	@ApiModelProperty(value = "new payroll date")
	private Date newPayrollDate;
	
	@ApiModelProperty(value = "new payroll date")
	private Date oldPayrollDate;
	
	@ApiModelProperty(value = "Status")
	private String status;

	@ApiModelProperty(value = "Reason")
	private String statusDesc;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public Date getNewPayrollDate() {
		return newPayrollDate;
	}

	public void setNewPayrollDate(Date newPayrollDate) {
		this.newPayrollDate = newPayrollDate;
	}

	public Date getOldPayrollDate() {
		return oldPayrollDate;
	}

	public void setOldPayrollDate(Date oldPayrollDate) {
		this.oldPayrollDate = oldPayrollDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
}
