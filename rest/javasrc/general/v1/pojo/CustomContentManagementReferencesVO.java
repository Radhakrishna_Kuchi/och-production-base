package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Content Management Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomContentManagementReferencesVO {

	@ApiModelProperty(value = "Header")
	private String header;
	
	@ApiModelProperty(value = "Description")
	private String description;
	
	@ApiModelProperty(value = "Image Path")
	private String imagePath;
	
	@ApiModelProperty(value = "Text Type")
	private String textType;
	
	@ApiModelProperty(value = "Merchant Id")
	private String merchantId;
	
	@ApiModelProperty(value = "Android App URL")
	private String andAppURL;

	@ApiModelProperty(value = "Play Store URL")
	private String playStoreURL;
	
	@ApiModelProperty(value = "iOS App URL")
	private String iosAppURL;

	@ApiModelProperty(value = "iOS Store URL")
	private String iosStoreURL;
	
	
	
	public CustomContentManagementReferencesVO() {
		super();
	}

	public CustomContentManagementReferencesVO(String header, String description, String imagePath, String textType,
			String merchantId, String andAppURL, String playStoreURL, String iosAppURL, String iosStoreURL) {
		super();
		this.header = header;
		this.description = description;
		this.imagePath = imagePath;
		this.textType = textType;
		this.merchantId = merchantId;
		this.andAppURL = andAppURL;
		this.playStoreURL = playStoreURL;
		this.iosAppURL = iosAppURL;
		this.iosStoreURL = iosStoreURL;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTextType() {
		return textType;
	}

	public void setTextType(String textType) {
		this.textType = textType;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAndAppURL() {
		return andAppURL;
	}

	public void setAndAppURL(String andAppURL) {
		this.andAppURL = andAppURL;
	}

	public String getPlayStoreURL() {
		return playStoreURL;
	}

	public void setPlayStoreURL(String playStoreURL) {
		this.playStoreURL = playStoreURL;
	}

	public String getIosAppURL() {
		return iosAppURL;
	}

	public void setIosAppURL(String iosAppURL) {
		this.iosAppURL = iosAppURL;
	}

	public String getIosStoreURL() {
		return iosStoreURL;
	}

	public void setIosStoreURL(String iosStoreURL) {
		this.iosStoreURL = iosStoreURL;
	}




	

	
}
