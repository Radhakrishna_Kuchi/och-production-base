package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Offline KTP Fetch")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomOfflineKTPFetchReferencesVO {
	@ApiModelProperty(value = "Application id", required = true)
	private Long applicationId;
	
	@ApiModelProperty(value = "Document type", required = true)
	private String documentType;
	
	@ApiModelProperty(value = "Document code", required = true)
	private String documentCode;
	
	@ApiModelProperty(value = "document Key", required = true)
	private String docKey;
	
	@ApiModelProperty(value = "file", required = true)
	private String file;
	
	@ApiModelProperty(value = "file name", required = true)
	private String fileName;
	
	@ApiModelProperty(value = "file seq no")
	private String fileSeqNo;
	
	
	@ApiModelProperty(value = "file upload path", required = true)
	private String fileUploadPath;
	

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getDocKey() {
		return docKey;
	}

	public void setDocKey(String docKey) {
		this.docKey = docKey;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSeqNo() {
		return fileSeqNo;
	}

	public void setFileSeqNo(String fileSeqNo) {
		this.fileSeqNo = fileSeqNo;
	}


	public String getFileUploadPath() {
		return fileUploadPath;
	}

	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}
}
