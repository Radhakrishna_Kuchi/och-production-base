package com.infosys.custom.ebanking.rest.general.v1.pojo;

import io.swagger.annotations.ApiModelProperty;
public class CustomPayrollDateInput {
	@ApiModelProperty(value = "Customer mobile number")
	private String mobileNo;
	
	@ApiModelProperty(value = "Customer application ID")
	private String applicationId;
	
	@ApiModelProperty(value = "new payroll date")
	private String payrollDate;
	
	public String getMobileNo() {
		return mobileNo;
	}
	
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(String payrollDate) {
		this.payrollDate = payrollDate;
	}


}