package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.ci.common.CryptManager;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomApplicationStatusUpdateReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomApplicationStatusUpdateReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationUpdateStatusDetailsVO;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Api(value = "Application Status Update")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/v1/banks/{bankId}/custom/applicationStatusUpdate")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomApplicationStatusUpdateResource extends ResourceAuthenticator {
	RestHeaderFooterHandler restHeaderFooterHandler;

	@Context
	HttpServletRequest request1;
	@Context
	HttpServletResponse httpResponse;

	@POST
	@ApiOperation(value = "Application Status Update", response = CustomApplicationStatusUpdateReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.APPLICATION_STATUS_UPDATE_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Application Status Update", response = CustomApplicationStatusUpdateReferencesResponseVO.class),
			@ApiResponse(code = 500, message = "Table update failed") })
	public Response applicationStatusUpdate(CustomApplicationStatusUpdateReferencesVO customReferenceVO,
			@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId)
			 {
		CustomApplicationStatusUpdateReferencesResponseVO responseVO = new CustomApplicationStatusUpdateReferencesResponseVO();
		try {
			IOpContext opcontext = (IOpContext) request1.getAttribute("OP_CONTEXT");
			
			CustomApplicationUpdateStatusDetailsVO detailsVO = (CustomApplicationUpdateStatusDetailsVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomApplicationUpdateStatusDetailsVO);	
			detailsVO.setUid(customReferenceVO.getUid());
			
			fetchDetails(opcontext, detailsVO);
			 
			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomApplicationStatusService"));
			serviceStub.callService(opcontext, detailsVO, new FEBAUnboundString("update"));			
			
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request1, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();

	}

public void fetchDetails(IOpContext opcontext, CustomApplicationUpdateStatusDetailsVO detailsVO) throws BusinessException
{
	try
	{
	String key = CryptManager.decrypt(detailsVO.getUid().getValue());
	String[] values = key.split("#");
	
	 detailsVO.setBankId(new BankId(values[0]));
	 detailsVO.setApplicationId(new ApplicationNumber(values[1]));
	 detailsVO.setUserId(new FEBAUnboundString(values[2]));
	}
	catch (FEBATypeSystemException e)
	{
		throw new BusinessException(true, opcontext, CustomEBankingIncidenceCodes.INVALID_UID,
				"Invalid uid Passed", null, CustomEBankingErrorCodes.INVALID_UID, null);
	}
}
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211052, "BE", 500));
		return restResponceErrorCode;
	}
	
}
