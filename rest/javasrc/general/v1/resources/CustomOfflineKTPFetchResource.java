package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPFetchReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPFetchReferencesVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPVerificationReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FileSequenceNumber;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FileDownloadDetailsVO;
import com.infosys.fentbase.tao.FDTTTAO;
import com.infosys.fentbase.tao.info.FDTTInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "common")
@Api(value = "Offline KTP Fetch")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path(CustomResourcePaths.OFFLINE_KTP_FETCH_URL)
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomOfflineKTPFetchResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request1;
	@Context
	HttpServletResponse httpResponse;
	
	public static final String BANK_ID="BANK_ID";

	final IClientStub serviceCStub = ServiceUtil
			.getService(new FEBAUnboundString("CustomOfflineKTPFetchService"));
	
	public static final String MESSAGE = "No records fetched";

	@GET
	@ApiOperation(value = "Offline KTP Image Fetch", response = CustomOfflineKTPFetchReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.OFFLINE_KTP_FETCH_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful KTP image Retrieval", response = CustomOfflineKTPFetchReferencesResponseVO.class) })
	public Response hitCallback(CustomOfflineKTPVerificationReferencesVO customOfflineKTPVerificationReferencesVO,
			@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId,
			@ApiParam(value = "app id", required = true) @PathParam("appId") String appId) {
		CustomOfflineKTPFetchReferencesResponseVO responseVO = new CustomOfflineKTPFetchReferencesResponseVO();

		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		try {
			
			CustomOfflineKTPFetchReferencesVO refVO = fetchKTPImage(appId, context);
			
			if(null !=refVO && refVO.getFile() == null){
				throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}
			
			responseVO.setData(refVO);
			
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request1, 0);
		} catch (Exception e) {
			LogManager.logError(context, e);
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}finally {
			if (context != null) {
				context.cleanup();
			}
		}
		return Response.ok().entity(responseVO).build();

	}
	@SuppressWarnings("rawtypes")
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211052, "BE", 400));
		return restResponceErrorCode;
	}

	private CustomOfflineKTPFetchReferencesVO fetchKTPImage(String appId, IOpContext context) throws FEBAException{
		
		CustomOfflineKTPFetchReferencesVO refVO = new CustomOfflineKTPFetchReferencesVO();
		
		CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationDetailsVO);
		detailsVO.getLoanApplicantDetails().setApplicationId(new ApplicationNumber(appId));
		detailsVO = (CustomLoanApplicationDetailsVO) serviceCStub.callService(context, detailsVO,
				new FEBAUnboundString("retrieval"));
		try {
			
			if(null != detailsVO){
				getDocumentDetails(context,refVO,detailsVO.getLoanDocumentDetails());
			}
						
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		}
		
		return refVO;
	}
	
	protected static void getDocumentDetails(IOpContext opContext, CustomOfflineKTPFetchReferencesVO documentDetails,
			CustomLoanApplnDocumentsDetailsVO documentDetailsVO) throws CriticalException, FEBATableOperatorException {

		FEBATransactionContext objTxnContext = null;
		documentDetails.setDocumentCode(documentDetailsVO.getDocCode().toString());
		documentDetails.setDocumentType(documentDetailsVO.getDocType().toString());
		documentDetails.setDocKey(documentDetailsVO.getDocKey().toString());
		documentDetails.setFileSeqNo(documentDetailsVO.getFileSeqNo().toString());
		documentDetails.setFileUploadPath(documentDetailsVO.getDocStorePath().toString());

		FEBAArrayList list = null;
		StringBuilder file = new StringBuilder();
		try {
			objTxnContext = RestCommonUtils.getTransactionContext(opContext);
			FDTTInfo fdttInfo = FDTTTAO.select(objTxnContext, documentDetailsVO.getFileSeqNo(),
					objTxnContext.getBankId());
			documentDetails.setFileName(fdttInfo.getFileName().toString());

			list = fetchFileChunksFromTable((FEBATransactionContext) objTxnContext, objTxnContext.getBankId(),
					documentDetailsVO.getFileSeqNo());
			int size = list.size();
			for (int i = 0; i < size; i++) {

				FileDownloadDetailsVO rowData = (FileDownloadDetailsVO) list.get(i);

				FEBABinary fetchedData = rowData.getDownloadedFile();

				file = file.append(new String(fetchedData.getValue()));

			}
		} catch (BusinessException e1) {
			LogManager.logError(null, e1);
		} finally {
			if (objTxnContext != null) {
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
		documentDetails.setFile(file.toString());

	}

	private static FEBAArrayList fetchFileChunksFromTable(FEBATransactionContext context, BankId bankId,
			FileSequenceNumber sequenceNumber) throws BusinessException {

		String sQueryIdentifier = "FileDownload";

		QueryOperator queryOperator = QueryOperator.openHandle((FEBATransactionContext) context, sQueryIdentifier);

		queryOperator.associate("FILE_SEQ_NO", sequenceNumber);

		queryOperator.associate(BANK_ID, bankId);

		try {

			return queryOperator.fetchList((FEBATransactionContext) context);

		}

		catch (DALException e) {

			throw new BusinessException(context, FEBAIncidenceCodes.FU_FUCT_FETCH_FAILED,
					ErrorCodes.FU_FUCT_FETCH_FAILED, e);

		}

	}
	
}
