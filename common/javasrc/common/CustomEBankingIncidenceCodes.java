package com.infosys.custom.ebanking.common;

public class CustomEBankingIncidenceCodes {

	private CustomEBankingIncidenceCodes() {
		super();
	}

	public static final String STTXT_TEXT_TYPE_MANDATORY = "STTXT0001";
	public static final String STANDARD_TEXT_TYPE_INVALID = "STTXT0002";
	public static final String CLNSIM_MERCHANT_ID_MANDATORY = "CLNSIM0001";
	public static final String CLNSIM_PAYMENT_AMOUNT_MANDATORY = "CLNSIM0002";
	public static final String CLNSIM_INVALID_LOAN_CONFIG_TYPE = "CLNSIM0003";
	public static final String CLNSIM_INVALID_LOAN_MERCHANT_ID = "CLNSIM0004";
	public static final String CLNSIM_INVALID_LOAN_TENURE = "CLNSIM0005";
	public static final String CONTENT_TYPE_MANDATORY = "CCONMG0001";
	public static final String CONTENT_TYPE_INVALID = "CCONMG0002";

	// Added for Credit Score Calculation Start
	public static final String UNABLE_TO_SELECT_RECORD_FOR_CREDIT_SCORE_CALCULATION = "CRESCR0001";
	// Added for Credit Score Calculation End

	public static final String CARD_NUMBER_INVALID = "CRDNIN001";
	public static final String EXPIRY_DATE_INCORRECT = "CRDNIN002";
	public static final String CARD_NOT_ACTIVE = "CRDNIN003";
	public static final String CAMS_HOST_CALL_ERROR = "CAMSER0001";
	public static final String WHITELIST_PINANG_HOST_CALL_ERROR = "WHTELST001";
	public static final String WHITELIST_SAHABAT_HOST_CALL_ERROR = "WHTELST002";
	public static final String SILVERLAKE_ACCT_HOST_CALL_ERROR = "SLVKAER001";
	public static final String SILVERLAKE_CIF_HOST_CALL_ERROR = "SLVKCER001";
	public static final String ACCOUNT_NOT_OPEN = "ACCTNO001";
	public static final String KTP_CARD_NUMBER_EMPTY = "KTPCNOEM01";
	public static final String KTP_NUMBER_MISMATCH = "KTPMSMH001";

	public static final String ERROR_MIN_AMOUNT = "CUSTERR01";

	public static final String UNABLE_TO_CREATE_UPRT_RECORD = "PRYREGT0001";
	public static final String UNABLE_TO_SELECT_LAAD_LACD_RECORD = "PRYREGT0002";
	public static final String DOC_TEXT_DEFN_NOT_FOUND = "DOCTXT0001";
	public static final String CREDIT_SCORE_CALLBACK_TABLE_EXC = "CRSCTBEX01";
	public static final String INVALID_DISPLAY_ORDER = "STTXT0003";

	// Added for Token generation Start
	public static final String UNABLE_TO_SELECT_RECORD_FOR_PRIVY_TOKEN_CREATION = "PRYTOKCR0001";
	// Added for Token generation End

	public static final String NO_RECORDS_FETCHED = "USERLN001";

	// By Prasad for OTP use case 20 SeparatorUI
	public static final String TOKEN_NOT_FOUND = "PRYTOKCR0002";

	public static final String COMPANY_NAME_MANDATORY = "CCOMPNM001";
	public static final String COMPANY_ADDRESS_MANDATORY = "CCOMPAD002";

	public static final String CIF_UPDATE_FAILED = "DEVUSRCIF01";

	public static final String LOAN_APPLICATION_FETCH = "LNAPDOCF01";
	public static final String LOAN_APPLICATION_FETCH_NO_DOCUMENTS_FOUND = "LNAPDOCF02";
  	  public static final String PROCESSING_FAILED_IN_LNDTLDWLD_BATCH = "BTCHLNDTL0001";
	public static final String BOTH_DATES_REQ = "LOANORG001";
	public static final String MAX_DATE_RANGE = "LOANORG002";
	public static final String INVALID_UID = "CCONMG0003";	
	public static final String CBET_UPD_FAILED = "CCONMG0004";
	public static final String CLAT_UPD_FAILED = "CCONMG0005";
	public static final String CNSD_UPD_FAILED = "CCONMG0006";
	public static final String HP_NUBER_CHANGED = "DEVREG0001";
	public static final String DOCUMENT_SIGNING_FAILED = "DOCSIGNF0001";

	public static final String LN_APP_ID_MANDATORY = "GETPRIV0001";
	
	public static final String CUSTOMER_DEDUP_CHECK_FAILD = "ATMDDF0001";
	public static final String OFFLINE_KTP_VERIFICATION_FAILED = "OKTPF0001";
	
	// Added for BRI AGRO
	public static final String BANK_NAME_EMPTY = "BNEMP0001";
	public static final String BANK_DATA_NOT_FOUND = "BNKDANF0001";
	public static final String PAYROLL_REJECTION = "PAYREJ0001";

		// Added for privy mobile no update

		public static final String PRIVY_MOBILE_UPDATE = "PRYMOBCR0001";;
}

