package com.infosys.custom.ebanking.common;

import com.infosys.feba.framework.common.exception.IErrorCodes;

public class CustomEBankingErrorCodes implements IErrorCodes {

	@Override
	public int getHighestErrorCode() {
		return HIGHEST_ERROR_CODE;

	}

	private static final int HIGHEST_ERROR_CODE = 211036;

	public static final int STTXT_TEXT_TYPE_MANDATORY = 211024;
	public static final int STANDARD_TEXT_TYPE_INVALID = 211032;
	public static final int CLNSIM_MERCHANT_ID_MANDATORY = 211025;
	public static final int CLNSIM_PAYMENT_AMOUNT_MANDATORY = 211026;
	public static final int INVALID_LOAN_CONFIG_TYPE = 211027;
	public static final int INVALID_LOAN_MERCHANT_ID = 211028;
	public static final int INVALID_LOAN_TENURE = 211029;
	public static final int CONTENT_TYPE_MANDATORY = 211030;
	public static final int CONTENT_TYPE_INVALID = 211031;
	public static final int CARD_NUMBER_INVALID = 211041;
	public static final int EXPIRY_DATE_INCORRECT = 211042;
	public static final int CARD_NOT_ACTIVE = 211043;
	public static final int UNABLE_TO_CREATE_UPRT_RECORD = 211035;
	public static final int DOC_TEXT_DEFN_NOT_FOUND = 211036;
	public static final int CAMS_HOST_CALL_ERROR = 211044;
	public static final int WHITELIST_SAHABAT_HOST_CALL_ERROR = 211045;
	public static final int WHITELIST_PINANG_HOST_CALL_ERROR = 211046;
	public static final int SILVERLAKE_ACCT_HOST_CALL_ERROR = 211047;
	public static final int SILVERLAKE_CIF_HOST_CALL_ERROR = 211048;
	public static final int ACCOUNT_NOT_OPEN = 211049;
	public static final int KTP_CARD_NUMBER_EMPTY = 211050;
	public static final int KTP_NUMBER_MISMATCH = 211051;
	public static final int ERROR_MIN_AMOUNT = 211040;
	public static final int CONFIG_TYPE_MANDATORY = 211036;
	public static final int LN_PURPOSE_MANDATORY = 211037;
	public static final int PROD_SUBCAT_MANDATORY = 211035;
	public static final int PROD_CAT_MANDATORY = 211034;
	public static final int PROD_CODE_MANDATORY = 211033;
	public static final int MERCHANT_ID_MANDATORY = 211025;
	public static final int TENOR_MANDATORY = 211038;
	public static final int INTEREST_MANDATORY = 211039;
	public static final int CREDIT_SCORE_CALLBACK_TABLE_EXC = 211052;
	public static final int ENTER_COMPANY_NAME = 211054;
	public static final int ENTER_COMPANY_ADDR = 211055;
	public static final int INVALID_DISPLAY_ORDER = 211070;
	public static final int INVALID_TOKEN = 211080;
	public static final int CIF_UPDATION_FAILED = 211081;
	public static final int USER_PHONE_NUM_MANDATORY = 211084;
	public static final int LOAN_APPLICATION_FETCH = 200605;
	public static final int LOAN_APPLICATION_FETCH_NO_DOCUMENTS_FOUND = 200606;
	public static final int TO_ADDRESS_MANDATORY = 200608;
	public static final int SUBJECT_MANDATORY = 200609;
	public static final int MESSAGE_CONTENT_MANDATORY = 200610;
	
	public static final int STATUS_CANNOT_BE_UPDATED = 211087;
	public static final int PROCESSING_FAILED_IN_LNDTLDWLD_BATCH = 211301;
	public static final int BOTH_DATES_REQ = 211085;
	public static final int MAX_DATE_RANGE = 211086;
	public static final int INVALID_UID = 211088;
	public static final int CBET_UPD_FAILED = 211089;
	public static final int CLAT_UPD_FAILED = 211090;
	public static final int CNSD_UPD_FAILED = 211091;
	public static final int CIF_ID_NOT_AVAILABLE = 211092;
	public static final int HP_NUMBER_CHANGED = 211093;
	public static final int DOCUMENT_SIGNING_FAILED = 211094;
	public static final int LN_APP_ID_MANDATORY = 211095;
	public static final int USER_NUM_CANNOT_BE_REGISTERED = 211096;
	public static final int CUSTOMER_DEDUP_CHECK_FAILD = 211097;
	public static final int OFFLINE_KTP_VERIFICATION_FAILED = 211098;
	
	// Added for BRI AGRO
	public static final int BANK_NAME_EMPTY = 211099;
	public static final int BANK_DATA_NOT_FOUND = 211100;
	public static final int PAYROLL_REJECTION = 211101;

		// Added for privy mobile no update
		public static final int	PRIVY_MOBILE_UPDATE =211149;
}
