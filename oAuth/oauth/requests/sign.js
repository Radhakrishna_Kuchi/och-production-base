var express = require('express');
var fs = require('fs');
var config = require('./../config');
var http = require('http');
var path = require('path');
var errors = require('./../errors').errors;
var mustache = require('mustache');
var log = require('logger');
var invokePost = require('./common/invokehost');

var SIGN = function(request, response, next) {

	PostCode(request, response, next);

};

function PostCode(request, response, next) {
	var parseString = require('xml2js').parseString;
	var input = '';
	username = request.persistParams;
	var loginFlag = username.LOGIN_FLAG;

	input = JSON.stringify({
		"header" : {
			"BANK_ID" : username.BANK_ID,
			"LANGUAGE_ID" : username.LANGUAGE_ID,
			"CHANNEL_ID" : username.CHANNEL_ID,
			"LOGIN_FLAG" : username.LOGIN_FLAG,
			"USER_PRINCIPAL" : username.USER_PRINCIPAL,
			"CORP_PRINCIPAL" : username.CORP_PRINCIPAL,
			"ACCESS_CODE" : username.ACCESS_CODE,
			"__SRVCID__" : config.hif.ocf.useCase.sign.__SRVCID__,
			"OPFMT" : username.OPFMT,
			"IPFMT" : username.IPFMT,
			"STATEMODE" : config.hif.ocf.useCase.sign.STATEMODE,
			"DEVICE_ID" : request.body.DEVICE_ID,
			"ACCESSMODE" : request.body.ACCESSMODE,
			//Added ACCESSTOKEN for passing the Virtual Token in case of Actual Token generation
			"ACCESSTOKEN" : request.body.ACCESSTOKEN,
			"IS_SESSION_DELETION": "Y",
			"PROTOCOL" : config.hif.ocf.useCase.sign.protocol,
			"PRELOGIN_FLAG" : request.body.PRELOGIN_FLAG,
			"PRELOGIN_DETAILS" : request.body.PRELOGIN_DETAILS
		},
		"body" : {
			"basic" : {},
			"array" : {}
		}
	});

	var post_options = {
		host : config.hif.ocf.common.host,
		port : config.hif.ocf.common.port,
		path : config.hif.ocf.common.path,
		method : config.hif.ocf.common.method,
		headers : {
			'User-Agent' : 'trying for node.js',
			'Content-Type' : 'application/json',
			'IPTYPE' : 'MCFJSON',
			'Connection' : 'Keep-ALive',
			'Content-Length' : input.length
		}
	};

	log.logDebug({
		Request : post_options,
		Body : JSON.parse(input)
	});

	invokePost.invoke(post_options, input, function(err, data) {

		if (err) {
			log.logDebug('Sign request failed');
			return next(err);
		}

		prepareOutput(post_options, input, data, next);

	});
}

function prepareOutput(post_options, input, parsedData, next) {

	var obj = '';

	var sgnstatus = '';
	var isNodeDummyClient = 'Y';

	if (parsedData && parsedData.header && parsedData.header.STATUS && parsedData.header.STATUS.MESSAGE && parsedData.header.STATUS.MESSAGE[0]
			&& parsedData.header.STATUS.MESSAGE[0].MESSAGE_CODE) {

		if (parsedData.CustomSignInResponse && parsedData.CustomSignInResponse.UserDetails && parsedData.CustomSignInResponse.UserDetails.USER_SIGNON_STATUS) {
			sgnstatus = parsedData.CustomSignInResponse.UserDetails.USER_SIGNON_STATUS;
		}

		obj = parsedData.header.STATUS.MESSAGE[0].MESSAGE_CODE;

	} else {

		return next(new errors.NotFound('Error while Login'));

	}

	if (obj === '0000') {

		var user = '';

		if (sgnstatus && sgnstatus === 'P' && isNodeDummyClient && isNodeDummyClient === 'Y') {

			user = {
				"message_code" : obj,
				"form_id" : parsedData.header.SESSION.FORM_ID,
				"session_id" : parsedData.header.SESSION.SESSION_ID,
				"actual_user_id" : JSON.parse(input).header.USER_PRINCIPAL,
				"sign_on_status" : sgnstatus,
				"mode" : parsedData.CustomSignInResponse.UserDetails.MODE
			};

			log.logDebug({
				UserInSign : JSON.stringify(user)
			});

		} else {

			if (JSON.parse(input).header.LOGIN_FLAG == '3') {
				user = JSON.parse(input).header.USER_ID;

			} else {

				user = {
					"confidenceScore" : parsedData.CustomSignInResponse.UserDetails.CONFIDENCE_SCORE,
					"recordUserId" : parsedData.CustomSignInResponse.UserDetails.RECORD_USER_ID
				}

				if (parsedData.CustomSignInResponse.UserDetails.PRELOGIN_FLAG == "Y") {
					user = {
						"confidenceScore" : parsedData.CustomSignInResponse.UserDetails.CONFIDENCE_SCORE,
						"recordUserId" : parsedData.CustomSignInResponse.UserDetails.RECORD_USER_ID,
						"preloginFlag" : "Y",
						"sign_on_status" : "S"

					}
				}
				
			}
		}
		return next(null, user);
	} else {

		if (parsedData && parsedData.header && parsedData.header.STATUS) {
			return next(new errors.NotFound(parsedData.header.STATUS.MESSAGE));
		} else {
			return next(new errors.NotFound('Error while Login'));
		}

	}
}

module.exports.SIGN = SIGN;
