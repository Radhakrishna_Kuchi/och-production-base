package com.infosys.custom.ebanking.corporateadministration.service;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomStandardTextCriteriaVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;

public class CustomStandardTextMaintenanceServiceFetchImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {

		CustomStandardTextCriteriaVO customBankBranchEnquiryVO = (CustomStandardTextCriteriaVO) objQueryCrit;
		queryOperator.associate("textType",customBankBranchEnquiryVO.getTextType());
		queryOperator.associate("bankId", context.getBankId());
		queryOperator.associate("textName", customBankBranchEnquiryVO.getTextName());
	}
	@Override
	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logDebug(be.getDispMessage());

		}
	}
	@Override
	/**
	 * 
	 * Associating DAL CustomShoppingMallStatusEnquiryDAL
	 * @see CustomShoppingMallStatusEnquiryDAL.xml
	 */
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_STANDARD_TEXT_MAINTENANCE;

	}
	@Override
	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {
		CustomStandardTextCriteriaVO stDetailsVO = (CustomStandardTextCriteriaVO) objInputOutput;
		FEBAValItem[] vls=null;

		if(null==stDetailsVO.getTextType().toString()||stDetailsVO.getTextType().toString().length()==0)
		{
			
			throw new BusinessException(
					true,
					objContext,
					CustomEBankingIncidenceCodes.STTXT_TEXT_TYPE_MANDATORY,
					"Text Type is mandatory",
					null,
					CustomEBankingErrorCodes.STTXT_TEXT_TYPE_MANDATORY,
					null);
		}else{

			FEBAHashList resultList1 = (FEBAHashList) AppDataManager.getList(
					objContext, FBAConstants.COMMONCODE_CACHE, 
					FBAConstants.codeType+FBAConstants.EQUAL_TO+CustomResourceConstants.STANDARD_TEXT_CODE_TYPE);
			CommonCodeVO commonCodeVO=null;
			String textType = stDetailsVO.getTextType().getValue();
			Boolean isValidTextType = false;

			for (Object obj : resultList1) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim()
						.equalsIgnoreCase(textType)){
					isValidTextType=true;
					break;
				}
			}
			if(!isValidTextType){
				throw new BusinessException(
						true,
						objContext,
						CustomEBankingIncidenceCodes.STANDARD_TEXT_TYPE_INVALID,
						"Text Type is invalid",
						null,
						CustomEBankingErrorCodes.STANDARD_TEXT_TYPE_INVALID,
						null);
			}
		}

		return new FEBAValItem[] {};
		
	}}
