
package com.infosys.custom.ebanking.applicationmaintenance.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomLoanProductMaintenancePopulateWfUtil;
import com.infosys.custom.ebanking.user.validator.CustomLoanTenureInterestValidator;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;

public class CustomLoanInterestMaintenanceServiceRecallImpl extends AbstractLocalUpdateTran {

	@Override
	public void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
		FBATransactionContext objContext = (FBATransactionContext) pObjContext;
		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLTI(pObjContext, customLoanTenureInterestDetailsVO,
					FEBATMOConstants.TMO_OPERATION_CANCEL);

			criteria.setDependencyFlag(FBAConstants.YES);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_CANCEL + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.USER_RECALL_TMO_EXCEPTION, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_RECALL, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}
		objContext.addBusinessInfo(CustomLoanMaintenanceTableOperationUtil.getSuccessMessage(ebContext,
				EBankingErrorCodes.REQUEST_SUCESSFULLY_RECALLED));
	}

	/**
	 * Method populateWorkflowVO() Description Populates workflow value object
	 * from context and InputOutputVO Input objContext, workflowVO,
	 * objInputOutput, objTxnWM
	 *
	 * @param objContext
	 * @param populateWorkflowVOUtility
	 * @param objInputOutput
	 * @param objTxnWM
	 */
	@Override
	protected void populateWorkflowVO(FEBATransactionContext objContext, IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		CustomLoanProductMaintenancePopulateWfUtil.populateLoanInterestWorkflowVO(objContext, wfVOPopulator,
				objInputOutput, EBankingConstants.RECALL);
	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem(customLoanTenureInterestDetailsVO, new CustomLoanTenureInterestValidator(),
						FEBAValEngineConstants.INDEPENDENT),

		};

	}
}
