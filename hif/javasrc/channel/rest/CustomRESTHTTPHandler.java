package com.infosys.custom.hif.channel.rest;


import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.hif.context.HostContext;
import com.infosys.feba.framework.hif.core.IChannelHandler;
import com.infosys.feba.framework.hif.core.exception.ChannelHandlerException;
import com.infosys.feba.framework.hif.model.IHostMessage;
import com.infosys.feba.framework.hif.model.impl.HostMessageImpl;
import com.infosys.feba.framework.monitor.MonitoringConstants;
import com.infosys.feba.framework.monitor.MonitoringContext;
import com.infosys.feba.framework.types.FEBAHashMap;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.hif.channel.rest.RestClient;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.valueobjects.MonitoringVO;

/**
 *  Class added as part of HIF Rest Service Integration
 *  Receives the message, logmessage, requestheaders, properties and urlparams.
 *  Passes the required parameters to JSONRestClient which invokes the REST API
 * @author Suresh
 */

public class CustomRESTHTTPHandler implements IChannelHandler {

	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";
	public static final String METHOD_OPTIONS = "OPTIONS";
	public static final String METHOD_HEAD = "HEAD";
	private static final String METHOD_TRACE = "TRACE";
	private static final String EMPTY = "";
	private static final String MULITPART_FORM_DATA_HTTP="MULITPART_FORM_DATA/HTTP";
	private static final String FIREBASE = "FIREBASE";

	private Properties properties;


	public void init(Properties pProperties) {
		this.properties = pProperties;
	}

	public IHostMessage transceive(
			HostContext pContext,
			IHostMessage pHostMessage) throws ChannelHandlerException {

		HostMessageImpl responseMessage = new HostMessageImpl();

		//getting the various parameters, message from the framework
		String messageName = pHostMessage.getMessageName();
		//getting the http method configured in HIF_Message.xml
		String httpMethod = pHostMessage.getHostMessageConfig().getHttpMethod();


		FBATransactionContext context = (FBATransactionContext)pContext.getFEBATransactionContext();
		MonitoringVO monitoringVo = null;
		String isHostMonitoringRequired = null;
		FEBAHashMap monitoringMap =null;
		MonitoringContext monitoringContext =null;    
		String responseString = null; 

		try 
		{
			
			System.out.println(" CUSTOM REST HTTP HANDLER");

			isHostMonitoringRequired = PropertyUtil.getProperty(MonitoringConstants.IS_MONITORING_ENABLED, context);

			if(null!=isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired)){
					monitoringContext = (MonitoringContext)(pContext).getFromHostContext(FBAConstants.MONITORING_CONTEXT);
					monitoringMap = monitoringContext.getMonitoringMap();
					monitoringVo = requestMonitoring(context, monitoringMap,messageName);
			}

			 RestClient.setProperties(properties);
			 CustomRestClient.setProperties(properties);

			/*
			 * BRI Change to add HMAC header to request
			 */
			String apigeeToken = PropertyUtil.getProperty(CustomEBConstants.APIGEE_TOKEN, context);
			String apigeeSecretKey = PropertyUtil.getProperty(CustomEBConstants.APIGEE_SECRET_KEY, context);
			String reqMethod = pHostMessage.getHostMessageConfig().getHttpMethod();
			String reqURL = pHostMessage.getUrlPath();
			String payload = "";
			if(null!= pHostMessage.getMessage()){
				payload = pHostMessage.getMessage().toString();
			}
			
			
			Map<String, String> reqHeader = pHostMessage.getRequestHeaders();
			if(null == reqHeader){
				reqHeader = new HashMap<String, String>();
			}
			
			if(!pHostMessage.getHostMessageConfig().getRouteName().equalsIgnoreCase(MULITPART_FORM_DATA_HTTP)){
				String timestamp = getCurrentTimeStamp();
				String generateToken = generateHMACHeader(reqURL,reqMethod, apigeeToken, apigeeSecretKey, timestamp, payload);
				reqHeader.put(CustomEBConstants.BRI_SIGNATURE_TOKEN_TAG, generateToken);
				reqHeader.put(CustomEBConstants.BRI_SIGNATURE_TIMESTAMP_TAG, timestamp);
			}
			pHostMessage.setRequestHeaders(reqHeader);
			
			System.out.println(" CUSTOM REST HTTP HANDLER  -->>>"+pHostMessage);
			System.out.println(" HOST name  -->>>"+pHostMessage.getHostMessageConfig().getHostName());


			//invokes different methods of JSONRestClient based on the http method configured in HIF_Message.xml(transformer entry)
			if(httpMethod == null || httpMethod.equals(EMPTY)){
				throw new CriticalException(context, FEBAIncidenceCodes.REST_HTTP_METHOD_NOT_CONFIGURED, ErrorCodes.REST_HTTP_METHOD_NOT_CONFIGURED);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_POST) &&  pHostMessage.getHostMessageConfig().getHostName().equalsIgnoreCase(FIREBASE)){
				responseString= CustomRestFirebaseClient.post(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_POST) &&  pHostMessage.getHostMessageConfig().getRouteName().equalsIgnoreCase(MULITPART_FORM_DATA_HTTP)){
				responseString= CustomRestClient.post(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_POST)){
				responseString= RestClient.post(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_GET)){
				responseString = RestClient.get(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_PUT)){
				responseString = RestClient.put(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_DELETE)){
				responseString = RestClient.delete(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_HEAD)){
				responseString = RestClient.head(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_OPTIONS)){
				responseString = RestClient.options(context,pHostMessage);
			}
			else if(httpMethod.equalsIgnoreCase(METHOD_TRACE)){
				responseString = RestClient.trace(context,pHostMessage);
			}
			else if(!httpMethod.equalsIgnoreCase(EMPTY))
			{
				responseString = RestClient.method(context, pHostMessage, httpMethod);
			}
			else
			{
				throw new CriticalException(context, FEBAIncidenceCodes.REST_HTTP_METHOD_NOT_CONFIGURED, ErrorCodes.REST_HTTP_METHOD_NOT_CONFIGURED);
			}

			// Start Coding specific to Monitoring Tool
			if(null!=isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired) && monitoringVo!=null){
				monitoringVo.setResponseTime(new FEBADate(DateUtil.currentDate(context)));
				monitoringMap.put(messageName, monitoringVo);            
			}
			// End Coding specific to Monitoring Tool


			responseMessage.setResponseHeaders(pHostMessage.getResponseHeaders());

			// setting the response string in response message 
			responseMessage.setMessage(responseString);

		} catch (CriticalException ce) {
			ce.printStackTrace();
			// Start Coding specific to Monitoring Tool
			if(null!=isHostMonitoringRequired && "Y".equalsIgnoreCase(isHostMonitoringRequired) && monitoringVo!=null){

				monitoringVo.setResponseTime(new FEBADate(DateUtil.currentDate(context)));
				monitoringVo.setStatus("FAILURE");     
				monitoringVo.setEbFailureCode(Integer.toString( FBAErrorCodes.HOST_CALL_TIMEOUT));
				monitoringMap.put(messageName, monitoringVo);

			}
			// End Coding specific to Monitoring Tool
			throw new ChannelHandlerException(
					pContext.getFEBATransactionContext(),
					FEBAIncidenceCodes.HTTP_TRANSPORT_FAILED,
					"Rest api invocation failed for message["
							+ pHostMessage.getMessageName() + "]",
							ErrorCodes.HTTP_TRANSPORT_FAILED,
							ce);
		}catch(Exception e){
			e.printStackTrace();
//			LogManager.logDebug(e.getMessage());
		}




		return responseMessage;
	}


	/**
	 * This method capture user type, corp id and request time.
	 * @author Suresh
	 * @param context
	 * @param monitoringMap
	 * @param sMessageName
	 * @return
	 */
	private MonitoringVO requestMonitoring(FBATransactionContext context,
			FEBAHashMap monitoringMap, String sMessageName) {
		MonitoringVO monitoringVo;
		if(monitoringMap.containsKey(sMessageName)){
			monitoringVo = (MonitoringVO)monitoringMap.get(sMessageName);
		}else{
			monitoringVo =  (MonitoringVO)FEBAAVOFactory.createInstance(TypesCatalogueConstants.MonitoringVO);
		}
		if(monitoringVo!=null){
			monitoringVo.setRequestTime(new FEBADate(DateUtil.currentDate(context)));
			if(null!=context.getRecordUserId() && context.getRecordUserId().toString()!=""){
				monitoringVo.setUserID(context.getRecordUserId().toString());
			}
			if(null!=context.getUserType()){
				monitoringVo.setUserType(context.getUserType());
			}
		}   
		return monitoringVo;
	}

	private String generateHMACHeader(String url, String reqMethod, String apigeeToken, String apigeeSecretKey, String timestamp, String body ){

		StringBuilder inputText = new StringBuilder();
		String generatedToken = null;
		try {

			inputText.append("path="+url+"&");
			inputText.append("verb="+reqMethod.toUpperCase()+"&");
			inputText.append("token="+apigeeToken+"&");
			inputText.append("timestamp="+timestamp+"&");
			inputText.append("body="+body);

			Base64.Encoder encoder = Base64.getEncoder();
			Mac sha256HMAC = Mac.getInstance(CustomEBConstants.HMAC_ALGO);
			SecretKeySpec apigeeSecretKeySpec = new SecretKeySpec(apigeeSecretKey.getBytes(), CustomEBConstants.HMAC_ALGO);
			sha256HMAC.init(apigeeSecretKeySpec);

			generatedToken = encoder.encodeToString(sha256HMAC.doFinal(inputText.toString().getBytes()));
		} catch (Exception e) {
			LogManager.logDebug(e.getMessage());
		}
		return generatedToken;
	}

	private String getCurrentTimeStamp(){		
		return  ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT);		
	}

	public Properties getProperties() {
		return this.properties;
	}
}

