package com.infosys.custom.ebanking.hif;

public class CustomEBRequestConstants {
	private CustomEBRequestConstants() {
		super();
	}

	public static final String CUSTOM_SEND_SMS = "CustomSendSmsRequest";

	// Added for Credit Score Calculation Start
	public static final String CUSTOM_PINANG_CREDIT_SCORE_REQUEST = "CustomPinangCreditScoreRequest";
	public static final String CUSTOM_SAHABAT_CREDIT_SCORE_REQUEST = "CustomSahabatCreditScoreRequest";
	// Added for Credit Score Calculation End

	public static final String CUSTOM_PRIVY_REGISTER_REQUEST = "CustomPrivyRegisterRequest";
	public static final String CUSTOM_EKYC_REGISTER_REQUEST = "CustomEKYCRegisterRequest";


	// Added for Token generation Start
	public static final String CUSTOM_PRIVY_TOKEN_CREATION_REQUEST = "CustomPrivyTokenCreateRequest";
	public static final String CUSTOM_PRIVY_OTP_REQUEST = "CustomPrivyOTPRequest";
	public static final String CUSTOM_PRIVY_OTP_VERIFICATION_REQUEST = "CustomPrivyOTPVerificationRequest";
	// Added for Token generation End

	// Added for CIF and loan account create start
	public static final String CUSTOM_RETAIL_CUST_ADD_REQUEST = "CustomRetailCustAddRequest";

	public static final String CUSTOM_CREATE_LOAN_ACCNT_REQUEST = "CustomCreateLoanAcctRequest";
	// Added for CIF and loan account create end

	public static final String CUSTOM_FETCH_LOAN_LIST_REQUEST = "CustomLoanFetchListRequest";
	// Added for OCR Upload start
	public static final String CUSTOM_OCR_UPLOAD_REQUEST = "CustomOCRUploadRequest";
	public static final String CUSTOM_OCR_DATA_REQUEST = "CustomOCRDataRequest";
	// Added for OCR Upload end
	public static final String FIREBASE_PUSH_NOTIFICATION_REQUEST = "CustomFirebasePushNotificationRequest";

	public static final String HP_NUM_UPDATE = "CustomRetailCustModRequest";

	public static final String CAMS_INQUIRY = "CustomCAMSCallInquiryRequest";
	public static final String WHITELIST_PINANG_INQUIRY = "CustomWhiteListPinangInquiryRequest";
	public static final String WHITELIST_SAHABAT_INQUIRY = "CustomWhiteListSahabatInquiryRequest";
	public static final String SILVERLAKE_ACCT_INQUIRY = "CustomSilverlakeAcctInquiryRequest";
	public static final String SILVERLAKE_CIF_INQUIRY = "CustomSilverlakeCIFInquiryRequest";
	public static final String CUSTOM_SEND_EMAIL = "CustomSendEmailRequest";
	
	public static final String CUSTOM_CREATE_CREDIT_LIMIT_REQUEST = "CustomCreateCreditLimitRequest";
    public static final String CUSTOM_ACCOUNT_LIST_INQUIRY_REQUEST = "CustomAccountListInquiryRequest";
	
		public static final String CUSTOM_LOAN_LIMIT_INQUIRY = "CustomLoanLimitInqRequest";
	public static final String CUSTOM_GOOGLE_VISION_OCR_REQUEST = "CustomGoogleVisionOCRRequest";
	
	// Added for BRI AGRO
	 public static final String CUSTOM_BANK_LIST_INQUIRY_REQUEST = "CustomBankListInquiryRequest";
	// Added for ASLI RI
	 public static final String CUSTOM_ASLIRI_REQUEST = "CustomASLIRegisterRequest";
	 
	 // Added for PINANG - Notification batch alerts
	 public static final String CUSTOM_BATCH_AGRO_SEND_SMS_REQUEST = "CustomBatchAgroSendSmsRequest";
	 public static final String CUSTOM_BATCH_AGRO_SEND_EMAIL_REQUEST = "CustomBatchAgroSendEmailRequest";
	 public static final String FIREBASE_PUSH_NOTIFICATION_APIGEE_REQUEST = "CustomFirebasePushNotificationApigeeRequest";
	 
//		Added for Privy Mobile Number update
		public static final String CUSTOM_PRIVY_MOBILE_NUMBER_UPDATE_REQUEST = "CustomPrivyMobileNumUpdateRequest";

	 
}
