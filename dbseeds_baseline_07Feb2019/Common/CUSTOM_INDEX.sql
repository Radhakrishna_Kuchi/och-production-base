ALTER SEQUENCE NXT_APPLICATION_ID_SEQ01 MAXVALUE 9999999999;

DROP INDEX IDX_CLAT_USER_SEARCH;
create index IDX_CLAT_USER_SEARCH
on CUSTOM_LOAN_APPLN_MASTER_TABLE( BANK_ID, USER_ID )
TABLESPACE IDX_MASTER;

DROP INDEX IDX_CLAT_STATUS_SEARCH;
create index IDX_CLAT_STATUS_SEARCH
on CUSTOM_LOAN_APPLN_MASTER_TABLE( BANK_ID, APPLICATION_STATUS )
TABLESPACE IDX_MASTER;

DROP INDEX IDX_CLDD_APP_SEARCH;
create index IDX_CLDD_APP_SEARCH
on CUSTOM_LOAN_APPLN_MASTER_TABLE( BANK_ID, APPLICATION_ID )
TABLESPACE IDX_MASTER;

DROP INDEX IDX_CNED_USER_ID;
create index IDX_CNED_USER_ID
on CUSTOM_NOTIFICATION_EVENT_DETAILS( BANK_ID, USER_ID )
TABLESPACE IDX_MASTER;

